
import utils
import sys
from Config import Configuration
import json  
import numpy as np             


obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{k}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{k}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{n}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{n}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{k}cos\theta_{-}^{k}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{n}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{n}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{n}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{n}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{n}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{r}$"


sys_dict = {
'Background'   : ['zjets','zjets_low','single_top','dibosons','fakes','ttv'],

'Lepton reconstruction, identification and trigger':['el_trigSF','el_recSF','el_idSF','mu_trigSF', 'mu_recSF', 'mu_idSF'],
'Lepton momentum scale and resolution':['ees', 'eer', 'musc','mums_res','muid_res'],
'Jet reconstruction and vertex fraction efficiency':['jeff','jvf'],
'Jet energy scale':['JesEffectiveStat1', 'JesEffectiveStat2', 'JesEffectiveStat3', 'JesEffectiveStat4', 'JesEffectiveModel1', 'JesEffectiveModel2', 'JesEffectiveModel3', 'JesEffectiveModel4', 'JesEffectiveDet1', 'JesEffectiveDet2', 'JesEffectiveDet3', 'JesEffectiveMix1', 'JesEffectiveMix2', 'JesEffectiveMix3', 'JesEffectiveMix4', 'EtaIntercalibrationModel', 'EtaIntercalibrationTotalStat', 'SinglePart', 'Pileup_OffsetMu', 'Pileup_OffsetNPV', 'Pileup_Pt', 'Pileup_Rho', 'flavor_comp', 'flavor_response', 'BJesUnc', 'PunchThrough'],
'Jet resolution':['jer'],
'Missing transverse momentum':['res_soft', 'sc_soft'],
'B-tagging/mis-tag efficiency':['mistag', 'btag0', 'btag1', 'btag2', 'btag3', 'btag4', 'btag5', 'btag6', 'btag7', 'btag8', 'ctautag0', 'ctautag1', 'ctautag2', 'ctautag3', 'ctautag4', 'ctautag5', 'ctautag6']

}


order = [
'Background',
'Lepton reconstruction, identification and trigger',
'Lepton momentum scale and resolution',
'Jet energy scale',
'Jet reconstruction and vertex fraction efficiency',
'Jet resolution',
'B-tagging/mis-tag efficiency',
'Missing transverse momentum',
]


def createLatex(detectorSyst,name,var):
 
  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{|lcc|}
\hline
Source of systematic uncertainty & Fiducial  & Full \\\\ \hline \hline
"""
  footer = """
\hline
\end{tabular}
\caption{Detector and background systematics for %(var)s in the fiducial region and in the full phase space}
\end{table}
\end{center}
  """ % {'var':obs_label[var]}

  table = ''

  totalFull = 0.
  totalF    = 0.

  for s in order:

    partonTotal   = 0
    fiducialTotal = 0
    for syst in sys_dict[s]: 

        
        if syst == 'fakes': sf = 0.5
        else: sf = 1 
        try:

          partonTotal    = partonTotal +  pow(detectorSyst['full'][syst.lower()]*sf,2)
          fiducialTotal  = fiducialTotal +  pow(detectorSyst['fiducial'][syst.lower()]*sf,2)

        except:

          continue

    totalFull = totalFull + partonTotal
    totalF    = totalF + fiducialTotal

    partonTotal   = np.sqrt(partonTotal)
    fiducialTotal = np.sqrt(fiducialTotal)

    table = table + '%(name)s & $\pm %(fiducial).5f$ & $\pm %(full).5f$ \\\\ \hline \n' %{'name':s,'fiducial':fiducialTotal,'full':partonTotal}

  totalFull = np.sqrt(totalFull)
  totalF    = np.sqrt(totalF)

  table = table + '%(name)s & $\pm %(fiducial).5f$ & $\pm %(full).5f$ \\\\ \hline \n' %{'name':'Total','fiducial':totalF,'full':totalFull} 
    
  latex = header + table + footer

  f1 = open(name,'w')
  print name
  f1.write(latex)
  f1.close()

        

    





if __name__ == '__main__':
  
  var =  utils.setVar(sys.argv)
   

  outDirFiducial      = Configuration.outputDir
  outDirParton        = Configuration.outputDirt
  detector_systs      = Configuration.possibleSymmetricSyst + Configuration.possibleAsymmetricSyst

  detectDict = { 'fiducial' : {} , 'full': {} }

  from binningOptimization import binnings_dict
  btemp = binnings_dict(var)
  

  if 'corr' in var or 'diff' in var or 'sum' in var:
      factor = -9
  else:
      factor = 1

  for bn,bc in btemp.iteritems():
     binName = bn

  for syst in detector_systs:
 
     jsondirParton = outDirParton+'/'+var+'/nominal/ALL/syst/%(syst)s/'%{'syst':syst.lower()}
     jsondirFiducial = outDirFiducial+'/'+var+'/nominal/ALL/syst/%(syst)s/'%{'syst':syst.lower()}


     try:

       psigmaParton   = json.load(open(jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'psigma','binName':binName}))  
       psigmaFiducial = json.load(open(jsondirFiducial+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'psigma','binName':binName}))

       msigmaParton   = json.load(open(jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'msigma','binName':binName}))  
       msigmaFiducial = json.load(open(jsondirFiducial+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'msigma','binName':binName}))


       detectDict['full'][syst]     = abs((abs(psigmaParton[0])-abs(msigmaParton[0]))*factor)/2.
       detectDict['fiducial'][syst] = abs((abs(psigmaFiducial[0])-abs(msigmaFiducial[0]))*factor)/2.

       
     except:
       print syst,jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'psigma','binName':binName}


       continue


  createLatex(detectDict,'systematics/tables/detect_'+var+'.tex',var) 




