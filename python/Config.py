import os

class Configuration:

    outputDir   = os.path.join( os.getenv("AcDilFBU_DIR") , "results_particle" )
    outputDirt   = os.path.join( os.getenv("AcDilFBU_DIR") , "results_parton" )
    outputDirB   = os.path.join( os.getenv("AcDilFBU_DIR") , "results_particle_bootstrap" )
    outputDirBt   = os.path.join( os.getenv("AcDilFBU_DIR") , "results_parton_bootstrap" )

    workDir = os.path.join( os.getenv("AcDilFBU_DIR"))

    possibleEnsemble = ['ensemble']
    possibleChannels = ['ALL','EE', 'MUMU','EMMU']
    possibleRew = ['rewObs']
    possibleRewType = ['linear','protos','fitprotos','obs']
    possibleSelections = ['pretag','tag','pretagtruth','tagtruth']
    possibleVars = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                    'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                    'transheldiff']
    possibleSignal2Unfold = ['data','reco','partonic','psigma','msigma',\
                               'linear0noRew','linear2pos','linear4pos','linear6pos',\
                               'linear2neg','linear4neg','linear6neg',\
                               'fitprotosA0noRew','fitprotosA2pos','fitprotosA4pos','fitprotosA6pos',\
                               'fitprotosA2neg','fitprotosA4neg','fitprotosA6neg',\
                               'protosA0noRew','protosA2pos','protosA4pos','protosA6pos',\
                               'protosA2neg','protosA4neg','protosA6neg',\
                               'obsneg18','obsneg16','obsneg15','obsneg14','obsneg12','obsneg10',\
                               'obsneg7','obsneg5','obsneg4','obsneg3','obsneg2','obsneg1.5','obsneg1',\
                               'obsneg0.5','obspos0','obspos0.5','obspos1','obspos1.5','obspos2','obspos3',\
                               'obspos4','obspos5','obspos6','obspos7','obspos8','obspos10','obspos12',\
                               'obspos14','obspos15','obspos16','obspos18','obspos20','obspos25','obspos28',\
                               'obspos30','obspos32','obspos34','obspos36','obspos39','obspos44'
                               ]

    possibleRegFunctions = ['Tikhonov', 'Entropy']

    possibleModSyst = ['mcatnlo','perugiampihi','perugialocr',\
                        'topmass_167.5','topmass_170','topmass_175','topmass_177.5',\
                        'topmass2_167.5','topmass2_170','topmass2_175','topmass2_177.5',\
                        'radhi','radlo',\
                        'herwig','fastpythia','perugia']

    possibleSyst = ['nominal','ttV','zjets','zjets_low','single_top','dibosons','fakes',\
        'jer', 'jeff', 'ees_up', 'ees_down', 'eer_up', 'eer_down', 'el_trigSF_up', 'el_trigSF_down',\
        'el_recSF_up', 'el_recSF_down', 'el_idSF_up', 'el_idSF_down', 'musc_up', 'musc_down',\
        'mums_res', 'muid_res', 'mu_trigSF_up', 'mu_trigSF_down', 'mu_recSF_up', 'mu_recSF_down',\
        'mu_idSF_up', 'mu_idSF_down', 'jvf_up', 'jvf_down', 'res_soft_up', 'res_soft_down',\
        'sc_soft_up', 'sc_soft_down', 'JesEffectiveStat1_up', 'JesEffectiveStat2_up',\
        'JesEffectiveStat3_up', 'JesEffectiveStat4_up', 'JesEffectiveModel1_up',\
        'JesEffectiveModel2_up', 'JesEffectiveModel3_up', 'JesEffectiveModel4_up', \
        'JesEffectiveDet1_up', 'JesEffectiveDet2_up', 'JesEffectiveDet3_up', 'JesEffectiveMix1_up', \
        'JesEffectiveMix2_up', 'JesEffectiveMix3_up', 'JesEffectiveMix4_up', \
        'EtaIntercalibrationModel_up', 'EtaIntercalibrationTotalStat_up', 'SinglePart_up',\
        'Pileup_OffsetMu_up', 'Pileup_OffsetNPV_up', 'Pileup_Pt_up', 'Pileup_Rho_up',
        'flavor_comp_up', 'flavor_response_up', 'BJesUnc_up', 'PunchThrough_up', 'JesEffectiveStat1_down', \
        'JesEffectiveStat2_down', 'JesEffectiveStat3_down', 'JesEffectiveStat4_down', 'JesEffectiveModel1_down', \
        'JesEffectiveModel2_down', 'JesEffectiveModel3_down', 'JesEffectiveModel4_down', 'JesEffectiveDet1_down',\
        'JesEffectiveDet2_down', 'JesEffectiveDet3_down', 'JesEffectiveMix1_down', 'JesEffectiveMix2_down',\
        'JesEffectiveMix3_down', 'JesEffectiveMix4_down', 'EtaIntercalibrationModel_down', 'EtaIntercalibrationTotalStat_down', 'SinglePart_down', 'Pileup_OffsetMu_down', 'Pileup_OffsetNPV_down', 'Pileup_Pt_down', 'Pileup_Rho_down', 'flavor_comp_down', 'flavor_response_down', 'BJesUnc_down', 'PunchThrough_down', 'mistag_up', 'mistag_down', 'btag0_up', 'btag1_up', 'btag2_up', 'btag3_up', 'btag4_up', 'btag5_up', 'btag6_up', 'btag7_up', 'btag8_up', 'btag0_down', 'btag1_down', 'btag2_down', 'btag3_down', 'btag4_down', 'btag5_down', 'btag6_down', 'btag7_down', 'btag8_down', 'ctautag0_up', 'ctautag1_up', 'ctautag2_up', 'ctautag3_up', 'ctautag4_up', 'ctautag5_up', 'ctautag6_up', 'ctautag0_down', 'ctautag1_down', 'ctautag2_down', 'ctautag3_down', 'ctautag4_down', 'ctautag5_down', 'ctautag6_down'] + possibleModSyst + ['pol_up','pol_down','corr_up','corr_down']
    possibleTheoSyst = ['pol_up','pol_down','corr_up','corr_down']
    possibleSystN = ['jer','jeff','mums_res','muid_res','ees_up', 'ees_down', 'eer_up', 'eer_down', 'el_trigSF_up', 'el_trigSF_down', 'el_recSF_up', 'el_recSF_down', 'el_idSF_up', 'el_idSF_down', 'musc_up', 'musc_down', 'mu_trigSF_up', 'mu_trigSF_down', 'mu_recSF_up', 'mu_recSF_down', 'mu_idSF_up', 'mu_idSF_down', 'jvf_up', 'jvf_down', 'res_soft_up', 'res_soft_down', 'sc_soft_up', 'sc_soft_down', 'JesEffectiveStat1_up', 'JesEffectiveStat2_up', 'JesEffectiveStat3_up', 'JesEffectiveStat4_up', 'JesEffectiveModel1_up', 'JesEffectiveModel2_up', 'JesEffectiveModel3_up', 'JesEffectiveModel4_up', 'JesEffectiveDet1_up', 'JesEffectiveDet2_up', 'JesEffectiveDet3_up', 'JesEffectiveMix1_up', 'JesEffectiveMix2_up', 'JesEffectiveMix3_up', 'JesEffectiveMix4_up', 'EtaIntercalibrationModel_up', 'EtaIntercalibrationTotalStat_up', 'SinglePart_up', 'Pileup_OffsetMu_up', 'Pileup_OffsetNPV_up', 'Pileup_Pt_up', 'Pileup_Rho_up', 'flavor_comp_up', 'flavor_response_up', 'BJesUnc_up', 'PunchThrough_up', 'JesEffectiveStat1_down', 'JesEffectiveStat2_down', 'JesEffectiveStat3_down', 'JesEffectiveStat4_down', 'JesEffectiveModel1_down', 'JesEffectiveModel2_down', 'JesEffectiveModel3_down', 'JesEffectiveModel4_down', 'JesEffectiveDet1_down', 'JesEffectiveDet2_down', 'JesEffectiveDet3_down', 'JesEffectiveMix1_down', 'JesEffectiveMix2_down', 'JesEffectiveMix3_down', 'JesEffectiveMix4_down', 'EtaIntercalibrationModel_down', 'EtaIntercalibrationTotalStat_down', 'SinglePart_down', 'Pileup_OffsetMu_down', 'Pileup_OffsetNPV_down', 'Pileup_Pt_down', 'Pileup_Rho_down', 'flavor_comp_down', 'flavor_response_down', 'BJesUnc_down', 'PunchThrough_down', 'mistag_up', 'mistag_down', 'btag0_up', 'btag1_up', 'btag2_up', 'btag3_up', 'btag4_up', 'btag5_up', 'btag6_up', 'btag7_up', 'btag8_up', 'btag0_down', 'btag1_down', 'btag2_down', 'btag3_down', 'btag4_down', 'btag5_down', 'btag6_down', 'btag7_down', 'btag8_down', 'ctautag0_up', 'ctautag1_up', 'ctautag2_up', 'ctautag3_up', 'ctautag4_up', 'ctautag5_up', 'ctautag6_up', 'ctautag0_down', 'ctautag1_down', 'ctautag2_down', 'ctautag3_down', 'ctautag4_down', 'ctautag5_down', 'ctautag6_down']

    possibleSymmetricSyst = ['jer','jeff','mums_res','muid_res']#,'zjets','zjets_low','single_top','dibosons','fakes','ttV']

    possibleAsymmetricSyst= ['ees', 'eer', 'el_trigSF', 'el_recSF', 'el_idSF', 'musc', 'mu_trigSF', 'mu_recSF', 'mu_idSF', 'jvf', 'res_soft', 'sc_soft', 'JesEffectiveStat1', 'JesEffectiveStat2', 'JesEffectiveStat3', 'JesEffectiveStat4', 'JesEffectiveModel1', 'JesEffectiveModel2', 'JesEffectiveModel3', 'JesEffectiveModel4', 'JesEffectiveDet1', 'JesEffectiveDet2', 'JesEffectiveDet3', 'JesEffectiveMix1', 'JesEffectiveMix2', 'JesEffectiveMix3', 'JesEffectiveMix4', 'EtaIntercalibrationModel', 'EtaIntercalibrationTotalStat', 'SinglePart', 'Pileup_OffsetMu', 'Pileup_OffsetNPV', 'Pileup_Pt', 'Pileup_Rho', 'flavor_comp', 'flavor_response', 'BJesUnc', 'PunchThrough', 'mistag', 'btag0', 'btag1', 'btag2', 'btag3', 'btag4', 'btag5', 'btag6', 'btag7', 'btag8', 'ctautag0', 'ctautag1', 'ctautag2', 'ctautag3', 'ctautag4', 'ctautag5', 'ctautag6']


    possibleSyst = [item.lower() for item in possibleSyst]

    possibleSystN = [item.lower() for item in possibleSystN]

    possibleSymmetricSyst = [item.lower() for item in possibleSymmetricSyst]

    possibleAsymmetricSyst= [item.lower() for item in possibleAsymmetricSyst]

def getNames():
    mapnames = ['map_incl']#map_dymtt','map_dybetatt','map_dypttt']
    resmatnames = ['resmat_incl']#,'resmat_dymtt','resmat_dybetatt','resmat_dypttt']

    return mapnames,resmatnames




def getSampleLists(syst='nominaltruth'):
    ttbarsample = 'ttbar'
    ispecial = Configuration.possibleModSyst
    if syst in ispecial: truthsample = [syst]
    else: truthsample = ['partonicTtbar']
    samples_qcd = ['fakes']
    samples_data = ['data']
    samples_BCKG = ['dibosons','ttV','zjets','zjets_low','single_top','fakes','fiducial']
    samples_MC = samples_BCKG+[ttbarsample]
    samples = samples_BCKG+[ttbarsample] + samples_data + truthsample
    return samples,samples_MC,samples_BCKG,samples_data,truthsample,ttbarsample

def getNtupleDir():
    baseDir = os.getenv("AcDilFBU_NTPDIR")
    return baseDir

def getPath(ch='',sys = 'nominal'):
    if 'partonicTtbar' in ch:
       path = 'particles'
       return path
    ispecial = Configuration.possibleModSyst
    if sys == 'nominal' or sys in ispecial :
       path = 'mini'
    else:
       path = sys
    return path


def getModSyst():
    return Configuration.possibleModSyst
def getProtosSamples():
    return ['protosA0noRew','protosA2pos','protosA4pos','protosA6pos','protosA2neg','protosA4neg','protosA6neg']

def getProtosSamples2Json():
    return ['protosA0noRew','protosA2pos','protosA4pos','protosA6pos','protosA2neg','protosA4neg','protosA6neg',
            'partonicprotosA0noRew','partonicprotosA2pos','partonicprotosA4pos',
            'partonicprotosA6pos','partonicprotosA2neg','partonicprotosA4neg','partonicprotosA6neg']

def getFitProtosSamples():
    return ['fitprotosA0noRew','fitprotosA2pos','fitprotosA4pos','fitprotosA6pos','fitprotosA2neg','fitprotosA4neg','fitprotosA6neg']

def getFitProtosSamples2Json():
    return ['fitprotosA0noRew','fitprotosA2pos','fitprotosA4pos','fitprotosA6pos','fitprotosA2neg','fitprotosA4neg','fitprotosA6neg',
		        'partonicfitprotosA0noRew','partonicfitprotosA2pos','partonicfitprotosA4pos',
					'partonicfitprotosA6pos','partonicfitprotosA2neg','partonicfitprotosA4neg','partonicfitprotosA6neg']


def getLinearSamples():
    return ['linear0noRew','linear2pos','linear4pos','linear6pos','linear2neg','linear4neg','linear6neg']

def getLinearSamples2Json():
    return ['partoniclinear0noRew','partoniclinear2pos','partoniclinear4pos','partoniclinear6pos',
            'partoniclinear2neg','partoniclinear4neg','partoniclinear6neg','linear0noRew','linear2pos',
            'linear4pos','linear6pos','linear2neg','linear4neg','linear6neg',]


def getObsSamples(var):
    if 'pol' in var:
        return ['obspos0','obspos2','obspos4','obspos7','obspos10','obsneg2','obsneg4','obsneg7','obsneg10']
    elif 'corr' in var:
        if var == 'rcorr': return ['obspos0','obspos2','obspos4','obspos7','obspos10','obsneg2','obsneg4','obsneg7','obsneg10']
        else: return ['obspos0','obspos20','obspos25','obspos28','obspos30','obspos32','obspos34','obspos36','obspos39','obspos44']
    elif 'sum' in var or 'diff' in var:
        return ['obspos0','obspos2','obspos7','obspos15','obsneg2','obsneg7','obsneg15']



def getObsResmatSamples(var):
    if 'pol' in var:
        return ['migobspos0','migobspos2','migobspos4','migobspos7','migobspos10','migobsneg2','migobsneg4','migobsneg7','migobsneg10']
    elif 'corr' in var:
        if var == 'rcorr': return ['migobspos0','migobspos2','migobspos4','migobspos7','migobspos10','migobsneg2','migobsneg4','migobsneg7','migobsneg10']
        else: return ['migobspos0','migobspos20','migobspos25','migobspos28','migobspos30','migobspos32','migobspos34','migobspos36','migobspos39','migobspos44']
    elif 'sum' in var or 'diff' in var:
        return ['migobspos0','migobspos2','migobspos7','migobspos15','migobsneg2','migobsneg7','migobsneg15']

def getObsSamples2Json(var):
    if 'pol' in var:
        return ['obspos0','obspos2','obspos4','obspos7','obspos10','obsneg2','obsneg4','obsneg7','obsneg10','partonicobspos0','partonicobspos2','partonicobspos4','partonicobspos7','partonicobspos10','partonicobsneg2','partonicobsneg4','partonicobsneg7','partonicobsneg10']
    elif 'corr' in var:
        if var == 'rcorr':
            return ['obspos0','obspos2','obspos4','obspos7','obspos10','obsneg2','obsneg4','obsneg7','obsneg10','partonicobspos0','partonicobspos2','partonicobspos4','partonicobspos7','partonicobspos10','partonicobsneg2','partonicobsneg4','partonicobsneg7','partonicobsneg10']
        else: return ['obspos0','obspos20','obspos25','obspos28','obspos30','obspos32','obspos34','obspos36','obspos39','obspos44','partonicobspos0','partonicobspos20','partonicobspos25','partonicobspos28','partonicobspos30','partonicobspos32','partonicobspos34','partonicobspos36','partonicobspos39','partonicobspos44']
    elif 'sum' in var or 'diff' in var:
        return ['obspos0','obspos2','obspos7','obspos15','obsneg2','obsneg7','obsneg15','partonicobspos0','partonicobspos2','partonicobspos7','partonicobspos15','partonicobsneg2','partonicobsneg7','partonicobsneg15'] #it's ok for rhelsum, too. I cannot remove the inherent correlation, so it's just on top of it
