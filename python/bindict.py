

def binnings_dict(var="helpol"):


    binnings_helpol_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
    }

    binnings_transpol_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
    }

    binnings_beampol_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
    }


    binnings_rpol_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
    }

    binnings_helcorr_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_transcorr_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_rcorr_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_transhel_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_transrsum_dict = {
    '200Bin': [i/100. for i in xrange(-100,101)]
    }

    binnings_transrdiff_dict = {
'200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_rhelsum_dict = {
'200Bin': [i/100. for i in xrange(-100,101)]
        }

    binnings_rheldiff_dict = {
'200Bin': [i/100. for i in xrange(-100,101)]
        }



    if "helpol" in var: return binnings_helpol_dict
    elif "transpol" in var :         return binnings_transpol_dict
    elif "beampol" in var :         return binnings_beampol_dict
    elif "rpol" in var :         return binnings_rpol_dict
    elif var=="helcorr": return binnings_helcorr_dict
    elif var=="transcorr": return binnings_transcorr_dict
    elif var=="rcorr": return binnings_rcorr_dict
    elif 'transhel' in var: return binnings_transhel_dict
    elif var=='transrsum': return binnings_transrsum_dict
    elif var=='transrdiff': return binnings_transrdiff_dict
#    elif 'transr' in var: return binnings_transr_dict
#    elif 'rhel' in var: return binnings_rhel_dict
    elif var=='rhelsum' in var: return binnings_rhelsum_dict
    elif var=='rheldiff' in var: return binnings_rheldiff_dict
    else :
        print "no binning found"
        return {}
