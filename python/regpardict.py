

def regpar_dict():

    regpar_ttbar_dict = {
        'noReg': 0.0,  #not setting regularization at all
        'rpZero': 0.0, #setting regularization with alpha=0.0
	'rp12': 1e-12,
	'rp11': 1e-11,
	'rp10': 1e-10,
        'rp9':  1e-09,
        'rp8':  1e-8,
        'rp7':  1e-7,
        'rp6':  1e-6,
        'rp3':  1e-3,
        'rp2':  1e-2,
        'rp1':  1e-1,
        'rp0':  1.,
        'rpp1':  1.e1,
        'rpp2':  1.e2,
        'rpp3':  1.e3,
        'rpp6':  1.e6,
        }

    return regpar_ttbar_dict
