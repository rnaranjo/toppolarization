
import utils
import sys
from Config import Configuration
import json  
import numpy as np             


obs_label = {}

obs_label["helpol_plus"]=r"cos\theta_{+}^{k}"
obs_label["helpol_minus"]=r"cos\theta_{-}^{k}"
obs_label["transpol_plus"]=r"cos\theta_{+}^{n}"
obs_label["transpol_minus"]=r"cos\theta_{-}^{n}"
obs_label["rpol_plus"]=r"cos\theta_{+}^{r}"
obs_label["rpol_minus"]=r"cos\theta_{-}^{r}"
obs_label["helcorr"]=r"cos\theta_{+}^{k}cos\theta_{-}^{k}"
obs_label["transcorr"]=r"cos\theta_{+}^{n}cos\theta_{-}^{n}"
obs_label["rcorr"]=r"cos\theta_{+}^{r}cos\theta_{-}^{r}"
obs_label["transhelsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transheldiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transrsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["transrdiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["rhelsum"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{r}"
obs_label["rheldiff"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{r}"


sys_dict = {
'background'   : ['zjets','zjets_low','single_top','dibosons','fakes','ttv'],
'detector'     : ['el_trigSF','el_recSF','el_idSF','mu_trigSF', 'mu_recSF', 'mu_idSF','ees', 'eer', 'musc','mums_res','muid_res','jeff','jvf','JesEffectiveStat1', 'JesEffectiveStat2', 'JesEffectiveStat3', 'JesEffectiveStat4', 'JesEffectiveModel1', 'JesEffectiveModel2', 'JesEffectiveModel3', 'JesEffectiveModel4', 'JesEffectiveDet1', 'JesEffectiveDet2', 'JesEffectiveDet3', 'JesEffectiveMix1', 'JesEffectiveMix2', 'JesEffectiveMix3', 'JesEffectiveMix4', 'EtaIntercalibrationModel', 'EtaIntercalibrationTotalStat', 'SinglePart', 'Pileup_OffsetMu', 'Pileup_OffsetNPV', 'Pileup_Pt', 'Pileup_Rho', 'flavor_comp', 'flavor_response', 'BJesUnc', 'PunchThrough','jer','res_soft', 'sc_soft','mistag', 'btag0', 'btag1', 'btag2', 'btag3', 'btag4', 'btag5', 'btag6', 'btag7', 'btag8', 'ctautag0', 'ctautag1', 'ctautag2', 'ctautag3', 'ctautag4', 'ctautag5', 'ctautag6']

}


order = [
'background',
'detector' 
]


def createLatex(nominal,stats,detectorSyst,modeling,others,name):

  
 
  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{|l|c|cccccc|}
\hline
Observable & Central  & Statistical & Bckg & Detector & Modelling & Others & Total \\\\ \hline \hline
"""
  footer = """
\hline
\end{tabular}
\caption{Results for the different observables. Statistical and Systematic uncertainties are shown.}
\end{table}
\end{center}
  """ 

  table = ''

  values = {}
 
  variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                 'helcorr',\
                 'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 'transheldiff']



  for var in variables:
     values[var] = {}     
     for s in order:
        detectTotal = 0
        for syst in sys_dict[s]: 
            if syst == 'fakes': sf = 0.5
            else: sf = 1 
            try:

              detectTotal    = detectTotal +  pow(detectorSyst[var][syst.lower()]*sf,2)

            except:

              continue
        values[var][s] =  np.sqrt(detectTotal)

     if region == 'fiducial':
        if 'sum' in var or 'diff' in var or 'corr' in var: 
           nameVar = r'-9$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}
        else:
           nameVar = r'3$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}

     else:
        nameVar = '$ %(var)s $' %{'var':'\\'+var.replace('_','')} 


     globalFactor = 1
     if (var == 'helcorr' or var == 'rcorr' or var == 'transcorr') and region == 'full':  globalFactor = 0.9737098

     table = table + '%(name)s & $%(nominal)s$ &  $\pm %(stat).3f$ &  $\pm %(bckg).3f$ &  $\pm %(detector).3f$ &  $\pm %(modeling).3f$ &  $\pm %(others).3f$ &  $\pm %(total).3f$ \\\\ \hline \n' %{
       'name': nameVar ,
       'nominal':'\, %0.3f'%(nominal[var][0]*globalFactor) if nominal[var][0] > 0 else '%0.3f'%(nominal[var][0]*globalFactor) ,
       'stat':abs(stats[var][1]*globalFactor),
       'bckg':values[var]['background']*globalFactor,
       'detector':values[var]['detector']*globalFactor, 
       'modeling':modeling[var]*globalFactor,
       'others' : others[var]*globalFactor,
       'total': np.sqrt(pow(nominal[var][1],2)+pow(modeling[var],2)+pow(others[var],2))*globalFactor

     }



    
  latex = header + table + footer

  f1 = open(name,'w')
  print name
  f1.write(latex)
  f1.close()

        

    





if __name__ == '__main__':
  
    fiducial =  utils.setFiducial(sys.argv)
     

    outDir      = Configuration.outputDirt
    region = 'full'

    if fiducial: 
        outDir        = Configuration.outputDir
        region        = 'fiducial'

    detector_systs             = Configuration.possibleSymmetricSyst + Configuration.possibleAsymmetricSyst


    variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                 'helcorr',\
                 'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 'transheldiff']


    detectDict = { }
    modelling  = { }
    unfolded   = { }
    dataStat   = { }
    others     = { }

    for var in variables:

        detectDict[var] = {}

        from binningOptimization import binnings_dict
        btemp = binnings_dict(var)
        
      
        if 'corr' in var or 'diff' in var or 'sum' in var:
            factor = -9
        else:
            factor = 1
      
        for bn,bc in btemp.iteritems():
            binName = bn
      
        for syst in detector_systs:
       
            jsondirParton = outDir+'/'+var+'/nominal/ALL/syst/%(syst)s/'%{'syst':syst.lower()}
      
            try:
                pSigma   = json.load(open(jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'psigma','binName':binName}))  
                mSigma   = json.load(open(jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'msigma','binName':binName}))  
                detectDict[var][syst]     = abs(abs(pSigma[0])-abs(mSigma[0]))*factor/2.
            except:
                print syst,jsondirParton+'Unfolded_%(signal)s_%(syst)s_%(var)s_%(binName)s_rpZero.json' %{'syst':syst,'var':var,'signal':'psigma','binName':binName}
                continue

        modelling[var]  = json.load(open(outDir+'/%(var)s/nominal/ALL/modeling.json' %{'var':var}))
        unfolded[var]   = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/Unfolded_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))
        if 'sum' in var or 'diff' in var or 'corr' in var:
           unfolded[var][0] = unfolded[var][0]*(-9)
           unfolded[var][1] = unfolded[var][1]*(-9)


        dataStat[var]   = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/UnfoldedStat_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))
        others[var]     = json.load(open(outDir+'/%(var)s/nominal/ALL/others.json' %{'var':var}))
        dataStat[var][0] = dataStat[var][0]*factor
        dataStat[var][1] = dataStat[var][1]*factor 
        
    createLatex(unfolded,dataStat,detectDict,modelling,others,'summaryResults_%s.tex' % (region))   




