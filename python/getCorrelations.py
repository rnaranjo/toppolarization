import Rebin
from ROOT import *
from utils import *
from Config import *
import numpy as np
import matplotlib.pyplot as plt

gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
obs_label = {}

obs_label["helpol_plus"]=r"$B_{+}^{k}$"
obs_label["helpol_minus"]=r"$B_{-}^{k}$"
obs_label["transpol_plus"]=r"$B_{+}^{n}$"
obs_label["transpol_minus"]=r"$B_{-}^{n}$"
obs_label["rpol_plus"]=r"$B_{+}^{r}$"
obs_label["rpol_minus"]=r"$B_{-}^{r}$"
obs_label["helcorr"]=r"$C(k,k)$"
obs_label["transcorr"]=r"$C(n,n)$"
obs_label["rcorr"]=r"$C(r,r)$"
obs_label["transhelsum"]=r"$C(n,k)+C(k,n)$"
obs_label["transheldiff"]=r"$C(n,k)-C(k,n)$"
obs_label["transrsum"]=r"$C(n,r)+C(r,n)$"
obs_label["transrdiff"]=r"$C(n,r)-C(r,n)$"
obs_label["rhelsum"]=r"$C(r,k)+C(k,r)$"
obs_label["rheldiff"]=r"$C(r,k)-C(k,r)$"


def getDataBootstrap(hnominal,i,binning):
    #hnominal = TH2DBootstrap()
    #hnominal = hfile.Get(dataname)
    #print hnominal.GetReplica(i)
    #hfile.GetObject(dataname, hnominal)
    histo = hnominal.GetReplica(i)
    histo = Rebin.rebin(histo, binning)
    bins = histo2list(histo)
    histo.Reset()

    histo.IsA().Destructor( histo )


    truthObs = computeObs(var,bins,binning) 




    return truthObs

def histo2list(histo):
    nbinsx = histo.GetNbinsX()
    nbinsy = histo.GetNbinsY()
    binlist = [histo.GetBinContent(x,y) for y in xrange(1,nbinsy+1) for x in xrange(1,nbinsx+1)]
    return binlist


if __name__ == '__main__':


    outDir      = Configuration.outputDirBt
    region = 'full'


    variables   = [
                   'helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                   'helcorr',\
                   'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                   'transheldiff'
    ]


    data = []

    for i,var in enumerate(variables):
       print "Processing variable:",var

       data.append([])

       from binningOptimization import binnings_dict

       btemp = binnings_dict(var)

       for bn,bc in btemp.iteritems():
         binName = bn
         binning = bc


       jsondir = outDir+"/%(var)s/nominal/EMMU/"%{'var':var}
       filename = jsondir+'%(var)s_%(sel)s_%(binName)s.root'%{'var':'map_incl', 'sel':'btag','binName':'200Bin'}


       hfile    = TFile(filename)
       hnominal = TH2DBootstrap()
       hnominal = hfile.Get('partonic')

       for j in xrange(2000):

          data[i].append(getDataBootstrap(hnominal,j,binning))

    c = np.corrcoef(data)

    for i in xrange(len(c)):
      for j in xrange(len(c[i])):
        if abs(c[i][j]) > 0.05 : print c[i][j]
        c[i][j] = 0. if abs(c[i][j]) < 0.05 else c[i][j] 
  #fig = plt.figure(figsize=(10,10))
    fig, ax = plt.subplots()
    matrix  = ax.matshow(c,cmap='seismic_r',vmin=-1.,vmax=1.)
    fig.colorbar(matrix,ticks=[-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1, 0, 0.1, 0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0])
    plt.xticks(range(len(variables)),[obs_label[var] for var in variables],rotation='vertical')
    plt.yticks(range(len(variables)),[obs_label[var] for var in variables])
    plt.tight_layout()
    plt.show()


