#!/bin/env python
import os
import json
import sys
from ROOT import *
import utils
from numpy import std, mean, array
from Config import Configuration
import numpy as np
import matplotlib.pyplot as plt
import Rebin
from progressbar import *
Debug        = 10
mybins       = None
mybins_truth = None
bound        = None
bound_truth  = None
Ndiff        = None



gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')



if __name__ == '__main__':


    resultDir = Configuration.outputDirt

    var                 = utils.setVar(sys.argv)

    fiducial            = utils.setFiducial(sys.argv)

   
    systematics         = {'zjets':0.06,'zjets_low':0.3,'single_top':0.07,'dibosons':0.05,'fiducial':0.,'ttv':0.1}
    systlist            = ['zjets','zjets_low','single_top','dibosons','fakes','fiducial','ttv']
 
    from binningOptimization import binnings_dict

    binning 		= '100Bin'#'%(bin)3'
    sigmas              = ['psigma','msigma']


    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bn


    if fiducial : resultDir = Configuration.outputDir


    variationFile = resultDir+'/%(var)s/nominal/ALL/%(var)s_pseudo_marginalization.json'%{'var':var,'var':var}
  
    variationdict = json.load(open(variationFile))
    signalvariationdict = variationdict
    bkgvariationdict = variationdict
    l = len(variationdict)
    #print "len", l

    systematics['fakes'] = [1.67]*(l/3)+[0.49]*(l/3)+[0.77]*(l/3)



    recoFile = resultDir+'/%(var)s/nominal/ALL/optimization/%(var)s_reco_btag_%(bin)s.json'%{'var':var,'var':var,'bin':binning}
    bckgFile = resultDir+'/%(var)s/nominal/ALL/optimization/%(var)s_bckg_btag_%(bin)s.json'%{'var':var,'var':var,'bin':binning}

    reco  = json.load(open(recoFile))
    bckg  = json.load(open(bckgFile))




    for syst in systlist:

       
      #Let's vary the signal 

      reco_p,reco_m = [],[]


      for i in xrange(len(reco)):

         reco_p.append(reco[i])
         reco_m.append(reco[i])
      
      directory = resultDir+'/%(var)s/nominal/ALL/syst/%(syst)s'%{'var':var,'syst':syst}

      if not os.path.exists(directory):
                 os.makedirs(directory)

      namep = '%(var)s_psigma_btag_%(bin)s.json'%{'var':var,'bin':binning}  
      namem = '%(var)s_msigma_btag_%(bin)s.json'%{'var':var,'bin':binning}      
     
      with open(directory+'/'+namep,'w') as outfile:
        json.dump(reco_p,outfile)
      with open(directory+'/'+namem,'w') as outfile:
        json.dump(reco_m,outfile)


      #Let's vary the background
      newbckg_p,newbckg_m = {},{}
      for key, values in bckg.iteritems():
        valuesp,valuesm = [],[] 
        for i in xrange(len(values)):
           if values[i] < 0. or bckg[key][i]<0.: print key,bckg[key][i], values[i], "EPPAAAAA"



           if syst == 'fakes':


             variationP = bckg[key][i]+(systematics[syst][i])*bckg[key][i]
             variationN = bckg[key][i]-(systematics[syst][i])*bckg[key][i]
             if syst != key : 
                 variationP = bckg[key][i]
                 variationN = bckg[key][i] 
                 

             valuesp.append(variationP)
             valuesm.append(variationN)

           else:

             variationP = bckg[key][i]+(systematics[syst])*bckg[key][i]
             variationN = bckg[key][i]-(systematics[syst])*bckg[key][i]

             if syst != key :
                 variationP = bckg[key][i]
                 variationN = bckg[key][i]

             valuesp.append(variationP)
             valuesm.append(variationN)



            
         
        newbckg_p[key] = valuesp
        newbckg_m[key] = valuesm

      namepbckg = '%(var)s_bckg_psigma_btag_%(bin)s.json'%{'var':var,'bin':binning}
      namembckg = '%(var)s_bckg_msigma_btag_%(bin)s.json'%{'var':var,'bin':binning}

      with open(directory+'/'+namepbckg,'w') as outfile:
        json.dump(newbckg_p,outfile)
      with open(directory+'/'+namembckg,'w') as outfile:
        json.dump(newbckg_m,outfile)
