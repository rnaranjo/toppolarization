#!/bin/env python
import os
import json
import sys
from ROOT import *
import utils
from numpy import std, mean, array
from Config import Configuration
import numpy as np
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import Rebin
from progressbar import *
import binningOptimization
Debug        = 10
mybins       = None
mybins_truth = None
bound        = None
bound_truth  = None
Ndiff        = None
doRebin      = True


gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')


def diff(a, b):
 return [aa for aa in a if aa not in b]

def plotHistogram(data,name,directory):
   if not os.path.isdir(directory): os.system("mkdir -p %s"%directory)
   import matplotlib
   matplotlib.use('Agg')

   import numpy as np
   import matplotlib.mlab as mlab
   import matplotlib.pyplot as plt

   num_bins = 50
   # the histogram of the data
   fig = plt.figure()
   n, bins, patches = plt.hist(data, num_bins, normed=0, facecolor='green', alpha=0.5)

   plt.xlabel('Relative Difference: Nominal - '+name)
   plt.ylabel('Number of Events')
   plt.title('mean g:  '+str(mean(data))+' - rms g: '+str(std(data)))
   plt.tight_layout()
   plt.savefig(directory+name+'_histogram.eps', format='eps')
   plt.close(fig)


def plotHistogramEvent(data,data2,name,directory):
   if not os.path.isdir(directory): os.system("mkdir -p %s"%directory)
   import matplotlib
   matplotlib.use('Agg')

   import numpy as np
   import matplotlib.mlab as mlab
   import matplotlib.pyplot as plt

   num_bins = 10
   # the histogram of the data
   fig = plt.figure()
   n, bins, patches = plt.hist([data,data2], num_bins, normed=0,color=['green','red'] , facecolor='green', alpha=0.5, histtype='step')
   #n2, bins2, patches2 = plt.hist(data, num_bins, normed=0, facecolor='red', alpha=0.2, stacked=True,histtype='step')
   plt.xlabel('Number of events '+name)
   plt.ylabel('Number of Events')
   plt.title('mean g:  '+str(mean(data))+' - rms g: '+str(std(data))+' \n mean r: '+str(mean(data2)) +' - rms r: '+str(std(data2)))
   plt.tight_layout()
   plt.savefig(directory+name+'_histogram_events.eps', format='eps')
   plt.close(fig)

def plotVariations(results,statdict,dy,name,channels,directory):

  print results
    

  for r in xrange(len(results['EE'][name])):

     if not os.path.isdir(directory): os.system("mkdir -p %s"%directory)
     y,errors = [],[]
     list_y,list_errors,stat = [],[],[]

     for channel in channels:

         
         
         tmp_results = results[channel][name][r]['points']
         tmp_stat    = statdict[channel][r]
         tmp_y       = [i[0] for i in tmp_results]
         tmp_errors  = [i[1] for i in tmp_results]
         y          = y + tmp_y
         errors     = errors  + tmp_errors
         stat = stat + tmp_stat





     errc        = [0. for i in xrange(1,len(y)+1) ]
     x      = [i for i in xrange(1,len(y)+1) ]
     labels = x #['1st bin - ee', '2nd bin - ee', '3rd bin - ee', '4th bin - ee','1st bin - $\mu\mu$', '2nd bin - $\mu\mu$', '3rd bin - $\mu\mu$', '4th bin - $\mu\mu$','1st bin - e$\mu$', '2nd bin - e$\mu$', '3rd bin - e$\mu$', '4th bin - e$\mu$']
     
     fig = plt.figure()
     plt.errorbar(x, errc, yerr=stat,color='#D6D6D6',linewidth=10,alpha=0.3,fmt='.',label='data statistics')
     plt.errorbar(x, y, yerr=errors, fmt='o',color='b')
     ymax = (max(y)+max(errors))*5. if (max(y)+max(errors))*5. > (max(stat)+max(errc))*5. else (max(stat)+max(errc))*5.
     plt.axis([0, len(x)+1,-ymax,ymax])


     plt.text( 0.3,0.87*ymax, 'ATLAS',fontsize=14, fontweight='bold',style='italic')
     plt.text( 1.8,0.87*ymax, 'Internal',fontsize=13,style='italic')
     plt.text( 0.3,0.75*ymax, "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=13)
     plt.text( 0.3,0.65*ymax, 'nominal-'+name,fontsize=10,style='italic')
     # draw a thick red hline at y=0 that spans the xrange
     l = plt.axhline(linewidth=2, color='k') 
     plt.vlines(x=len(x)/3+0.5, ymin=-ymax*0.6, ymax=ymax*0.6, linewidth=1, color='k',alpha=0.5)
     plt.vlines(x=len(x)*2/3+0.5, ymin=-ymax*0.6, ymax=ymax*0.6, linewidth=1, color='k',alpha=0.5)
     plt.text( 2.5,ymax*0.5, '$ee$',fontsize=13)
     plt.text( 6.5,ymax*0.5, '$\mu\mu$',fontsize=13)
     plt.text( 10.5,ymax*0.5, '$e\mu$',fontsize=13)

     plt.xticks(x, labels, rotation='vertical')
     plt.ylabel('Relative Difference')
     plt.tight_layout()
     # Tweak spacing to prevent clipping of tick-labels
     #plt.show()
     plt.savefig(directory+name+'_'+dy+'_bin'+str(r)+'.eps', format='eps')
     plt.close(fig)
 


def plotDoubleVariations(results,statdict,dy,name,channels,directory):
  import matplotlib
  matplotlib.use('Agg')


  for r in xrange(len(results['EE'][name+'_up'])):
   
    if not os.path.isdir(directory): os.system("mkdir -p %s"%directory)
    y_down,errors_down,y_up,errors_up,stat = [],[],[],[],[]
    for channel in channels:
       tmp_stat = statdict[channel][r]
       tmp_results_down = results[channel][name+'_down'][r]['points']
       tmp_results_up   = results[channel][name+'_up'][r]['points']
       tmp_y_down      = [i[0] for i in tmp_results_down]
       tmp_errors_down = [i[1] for i in tmp_results_down]
       tmp_y_up      = [i[0] for i in tmp_results_up]
       tmp_errors_up = [i[1] for i in tmp_results_up]
       stat = stat + tmp_stat 
       y_down = y_down + tmp_y_down
       y_up = y_up + tmp_y_up
       errors_down = errors_down + tmp_errors_down 
       errors_up = errors_up + tmp_errors_up

    x_up        = [i for i in xrange(1,len(y_up)+1) ]
    x_down      = [i for i in xrange(1,len(y_down)+1) ]
    errc        = [0. for i in xrange(1,len(y_down)+1) ]
    labels = x_up #['1st bin - ee', '2nd bin - ee', '3rd bin - ee', '4th bin - ee','1st bin - $\mu\mu$', '2nd bin - $\mu\mu$', '3rd bin - $\mu\mu$', '4th bin - $\mu\mu$','1st bin - e$\mu$', '2nd bin - e$\mu$', '3rd bin - e$\mu$', '4th bin - e$\mu$']

    fig=plt.figure()
    plt.errorbar(x_down, errc, yerr=stat,color='#D6D6D6',linewidth=10,alpha=0.3,fmt='.',label='data statistics')
    plt.errorbar(x_up, y_up, yerr=errors_up, fmt='o',color='b',label='up')
    plt.errorbar(x_down, y_down, yerr=errors_down, fmt='o',color='r',label='down')

    

    ymax_up = (max(y_up)+max(errors_up))*5. if (max(y_up)+max(errors_up))*5. > (max(errc)+max(stat))*5.  else (max(errc)+max(stat))*5. 
    ymax_down = (max(y_down)+max(errors_down))*5. if (max(y_down)+max(errors_down))*5. > (max(errc)+max(stat))*5.  else (max(errc)+max(stat))*5

    ymax = ymax_up if ymax_up > ymax_down else ymax_down
    plt.axis([0, len(x_down)+1,-ymax,ymax])




    plt.text( 0.3,0.87*ymax, 'ATLAS',fontsize=14, fontweight='bold',style='italic')
    plt.text( 7.0,0.87*ymax, 'Internal',fontsize=13,style='italic')
    plt.text( 0.3,0.75*ymax, "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=13)
    plt.text( 0.3,0.65*ymax, 'nominal-'+name,fontsize=10,style='italic')
    # draw a thick red hline at y=0 that spans the xrange
    l = plt.axhline(linewidth=2, color='k')
    plt.vlines(x=16.5, ymin=-ymax*0.6, ymax=ymax*0.6, linewidth=1, color='k',alpha=0.5)
    plt.vlines(x=32.5, ymin=-ymax*0.6, ymax=ymax*0.6, linewidth=1, color='k',alpha=0.5)
    plt.text( 8.5,ymax*0.5, '$ee$',fontsize=13)
    plt.text( 25.5,ymax*0.5, '$\mu\mu$',fontsize=13)
    plt.text( 43.5,ymax*0.5, '$e\mu$',fontsize=13)

    plt.xticks(x_down, labels, rotation='vertical')
    plt.ylabel('Relative Difference')
    plt.tight_layout()
    plt.legend(loc="upper right",numpoints = 1, fontsize='small',frameon=False)
    #plt.show()
    #print directory+name+'_'+dy+'_bin'+str(r)+'.eps'
    plt.savefig(directory+name+'_'+dy+'_bin'+str(r)+'.eps', format='eps')
    plt.close(fig)

def getStat(nominalfile, hname):
   tnominal = TFile(nominalfile)
   hnominal = TH2DBootstrap()
   hnominal = tnominal.Get(hname)

   bins = binningOptimization.binnings_dict(var)
   mybins = bins.get(bins.keys()[0])


   if doRebin:
     hn = Rebin.rebin(TH1D(hnominal.GetReplica(0)), mybins)
   else:
     hn = TH1D(hnominal.GetReplica(0))
   #Let's calculate the data statistical component
   nbinsx = hn.GetNbinsX()
   stat = [sqrt(hn.GetBinContent(x))/hn.GetBinContent(x) if hn.GetBinContent(x) != 0. else 0.  for x in xrange(1,nbinsx+1)]
   print  stat
   hn.Clear()
   tnominal.Close()

   return [stat]



def getVariations(nominalfile, systfile,var, hname,syst,channel, plot = False, replicas = 500):
   tnominal = TFile(nominalfile)
   tsyst    = TFile(systfile)
  
   hnominal = TH1DBootstrap()
   hsyst    = TH1DBootstrap()

   hnominal = tnominal.Get(hname)
   hsyst    = tsyst.Get(hname)


   bins = binningOptimization.binnings_dict(var)
   mybins = bins.get(bins.keys()[0])

   if doRebin:
     dummy = Rebin.rebin(TH1D(hnominal.GetReplica(0)), mybins)
   else:
     dummy = TH1D(hnominal.GetReplica(0))

   #Let's calculate the data statistical component
   


   nbinsx = dummy.GetNbinsX()
   variations,results,stat = [],[],[]
   evtnom, evtsyst = [],[]
   vari = []
   for i in xrange(replicas):

      if doRebin:
         hn = Rebin.rebin(TH1D(hnominal.GetReplica(i)),mybins)
         hs = Rebin.rebin(TH1D(hsyst.GetReplica(i)),mybins)
      else:
         hn = TH1D(hnominal.GetReplica(i))
         hs = TH1D(hsyst.GetReplica(i))

      variations.append([(hn.GetBinContent(x)-hs.GetBinContent(x))/hn.GetBinContent(x) if hn.GetBinContent(x) != 0. else 0. for x in xrange(1,nbinsx+1)])
      hn.IsA().Destructor( hn )
      hs.IsA().Destructor( hs )

   hnominal.IsA().Destructor( hnominal )
   hsyst.IsA().Destructor( hsyst )

   tnominal.Close()
   tsyst.Close() 
   #Let's convert to tuple bins   
   binvariation = zip(*variations)
   listSig = []      
   for i in xrange(len(binvariation)):
     #print variations[i]
     #if plot: 
     #    plotHistogram(binvariation[i],syst+'_'+var+'_'+channel+'_bin'+str(i+1),outDir+'/%(asym)s/nominal/ALL/variations/'%{'asym':asym,'var':var})
         
     sigma = std(binvariation[i])
     meanv = mean(binvariation[i])
     listSig.append(meanv/sigma if sigma != 0. else 0.)
     results.append([meanv,sigma])
   
   return [{'points':results}]
 
      
def whichSignificant(variations):


   rlist = []
   for r in xrange(len(variations['EE']['jer'])):

     l  = [] #At least one bin with mora than one sigma 
     l2 = [] #two bins with one sigma or one bin with 2
   
   
   
     for syst in systematics:
        isSig = False;
   
        for ch in channels:
            if variations[ch][syst][r]['isSignificant'] == True: 
              l.append(syst)
   #         if variations[ch][syst]['isSignificant2'] == True or  variations[ch][syst]['isSignificant3'] == True:
   
            if variations[ch][syst][r]['isSignificant3'] == True:
              l2.append(syst)
   
   
     #Let's clean the list
     final,final2 = [],[]
    
     for i in l:
       for j in asymSyst:
         if  j in i: final.append(j)
    
       if 'jeff' in i: final.append('jeff')
       if 'jer' in i: final.append('jer') 
   
     for i in l2:
       for j in asymSyst:
         if  j in i: final2.append(j)
   
       if 'jeff' in i: final2.append('jeff')
       if 'jer' in i:  final2.append('jer')
     #remove duplicated
     final = list(set(final))
     final2 = list(set(final2))
     all = asymSyst+symSyst  
     
     rlist.append([final,diff(all,final),final2,diff(all,final2)])

   return rlist


def listfabs(list):
  tmp=[]
  for i in list:
    tmp.append(fabs(i))
  return tmp

def average(up,down):
  list = []
  for i in xrange(len(up)):
     list.append((fabs(up[i])+fabs(down[i]))/2.)
  return list

def createSystDict(filename, systs, channels,isfiducial, **kwargs):

  dicts = []

  numbins = len(kwargs['signal']['EE']['jer']) 
  print numbins
   #Let's do it first for signal
 
  dict = {}
  dict['signal'] = {}
  for name in systs:
    y = []
    y_up = []
    y_down = []

    if name in Configuration.possibleSymmetricSyst:
 
      for channel in channels:
         tmp_results = []
         for r in xrange(numbins): tmp_results = tmp_results + kwargs['signal'][channel][name][r]['points']
         tmp_y       = [i[0] for i in tmp_results]
         y           = y + tmp_y
      dict['signal'][name] = listfabs(y) 
    else:
       for channel in channels:
         tmp_results_down = []
         tmp_results_up   = []
         for r in xrange(numbins):  tmp_results_down = tmp_results_down + kwargs['signal'][channel][name+'_down'][r]['points']  
         for r in xrange(numbins):  tmp_results_up   = tmp_results_up   + kwargs['signal'][channel][name+'_up'][r]['points']
         tmp_y_down       = [i[0] for i in tmp_results_down]
         tmp_y_up         = [i[0] for i in tmp_results_up]
         y_down           = y_down + tmp_y_down
         y_up             = y_up + tmp_y_up
       dict['signal'][name] = average(y_up,y_down)  
    
  #Let's do it for the background
  dict['background'] = {} 
  
  for name in systs:
    dict['background'][name]={}
    for key, value in kwargs.iteritems():
       if key == 'signal': continue
       if not isfiducial and key == 'fiducial': continue
       y = []
       y_up = []
       y_down = []  
       if name in Configuration.possibleSymmetricSyst:
           for channel in channels:
              tmp_results = []
              for r in xrange(numbins): tmp_results = tmp_results + kwargs[key][channel][name][r]['points']
              tmp_y       = [i[0] for i in tmp_results]
              y           = y + tmp_y
           #if key == 'fakes': dict['background'][name][key] = [0.0]*len(y)
           dict['background'][name][key] = listfabs(y)
       else:
           for channel in channels:
             tmp_results_down = []
             tmp_results_up   = []
             for r in xrange(numbins):  tmp_results_down = tmp_results_down + kwargs[key][channel][name+'_down'][r]['points']
             for r in xrange(numbins):  tmp_results_up   = tmp_results_up   + kwargs[key][channel][name+'_up'][r]['points']
             tmp_y_down       = [i[0] for i in tmp_results_down]
             tmp_y_up         = [i[0] for i in tmp_results_up]
             y_down           = y_down + tmp_y_down
             y_up             = y_up + tmp_y_up
           #if key == 'fakes': dict['background'][name][key] = [0.0]*len(y)
           dict['background'][name][key] = average(y_up,y_down)
 
  dicts.append(dict)


  with open(filename,'w') as outfile:
    json.dump(dicts[0],outfile)
  
  return dicts

if __name__ == '__main__':


    outDir    = Configuration.outputDirBt
    resultDir = Configuration.outputDirt

    var               = utils.setVar(sys.argv)
    isfiducial        = utils.setFiducial(sys.argv)
    channels          = ['EE','MUMU','EMMU']
    systematics       = ['jer','jeff'] #Configuration.possibleSystN
    asymSyst          = [] #Configuration.possibleAsymmetricSyst
    symSyst           = ['jer', 'jeff'] #Configuration.possibleSymmetricSyst
    systematics       = Configuration.possibleSystN
    asymSyst          = Configuration.possibleAsymmetricSyst
    symSyst           = Configuration.possibleSymmetricSyst
    binning           = '200Bin'#'fourBin3'
    variations,statdict          = {},{}
    v_zjets, v_zjets_low,v_single_top,v_diboson,v_fakes,v_fiducial,v_ttv = {},{},{},{},{},{},{}
    if isfiducial: 
      outDir = Configuration.outputDirB
      resultDir = Configuration.outputDir


    #Let's loop over the channels
    print "################################"
    print "#### Beginning calculations ####"
    print "################################"
    print ""
    for channel in channels:
        variations[channel]={}
        v_zjets[channel]={}
        v_zjets_low[channel]={}
        v_single_top[channel]={}
        v_diboson[channel]={}
        v_fakes[channel]={}
        v_fiducial[channel]={}
        v_ttv[channel]={}

        #We read the nominal file
        nominalDir = outDir+'/%(var)s/nominal/%(channel)s/'%{'channel':channel,'var':var}
        nominalfile  = nominalDir + 'map_incl' +'_btag_'+binning+'.root'


        statdict[channel] = getStat(nominalfile, 'reco')
        print "#### Calculating systematics for %s  ####" % channel
        widgets = ['Something: ', Percentage(), ' ', Bar(marker=RotatingMarker()), ' ', ETA(), ' ', FileTransferSpeed()]
        bar = ProgressBar(widgets=[Percentage(), Bar()], maxval=len(systematics)).start()
        #we loop over systematics 
        for i,syst in enumerate(systematics):
            bar.update(i+1)
            #print "# Calculating variation for %s #" % syst
            systDir    = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'channel':channel,'syst':syst,'var':var}
            systfile     = systDir + 'map_incl' + '_btag_'+binning+'.root'
            #we calculate the variations using the 'reco' histogram
            variations[channel][syst] = getVariations(nominalfile, systfile,var, 'reco',syst, channel)

            v_zjets[channel][syst] = getVariations(nominalfile, systfile,var, 'zjets',syst, channel)

            v_zjets_low[channel][syst] = getVariations(nominalfile, systfile,var, 'zjets_low',syst, channel)

            v_single_top[channel][syst] = getVariations(nominalfile, systfile,var, 'single_top',syst, channel)

            v_diboson[channel][syst] = getVariations(nominalfile, systfile,var, 'dibosons',syst, channel)

            v_fakes[channel][syst] = getVariations(nominalfile, systfile,var, 'fakes',syst, channel)

            v_ttv[channel][syst] = getVariations(nominalfile, systfile,var, 'ttv',syst, channel)

            if isfiducial: v_fiducial[channel][syst] = getVariations(nominalfile, systfile,var, 'fiducial',syst, channel) 

        bar.finish()
    print ""
    print "#### Ending  calculations ####"
    print ""
    #print variations 
    #print statdict
    #Which systematics are significant
    
    for name in asymSyst:
       print "Plotting %s" %name 
       plotDoubleVariations(variations,statdict,var,name,channels,outDir+'/%(var)s/nominal/ALL/variations/'%{'var':var})
    for name in symSyst:
       print "Plotting %s" %name
       plotVariations(variations,statdict,var,name,channels,outDir+'/%(var)s/nominal/ALL/variations/'%{'var':var})

    
    fileALL  =   resultDir+'/%(var)s/nominal/ALL/SystDict_%(var)s_btag_ALL.json'%{'var':var}
    print fileALL
    createSystDict(fileALL, asymSyst+symSyst, channels,isfiducial, signal=variations,zjets=v_zjets,zjets_low=v_zjets_low,single_top=v_single_top,dibosons=v_diboson,fakes=v_fakes, fiducial=v_fiducial, ttv=v_ttv)

