import json

if __name__ == "__main__":

    var_list = ['helpol_plus']

    for var in var_list:
        
        syst_dict_name = '/nfs/dust/atlas/user/naranjo/TopPolarization/results_particle/helpol_plus/nominal/ALL/SystDict_'+var+'_btag_ALL.json'

        syst_dict = json.load(open(syst_dict_name,'r'))

        signal_list = syst_dict.keys()

        bkg_list = syst_dict['background']['jer'].keys()

        syst_list = syst_dict['signal'].keys()

        nbins = len(syst_dict['signal']['jer'])

        nbins_ee = nbins/3

        new_syst_dict = {}

        for sig in signal_list:

            print sig
            new_syst_dict[sig] = {}
            
            if sig == 'signal':
        
                for syst in syst_list:

                    new_syst_dict[sig][syst] = syst_dict[sig][syst][0:nbins_ee]

            if sig == 'background':
                
                for syst in syst_list:

                    new_syst_dict[sig][syst] = {}

                    for bkg in bkg_list:

                        new_syst_dict[sig][syst][bkg] = syst_dict[sig][syst][bkg][0:nbins_ee]

        with open('/nfs/dust/atlas/user/naranjo/TopPolarization/results_particle/helpol_plus/nominal/EE/SystDict_'+var+'_btag_ALL.json', 'w') as outfile:
            json.dump(new_syst_dict, outfile)
