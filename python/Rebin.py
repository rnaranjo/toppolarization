import array as a
from ROOT import TH2D,TAxis



def rebin2D(histogram,binning):

	binning = a.array('d',binning)

	newHistogram = TH2D(histogram.GetName()+'_old',histogram.GetTitle(),len(binning)-1,binning,len(binning)-1,binning)

	for j in xrange(1,histogram.GetYaxis().GetNbins()+1):
		for i in xrange(1,histogram.GetXaxis().GetNbins()+1):
			newHistogram.Fill(histogram.GetXaxis().GetBinCenter(i),histogram.GetYaxis().GetBinCenter(j),histogram.GetBinContent(i,j))

	return newHistogram

def rebin(histogram,binning):

	binning = a.array('d',binning)

	return histogram.Rebin(len(binning)-1,"oldrebin",binning)
