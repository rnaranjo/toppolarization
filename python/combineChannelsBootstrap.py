#!/bin/env python

import json
import time
import os
import sys
import itertools
from math import sqrt
from array import array
import ROOT as r # variable r is 'ROOT'
import utils
import numpy as np
from utils import writefile
from Config import Configuration, getObsSamples, getSampleLists, getNtupleDir, getProtosSamples2Json, getLinearSamples2Json, getFitProtosSamples2Json, getObsSamples2Json, getObsResmatSamples



def getMigration(resmat, partonic ):

    nbinsx = len(resmat[0]) # reconstructed distribution
    nbinsy = len(partonic) # truth distribution
    #print nbinsx,nbinsy
    migration    = []
    migrationErr = []
    acceff       = []

    for y in xrange(0,nbinsy):
        truthbin = []
        truthbinErr = []
        norm = np.sum(resmat[y]) # integral of the rows (along de X axis)
        #nprint  norm
        for x in xrange(0,nbinsx):
            value  = resmat[y][x]/norm
            effbin = getEff(resmat,partonic,y)
            value  = value*effbin
            truthbin.append(value)
        migration.append(truthbin)
        migrationErr.append(truthbinErr)
        acceff.append(effbin)
    return migration, acceff


def getEff(resmat,partonic,y):
    #get coordinates in 2D truth hist
    #note: resp matrix is 2D: (nXtruth*nYtruth) x (nXtruth*nYtruth) --> y = (yy-1)*Nbins_dy + xx
    return np.sum(resmat[y]) / partonic[y]



def mergeObject(name,binning,var,pdf=False,pdfType=""):

    for i in xrange(1000):
       channels = ['EE','MUMU','EMMU']
       newobject = []
       newdict,firstdict,firstresmat = {}, True, True
       for ch in channels:
            jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/bootstrap/%(pos)d/'%{'var':var,'syst':syst.lower(),'channel':ch,'pos':i}
            jsonobj = jsondir+'%(var)s_%(obj)s_btag_%(binning)s.json'%{'var':var,'obj':name,'binning':binning}
            #print jsonobj
            if 'bckg' not in name:
              if 'mig' in name:
                  if firstresmat:
                    newobject = json.load(open(jsonobj))
                    firstresmat = False
                  else:
                    for i in xrange(len(newobject)):
                      tmp = json.load(open(jsonobj))
                      newobject[i] = newobject[i] + tmp[i]



              else:
                 if 'partonic' in name:
                    newobject = json.load(open(jsonobj))
                 else:
                    newobject = newobject + json.load(open(jsonobj))
            else:
              d = json.load(open(jsonobj))
              if firstdict:
                for key, value in d.iteritems():
                   newdict[key] = d[key]
                firstdict = False
              else:
                for key, value in d.iteritems():
                   newdict[key] = newdict[key] + d[key]


       newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/'%{'var':var,'syst':syst.lower(),'pos':i}
       if pdf: newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/'%{'var':var,'syst':pdfType.lower()}
       if not os.path.isdir(newdir): os.system("mkdir -p %s"%newdir)
       newfile = newdir +'%(var)s_%(obj)s_btag_%(binning)s'%{'var':var,'obj':name,'binning':binning}
       if 'bckg' not in name:
         writefile(newfile,newobject)
       else:
          writefile(newfile,newdict)

if __name__ == '__main__':

    outDir      = Configuration.outputDirBt
    variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                    'helcorr',\
                    'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                    'transheldiff']

    #variables = [sys.argv[1]] #"transheldiff transrsum".split()
    fiducial    = utils.setFiducial(sys.argv)
    if fiducial: outDir = Configuration.outputDirB

    systs =['nominal','perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig']

    systs = ['fastpythia']



    for var in variables:

       objects2Merge    = getObsSamples2Json(var)
       #print objects2Merge
       migrations2merge =  getObsSamples(var)
       import binningOptimization
       binnings = binningOptimization.binnings_dict(var)


       for syst in systs:
         for bin in binnings.iterkeys():
             for obj in objects2Merge:
               print obj,bin,var
               mergeObject(obj,bin,var)
             
