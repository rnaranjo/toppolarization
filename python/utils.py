#!/bin/env python

import string
import json
import numpy as np

from Config import Configuration, getProtosSamples,getLinearSamples,getFitProtosSamples, getObsSamples
from math import sqrt
import ROOT as r
from ROOT import *
import array as arr

def writefile(fname='',datatowrite=None):
    with open(fname+'.json','w') as outfile:
            json.dump(datatowrite,outfile)
def setVar(args):
    var = ''
    for c in args:
        if c in Configuration.possibleVars: var = c
    assert var, 'invalid variable' #is that actually possible? it should be either in possible Vars or not...
    return var
def setvarandsel(args):
    sel = ''
    var = ''
    for c in args :
        if c in Configuration.possibleSelections : sel = c
        if c in Configuration.possibleVars : var = c

    assert sel, 'invalid selection'
    assert var, 'invalid variable'
    return var,sel

def setBinningOptimization(args):
    for c in args:
        if c == 'optimization': return True
    return False

def computeObs(obsname,bins,binning): #obs for observable
    if 'pol' in obsname:
        return computePol(bins,binning)
    else:
        return computeExpValue(bins,binning)

def computeObsUncert(obsname,bins,binning):
    if 'pol' in obsname:
        return computePolUncert(bins,binning)
    else:
        return computeExpValueUncert(bins,binning)

def computePol(bins,binning):
    r.gROOT.SetBatch(1) # Avoid histograms
    #gibt den list index, bei dem der eintrag (0) gefunden wurde
    #if 0 in binning:
#        zero_bin = binning.index(0)
    #this is the correct bin for the summation
    #    print "RALPH compute pol", zero_bin
  #      print "bins check1",bins
#        Npos = sum(bins[zero_bin:]) #positive cos theta
#        Nneg = sum(bins[:zero_bin]) #negative cos theta
  #      print "RALPH compute pol neg pos",Nneg,Npos
  #      print "RALPH compute old pol", float (Npos-Nneg)/(Npos+Nneg)*2
#        return float (Npos-Nneg)/(Npos+Nneg)*2.
    #else:
#    print 'ralph compute Pol bins',bins
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
  #      print "RALPH compute pol nonzero bins and array",nbins,bin_arr
    fithisto = TH1F("pol_histo","histogram to fit polarization",nbins,bin_arr)
    fithisto.Sumw2() #do I need it here? hm... for equidistant bins maybe not...
    binx=1
    for bin in bins: #filling the histogram with the bin contents and dividing it by the binwidth in case of asymmetric binning
        fithisto.SetBinContent(binx,bins[binx-1]/fithisto.GetBinWidth(binx))
        fithisto.SetBinError(binx,TMath.Sqrt(bins[binx-1])/fithisto.GetBinWidth(binx))
        binx=binx+1
    fithisto.Scale(nbins/fithisto.Integral())
#    print "bins check2",bins
#    fithisto.Print("range")
    if nbins > 2:
        fitresultptr = fithisto.Fit("pol1","FSQ")
        results = fitresultptr.GetParams()
        return results[1]
    elif nbins == 2: #calculate the polarization from the asymmetry (from the histogram, would be easier from the numbers, but nevermind)
        pol = 2*(fithisto.GetBinContent(2) -  fithisto.GetBinContent(1))/(fithisto.GetBinContent(2) + fithisto.GetBinContent(1)) #or just integral in the denominator
        return pol
#    print fitresultptr.Chi2()
    #print 'hier funktionierts?'
#    print results
    #    print "RALPH compute new pol",results[1]


def computePolUncert(bins,binning):
    r.gROOT.SetBatch(1) # Avoid histograms
    #print "ralph computePolUncert",bins,binning
    #RALPH NOTE: should be fixed soon
    #zero_bin = binning.index(0)
    #dNpos = sqrt( sum(bins[zero_bin:])
    #dNneg = sqrt( sum(bins[:zero_bin]) )
    #dNpos2 = dNpos*dNpos
    #dNneg2 = dNneg*dNneg
    #Npos = sum(bins[zero_bin:])
    #Nneg = sum(bins[:zero_bin])
    #Ntot = Npos+Nneg
    #Ntot2 = Ntot*Ntot
    #Uncert = 4. * sqrt((dNneg2 * Npos * Npos + dNpos2 * Nneg * Nneg) / (Ntot2 * Ntot2));
    #compute fit a second time although it might be unnecessary
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    fithisto = TH1F("pol_histo_unc","histogram to fit polarization",nbins,bin_arr)
    #the sumw2 is causing problems here... don't know why
    fithisto.Sumw2()  #not sure about it... but since I scale it...
    binx=1
    for bin in bins: #filling the histogram with the bin contents and dividing it by the binwidth in case of asymmetric binning
        fithisto.SetBinContent(binx,bins[binx-1]/fithisto.GetBinWidth(binx))
        fithisto.SetBinError(binx,TMath.Sqrt(bins[binx-1])/fithisto.GetBinWidth(binx))
        binx=binx+1
    fithisto.Scale(nbins/fithisto.Integral())
    #fithisto.Print("range")
    #fithisto.Print()
    if nbins > 2:
        fitresultptr = fithisto.Fit("pol1","FSQ")
        results = fitresultptr.GetErrors()
        return results[1] #now results are the errors and not the fit value itself
    elif nbins == 2: #calculate the polarization from the asymmetry (from the histogram, would be easier from the numbers, but nevermind)
        poluncert = 2*TMath.Sqrt(1/(fithisto.GetBinContent(2) + fithisto.GetBinContent(1))) #or just integral in the denominator
        return poluncert
    #print 'hier funktionierts nicht?'

#    return float(Uncert)

def computeExpValue(bins,binning):
#gives just the expectation value for histogram or the sampled
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    histo = TH1F("histo","histogram to get the mean of the distribution",nbins,bin_arr)
    #note ralph
    #print bins
    #print bin_arr
    binx=1
    for bin in bins: #filling the histogram the bin contents
        histo.SetBinContent(binx,bins[binx-1])
        binx=binx+1

    mean = histo.GetMean()
#    print mean
#    mean = (histo.Integral(5,8) - histo.Integral(1,4))/histo.Integral() # just as a test for the spin correlations

    return float(mean)

def computeExpValueUncert(bins,binning):
#gives just the expectation value for histogram or the sampled
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    histo = TH1F("histo","histogram to get the mean of the distribution",nbins,bin_arr)

    binx=1
    for bin in bins: #filling the histogram the bin contents
        histo.SetBinContent(binx,bins[binx-1])
        binx=binx+1

    mean_err = histo.GetMeanError()

    return float(mean_err)



def setsel(args):
    sel = ''
    for c in args :
        if c in Configuration.possibleSelections : sel = c

    assert sel, 'invalid selection'
    return sel

def setSyst(args):
    sel = ''
    for c in args :
        print c
        if c.lower() in Configuration.possibleSyst : sel = c.lower()

    assert sel, 'invalid selection'
    return sel

def setSystAsimov(args):
    sel = ''
    for c in args :
        if c.lower() in Configuration.possibleSymmetricSyst+Configuration.possibleAsymmetricSyst : sel = c.lower()

    assert sel, 'invalid selection'
    return sel

def setFiducial(args):
   f = False
   for c in args :
        if c == 'fiducial' : f = True
   return f

def setTtbar(args):
    ttbarAsym = False
    for c in args : # Get argument for channel and selection in command line
        if c in Configuration.possibleTtbar    :
          if c=='lepton': ttbarAsym = False
          if c=='ttbar' : ttbarAsym = True
    return ttbarAsym

def setBootstrap(args):
   f = False
   for c in args :
        if c == 'bootstrap' : f = True
   return f

def setBootstrapP(args):
   f = False
   pos = 0
   for i,c in enumerate(args) :
        if c == 'bootstrap' :
         f = True
         pos = i
   return f,args[pos+1]


def setPDF(args):

    list = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]
    pdf = False
    pname = ""
    for c in args:
      if c in list:
         pdf = True
         pname = c
    return pdf, pname

def setPDFNum(args):

    list = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]
    pdf = False
    pname = ""
    num = 0
    for i,c in enumerate(args):
      if c in list:
         pdf = True
         num = args[i+1]
    return num


def setLumiWeight(args):

    lumi = True
    for c in args :
        if c == 'noLumiW' : lumi = False
    return lumi

def setUseBackground(args):
   bkg = True
   for c in args :
        if c == 'noBKG' : bkg = False
   return bkg

def setSignalModeling(args):
   bkg = False
   for c in args :
        if c == 'isSignalMod' : bkg = True
   return bkg

def setSignal2Unfold(args):
    signal = ''
    for c in args :
        if c in Configuration.possibleSignal2Unfold : signal = c
    return signal

def setEnsemble(args):
    ensemble = False
    for c in args :
        if c == 'ensemble' : ensemble = True
    return ensemble

def setTtbarAndChan(args):
    ttbarAsym = False
    channel = ''
    for c in args : # Get argument for channel and selection in command line
        if c in Configuration.possibleChannels : channel    = c
        if c in Configuration.possibleTtbar    :
          if c=='lepton': ttbarAsym = False
          if c=='ttbar' : ttbarAsym = True
    if not channel : assert False, "You have to specify a channel from %s" % Configuration.possibleChannels
    return ttbarAsym, channel


def setSystematics(args):

    for c in args:
        if c.lower() in Configuration.possibleSyst or c  in Configuration.possibleSyst : return c
    return False


def setEnsemble(args):
    ensemble = False
    for c in args:
      if c in Configuration.possibleEnsemble : ensemble = True
    return ensemble


def setOptimalBinning(args,var):

    from binningOptimization import binnings_dict
    binnings_dict = binnings_dict(var)
    #default: get nominal binning
    binName = [(k) for k in binnings_dict.keys()][0]
    #print binName
    for c in args:
      if c in binnings_dict.keys() : binName = c

    assert binName , 'invalid binning'

    return binName

def getOptimalBinning(args,var):

    from binningOptimization import binnings_dict
    binnings_dict = binnings_dict(var)
    #default: get nominal binning
    binName = [(k) for k in binnings_dict.keys()][0]
    #print binName
    for c in args:
      if c in binnings_dict.keys() : binName = c

    assert binName , 'invalid binning'

    return binnings_dict[binName]

def setRegPar(args):

    from regpardict import regpar_dict
    regpar_dict = regpar_dict()

    #default: get nominal regularization
    regparName = [(k) for k in regpar_dict.keys()][0]

    for c in args:
      if c in regpar_dict.keys() : regparName = c

    assert regparName, 'invalid regparName'

    value = regpar_dict[regparName]
    return regparName,value


def setRegFunction(args):

    regFunc = 'Tikhonov'

    for c in args:
      if c in Configuration.possibleRegFunctions : regFunc = c

    return regFunc


def setRew(args):
    rewAsym = False
    rewType = ''
    for c in args:
        if c in Configuration.possibleRew : rewAsym = True
        if c in Configuration.possibleRewType : rewType = c
    return rewAsym, rewType


def setRewSignal(args,var):
    print getProtosSamples()+getFitProtosSamples()+getLinearSamples()+getObsSamples(var)
    for c in args:
       if c in getProtosSamples()+getFitProtosSamples()+getLinearSamples()+getObsSamples(var):
         return [c]
    return False

def computeAC(bins):
    halfn = len(bins)/2
    Npos = float(sum(bins[halfn:]))
    Nneg = float(sum(bins[:halfn]))
    return (Npos-Nneg)/(Npos+Nneg)

def computeUncertAC(bins):
    halfn = len(bins)/2
    Npos = float(sum(bins[halfn:]))
    Nneg = float(sum(bins[:halfn]))
    Ntot = Npos + Nneg
    Ntot2 = Ntot * Ntot
    dNpos = sqrt( Npos )
    dNneg = sqrt( Nneg )
    dNpos2 = dNpos * dNpos
    dNneg2 = dNneg * dNneg
    Uncert = 2. * sqrt( (dNneg2 * Npos * Npos + dNpos2 * Nneg * Nneg) / (Ntot2 * Ntot2));
    return Uncert





#list of [Ac,AcErr] for each differential bin
def computeAC_2D(bins, binsX):
    nBins_X  = len(binsX) - 1
    nBins_dY = len(bins) / nBins_X

    ACs = []
    #print 'bins:', bins
    for iBin in range(nBins_X): #for each differenitial bin
        bins_dY = bins[ iBin*nBins_dY : (iBin+1)*nBins_dY ]
        Ac    = computeAC(bins_dY)
        AcErr = computeUncertAC(bins_dY)
        ACs.append( [Ac, AcErr] )

    return np.array(ACs)


def computeAC_2DT(bins, binsX):
    nBins_X  = len(binsX) - 1
    nBins_dY = len(bins) / nBins_X

    ACs = []
    #print 'bins:', bins
    for iBin in range(nBins_X): #for each differenitial bin
        bins_dY = bins[ iBin*nBins_dY : (iBin+1)*nBins_dY ]
        Ac    = computeAC(bins_dY)
        AcErr = computeUncertAC(bins_dY)
        ACs.append( [Ac, AcErr] )

    return ACs



def computeAC_2D_Extra(binsi, binsX):

    bins = binsi[:-2]
    nBins_X  = len(binsX) - 1
    nBins_dY = len(bins) / nBins_X

    ACs = []
    #print 'bins:', bins
    for iBin in range(nBins_X): #for each differenitial bin
        bins_dY = bins[ iBin*nBins_dY : (iBin+1)*nBins_dY ]
        Ac    = computeAC(bins_dY)
        AcErr = computeUncertAC(bins_dY)
        ACs.append( [Ac, AcErr] )
    #print np.array(ACs)
    return np.array(ACs)



def getCurvature(value):
    size=len(value)
    def computeCurvature(bin): return value[bin-1]-2.0*value[bin]+value[bin+1]
    curvature = sum([c*c for c in map(computeCurvature,range(1,size-1))])
    return curvature



def computePol(bins,binning):
    r.gROOT.SetBatch(1) # Avoid histograms
    #gibt den list index, bei dem der eintrag (0) gefunden wurde
    #if 0 in binning:
#        zero_bin = binning.index(0)
    #this is the correct bin for the summation
    #    print "RALPH compute pol", zero_bin
  #      print "bins check1",bins
#        Npos = sum(bins[zero_bin:]) #positive cos theta
#        Nneg = sum(bins[:zero_bin]) #negative cos theta
  #      print "RALPH compute pol neg pos",Nneg,Npos
  #      print "RALPH compute old pol", float (Npos-Nneg)/(Npos+Nneg)*2
#        return float (Npos-Nneg)/(Npos+Nneg)*2.
    #else:
#    print 'ralph compute Pol bins',bins
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
  #      print "RALPH compute pol nonzero bins and array",nbins,bin_arr
    fithisto = TH1F("pol_histo","histogram to fit polarization",nbins,bin_arr)
    fithisto.Sumw2() #do I need it here? hm... for equidistant bins maybe not...
    binx=1
    for bin in bins: #filling the histogram with the bin contents and dividing it by the binwidth in case of asymmetric binning
        fithisto.SetBinContent(binx,bins[binx-1]/fithisto.GetBinWidth(binx))
        fithisto.SetBinError(binx,TMath.Sqrt(bins[binx-1])/fithisto.GetBinWidth(binx))
        binx=binx+1
    fithisto.Scale(nbins/fithisto.Integral())
#    print "bins check2",bins
#    fithisto.Print("range")
    if nbins > 2:
        fitresultptr = fithisto.Fit("pol1","FSQ")
        results = fitresultptr.GetParams()
        return results[1]
    elif nbins == 2: #calculate the polarization from the asymmetry (from the histogram, would be easier from the numbers, but nevermind)
        pol = 2*(fithisto.GetBinContent(2) -  fithisto.GetBinContent(1))/(fithisto.GetBinContent(2) + fithisto.GetBinContent(1)) #or just integral in the denominator
        return pol
#    print fitresultptr.Chi2()
    #print 'hier funktionierts?'
#    print results
    #    print "RALPH compute new pol",results[1]


def computePolUncert(bins,binning):
    r.gROOT.SetBatch(1) # Avoid histograms
    #print "ralph computePolUncert",bins,binning
    #RALPH NOTE: should be fixed soon
    #zero_bin = binning.index(0)
    #dNpos = sqrt( sum(bins[zero_bin:]) )
    #dNneg = sqrt( sum(bins[:zero_bin]) )
    #dNpos2 = dNpos*dNpos
    #dNneg2 = dNneg*dNneg
    #Npos = sum(bins[zero_bin:])
    #Nneg = sum(bins[:zero_bin])
    #Ntot = Npos+Nneg
    #Ntot2 = Ntot*Ntot
    #Uncert = 4. * sqrt((dNneg2 * Npos * Npos + dNpos2 * Nneg * Nneg) / (Ntot2 * Ntot2));
    #compute fit a second time although it might be unnecessary
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    fithisto = TH1F("pol_histo_unc","histogram to fit polarization",nbins,bin_arr)
    #the sumw2 is causing problems here... don't know why
    fithisto.Sumw2()  #not sure about it... but since I scale it...
    binx=1
    for bin in bins: #filling the histogram with the bin contents and dividing it by the binwidth in case of asymmetric binning
        fithisto.SetBinContent(binx,bins[binx-1]/fithisto.GetBinWidth(binx))
        fithisto.SetBinError(binx,TMath.Sqrt(bins[binx-1])/fithisto.GetBinWidth(binx))
        binx=binx+1
    fithisto.Scale(nbins/fithisto.Integral())
    #fithisto.Print("range")
    #fithisto.Print()
    if nbins > 2:
        fitresultptr = fithisto.Fit("pol1","FSQ")
        results = fitresultptr.GetErrors()
        return results[1] #now results are the errors and not the fit value itself
    elif nbins == 2: #calculate the polarization from the asymmetry (from the histogram, would be easier from the numbers, but nevermind)
        poluncert = 2*TMath.Sqrt(1/(fithisto.GetBinContent(2) + fithisto.GetBinContent(1))) #or just integral in the denominator
        return poluncert
    #print 'hier funktionierts nicht?'

#    return float(Uncert)

def computeExpValue(bins,binning):
#gives just the expectation value for histogram or the sampled
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    histo = TH1F("histo","histogram to get the mean of the distribution",nbins,bin_arr)
    #note ralph
    #print bins
    #print bin_arr
    binx=1
    for bin in bins: #filling the histogram the bin contents
        histo.SetBinContent(binx,bins[binx-1])
        binx=binx+1

    mean = histo.GetMean()
#    print mean
#    mean = (histo.Integral(5,8) - histo.Integral(1,4))/histo.Integral() # just as a test for the spin correlations

    return float(mean)

def computeExpValueUncert(bins,binning):
#gives just the expectation value for histogram or the sampled
    nbins = len(binning) - 1
    bin_arr = arr.array('d',binning)
    histo = TH1F("histo","histogram to get the mean of the distribution",nbins,bin_arr)

    binx=1
    for bin in bins: #filling the histogram the bin contents
        histo.SetBinContent(binx,bins[binx-1])
        binx=binx+1

    mean_err = histo.GetMeanError()

    return float(mean_err)


def setChan(args):
    channel = ''
    for c in args : # Get argument for channel and selection in command line
        if c in Configuration.possibleChannels : channel    = c
    if not channel : assert False, "You have to specify a channel from %s" % Configuration.possibleChannels
    return channel

def getHelCos(ttbar, top_cp, lep_cp): #lep_charge is just the sign that is applied

    top = r.TLorentzVector(top_cp)
    lep = r.TLorentzVector(lep_cp)

    ttbarBoost = - ttbar.BoostVector()
    top.Boost( ttbarBoost )
    lep.Boost( ttbarBoost )

    topBoost = -top.BoostVector() #for negative leptons, boost by antitop (which is -1*top in ttbar restframe, the same down there)
    lep.Boost( topBoost )

    lepvec = lep.Vect()
    topvec = top.Vect()
#    topvec.Print()

    costheta = (lepvec.Dot(topvec))/(lepvec.Mag()*topvec.Mag())

    return costheta

def getTransCos(top1_cp, top2_cp, lep_cp, lep_charge): #lep_charge is just the sign that is applied

    top1 = r.TLorentzVector(top1_cp)
    top2 = r.TLorentzVector(top2_cp)
    lep = r.TLorentzVector(lep_cp)

    ttbar = top1 + top2

    ttbarBoost = - ttbar.BoostVector()
    top1.Boost( ttbarBoost )
    top2.Boost( ttbarBoost )
    lep.Boost( ttbarBoost )

    tBoost = - top1.BoostVector() #top1 has to be the parent top, i.e. the antitop for the negative lepton
    lep.Boost( tBoost )

    top = r.TLorentzVector()

    if( lep_charge == 1.): top = top1
    elif( lep_charge == -1. ): top = top2 #here I really want the top in the ttbar rest frame
    else: print "wrong lepton charge"

    lepvec = lep.Vect()
    topvec = top.Vect()
    beamvec = r.TVector3(0.,0.,1.)
    topvec.SetMag(1.)

    yval = topvec.Dot(beamvec)
    rval = TMath.Sqrt(1 - yval*yval)
    transvec = lep_charge*TMath.Sign(1.,yval)/rval*beamvec.Cross(topvec)
    #transvec.SetMag(1.) #shouldn't be needed anymore

    # CosTheta l wrt t in the ttbar RF
    costheta = (lepvec.Dot(transvec))/(lepvec.Mag())

    return costheta

def getBeamCos(top1_cp, top2_cp, lep_cp, lep_charge):

  top1 = r.TLorentzVector(top1_cp)
  top2 = r.TLorentzVector(top2_cp)
  lep = r.TLorentzVector(lep_cp)

  ttbar = top1 + top2

  ttbarBoost = -ttbar.BoostVector()
  top1.Boost( ttbarBoost )
  top2.Boost( ttbarBoost )
  lep.Boost( ttbarBoost )

  tBoost = - top1.BoostVector()
  lep.Boost( tBoost )

  top = r.TLorentzVector()

  if( lep_charge == 1.): top = top1
  elif( lep_charge == -1. ): top = top2 #here I really want the top in the ttbar rest frame
  else: print "wrong lepton charge"

  lepvec = lep.Vect()
  topvec = top.Vect()
  beamvec = TVector3(0.,0.,1.) #this is already the modified vector, so rather z_hat
  topvec.SetMag(1.)

  y = topvec.Dot(beamvec)

  # CosTheta l wrt proton/gluon(beam axis), lep in top restframe                                                          #here I have to multiply in the end, because for y I need the unmodified beamvector
  costheta = lep_charge*TMath.Sign(1.,y)*(lepvec.Dot(beamvec))/(lepvec.Mag())
  #cout << "ct: " << costheta << endl;
  return costheta


def getRCos(top1_cp, top2_cp, lep_cp, lep_charge):

    top1 = r.TLorentzVector(top1_cp)
    top2 = r.TLorentzVector(top2_cp)
    lep = r.TLorentzVector(lep_cp)

    ttbar = top1+top2

    ttbarBoost = -ttbar.BoostVector()
    top1.Boost( ttbarBoost )
    top2.Boost( ttbarBoost )
    lep.Boost( ttbarBoost )

    tBoost = -top1.BoostVector()
    lep.Boost( tBoost )

    top = r.TLorentzVector()

    if( lep_charge == 1.): top = top1
    elif( lep_charge == -1. ): top = top2 #here I really want the top in the ttbar rest frame for the r axis construction later
    else: print "wrong lepton charge"

    lepvec = lep.Vect()
    topvec = top.Vect()
    beamvec = r.TVector3(0.,0.,1.)
    topvec.SetMag(1.)

    yval = topvec.Dot(beamvec)
    rval = TMath.Sqrt(1 - yval*yval)
    rvec = lep_charge*TMath.Sign(1.,yval)/rval*(beamvec - yval*topvec)
    #transvec = beamvec.Cross(topvec)/r
    #transvec.SetMag(1.) #shouldn't be needed anymore

    # CosTheta l wrt t in the ttbar RF
    costheta = (lepvec.Dot(rvec))/(lepvec.Mag())

    return costheta


#should just do it via the different Cos and not with the different Corr functions
def getHelCorr(ttbar, top_cp, antitop_cp, lep_p_cp, lep_n_cp):
    #well, antitop could be expressed as -top, but I like it better that way

    top = r.TLorentzVector(top_cp)
    antitop = r.TLorentzVector(antitop_cp)
    lep_p = r.TLorentzVector(lep_p_cp)
    lep_n = r.TLorentzVector(lep_n_cp)

    ttbarBoost = - ttbar.BoostVector()
    top.Boost( ttbarBoost )
    antitop.Boost( ttbarBoost )
    lep_p.Boost( ttbarBoost )
    lep_n.Boost( ttbarBoost )

    topBoost = - top.BoostVector()
    lep_p.Boost( topBoost )
    lep_pvec = lep_p.Vect()
    topvec = top.Vect()

    antitopBoost = - antitop.BoostVector()
    lep_n.Boost( antitopBoost )
    lep_nvec = lep_n.Vect()
    antitopvec = antitop.Vect()

    costheta_p = (lep_pvec.Dot(topvec))/(lep_pvec.Mag()*topvec.Mag())
    costheta_n = (lep_nvec.Dot(antitopvec))/(lep_nvec.Mag()*antitopvec.Mag())

    spincorr = costheta_p*costheta_n

    return spincorr


def getCosThetas(var, top, antitop, lep_p, lep_n): #maybe put here the variable as argument?

    ttbar = top + antitop
    if "transpol" in var:
      if "plus" in var:
        obs1 = getTransCos(top, antitop, lep_p, 1.)
        obs2 = getTransCos(antitop, top, lep_n, -1.)
      if "minus" in var:
        obs1 = getTransCos(antitop, top, lep_n, -1.)
        obs2 = getTransCos(top, antitop, lep_p, 1.)
    elif "beampol" in var: #don't use the beam basis anymore
      if "plus" in var:
        obs1 = getBeamCos(top, antitop, lep_p, 1.)
        obs2 = getBeamCos(antitop, top, lep_n, -1.)
      if "minus" in var:
        obs1 = getBeamCos(antitop, top, lep_n, -1.)
        obs2 = getBeamCos(top, antitop, lep_p, 1.)
    elif "rpol" in var:
      if "plus" in var:
        obs1 = getRCos(top, antitop, lep_p, 1.)
        obs2 = getRCos(antitop, top, lep_n, -1.)
      if "minus" in var:
        obs1 = getRCos(antitop, top, lep_n, -1.)
        obs2 = getRCos(top, antitop, lep_p, 1.)
    elif "helpol" in var:
      if "plus" in var:
        obs1 = getHelCos(ttbar, top, lep_p)
        obs2 = getHelCos(ttbar, antitop, lep_n)
      if "minus" in var:
        obs1 = getHelCos(ttbar, antitop, lep_n)
        obs2 = getHelCos(ttbar, top, lep_p)
    elif var == "helcorr":
      obs1 = getHelCos(ttbar, top, lep_p)
      obs2 = getHelCos(ttbar, antitop, lep_n)
    elif var == "transcorr":
      obs1 = getTransCos(top, antitop, lep_p, 1.)
      obs2 = getTransCos(antitop, top, lep_n, -1.)
    elif var == "rcorr":
      obs1 = getRCos(top, antitop, lep_p, 1.)
      obs2 = getRCos(antitop, top, lep_n, -1.)
    elif "transhel" in var:
      costransplus = getTransCos(top, antitop, lep_p, 1.)
      coshelplus = getHelCos(ttbar, top, lep_p)
      costransminus = getTransCos(antitop, top, lep_n, -1.)
      coshelminus = getHelCos(ttbar, antitop, lep_n)
      obs1 = costransplus*coshelminus
      obs2 = coshelplus*costransminus
    elif "transr" in var:
      costransplus = getTransCos(top, antitop, lep_p, 1.)
      cosrplus = getRCos(top, antitop, lep_p, 1.)
      costransminus = getTransCos(antitop, top, lep_n, -1.)
      cosrminus = getRCos(antitop, top, lep_n, -1.)
      obs1 = costransplus*cosrminus
      obs2 = cosrplus*costransminus
    elif "rhel" in var:
      cosrplus = getRCos(top, antitop, lep_p, 1.)
      coshelplus = getHelCos(ttbar, top, lep_p)
      cosrminus = getRCos(antitop, top, lep_n, -1.)
      coshelminus = getHelCos(ttbar, antitop, lep_n)
      obs1 = cosrplus*coshelminus
      obs2 = coshelplus*cosrminus
    else:
      obs1 = 0.
      obs2 = 0.
    return obs1,obs2
