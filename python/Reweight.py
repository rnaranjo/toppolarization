import ROOT as r
from math import fabs
import Config
import re

class ReweightProtos(object):

    def __init__(self,asymmetry):
        super(ReweightProtos,self).__init__()

        outDir = Config.Configuration.workDir

        if 'protosA0' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratio_A0.root')
        if 'protosA2' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratio_A2.root')
        elif 'protosA4' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratio_A4.root') 
        elif 'protosA6' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratio_A6.root')

        self.histo = self.file.Get('hdeltaabsy_truth_beforesel_resobin')

        if 'neg' in asymmetry:
            firstbin = self.histo.GetXaxis().GetFirst()
            lastbin = self.histo.GetXaxis().GetLast()
            histo_tmp = self.histo.Clone()
            for ii in xrange(firstbin,lastbin):
                self.histo.SetBinContent(ii,histo_tmp.GetBinContent(lastbin-ii+1))

    def reweight(self,truthdy):

        if fabs(truthdy)>3.0: return 1.0
        return self.histo.GetBinContent(self.histo.FindBin(truthdy))

class ReweightProtosFit(object):

    def __init__(self,asymmetry):
        super(ReweightProtosFit,self).__init__()

        outDir = Config.Configuration.workDir

        if 'fitprotosA0' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratiofit_A0.root')
        if 'fitprotosA2' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratiofit_A2.root')
        elif 'fitprotosA4' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratiofit_A4.root')
        elif 'fitprotosA6' in asymmetry:
            self.file = r.TFile(outDir+'/data/protos_ratiofit_A6.root')

        self.histo = self.file.Get('hdeltaabsy_truth_beforesel_resobin')

        if 'neg' in asymmetry:
            firstbin = self.histo.GetXaxis().GetFirst()
            lastbin = self.histo.GetXaxis().GetLast()
            histo_tmp = self.histo.Clone()
            for ii in xrange(firstbin,lastbin):
                self.histo.SetBinContent(ii,histo_tmp.GetBinContent(lastbin-ii+1))

    def reweight(self,truthdy):

        if fabs(truthdy)>3.0: return 1.0
        return self.histo.GetBinContent(self.histo.FindBin(truthdy))

class ReweightLin(object):

    def __init__(self,asymmetry):
        super(ReweightLin,self).__init__()

        outDir = Config.Configuration.workDir

        if 'linear0' in asymmetry:
            self.file = r.TFile(outDir+'/data/linear_ratio_slope0.root')
        elif 'linear2' in asymmetry:
            self.file = r.TFile(outDir+'/data/linear_ratio_slope2.root')
        elif 'linear4' in asymmetry:
            self.file = r.TFile(outDir+'/data/linear_ratio_slope4.root')
        elif 'linear6' in asymmetry:
            self.file = r.TFile(outDir+'/data/linear_ratio_slope6.root')

        self.histo = self.file.Get('h_ratio')

        if 'neg' in asymmetry:
            firstbin = self.histo.GetXaxis().GetFirst()
            lastbin = self.histo.GetXaxis().GetLast()
            histo_tmp = self.histo.Clone()
            for ii in xrange(firstbin,lastbin):
                self.histo.SetBinContent(ii,histo_tmp.GetBinContent(lastbin-ii+1))

    def reweight(self,truthdy):

        if fabs(truthdy)>3.0: return 1.0
        ibin = self.histo.FindBin(truthdy)
        if ( ibin==0 | ibin==self.histo.GetNbinsX()+1 ): return -1.
        return self.histo.GetBinContent(ibin)         

class ReweightObs(object):

    def __init__(self,reweight,var,syst): 
        super(ReweightObs,self).__init__()

        #var to distinguish between polarization and spin correlation, probably don't need it... could be part of reweight, but I don't always want to check fo both of them... so kann ich es spaeter vielleicht auch flexibler machen wegen mancher zahlen, die benutzt werden muessen
        #reweight is like the asymmetry from the other reweighting classes
        #don't use histograms, but calculated weights for polarization and spin correlation
        
        #outDir = Config.Configuration.workDir

        #if 'linear0' in asymmetry:
        #    self.file = r.TFile(outDir+'/data/linear_ratio_slope0.root')
        #elif 'linear2' in asymmetry:
        #    self.file = r.TFile(outDir+'/data/linear_ratio_slope2.root')
        #elif 'linear4' in asymmetry:
        #    self.file = r.TFile(outDir+'/data/linear_ratio_slope4.root')
        #elif 'linear6' in asymmetry:
        #    self.file = r.TFile(outDir+'/data/linear_ratio_slope6.root')

        #self.histo = self.file.Get('h_ratio')
        #
        #if 'neg' in asymmetry:
        #    firstbin = self.histo.GetXaxis().GetFirst()
        #    lastbin = self.histo.GetXaxis().GetLast()
        #    histo_tmp = self.histo.Clone()
        #    for ii in xrange(firstbin,lastbin):
        #        self.histo.SetBinContent(ii,histo_tmp.GetBinContent(lastbin-ii+1))

        #variable bringt mir doch nichts, weil ich bei der reweight funktion wissen muss, ob ich nur cos*cos oder cos+ und cos- angebe... auch wenn ich natuerlich immer noch beide angeben kann fuer correlation, ists doch etwas bloed, finde ich... 
        #need to extract the reiweighting factor from the 'reweight' string
        
        self.var = var
        self.syst = syst
        #do I always have to put self....?
        self.gen_pol_p=0.
        self.gen_pol_n=0.
        self.gen_C=0.
        self.theo_C=0.
        self.theo_pol=0.

        #have to change those numbers for all different bases
        if( 'hel' in var):
#            self.gen_pol_p = 0.00275
#            self.gen_pol_n = 0.00230
#            self.gen_C = 0.2854
#            self.gen_pol_p = -0.00243
#            self.gen_pol_n = -0.00549
#            self.gen_C = 0.2860
#            self.gen_pol_p = 0.00567621
#            self.gen_pol_n = 0.0049748
#            self.gen_C = 0.288336
            self.gen_pol_p = 0.00607144
            self.gen_pol_n = 0.00677371
            self.gen_C = 0.292969
            #rather a function like: SetGenValues(var,syst) ?
            if syst == 'mcatnlo':
#                self.gen_pol_p = -0.000851966
#                self.gen_pol_n = 8.85942e-05
#                self.gen_C = 0.324606
                #
                self.gen_pol_p = -0.00175865
                self.gen_pol_n = -0.000282572
                self.gen_C = 0.315595
            elif syst == 'perugia_nom':
                self.gen_pol_p = 0.00572374
                self.gen_pol_n = 0.00639212
                self.gen_C = 0.287463
            elif syst == 'UEmpihi':
                self.gen_pol_p = 0.00567069
                self.gen_pol_n = 0.00671177
                self.gen_C = 0.289429
            elif syst == 'CRlow':
                self.gen_pol_p = 0.00597296
                self.gen_pol_n = 0.00606074
                self.gen_C = 0.288247
            elif syst == 'radLo':
                self.gen_pol_p = 0.00257752
                self.gen_pol_n = 0.00260681
                self.gen_C = 0.217662
            elif syst == 'radHi':
                self.gen_pol_p = 0.00146565
                self.gen_pol_n = 0.00291512
                self.gen_C = 0.211111
            elif syst == 'radLo_pow':
                self.gen_pol_p = 0.00472418
                self.gen_pol_n = 0.00583948
                self.gen_C = 0.284868
            elif syst == 'radHi_pow':
                self.gen_pol_p = 0.00692659
                self.gen_pol_n = 0.00559206
                self.gen_C = 0.298383
            elif syst == 'pow_her':
                self.gen_pol_p = 0.00461288
                self.gen_pol_n = 0.00489582
                self.gen_C = 0.28973
            elif syst == 'pow_fast':
                self.gen_pol_p = 0.00551163
                self.gen_pol_n = 0.00602326
                self.gen_C = 0.286724
            elif syst == 'topmass_167.5':
                self.gen_pol_p = 0.00804864
                self.gen_pol_n = 0.00607788
                self.gen_C = 0.293028
            elif syst == 'topmass_170':
                self.gen_pol_p = 0.000751728
                self.gen_pol_n = 0.00542765
                self.gen_C = 0.289768
            elif syst == 'topmass_175':
                self.gen_pol_p = 0.00895966
                self.gen_pol_n = 0.0072507
                self.gen_C = 0.288771
            elif syst == 'topmass_177.5':
                self.gen_pol_p = 0.00548907
                self.gen_pol_n = 0.00861882
                self.gen_C = 0.283253

            self.theo_C = 0.318
#            self.theo_pol = 0.0035 #rather 0.003?
            self.theo_pol = 0.0 

        elif( 'trans' in var):
#            self.gen_pol_p = 0.00403
#            self.gen_pol_n = -0.00101
#            self.gen_C = 0.3186
#            self.gen_pol_p = 0.00126
#            self.gen_pol_n = -0.00070
#            self.gen_C = 0.3173
#            self.gen_pol_p = 0.000727138
#            self.gen_pol_n = -0.000827515
#            self.gen_C = 0.31856
            self.gen_pol_p = 0.000175421
            self.gen_pol_n = -0.000187251
            self.gen_C = 0.319297
            if syst == 'mcatnlo':
#                self.gen_pol_p = -0.000681332
#                self.gen_pol_n = -0.000465201
#                self.gen_C = 0.350019
                self.gen_pol_p = -0.00163813
                self.gen_pol_n = 0.000565961
                self.gen_C = 0.33112
            elif syst == 'perugia_nom':
                self.gen_pol_p = -0.000852686
                self.gen_pol_n = -0.00119711
                self.gen_C = 0.324
            elif syst == 'UEmpihi':
                self.gen_pol_p = -0.00153911
                self.gen_pol_n = -0.00158134
                self.gen_C = 0.321937
            elif syst == 'CRlow':
                self.gen_pol_p = -0.000875777
                self.gen_pol_n = -0.000347426
                self.gen_C = 0.322005
            elif syst == 'radLo':
                self.gen_pol_p = 1.15316e-05
                self.gen_pol_n = -0.00069716
                self.gen_C = 0.312751
            elif syst == 'radHi':
                self.gen_pol_p = 0.000395629
                self.gen_pol_n = -0.00111583
                self.gen_C = 0.311369
            elif syst == 'radLo_pow':
                self.gen_pol_p = 0.000606611
                self.gen_pol_n = -0.000589321
                self.gen_C = 0.320009
            elif syst == 'radHi_pow':
                self.gen_pol_p = -0.000533339
                self.gen_pol_n = 0.00158983
                self.gen_C = 0.326213
            elif syst == 'pow_her':
                self.gen_pol_p = -0.00183452
                self.gen_pol_n = 0.000492289
                self.gen_C = 0.318982
            elif syst == 'pow_fast':
                self.gen_pol_p = -0.000461822
                self.gen_pol_n = 0.00037363
                self.gen_C = 0.318594
            elif syst == 'topmass_167.5':
                self.gen_pol_p = 0.00126388
                self.gen_pol_n = -0.00194126
                self.gen_C = 0.316407
            elif syst == 'topmass_170':
                self.gen_pol_p = -0.00311682
                self.gen_pol_n = -0.000964334
                self.gen_C = 0.325482
            elif syst == 'topmass_175':
                self.gen_pol_p = 0.00115216
                self.gen_pol_n = 0.000628082
                self.gen_C = 0.322262
            elif syst == 'topmass_177.5':
                self.gen_pol_p = -0.000119483
                self.gen_pol_n = 0.0025715
                self.gen_C = 0.319714

            self.theo_C = 0.332
#            self.theo_pol = 0.0032
            self.theo_pol = 0.0

        elif( 'rpol' in var or 'rcorr' in var):
#            self.gen_pol_p = 0.00437
#            self.gen_pol_n = 0.00445
#            self.gen_C = 0.0262
#            self.gen_pol_p = 0.00645
#            self.gen_pol_n = 0.00676
#            self.gen_C = 0.02670
#            self.gen_pol_p = 0.00206305
#            self.gen_pol_n = 0.0023055
#            self.gen_C = 0.0269893
            self.gen_pol_p = 0.0018533
            self.gen_pol_n = 0.00278293
            self.gen_C = 0.0255247
            if syst == 'mcatnlo':
#                self.gen_pol_p = 0.000733899
#                self.gen_pol_n = 0.000226179
#                self.gen_C = 0.0443945
                self.gen_pol_p = 0.000488981
                self.gen_pol_n = -0.000151554
                self.gen_C = 0.0141193
            elif syst == 'perugia_nom':
                self.gen_pol_p = 0.00393424
                self.gen_pol_n = 0.00218869
                self.gen_C = 0.0291952
            elif syst == 'UEmpihi':
                self.gen_pol_p = 0.00312179
                self.gen_pol_n = 0.00184787
                self.gen_C = 0.025952
            elif syst == 'CRlow':
                self.gen_pol_p = 0.00305287
                self.gen_pol_n = 0.00355775
                self.gen_C = 0.0259623
            elif syst == 'radLo':
                self.gen_pol_p = -0.0119764
                self.gen_pol_n = -0.0121475
                self.gen_C = -0.0303484
            elif syst == 'radHi':
                self.gen_pol_p = -0.0141833
                self.gen_pol_n = -0.0137885
                self.gen_C = -0.0268873
            elif syst == 'radLo_pow':
                self.gen_pol_p = 0.00143137
                self.gen_pol_n = 0.0020167
                self.gen_C = 0.0170337
            elif syst == 'radHi_pow':
                self.gen_pol_p = 0.00177503
                self.gen_pol_n = 0.00278436
                self.gen_C = 0.037688
            elif syst == 'pow_her':
                self.gen_pol_p = 0.00217755
                self.gen_pol_n = 0.00269918
                self.gen_C = 0.0266529
            elif syst == 'pow_fast':
                self.gen_pol_p = 0.0022801
                self.gen_pol_n = 0.00227688
                self.gen_C = 0.027528
            elif syst == 'topmass_167.5':
                self.gen_pol_p = 0.0028931
                self.gen_pol_n = 0.00463016
                self.gen_C = 0.0296045
            elif syst == 'topmass_170':
                self.gen_pol_p = -0.000272074
                self.gen_pol_n = 0.00157733
                self.gen_C = 0.0287662
            elif syst == 'topmass_175':
                self.gen_pol_p = 0.00151132
                self.gen_pol_n = 0.00260719
                self.gen_C = 0.0241704
            elif syst == 'topmass_177.5':
                self.gen_pol_p = 0.00513243
                self.gen_pol_n = 0.000871348
                self.gen_C = 0.0218836

            self.theo_C = 0.055
#            self.theo_pol = 0.0014
            self.theo_pol = 0.0

            #for the crosscorrelations I have to use something different... or maybe really check for every name

        #reweight_factor=0.;
        #reweight factor is the polarization or spin correlation you want to test for the calibration curve reweighting
        self.reweight_factor = float((re.search(r'\d+',reweight)).group(0))
        print self.reweight_factor
        print 'ralph reweight before neg mult: ',reweight
        if 'neg' in reweight:
            self.reweight_factor*=-1.
        self.reweight_factor=self.reweight_factor/100. #for polarization of 6% -> 0.06, I put obspos6, the same for spin correlation
        print 'reweight factor final',self.reweight_factor        

    def reweight(self,truth1,truth2=-99.): #in the beginning just used cos cos for spin correlation
        if 'pol' in self.var:
            return self.reweight_pol(truth1,truth2)
        elif 'corr' in self.var:
            return self.reweight_corr(truth1,truth2)
        elif 'sum' in self.var or 'diff' in self.var:
            return self.reweight_crosscorr(truth1,truth2)
        else: print 'wrong observable name'

    
    def reweight_pol(self,truth_p,truth_n):
        #RALPH NOTE at some point have to change one of the gen_C to the theoretical one
#        if self.reweight_factor == 0. and (self.theo_C == 0.318 or self.theo_C == 0.332 or self.theo_C == 0.055): #so really just the base spin correlation and not the modified one for the calibration curve reweighted one #which is already shit for the modelling systs...
#            return 1.
#        elif self.reweight_factor == 0.  and (self.theo_C != 0.318 and self.theo_C != 0.332 and self.theo_C != 0.055):
#            if 'plus' in self.var: self.reweight_factor == self.gen_pol_p
#            elif 'minus' in self.var: self.reweight_factor == self.gen_pol_n

   # RALPH NOTE: has to go back in, just want to test something for the fiducial checks
   #     if self.reweight_factor == 0.: #just want to correct for the true correlation, but not the polarization itself
   #         self.reweight_factor = (self.gen_pol_p + self.gen_pol_n)/2.  #for the calibration curves, partonic and resmat I take the merged distribution, so that's fine


#            if 'plus' in self.var: self.reweight_factor == self.gen_pol_p
#            elif 'minus' in self.var: self.reweight_factor == self.gen_pol_n
        
#        print 'reweight pol',self.reweight_factor,self.theo_C

        weight = (1+ self.reweight_factor*truth_p + self.reweight_factor*truth_n - self.theo_C*truth_p*truth_n)/(1 + self.gen_pol_p*truth_p + self.gen_pol_n*truth_n - self.gen_C*truth_p*truth_n)
        #print 'pol weight',weight
        return float(weight)

    def reweight_corr(self,truth_p,truth_n):
#        if self.reweight_factor == 0. and self.theo_pol == 0.:
#            return 1.
#        elif self.reweight_factor == 0 and self.theo_pol !=0:
#            self.reweight_factor == self.gen_C  #just want to change the polarization for the pol systematics
        #RALPH NOTE at some point have to change one of the gen_C to the theoretical one
        if self.reweight_factor == 0.:
            self.reweight_factor = self.gen_C  #just want to change to the true polarization, not the spin correlation itself

#        print 'reweight corr',self.reweight_factor,self.theo_pol

#        weight = (1 - self.reweight_factor*truth_p*truth_n)/(1 - self.gen_C*truth_p*truth_n) 
        weight = (1 +self.theo_pol*truth_p + self.theo_pol*truth_n - self.reweight_factor*truth_p*truth_n)/(1 + self.gen_pol_p*truth_p + self.gen_pol_n*truth_n - self.gen_C*truth_p*truth_n) 

        return float(weight)
    def reweight_crosscorr(self,truth1,truth2):
        if self.reweight_factor == 0.:
            return 1.        
        val=0.
        #could have just given the sum or difference, but initially wanted to reweight the single distributions
        if 'sum' in self.var: val = truth1+truth2
        elif 'diff' in self.var: val = truth1-truth2

        f_rew = r.TF1("f_rew","1+"+str(self.reweight_factor)+"*5/9*x",-1.,1.); #that's only for a flat line, that the mean shifts with 1/3 of the slope, not for polynomials in general... for a pol2 it's 1/5, so I take that one since it has a large pol2 structure; /9 then because of the correlation value that is calculated by -9*<coscos +- coscos>
#        f_old = r.TF1("f_old","1+"+str(old_mean)+"*x",-1.,1.);

        weight = f_rew.Eval(val)

        return float(weight)
