#!/bin/env python
import os
import json
import sys
from ROOT import *
import utils
from numpy import std, mean, array
from Config import Configuration
import numpy as np
import matplotlib.pyplot as plt
import Rebin
from progressbar import *
Debug        = 10
mybins       = None
mybins_truth = None
bound        = None
bound_truth  = None
Ndiff        = None



gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')



if __name__ == '__main__':


    resultDir = Configuration.outputDirt

    var                 = utils.setVar(sys.argv)

    fiducial            = utils.setFiducial(sys.argv)

    systematics         = Configuration.possibleSystN
    asymSyst            = Configuration.possibleAsymmetricSyst
    symSyst             = Configuration.possibleSymmetricSyst
    
    from binningOptimization import binnings_dict

    binning 		= '100Bin'#'%(bin)3'
    sigmas              = ['psigma','msigma']


    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bn


    if fiducial : resultDir = Configuration.outputDir


    variationFile = resultDir+'/%(var)s/nominal/ALL/%(var)s_pseudo_marginalization.json'%{'var':var,'var':var}
  
    variationdict = json.load(open(variationFile))
    signalvariationdict = variationdict
    bkgvariationdict = variationdict

    recoFile = resultDir+'/%(var)s/nominal/ALL/optimization/%(var)s_reco_btag_%(bin)s.json'%{'var':var,'var':var,'bin':binning}
    bckgFile = resultDir+'/%(var)s/nominal/ALL/optimization/%(var)s_bckg_btag_%(bin)s.json'%{'var':var,'var':var,'bin':binning}

    reco  = json.load(open(recoFile))
    bckg  = json.load(open(bckgFile))

    systlist = asymSyst+symSyst



    for syst in systlist:

      try:  
       
          variation     = signalvariationdict[syst]
          bckgvariation = bkgvariationdict[syst]
      except:
          print "error in ",syst
          continue

      #Let's vary the signal 

      reco_p,reco_m = [],[]

      print variation[1]

      for i in xrange(len(reco)):

         reco_p.append(reco[i] + (1-variation[1])*reco[i])
         reco_m.append(reco[i] - (1-variation[1])*reco[i])
      
      directory = resultDir+'/%(var)s/nominal/ALL/syst/%(syst)s'%{'var':var,'syst':syst}

      if not os.path.exists(directory):
                 os.makedirs(directory)

      namep = '%(var)s_psigma_btag_%(bin)s.json'%{'var':var,'bin':binning}  
      namem = '%(var)s_msigma_btag_%(bin)s.json'%{'var':var,'bin':binning}      
     
      with open(directory+'/'+namep,'w') as outfile:
        json.dump(reco_p,outfile)
      with open(directory+'/'+namem,'w') as outfile:
        json.dump(reco_m,outfile)


      #Let's vary the background
      newbckg_p,newbckg_m = {},{}
      for key, values in bckg.iteritems():
        valuesp,valuesm = [],[] 
        for i in xrange(len(values)):
           if values[i] < 0. or bckg[key][i]<0.: print key,bckg[key][i], values[i], "EPPAAAAA"
           valuesp.append(bckg[key][i]+(1-bckgvariation[1])*bckg[key][i])
           valuesm.append(bckg[key][i]-(1-bckgvariation[1])*bckg[key][i])
         
        newbckg_p[key] = valuesp
        newbckg_m[key] = valuesm

      namepbckg = '%(var)s_bckg_psigma_btag_%(bin)s.json'%{'var':var,'bin':binning}
      namembckg = '%(var)s_bckg_msigma_btag_%(bin)s.json'%{'var':var,'bin':binning}

      with open(directory+'/'+namepbckg,'w') as outfile:
        json.dump(newbckg_p,outfile)
      with open(directory+'/'+namembckg,'w') as outfile:
        json.dump(newbckg_m,outfile)
