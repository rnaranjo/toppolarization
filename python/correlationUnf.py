##system modules
import sys
import json
import gc
import os
from   numpy import mean,std,fabs
from array import array
#our modules
from Config import Configuration
import utils
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.mlab as mlab
import numpy as np


from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

obs_label = {}

#obs_label["helpol_plus"]=r"$cos\theta_{+}^{hel}$"
#obs_label["helpol_minus"]=r"$cos\theta_{-}^{hel}$"
#obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
#obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
#obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
#obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
#obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
#obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
#obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
#obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
#obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
#obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
#obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
#obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
#obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"



obs_label["helpol_plus"]=r"$B_{+}^{k}$"
obs_label["helpol_minus"]=r"$B_{-}^{k}$"
obs_label["transpol_plus"]=r"$B_{+}^{n}$"
obs_label["transpol_minus"]=r"$B_{-}^{n}$"
obs_label["rpol_plus"]=r"$B_{+}^{r}$"
obs_label["rpol_minus"]=r"$B_{-}^{r}$"
obs_label["helcorr"]=r"$C(k,k)$"
obs_label["transcorr"]=r"$C(n,n)$"
obs_label["rcorr"]=r"$C(r,r)$"
obs_label["transhelsum"]=r"$C(n,k)+C(k,n)$"
obs_label["transheldiff"]=r"$C(n,k)-C(k,n)$"
obs_label["transrsum"]=r"$C(n,r)+C(r,n)$"
obs_label["transrdiff"]=r"$C(n,r)-C(r,n)$"
obs_label["rhelsum"]=r"$C(r,k)+C(k,r)$"
obs_label["rheldiff"]=r"$C(r,k)-C(k,r)$"




def plotScatter(x,y, folder="plots/"):

#     print x
#     print y

     correlation = np.corrcoef(x,y)
     correlation = correlation[0][1]

     fig, axScatter = plt.subplots(figsize=(6.5,6.5))

     # the scatter plot:
     axScatter.scatter(x, y)
     axScatter.set_aspect(1.)

     divider = make_axes_locatable(axScatter)
     axHistx = divider.append_axes("top", 1.2, pad=0.2, sharex=axScatter)
     axHisty = divider.append_axes("right", 1.2, pad=0.2, sharey=axScatter)
     axScatter.text(0.95, 0.01, 'Correlation Factor: 0.285',
        verticalalignment='bottom', horizontalalignment='right',
        transform=axScatter.transAxes,
        color='green', fontsize=15)

     # make some labels invisible
     plt.setp(axHistx.get_xticklabels() + axHisty.get_yticklabels(),
              visible=False)
     binwidth = 0.001
     xymax = np.max( [np.max(np.fabs(x)), np.max(np.fabs(y))] )
     lim = ( int(xymax/binwidth) + 1) * binwidth
     bins = 10 #np.arange(lim-binwidth*10, lim + binwidth, binwidth)
     axHistx.hist(x, bins=bins)
     axHisty.hist(y, bins=bins, orientation='horizontal')
     axScatter.set_xlabel(r'$A_{C}^{\ell \ell}$')
     axScatter.set_ylabel(r'$A_{C}^{t\bar{t}}$')
     #axHistx.axis["bottom"].major_ticklabels.set_visible(False)
     for tl in axHistx.get_xticklabels():
         tl.set_visible(False)
     #axHisty.axis["left"].major_ticklabels.set_visible(False)
     for tl in axHisty.get_yticklabels():
         tl.set_visible(False)
     #axHisty.set_xticks([0, 50, 100])a
     axHisty.set_xticks([10, 50, 100])
     axScatter.set_xticks([-0.005, 0.005, 0.015])
     plt.tight_layout()
     plt.draw()
     #name = "scatter_bin%d_bin%d.eps"%(i,j)
     #plt.savefig(folder+name, format='eps')

     plt.show()

if __name__ == '__main__':


  outDir  = Configuration.outputDirBt
  nominal = Configuration.outputDirt
  gROOT.SetBatch(True)
  
  variables = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
               'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
               'transheldiff']

  from binningOptimization import binnings_dict


  outDir  = Configuration.outputDirBt
  nominal = Configuration.outputDirt

  observations = []

  mcgenerator = [] 
  isrfsr       = []
  color        = []
  undere       = []
  shower       = []


  for var in variables:
    observations.append([])
    mcgenerator .append([])
    isrfsr      .append([])
    color       .append([])
    undere      .append([]) 
    shower      .append([])

 
  

  for r in xrange(1000):

      print "Processing Nominal:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])
      try:

        for i,var in enumerate(variables):
            

            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc

            unfoldedFile = outDir+'/%(var)s/nominal/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_nominal_%(var)s_%(binName)s_rpZero.json' % {'var':var,'replica':r,'binName':binName}
            unfolded     = json.load(open(unfoldedFile))
            print var

            
            tmp[i].append(unfolded[0])



      except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        print e
        error = True
      except ValueError:
         print "Could not convert data to an integer."
         error = True
      except:
         print "Unexpected error:", sys.exc_info()[0]
         raise 
         print "Error"
         error = True

      if not error: 
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])): 
              observations[i].append(tmp[i][j])

        



  for r in xrange(1000):

      print "Processing MC Generator:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])

      try:

         for i,var in enumerate(variables):


            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc

            mcatnlo = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'mcatnlo','replica':r,'binName':binName}))
            herwig = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'herwig','replica':r,'binName':binName}))



            tmp[i].append(mcatnlo[0]-herwig[0])



      except:
        print "Error"
        error = True

      if not error:
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])):
              mcgenerator[i].append(tmp[i][j])

  
  for r in xrange(1000):

      print "Processing ISRFSR:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])

      try:

         for i,var in enumerate(variables):


            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc

            radhi = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'radhi','replica':r,'binName':binName}))
            radlo = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'radlo','replica':r,'binName':binName}))

            tmp[i].append((radhi[0]-radlo[0])/2.)


      except:
        print "Error"
        error = True

      if not error:
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])):
              isrfsr[i].append(tmp[i][j])


  for r in xrange(1000):

      print "Processing Color:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])

      try:

         for i,var in enumerate(variables):


            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc


            perugia = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'perugia','replica':r,'binName':binName}))
            perugialocr = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'perugialocr','replica':r,'binName':binName}))


            tmp[i]       .append(perugia[0]-perugialocr[0])



      except:
        print "Error"
        error = True

      if not error:
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])):
              color[i].append(tmp[i][j])





     
  for r in xrange(1000):

      print "Processing Underlying:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])

      try:

         for i,var in enumerate(variables):


            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc


            perugia = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'perugia','replica':r,'binName':binName}))
            perugiampihi = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'perugiampihi','replica':r,'binName':binName}))

            tmp[i]      .append(perugia[0]-perugiampihi[0])


      except:
        print "Error"
        error = True

      if not error:
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])):
              undere[i].append(tmp[i][j])



  for r in xrange(1000):

      print "Processing Shower:",r

      error = False
      tmp = []
      for i in xrange(len(variables)):  tmp.append([])

      try:

         for i,var in enumerate(variables):


            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc


            herwig = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'herwig','replica':r,'binName':binName}))
            fastpythia = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(replica)d/Unfolded_obspos0_%(syst)s_%(var)s_%(binName)s_rpZero.json'%{'var':var,'syst':'fastpythia','replica':r,'binName':binName}))

            tmp[i]      .append(fastpythia[0]-herwig[0])



      except:
        print "Error"
        error = True

      if not error:
          for i in xrange(len(tmp)):
            for j in xrange(len(tmp[i])):
              shower[i].append(tmp[i][j])



  print len(observations),len(observations[0]),len(observations[1])       
  print np.corrcoef(observations).tolist()
  c1 = np.cov(observations)
  c2 = np.cov(mcgenerator)
  c3 = np.cov(isrfsr)
  c4 = np.cov(color)
  c5 = np.cov(undere)
  c6 = np.cov(shower) 

  corrN = np.corrcoef(observations)

  print "Sanity check:"

  totalCovariance = np.array(c1) + np.array(c2) + np.array(c3) + np.array(c4) +np.array(c5) +np.array(c6)

  totalCorrelation = []

  for i in xrange(len(totalCovariance)):
    totalCorrelation.append([])
    for j in xrange(len(totalCovariance[i])):
      #print totalCovariance[i][j]/(np.sqrt(totalCovariance[i][i]*totalCovariance[j][j]))
      totalCorrelation[i].append(totalCovariance[i][j]/(np.sqrt(totalCovariance[i][i]*totalCovariance[j][j])))



  
  print (totalCorrelation)




  #fig = plt.figure(figsize=(10,10))
  fig, ax = plt.subplots()
  matrix  = ax.matshow(totalCorrelation,cmap='seismic_r',vmin=-1,vmax=1)
  fig.colorbar(matrix,ticks=[-1.0,-0.8,-0.6,-0.4,-0.2, 0, 0.2, 0.4, 0.6, 0.8, 1.0])
  plt.xticks(range(len(variables)),[obs_label[var] for var in variables],rotation='vertical')
  plt.yticks(range(len(variables)),[obs_label[var] for var in variables])
  plt.tight_layout()
  plt.show()



