from math import fabs
import ROOT as r
import re
from array import array
from numpy import zeros
import Config
from Config   import Configuration
from Reweight import ReweightProtosFit
from Reweight import ReweightProtos
from Reweight import ReweightLin
from Reweight import ReweightObs
import utils
import json
from ROOT import gROOT
GEV=1000

class Histogrammer(object):

  def __init__(self,tree,var,channel,selection,binning,filename,systname,lumiweight=True, fiducial = False,bootstrap = False,histFile=None,pdf = False, pdfType = "CT10nlo"):
    super(Histogrammer,self).__init__()

    self.inclbins    = binning
    self.fname     = filename
    self.fiducial  = fiducial
    self.bootstrap = bootstrap
    self.pdf       = pdf
    self.pdftype   = pdfType
    self.systname  = systname

    self.pdfdict   = {"CT10nlo":53, "MSTW2008nlo68cl":41,"NNPDF23_nlo_as_0119":101}

    if pdf: self.pdfset    = r.PdfSet(self.pdftype,"",0)

    self.nreplicas = 2000
    self.zsf = json.load(open("/nfs/dust/atlas/user/naranjo/Fbu/framework/zjetsSF.json"))

    self.zlight =  [147105, 147106, 147107, 147108, 147109, 147110, 147113, 147114, 147115, 147116,147117, 147118, 178354, 178355, 178356, 178357, 178358, 178359, 178360, 178361, 178362, 178363, 178369, 178369, 178370, 178371, 178372, 178373, 178374, 178375, 178376, 178377, 178378, 178379, 178380, 178381, 178382, 178383]
    self.zhf =      [200332, 200333, 200334, 200335, 200432, 200433, 200434, 200435, 200340, 200341, 200342, 200343, 200440, 200441, 200442, 200443, 200348, 200349, 200350, 200351, 200448, 200449, 200450, 200451,178384,178385,178386,178387,178388,178389,178390,178391,178391,178392,178393,178394,178395,178396,178397,178398,178399,178400,178401,178402,178403,178404,178405,178406,178407]


    self.ispecial = Config.getModSyst()

    self.spntuple = False

    if self.fname in self.ispecial:
      self.spntuple = True


    if ('ALL' in channel) or ('DIL' in channel):
      self.ch = 0
    if 'EE' in channel:
      self.ch = 1
    if 'MUMU' in channel:
      self.ch = 2
    if 'EMMU' in channel:
      self.ch = 3

    self.var = var
    self.selection = selection
    self.reweighter = None
    self.lumiweight = lumiweight

    print "selection",self.selection

    if 'linear' in selection:
        m2 = re.search('(linear)(\\d+)([a-z]+)',selection)
        self.reweighter = ReweightLin(m2.group(0))
    if 'protos' in selection:
        m2 = re.search('(protosA)(\\d+)([a-z]+)',selection)
        self.reweighter = ReweightProtos(m2.group(0))
    if 'fitprotos' in selection:
        m2 = re.search('(fitprotosA)(\\d+)([a-z]+)',selection)
        self.reweighter = ReweightProtosFit(m2.group(0))
    if 'obs' in selection:
        m2 = re.search('(obs)([a-z]+)(\\d+)',selection)
        self.reweighter = ReweightObs(m2.group(0),self.var,self.systname)

    if systname in Configuration.possibleTheoSyst and self.reweighter:
        if systname == 'pol_up':
          if self.var == 'helcorr': self.reweighter.theo_pol = 0.006
          elif self.var == 'transcorr': self.reweighter.theo_pol = 0.006
          elif self.var == 'rcorr': self.reweighter.theo_pol = 0.006
        elif systname == 'pol_down':
          if self.var == 'helcorr': self.reweighter.theo_pol = -0.006
          elif self.var == 'transcorr': self.reweighter.theo_pol = -0.006
          elif self.var == 'rcorr': self.reweighter.theo_pol = -0.006
        elif systname == 'corr_up':
          if 'helpol' in self.var: self.reweighter.theo_C = 0.42
          elif 'transpol' in self.var: self.reweighter.theo_C = 0.43
          elif 'rpol' in self.var: self.reweighter.theo_C = 0.15
        elif systname == 'corr_down':
          if 'helpol' in self.var: self.reweighter.theo_C = 0.22
          elif 'transpol' in self.var: self.reweighter.theo_C = 0.23
          elif 'rpol' in self.var: self.reweighter.theo_C = -0.05

    #histograms
    self.map_incl    = None

    #response matrix
    self.resmat_incl    = None

    self.loop(tree)


  def loop(self, tree):
    if self.pdf: self.book_histo_pdf()
    else: self.book_histo()
    print tree, self.selection
    if self.pdf and not 'partonicTtbar' in self.selection and not 'fiducial' in self.selection and not 'ttbar' in self.selection and not 'linear' in self.selection and not 'protos' in self.selection and not 'fitprotos' in self.selection and not 'obs' in self.selection:
      print "we Get out"
      return

    # -- Get partonic informations -- #
    if 'partonicTtbar' in self.selection:
      entriesTruth = tree.GetEntries()
      #entriesTruth = 10000


      true_top = r.TLorentzVector()
      true_tbar = r.TLorentzVector()
      true_lep_p = r.TLorentzVector()
      true_lep_n = r.TLorentzVector()

      for jentry in xrange(entriesTruth):

        tree.GetEntry(jentry)

        if self.fiducial:


            if (fabs(tree.par_lep_type[0])!=11 and fabs(tree.par_lep_type[0])!=13): continue
            if (fabs(tree.par_lep_type[1])!=11 and fabs(tree.par_lep_type[1])!=13): continue
            if tree.inFiducial!=1 or tree.OR != 1: continue

            true_top.SetPtEtaPhiM(tree.par_top_pt, tree.par_top_eta, tree.par_top_phi, tree.par_top_m)
            true_tbar.SetPtEtaPhiM(tree.par_tbar_pt, tree.par_tbar_eta, tree.par_tbar_phi, tree.par_tbar_m)
            if tree.par_lep_charge[0] == 1:
              true_lep_p.SetPtEtaPhiE(tree.par_lep_pt[0], tree.par_lep_eta[0], tree.par_lep_phi[0], tree.par_lep_E[0])
              true_lep_n.SetPtEtaPhiE(tree.par_lep_pt[1], tree.par_lep_eta[1], tree.par_lep_phi[1], tree.par_lep_E[1])
            else:
              true_lep_p.SetPtEtaPhiE(tree.par_lep_pt[1], tree.par_lep_eta[1], tree.par_lep_phi[1], tree.par_lep_E[1])
              true_lep_n.SetPtEtaPhiE(tree.par_lep_pt[0], tree.par_lep_eta[0], tree.par_lep_phi[0], tree.par_lep_E[0])

        else:

            if tree.true_top_eta == -9 : continue
            if (fabs(tree.true_lep_type[0])!=11 and fabs(tree.true_lep_type[0])!=13): continue
            if (fabs(tree.true_lep_type[1])!=11 and fabs(tree.true_lep_type[1])!=13): continue

            true_top.SetPtEtaPhiM( tree.true_top_pt, tree.true_top_eta, tree.true_top_phi, tree.true_top_m)
            true_tbar.SetPtEtaPhiM( tree.true_tbar_pt, tree.true_tbar_eta, tree.true_tbar_phi, tree.true_tbar_m)

            true_lep_p.SetPtEtaPhiM( tree.true_rew_lep_pt[0], tree.true_rew_lep_eta[0], tree.true_rew_lep_phi[0], tree.true_rew_lep_m[0]) # first one is positive
            true_lep_n.SetPtEtaPhiM( tree.true_rew_lep_pt[1], tree.true_rew_lep_eta[1], tree.true_rew_lep_phi[1], tree.true_rew_lep_m[1])

        true_ttbar = true_top + true_tbar

        p_truth, m_truth = self.getCosThetas(self.var, true_top, true_tbar, true_lep_p, true_lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol

        if self.lumiweight: weight = tree.eventWeightLumi
        else:
           if self.spntuple:
              weight = 1 if tree.eventWeight > 0 else -1 #*weightLSF[0]*aw #0.200083262490304142
           else:
              weight = 1.


        if self.reweighter:
          if 'pol' in self.var:
            if 'plus' in self.var:
              weight *= (self.reweighter.reweight(p_truth,m_truth)) # NOTE RALPH: just a test to see if the bigger slope is a problem of the reweighting
            elif 'minus' in self.var:
              weight *= (self.reweighter.reweight(m_truth,p_truth)) #here I need the correct order... first positive pol. and then negative. pol because of the different pol in MC
          elif 'corr' in self.var:
            weight *= (self.reweighter.reweight(p_truth,m_truth))
          elif 'sum' in self.var or 'diff' in self.var:
            weight *= (self.reweighter.reweight(p_truth,m_truth))


        truth = p_truth

        if 'corr' in self.var:
          truth = m_truth*p_truth # this can be changed later again depending on how I want to reweight spin correlation...  (just by the correlation or removing the polarization as well)
          # but also easier to just fill the truth value later
        elif 'sum' in self.var:
          truth = m_truth+p_truth
        elif 'diff' in self.var:
          truth = p_truth-m_truth

        if self.pdf:
            if 'pol' in self.var: #take both leptons for the calibration curve and resmat
              #self.fill_histo_truth_pdf(m_truth,weight/2.,tree)
              self.fill_histo_truth_pdf(p_truth,weight,tree)
            else: self.fill_histo_truth_pdf(truth,weight,tree)

        else:
            #if 'pol' in self.var: #take both leptons for the calibration curve and resmat
              #self.fill_histo_truth(m_truth,weight/2.,tree)
              #self.fill_histo_truth(p_truth,weight,tree)
            #else:
              self.fill_histo_truth(truth,weight,tree)


    else:

      # -- Reweight truth and reco leves for event which passed reco and final selection
      entries = tree.GetEntries()
      #entries = 10000
      print "is fiducial: ",self.fiducial

      reco_top = r.TLorentzVector()
      reco_tbar = r.TLorentzVector()
      reco_lep_p = r.TLorentzVector()
      reco_lep_n = r.TLorentzVector()
      true_top = r.TLorentzVector()
      true_tbar = r.TLorentzVector()
      true_lep_p = r.TLorentzVector()
      true_lep_n = r.TLorentzVector()

      for jentry in xrange(entries):

        nb = tree.GetEntry(jentry)
        if nb <=0: continue
        if not self.fiducial  and 'fiducial' in self.selection: continue

        if self.ch != 0:
          if tree.channel_DL != self.ch: continue

        if tree.channel_DL == 1 or tree.channel_DL == 2:
           if ((tree.flag_DL&1047551)!=1047551): continue

        if tree.channel_DL == 3:
           if ((tree.flag_DL&1048575)!=1048575): continue

        if self.fiducial:

           if 'ttbar' in self.selection:
               if tree.inFiducial!=1 or tree.OR != 1: continue
           if 'fiducial' in self.selection:
              if tree.inFiducial==1 and tree.OR == 1: continue

        ## SCALE FACTORS BY HAND!!!!!
        extraweight = 1.
        if tree.channel_DL == 1:
             sf_met = self.zsf[tree.GetName()]['ee']
             sf_hf  = self.zsf[tree.GetName()]['HF']
             sf_fakes = 1.17

        if tree.channel_DL == 2:
             sf_met = self.zsf[tree.GetName()]['mumu']
             sf_hf  = self.zsf[tree.GetName()]['HF']
             sf_fakes = 3.74


        if tree.channel_DL == 3:
             sf_fakes = 1.14


        if 'zjets' in self.selection and tree.channel_DL != 3:
          if tree.channelNumber in self.zlight:
            extraweight = extraweight*sf_met
          if tree.channelNumber in self.zhf and tree.channel_DL != 3:
            extraweight = extraweight*sf_met*sf_hf

        if 'fakes' in self.selection:
          extraweight = extraweight*sf_fakes

        weight = self.compute_weight(tree, self.lumiweight,self.spntuple)

        if not tree.isReconstructed: continue

        reco_top.SetPtEtaPhiM(tree.top_pt, tree.top_eta, tree.top_phi, tree.top_m)
        reco_tbar.SetPtEtaPhiM(tree.tbar_pt, tree.tbar_eta, tree.tbar_phi, tree.tbar_m)

        if tree.lep_charge[0] == 1:
          reco_lep_p.SetPtEtaPhiE( tree.lep_pt[0], tree.lep_eta[0], tree.lep_phi[0], tree.lep_E[0])
          reco_lep_n.SetPtEtaPhiE( tree.lep_pt[1], tree.lep_eta[1], tree.lep_phi[1], tree.lep_E[1])
        elif tree.lep_charge[1] == 1:
          reco_lep_p.SetPtEtaPhiE(tree.lep_pt[1], tree.lep_eta[1], tree.lep_phi[1], tree.lep_E[1])
          reco_lep_n.SetPtEtaPhiE(tree.lep_pt[0], tree.lep_eta[0], tree.lep_phi[0], tree.lep_E[0])
        else: print "No positive lepton found in event. Check lepton charges and charge comparison"

        reco_ttbar = reco_top + reco_tbar

        reco_p, reco_m = self.getCosThetas(self.var, reco_top, reco_tbar, reco_lep_p, reco_lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol

        reco = reco_p

        if 'corr' in self.var:
          reco = reco_m*reco_p # this can be changed later again depending on how I want to reweight spin correlation...  (just by the correlation or removing the polarization as well)
        elif 'sum' in self.var:
          reco = reco_p+reco_m
        elif 'diff' in self.var:
          reco = reco_p-reco_m

        if self.reweighter or 'truth' in self.selection or self.fname in self.ispecial:
            if self.fiducial:
                true_top.SetPtEtaPhiM(tree.par_top_pt, tree.par_top_eta, tree.par_top_phi, tree.par_top_m)
                true_tbar.SetPtEtaPhiM(tree.par_tbar_pt, tree.par_tbar_eta, tree.par_tbar_phi, tree.par_tbar_m)
                if tree.par_lep_charge[0] == 1:
                  true_lep_p.SetPtEtaPhiE(tree.par_lep_pt[0], tree.par_lep_eta[0], tree.par_lep_phi[0], tree.par_lep_E[0])
                  true_lep_n.SetPtEtaPhiE(tree.par_lep_pt[1], tree.par_lep_eta[1], tree.par_lep_phi[1], tree.par_lep_E[1])
                else:
                  true_lep_p.SetPtEtaPhiE(tree.par_lep_pt[1], tree.par_lep_eta[1], tree.par_lep_phi[1], tree.par_lep_E[1])
                  true_lep_n.SetPtEtaPhiE(tree.par_lep_pt[0], tree.par_lep_eta[0], tree.par_lep_phi[0], tree.par_lep_E[0])

            else:
                true_top.SetPtEtaPhiM(tree.true_top_pt, tree.true_top_eta, tree.true_top_phi, tree.true_top_m)
                true_tbar.SetPtEtaPhiM(tree.true_tbar_pt, tree.true_tbar_eta, tree.true_tbar_phi, tree.true_tbar_m)
                true_lep_p.SetPtEtaPhiM(tree.true_rew_lep_pt[0], tree.true_rew_lep_eta[0], tree.true_rew_lep_phi[0], tree.true_rew_lep_m[0])
                true_lep_n.SetPtEtaPhiM(tree.true_rew_lep_pt[1], tree.true_rew_lep_eta[1], tree.true_rew_lep_phi[1], tree.true_rew_lep_m[1])

            true_ttbar = true_top + true_tbar
            truth_p, truth_m = self.getCosThetas(self.var, true_top, true_tbar, true_lep_p, true_lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol, better to do that already in the function, otherwise there would be too much confusion
            truth = truth_p



        if self.reweighter:  #and not 'truth' in self.selection: #at the moment take only the calibration curve with reweight, not the inherent polarization
          if 'pol' in self.var:
            if 'plus' in self.var:
              weight *= self.reweighter.reweight(truth_p,truth_m)
            elif 'minus' in self.var:
              weight *= self.reweighter.reweight(truth_m,truth_p)
          elif 'corr' in self.var:
            weight *= self.reweighter.reweight(truth_p,truth_m)
          elif 'sum' in self.var or 'diff' in self.var:
            weight *= (self.reweighter.reweight(truth_p,truth_m))


        weight = weight*extraweight




        # if self.fname=='fiducialp':
        #   weight = weight*0.0557454


        if 'pol' in self.var and self.reweighter and not 'truth' in self.selection:
  #          print 'ralph obspos check in fuell condition'
            if self.pdf:
                #self.fill_histo_pdf(reco_m,weight/2.,tree)
                self.fill_histo_pdf(reco_p,weight,tree)
            else:
                #self.fill_histo(reco_m,weight/2.,tree)
                self.fill_histo(reco_p,weight,tree)

        else:
            if self.pdf:
                self.fill_histo_pdf(reco,weight,tree) #not pol or pol and not reweighted
            else:
                self.fill_histo(reco,weight,tree) #not pol or pol and not reweighted



        if 'truth' in self.selection or 'obs' in self.selection:

            if 'corr' in self.var:
                truth = truth_m*truth_p # this can be changed later again depending on how I want to reweight spin correlation...  (just by the correlation or removing the polarization as well)
            elif 'sum' in self.var:
                truth = truth_m+truth_p
            elif 'diff' in self.var:
                truth = truth_p-truth_m

            #if 'pol' in self.var:
            #    weight=weight/2.

            #print truth

            if not self.pdf: self.fillresmat(reco, truth ,weight,tree)

            #if 'pol' in self.var:
            #  self.fillresmat(reco_m, truth_m ,weight)  #RALPH NOTE: Have to uncomment that again, just for single resmat tests





  def book_histo(self):
      bins = self.inclbins
      nbins = len(bins)-1
      bins_arr  = array('d',bins)
      dummybins   = [-1.,1.]

      binslist = [dummybins]
      hname   = 'map_incl'

      if self.bootstrap:
        newhisto = r.TH1DBootstrap(hname,hname,nbins,bins_arr, self.nreplicas)
      else:
        newhisto = r.TH1F(hname,hname,nbins,bins_arr)
      #newhisto = r.TH1D(hname,hname,ndybins,dybins_arr)
      newhisto.Sumw2()
      #newhisto.SetDirectory(0)

      setattr(self,hname,newhisto)
      if ( ('truth' in self.selection)
            ):

        resmatname = hname.replace('map_','resmat_')
        #newresmat = r.TH2D(resmatname,resmatname,nbins,0.,nbins,nbins,0.,nbins)
        newresmat = r.TH2D(resmatname,resmatname,nbins,bins_arr,nbins,bins_arr)
        newresmat.Sumw2()
        #newresmat.SetDirectory(0)
        setattr(self,resmatname,newresmat)

  def book_histo_pdf(self):
      bins = self.inclbins
      nbins = len(bins)-1
      bins_arr  = array('d',bins)
      dummybins   = [-1.,1.]

      binslist = [dummybins]
      hname   = 'map_incl'

      hnameslist = []
      for i in xrange(self.pdfdict[self.pdftype]+1):
        hnameslist.append(r.TH1F(hname+'_'+self.pdftype+'_'+str(i),hname+'_'+self.pdftype+'_'+str(i),nbins,bins_arr))

      setattr(self,hname,hnameslist)

      if ( ('truth' in self.selection)      and
           ('protos' not in self.selection) and
           ('linear' not in self.selection) and
           ('fitprotos' not in self.selection) and
           ('obs' not in self.selection) ):
        resmatname = hname.replace('map_','resmat_')
        #newresmat = r.TH2D(resmatname,resmatname, nbins, 0., nbins, nbins, 0., nbins)
        newresmat = r.TH2D(resmatname,resmatname,nbins,bins_arr,nbins,bins_arr)
        newresmat.Sumw2()

        if self.bootstrap:
          newresmat =    r.TH2DBootstrap(resmatname,resmatname,nbins,bins_arr,nbins,bins_arr, self.nreplicas)
        else:
              newresmat = r.TH2D(resmatname,resmatname,nbins,bins_arr,nbins,bins_arr)
              newresmat.Sumw2()
        #newresmat.SetDirectory(0)
        setattr(self,resmatname,newresmat)

  def fill_histo(self, var, weight, tree):

    if not tree.isReconstructed: return

    if self.bootstrap:
       self.map_incl      .Fill(var, weight,tree.runNumber, tree.eventNumber)
    else:
       self.map_incl      .Fill(var,  weight)



  def fill_histo_pdf(self, var, weight, tree):

    self.pdfset.ComputePdfWeights(tree.mcevt_pdf_x1, tree.mcevt_pdf_x2,tree.mcevt_pdf_scale, tree.mcevt_pdf_id1, tree.mcevt_pdf_id2, tree.mcevt_pdf1,tree.mcevt_pdf2)
    pdfweight = list(self.pdfset.GetPdfWeights())

    for i,w in enumerate(pdfweight):
        if not tree.isReconstructed: return
        self.map_incl[i]      .Fill(var, weight*w)




  def fill_histo_truth(self, var, weight, tree):

    if self.bootstrap:
       self.map_incl      .Fill(var, weight,tree.runNumber, tree.eventNumber)

    else:
       self.map_incl      .Fill(var, weight)


  def fill_histo_truth_pdf(self, var, weight, tree):

    self.pdfset.ComputePdfWeights(tree.mcevt_pdf_x1, tree.mcevt_pdf_x2,tree.mcevt_pdf_scale, tree.mcevt_pdf_id1, tree.mcevt_pdf_id2, tree.mcevt_pdf1,tree.mcevt_pdf2)
    pdfweight = list(self.pdfset.GetPdfWeights())

    for i,w in enumerate(pdfweight):
      self.map_incl[i]      .Fill(var, weight*w)



  def compute_weight(self,tree,lumiweight, sp):

    if 'data' in self.selection:
      return tree.eventWeight_BTAG
    if 'partonicTtbar' in self.selection:
      weight = tree.eventWeightLumi
    else:
      if lumiweight:
         weight = tree.eventWeight_BTAG_LUMI
      else:
         weight = tree.eventWeight_BTAG

    return weight




  def fillresmat(self,reco, truth ,weight,tree):
    #mat_val = (reco, truth)

    # if running on differential variables
    #dummyVar_for1D = (0., 0.,) # (reco, truth)
    #dummyBins_for1D = [-1., 1.,]

    #varlist    = [ dummyVar_for1D  ]
    #xbinslist  = [ dummyBins_for1D ]
    #resmatlist = [ self.resmat_incl   ]
    #if self.bootstrap:

    #   self.resmat_incl.Fill(reco,truth,weight,tree.runNumber, tree.eventNumber)
    #else:
    self.resmat_incl.Fill(reco,truth,weight)

    #for var,resmat,xbins in zip(varlist,resmatlist,xbinslist):
      #ibin      = self.fillresmat_helper(mat_val[0],var[0],xbins)
      #ibintruth = self.fillresmat_helper(mat_val[1],var[1],xbins)
      #if ibin > 0 and ibintruth > 0:
      #  resmat.Fill(resmat.GetXaxis().GetBinCenter(ibin),resmat.GetYaxis().GetBinCenter(ibintruth),weight)



  def fillresmat_helper(self,dy,var,varbins):

    #remove events below low boundary
    #BUT, we include these events into distributions(maps) ???
    mindy   = self.inclbins[0]
    ndybins = len(self.inclbins)-1
    if dy < mindy:
      return 0

    minvar   = varbins[0]
    nvarbins = len(varbins)-1
    if var < minvar:
      return 0

    #get order of the bin for dy and for var
    ndy  = len([x for x in self.inclbins if x < dy ]) - 1
    nvar = len([x for x in varbins     if x < var]) - 1

    #include overflows for var, why not for dy???
    maxvar = varbins[-1]
    if var > maxvar:
      nvar = nvar-1

    #final bin ID: for each bin in var, we have matrix in dy
    return (ndy + (nvar*ndybins)) + 1




  def getCosThetas(self, var, top, antitop, lep_p, lep_n): #maybe put here the variable as argument?
    #wouldn't need to give var as an argument if I use self?

    ttbar = top + antitop

    if "transpol" in self.var:
      if "plus" in self.var:
        obs1 = utils.getTransCos(top, antitop, lep_p, 1.)
        obs2 = utils.getTransCos(antitop, top, lep_n, -1.)
      if "minus" in self.var:
        obs1 = utils.getTransCos(antitop, top, lep_n, -1.)
        obs2 = utils.getTransCos(top, antitop, lep_p, 1.)
    elif "beampol" in self.var: #don't use the beam basis anymore
      if "plus" in self.var:
        obs1 = utils.getBeamCos(top, antitop, lep_p, 1.)
        obs2 = utils.getBeamCos(antitop, top, lep_n, -1.)
      if "minus" in self.var:
        obs1 = utils.getBeamCos(antitop, top, lep_n, -1.)
        obs2 = utils.getBeamCos(top, antitop, lep_p, 1.)
    elif "rpol" in self.var:
      if "plus" in self.var:
        obs1 = utils.getRCos(top, antitop, lep_p, 1.)
        obs2 = utils.getRCos(antitop, top, lep_n, -1.)
      if "minus" in self.var:
        obs1 = utils.getRCos(antitop, top, lep_n, -1.)
        obs2 = utils.getRCos(top, antitop, lep_p, 1.)
    elif "helpol" in self.var:
      if "plus" in self.var:
        obs1 = utils.getHelCos(ttbar, top, lep_p)
        obs2 = utils.getHelCos(ttbar, antitop, lep_n)
      if "minus" in self.var:
        obs1 = utils.getHelCos(ttbar, antitop, lep_n)
        obs2 = utils.getHelCos(ttbar, top, lep_p)
    elif self.var == "helcorr":
      obs1 = utils.getHelCos(ttbar, top, lep_p)
      obs2 = utils.getHelCos(ttbar, antitop, lep_n)
    elif self.var == "transcorr":
      obs1 = utils.getTransCos(top, antitop, lep_p, 1.)
      obs2 = utils.getTransCos(antitop, top, lep_n, -1.)
    elif self.var == "rcorr":
      obs1 = utils.getRCos(top, antitop, lep_p, 1.)
      obs2 = utils.getRCos(antitop, top, lep_n, -1.)
    elif "transhel" in self.var:
      costransplus = utils.getTransCos(top, antitop, lep_p, 1.)
      coshelplus = utils.getHelCos(ttbar, top, lep_p)
      costransminus = utils.getTransCos(antitop, top, lep_n, -1.)
      coshelminus = utils.getHelCos(ttbar, antitop, lep_n)
      obs1 = costransplus*coshelminus
      obs2 = coshelplus*costransminus
    elif "transr" in self.var:
      costransplus = utils.getTransCos(top, antitop, lep_p, 1.)
      cosrplus = utils.getRCos(top, antitop, lep_p, 1.)
      costransminus = utils.getTransCos(antitop, top, lep_n, -1.)
      cosrminus = utils.getRCos(antitop, top, lep_n, -1.)
      obs1 = costransplus*cosrminus
      obs2 = cosrplus*costransminus
    elif "rhel" in self.var:
      cosrplus = utils.getRCos(top, antitop, lep_p, 1.)
      coshelplus = utils.getHelCos(ttbar, top, lep_p)
      cosrminus = utils.getRCos(antitop, top, lep_n, -1.)
      coshelminus = utils.getHelCos(ttbar, antitop, lep_n)
      obs1 = cosrplus*coshelminus
      obs2 = coshelplus*cosrminus
    else:
      obs1 = 0.
      obs2 = 0.
    return obs1,obs2
