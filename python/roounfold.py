from ROOT import gSystem, TH1F, TH2F, TCanvas


gSystem.Load("/nfs/dust/atlas/user/naranjo/RooUnfold/libRooUnfold.so")
gSystem.Load("/nfs/dust/atlas/user/naranjo/RooUnfold/libRooUnfold.rootmap")

from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldSvd
from ROOT import RooUnfoldTUnfold
from ROOT import RooUnfoldBinByBin
from ROOT import RooUnfoldInvert
from ROOT import RooUnfoldBayes


##system modules
import sys
import json
import gc
import os

from   numpy import mean,std,array, linspace,dot
import numpy as np
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

from   matplotlib import pyplot as plt

#
import ROOT as r
#our modules
from Config import Configuration
from utils import *
import array as a

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')

        rv[key] = value
    return rv


def histo2list(histo):
    nbinsx = histo.GetNbinsX()
    nbinsy = histo.GetNbinsY()
    binlist = [histo.GetBinContent(x,y) for y in xrange(1,nbinsy+1) for x in xrange(1,nbinsx+1)]
    return binlist

def json2histo(name,bincontent):
    h = TH1F(name,name,len(bincontent), 0, len(bincontent))
    for i,content in enumerate(bincontent):
        h.SetBinContent(i+1,content)

    return h

def json2resmat(name,bincontent):

    x = len(bincontent[0])
    y = len(bincontent)

    h = TH2F(name,name,x,0,x,y,0,y)

    for i in xrange(x):
        for j in xrange(y):
            h.SetBinContent(i+1,j+1,bincontent[j][i])


    return h



if __name__ == '__main__':

  debug = 10

  outDir  = Configuration.outputDir
  workDir = Configuration.workDir
  # -- Get settings from command line
  channel            = setChan(sys.argv)
  var                = setVar(sys.argv)
  signal             = setSignal2Unfold(sys.argv)
  binName            = setOptimalBinning(sys.argv,var)
  binning            = getOptimalBinning(sys.argv,var)

  rpName,rpValue     = 'rpZero', 0.0
  regFunction        = 'Tikhonov'
  ensemble           = setEnsemble(sys.argv)
  syst               = setSyst(sys.argv)
  sel                = 'btag'

  useBKG             = setUseBackground(sys.argv) # default value it's TRUE
  isSignalModeling   = setSignalModeling(sys.argv) # default value it's False
  fiducial           = setFiducial(sys.argv)
  bootstrap,pos      = setBootstrapP(sys.argv)
  pdf,pdfType        = setPDF(sys.argv)
  pdfNum             = setPDFNum(sys.argv)

  extra = pdfType+'_'+str(pdfNum)
  signalor = signal

  if pdf: signal = signal+'_'+pdfType+'_'+str(pdfNum)

  if not fiducial:
       outDir  = Configuration.outputDirt
       outDirC = Configuration.outputDirt

  if bootstrap:

    if fiducial: outDir = Configuration.outputDirB
    else: outDir = Configuration.outputDirBt




  if pdf: jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':pdfType.lower(),'channel':channel}
  else: jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':syst.lower(),'channel':channel}
  jsondirNom  = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':var,'channel':channel}
  jsondirB    = outDir+'/%(var)s/nominal/%(channel)s/bootstrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos}




  print "\nbinName, regPar, regFunction: ", binName, rpName, rpValue, regFunction
  if ensemble: print "\n => running ensembles\n"

  # Get input (signal+background) distributions
  data        = json.load(open(jsondirNom+'/'+var+'_'+signal+'_'+sel+'_'+binName+'.json'))
  reco        = json.load(open(jsondirNom+'/'+var+'_reco_'+sel+'_'+binName+'.json'))
  resmat      = json.load(open(jsondirNom+'/'+var+'_mig_'+sel+'_'+binName+'.json'))
  bckg        = json.load(open(jsondirNom+'/'+var+'_bckg_'+sel+'_'+binName+'.json'),object_hook=_decode_dict)
  truth       = json.load(open(jsondirNom+'/'+var+'_partonic_btag_'+binName+'.json')) # Get truth dist



  if 'reco' in signal or 'data' in signal: truthforcalib = json.load(open(jsondir+'/'+var+'_partonic_btag_'+binName+'.json'))
  else: truthforcalib = json.load(open(jsondir+'/'+var+'_partonic'+signal+'_btag_'+binName+'.json'))

  background  = array(bckg['dibosons'])+array(bckg['zjets'])+array(bckg['zjets_low'])+array(bckg['single_top'])+array(bckg['fakes'])+array(bckg['ttv'])

  if fiducial:
     fiducial_fraction =  array(bckg['fiducial'])/(array(reco)+array(bckg['fiducial']))
     background = background + (array(data)-(array(bckg['dibosons'])+array(bckg['zjets'])+array(bckg['zjets_low'])+array(bckg['ttv'])+array(bckg['single_top'])+array(bckg['fakes'])))*fiducial_fraction



  h_reco   = json2histo('reco',reco)
  h_data   = json2histo('data',data-background)

  h_truth   = json2histo('truth',truth)
  h_resmat  = json2resmat('resmat',resmat)

  response = RooUnfoldResponse(h_reco,  h_truth, h_resmat)
  response.UseOverflow(False)
  unfold   = RooUnfoldBayes(response, h_data, 50)

  result   = unfold.Hreco().Clone()


  unfolded = histo2list(result)

  print unfolded, truth


  truthObs = computeObs(var,truth,binning)
  truthObsCalib = computeObs(var,truthforcalib,binning)
  print "truth: ",truthObsCalib if 'obs' in signal else truthObs
  print "unfolded: ",computeObs(var,unfolded,binning),'+-',computeObsUncert(var,unfolded,binning)

  r.gROOT.SetBatch(0)

  c = TCanvas()
  h_truth.Draw()
  result.Draw("same")

  raw_input("wait")
