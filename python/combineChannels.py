#!/bin/env python

import json
import time
import os
import sys
import itertools
from math import sqrt
from array import array
import ROOT as r # variable r is 'ROOT'
import utils
import numpy as np
from utils import writefile
from Config import Configuration, getObsSamples, getSampleLists, getNtupleDir, getProtosSamples2Json, getLinearSamples2Json, getFitProtosSamples2Json, getObsSamples2Json, getObsResmatSamples



def getMigration(resmat, partonic ):

    nbinsx = len(resmat[0]) # reconstructed distribution
    nbinsy = len(partonic) # truth distribution
    #print nbinsx,nbinsy
    migration    = []
    migrationErr = []
    acceff       = []

    for y in xrange(0,nbinsy):
        truthbin = []
        truthbinErr = []
        norm = np.sum(resmat[y]) # integral of the rows (along de X axis)
        #nprint  norm
        for x in xrange(0,nbinsx):
            value  = resmat[y][x]/norm
            effbin = getEff(resmat,partonic,y)
            value  = value*effbin
            truthbin.append(value)
        migration.append(truthbin)
        migrationErr.append(truthbinErr)
        acceff.append(effbin)
    return migration, acceff


def getEff(resmat,partonic,y):
    #get coordinates in 2D truth hist
    #note: resp matrix is 2D: (nXtruth*nYtruth) x (nXtruth*nYtruth) --> y = (yy-1)*Nbins_dy + xx
    return np.sum(resmat[y]) / partonic[y]



def mergeObject(name,binning,var,pdf=False,pdfType=""):

   channels = ['EE','MUMU','EMMU']
   newobject = []
   newdict,firstdict,firstresmat = {}, True, True
   for ch in channels:
        jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':syst.lower(),'channel':ch}
        if pdf:jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':pdfType.lower(),'channel':ch}
        jsonobj = jsondir+'%(var)s_%(obj)s_btag_%(binning)s.json'%{'var':var,'obj':name,'binning':binning}
        #print jsonobj
        if 'bckg' not in name:
          if 'mig' in name:
              if firstresmat:
                newobject = json.load(open(jsonobj))
                firstresmat = False
              else:
                for i in xrange(len(newobject)):
                  tmp = json.load(open(jsonobj))
                  newobject[i] = newobject[i] + tmp[i]



          else:
             if 'partonic' in name:
                newobject = json.load(open(jsonobj))
             else:
                newobject = newobject + json.load(open(jsonobj))
        else:
          d = json.load(open(jsonobj))
          if firstdict:
            for key, value in d.iteritems():
               newdict[key] = d[key]
            firstdict = False
          else:
            for key, value in d.iteritems():
               newdict[key] = newdict[key] + d[key]


   newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/'%{'var':var,'syst':syst.lower()}
   if pdf: newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/'%{'var':var,'syst':pdfType.lower()}
   if not os.path.isdir(newdir): os.system("mkdir -p %s"%newdir)
   newfile = newdir +'%(var)s_%(obj)s_btag_%(binning)s'%{'var':var,'obj':name,'binning':binning}
   if 'bckg' not in name:
     writefile(newfile,newobject)
   else:
      writefile(newfile,newdict)

if __name__ == '__main__':

    outDir      = Configuration.outputDirt
    variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                    'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                    'transheldiff']

    #variables = ['rhelsum','rheldiff','transrdiff']
    pdf,pdfType = utils.setPDF(sys.argv)
    fiducial    = utils.setFiducial(sys.argv)
    if fiducial: outDir = Configuration.outputDir

    systs = ['nominal','fastpythia','mcatnlo','herwig','perugia','perugiampihi','perugialocr','radhi','radlo']#,'perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia']

    systs = ['topmass_167.5','topmass_170','topmass_175','topmass_177.5']

    systs = ['pol_up','pol_down','corr_up','corr_down']
   
    systs = ['nominal']

    if pdf:

      for var in variables:

       print var

       pdfdict   = {"CT10nlo":53, "MSTW2008nlo68cl":41,"NNPDF23_nlo_as_0119":101}

       objects2Merge = []
       objects2Merge_tmp    = ['bckg','reco','data'] #+ getObsSamples2Json(var) #+ getObsResmatSamples(var)


       for sig in  objects2Merge_tmp:
         for i in xrange(pdfdict[pdfType]):

           objects2Merge.append(sig+'_'+pdfType+'_'+str(i))


       import binningOptimization
       binnings = binningOptimization.binnings_dict(var)
       #binnings_dict = s{'bin16_ll':[-5., -0.75, 0., 0.75, 5.]}
       for syst in systs:
         for bin in binnings.iterkeys():
             for obj in objects2Merge:
               #print obj,asym,bin
               mergeObject(obj,bin,var,pdf,pdfType)




    else:


     for var in variables:

       objects2Merge    = ['obspos0'] #['bckg','data','partonic','reco','mig','acceff']# + getObsSamples2Json(var) + getObsResmatSamples(var)
       #print objects2Merge
       migrations2merge =  [] #getObsSamples(var)
       objects2MergeErr = [] #['bckgErr','dataErr','partonicErr','recoErr']#,'fitprotosA0noRewErr','fitprotosA2posErr','fitprotosA4posErr','fitprotosA6posErr', 'fitprotosA2negErr','fitprotosA4negErr','fitprotosA6negErr','partonicfitprotosA0noRewErr','partonicfitprotosA2posErr','partonicfitprotosA4posErr','partonicfitprotosA6posErr', 'partonicfitprotosA2negErr','partonicfitprotosA4negErr','partonicfitprotosA6negErr']
       import binningOptimization
       binnings = binningOptimization.binnings_dict(var)


       for syst in systs:
         for bin in binnings.iterkeys():
             for obj in objects2Merge:
             #  print obj,bin,var
              # try:
                 mergeObject(obj,bin,var)
               #except:
                #   continue
             for obj in objects2MergeErr:
               mergeObject(obj,bin,var)


             for obs in  migrations2merge:
                 try:
                  migration = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'mig'+obs,'binning':bin}))
                  partonic  = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'partonic'+obs,'binning':bin}))
                 except:
                     continue
                 resmat = getMigration(migration,partonic)
                 newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/'%{'var':var,'syst':syst.lower()}
                 if not os.path.isdir(newdir): os.system("mkdir -p %s" % newdir)
                 newfile = newdir +'%(var)s_%(obj)s_btag_%(binning)s'%{'var':var,'obj':'resmat'+obs,'binning':bin}
                 print newfile

                 writefile(newfile,resmat[0])



             #print outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'partonic','binning':bin}
             #print outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'partonic','binning':bin}
             try:
              migration = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'mig','binning':bin}))
              partonic  = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/%(var)s_%(obj)s_btag_%(binning)s.json'%{'syst':syst.lower(),'var':var,'obj':'partonic','binning':bin}))
             except:
                 continue
             resmat = getMigration(migration,partonic)
             newdir =  outDir+'/%(var)s/%(syst)s/ALL/optimization/'%{'var':var,'syst':syst.lower()}
             if not os.path.isdir(newdir): os.system("mkdir -p %s"%newdir)
             newfile = newdir +'%(var)s_%(obj)s_btag_%(binning)s'%{'var':var,'obj':'resmat','binning':bin}
             #print newfile

             writefile(newfile,resmat[0])
