def runChannel(ttbarAsym,ch,selection,systname,samples,histos_queue,binning,lumiweight,rewasymm='',fiducial = False, bootstrap = False, pdf = False, pdfType = "CT10nlo"):

    import Config
    ntupleDir   = Config.getNtupleDir()
    specialt    = False

    ispecial = Config.getModSyst()
    print systname
    for i in ispecial:
       if i in systname :
          print "Special Ntuples"
          specialt = True

    print samples
    histos = {}

    for sample in samples:
        dataflag = 'data' in sample
        fname = sample

        if 'nominal' not in systname:
            fname = fname.replace('_Truth','')

        if 'ttbar' in sample:#  and 'nominal' in systname:
            sel = 'truth_'+selection
        else: sel = selection
        if ('protos' in rewasymm) or ('linear' in rewasymm) or ('fitprotos' in rewasymm) or ('obs' in rewasymm):
            sel = 'truth_'+selection+'_%s'%rewasymm


        print 'channel:',ch,'sample:',sample,'selection:',selection

        import ROOT as r

        if fname == 'ttbar' and specialt:
           fname = systname

        if fname == 'fiducial' or fname == 'fiducialp' :
           print "Used File:",systname
           if specialt: treeFile = r.TFile.Open("%(basedir)s/%(filename)s.root" %{'basedir':ntupleDir,'filename':systname})
           else: treeFile = r.TFile.Open("%(basedir)s/%(filename)s.root" %{'basedir':ntupleDir,'filename':'ttbar'})

        else:
           print "Used File: ", fname
           treeFile = r.TFile.Open("%(basedir)s/%(filename)s.root" %{'basedir':ntupleDir,'filename':fname})

        path = Config.getPath(sample, systname)

        if ('data' in sample or  specialt or systname in Config.Configuration.possibleTheoSyst) and not path=='particles'   :
           tree = treeFile.Get("mini")
        else:
           tree = treeFile.Get("%s" % path)

        from Histogrammer import Histogrammer

        if sample in ispecial:
           tree = treeFile.Get("particles")
           histo = Histogrammer(tree,ttbarAsym,ch,sel+'_partonicTtbar',binning,fname,systname,lumiweight, fiducial, bootstrap, treeFile, pdf, pdfType)
        else:
           histo = Histogrammer(tree,ttbarAsym,ch,sel+'_'+sample,binning,fname,systname,lumiweight, fiducial, bootstrap, treeFile, pdf, pdfType)

        histos[sample] = histo



    histos_queue.put(histos)
