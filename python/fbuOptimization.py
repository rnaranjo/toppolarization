##system modules
import sys
import json
import gc
import os

from   numpy import mean,std,array, linspace,dot
import numpy as np
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
from   matplotlib import pyplot as plt
import fbu
#our modules
from Config import Configuration
from utils import *

DEBUG=False

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')

        rv[key] = value
    return rv


def write2FileForEnsemblesSyst(unfAC,outFile,binName,rpName,regFunction,signal,firstEnsSyst,lastEnsSyst,folder = 'ensembles'):

  ACmean = np.mean(unfAC)
  ACrms  = np.std(unfAC)
  outFolder = jsondir+'/'+folder+'/'

  outFileCentral = outFolder+'Syst_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.json'
  outFileErr     = outFolder+'SystErr_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.json'

  if not os.path.isdir(outFolder): os.system("mkdir -p %s"%outFolder)

  if not firstEnsSyst:
      data = json.load(open(outFileCentral))
      dataErr = json.load(open(outFileErr))
      value    = ACmean
      valueErr = ACrms
      data.append(value)
      dataErr.append(valueErr)

      with open(outFileCentral,'w') as outfile:
        json.dump(data,outfile)
      with open(outFileErr,'w') as outfile:
          json.dump(dataErr,outfile)
  else:
      data = [ACmean]
      dataErr = [ACrms]
      with open(outFileCentral,'w') as outfile:
        json.dump(data,outfile)
      with open(outFileErr,'w') as outfile:
        json.dump(dataErr,outfile)


def writeForCalibration(fname='',datatowrite=None):
    with open(fname+'.json','a') as outfile:
            json.dump(datatowrite,outfile)

def write2FileForCalib(truthAC,unfAC,signal,binName,rpName,regFunction,bootstrap = False):



  ACmean =np.mean(unfAC)
  ACrms  =np.std (unfAC)


  if bootstrap:

      if   'lin'       in signal: outFile = jsondirB+'/Calibration_linear'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'fitprotos' in signal: outFile = jsondirB+'/Calibration_fitprotos'+'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'protos'    in signal: outFile = jsondirB+'/Calibration_protos'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'obs'       in signal: outFile = jsondirB+'/Calibration_obs'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName

  else:

      if   'lin'       in signal: outFile = jsondir+'/Calibration_linear'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'fitprotos' in signal: outFile = jsondir+'/Calibration_fitprotos'+'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'protos'    in signal: outFile = jsondir+'/Calibration_protos'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'obs'    in signal: outFile = jsondir+'/Calibration_obs'   +'_'+var+'_'+binName+'_'+regFunction+"_"+rpName

  if pdf: outFile = outFile+"_"+pdfNum
  print outFile
  with open(outFile,'a') as outfile:
      outfile.write(str(truthAC))
      outfile.write(' ')
      outfile.write(str(truthAC))
      outfile.write(' ')
      outfile.write(str(ACmean))
      outfile.write(' ')
      outfile.write(str(ACrms))
      outfile.write('\n')

def write2FileForCalibNevent(trace,truth,signal,binName,rpName,regFunction,binsX):



  binContents = []
  binErr      = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    meanBin = mean(trace[i])
    rmsBin  = std(trace[i])
    binContents.append(meanBin)
    binErr.append(rmsBin)



    for bin in xrange(nBinsX):

      if   'lin'       in signal: outFile = jsondir+'/CalibrationNevent'+str(i)+'_linear'   +'_'+var+'_'+'bin'+str(bin)+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'fitprotos' in signal: outFile = jsondir+'/CalibrationNevent'+str(i)+'_fitprotos'+'_'+var+'_'+'bin'+str(bin)+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'protos'    in signal: outFile = jsondir+'/CalibrationNevent'+str(i)+'_protos'   +'_'+var+'_'+'bin'+str(bin)+'_'+binName+'_'+regFunction+"_"+rpName
      if pdf: outFile = outFile+"_"+pdfNum
      print outFile
      with open(outFile,'a') as outfile:
        outfile.write(str(truth[i]))
        outfile.write(' ')
        outfile.write(str(0))
        outfile.write(' ')
        outfile.write(str(meanBin))
        outfile.write(' ')
        outfile.write(str(rmsBin))
        outfile.write('\n')

def plotUnfoldedEvents(trace):
  plt.show()
  binContents = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    plt.hist(trace[i],bins=20,alpha=0.85,normed=True)
    plt.ylabel('Unfoled event probability')
    plt.savefig(jsondir+'UnfEvtBin'+str(i)+'_posterior.eps')
    plt.close()
    meanBin = mean(trace[i])
    binContents.append(meanBin)
    print 'UnfoldEventnBin',i,meanBin
  writefile(jsondir+'Unfolded_bin',binContents)

def write2FileForCalibReco(truthAC,reco,signal,binName,rpName,regFunction,binsX):


    outFile = jsondir+'/CalibrationReco_protos'   +'_'+var+'_'+'bin'+str(0)+'_'+binName+'_'+regFunction+"_"+rpName


    if bootstrap:

      if   'lin'       in signal: outFile = jsondirB+'/CalibrationReco_linear'   +'_'+var+'_'+'bin'+str(0)+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'fitprotos' in signal: outFile = jsondirB+'/CalibrationReco_fitprotos'+'_'+var+'_'+'bin'+str(0)+'_'+binName+'_'+regFunction+"_"+rpName

    else:

      if   'lin'       in signal: outFile = jsondir+'/CalibrationReco_linear'   +'_'+var+'_'+'bin'+str(0)+'_'+binName+'_'+regFunction+"_"+rpName
      elif 'fitprotos' in signal: outFile = jsondir+'/CalibrationReco_fitprotos'+'_'+var+'_'+'bin'+str(0)+'_'+binName+'_'+regFunction+"_"+rpName

    print signal
    print outFile
    print truthAC,reco
    with open(outFile,'a') as outfile:


     outfile.write(str(truthAC[0,0]))
     outfile.write(' ')
     outfile.write(str(truthAC[0,1]))
     outfile.write(' ')
     outfile.write(str(reco[0,0]))
     outfile.write(' ')
     outfile.write(str(reco[0,1]))
     outfile.write('\n')




def writeUnfoldInfo(unfAC,syst,binName,rpName):



  ACmean = np.mean(unfAC)
  ACrms  = np.std (unfAC)
  datatowrite = [ACmean,ACrms]

  fname = jsondir+'/Unfolded_'+signal+'_'+syst+'_'+var+'_'+binName+'_'+rpName
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)



def printTruthInfo(truth, truthAC, syst, binName, binsX):
  print ' '
  print '  ** Truth informations **'
  print 'Truth bin contents:',truth
  writefile(jsondir+'Truth_binContent',truth)
  print 'truth AC',truthAC
  print ' '
  fname = jsondir+'/Truth_'+var+'_'+syst+'_'+binName
  datatowrite = truth
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)


def printSpinInfo(truth, data, background, trace):


  print ' '
  print '  ** Spin informations **'
  print ' unfolded: ', ACunfolded, "  +-  ", ACunfoldedErr
  print ' truth: ',ACtruth[:,0]
  print ' '


def plot_distribution(trace, truth, var):
  from binningOptimization import binnings_dict
  bins = binnings_dict(var)[binName]
  binX    = []
  binXErr = []
  binWidth = []
  for i in xrange(len(bins)-1):
    binX.append( (bins[i+1] + bins[i])/2. )
    binXErr.append( (bins[i+1] - bins[i])/2. )
    binWidth.append(abs((bins[i+1] - bins[i])))

  binContents = []
  binErr      = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    meanBin = np.mean(trace[i])
    rmsBin  = np.std(trace[i])
    if 'pol' in var:  meanBin =meanBin / binWidth[i]

    binContents.append(meanBin)
    binErr.append(rmsBin)

  #print bins
  #print binContents
  #print binErr

  if 'pol' in var:
      truth = array(truth)/array(binWidth)

  plt.errorbar(binX, binContents, yerr=binErr, xerr=binXErr,fmt='o',ecolor='g',mfc='g',label='unfolded')
  plt.errorbar(binX, truth, xerr=binXErr,fmt='o',ecolor='b',mfc='b',label='truth')
  plt.xlabel(var)

  plt.ylabel(r'$Nevents$')
  plt.legend()
  plt.savefig(jsondir+channel+"_"+signal+"_"+binName+"_"+rpName+'_distribution.eps')
  plt.close()

  fname = jsondir+'/UnfoldedDistribution_'+signal+'_'+var+'_'+syst+'_'+binName+'_'+rpName
  datatowrite = [binContents, binErr]
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)

def plot_distribution_ensembles(trace, truth, var,folder ='stat'):
  from binningOptimization import binnings_dict
  bins = binnings_dict(var)[binName]
  binX    = []
  binXErr = []
  for i in xrange(len(bins)-1):
    binX.append( (bins[i+1] + bins[i])/2. )
    binXErr.append( (bins[i+1] - bins[i])/2. )

  binContents = []
  binErr      = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    meanBin = np.mean(trace[i])
    rmsBin  = np.std(trace[i])
    binContents.append(meanBin)
    binErr.append(rmsBin)

  #print bins
  #print binContents
  #print binErr
  f = plt.figure()
  plt.errorbar(binX, binContents, yerr=binErr, xerr=binXErr,fmt='o',ecolor='g',mfc='g',label='unfolded')
  plt.errorbar(binX, truth, xerr=binXErr,fmt='o',ecolor='b',mfc='b',label='truth')
  plt.xlabel(var)

  outFolder = jsondir+'/'+folder+'/'

  outFileCentral = outFolder+'UnfoldedDistribution_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.eps'

  plt.ylabel(r'$Nevents$')
  plt.legend()
  plt.savefig(outFolder+'UnfoldedDistribution_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.eps')
  f.clf()
  plt.close()

  fname = outFolder+'UnfoldedDistribution_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.json'
  datatowrite = [binContents, binErr]
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)

def printAndPlotUnfoldInfo(unfAC,truthAC):

  from scipy.stats import norm
  from scipy import stats

  ACmean =   np.mean(unfAC)
  ACrms  = np.std (unfAC)
  print '  ** Unfolded informations **'
  print 'unfAC', ACmean, '+-', ACrms

  mu, stdfit = norm.fit(unfAC)

  x,y,o = plt.hist(unfAC,bins=50,histtype='stepfilled', normed=True)

  xmin, xmax = plt.xlim()
  xfit = linspace(xmin, xmax, 100)
  p = norm.pdf(xfit, mu, stdfit)

  plt.ylabel('Unf AC probability')
  plt.vlines(truthAC,0,x.max(),color='red')
  plt.vlines(ACmean,0,x.max(),color='black')
  plt.figtext(0.03,0.02,'black: AC(unf) = %0.4f +/- %0.4f' %(ACmean,ACrms))
  plt.figtext(0.03,0.95,'red: AC(truth) = %0.4f'%(truthAC))
  plt.plot(xfit, p, 'k', linewidth=2)
  plt.savefig(jsondir+'_'+channel+"_"+signal+"_"+binName+"_"+rpName+'_'+var+'_posterior.eps')
  pFile = jsondir+'/posteriorAC_'+binName+'_'+regFunction+"_"+rpName+"_"+var
  import numpy
  numpy.save(pFile, ACmean)
  plt.savefig(jsondir+channel+"_"+signal+"_"+binName+"_"+rpName+'_'+var+'_posterior.png')
  plt.close()
  print ' '


def plotInput(reco, background, pseudodata):
 f = plt.figure()
 bin=range(len(reco))
 plt.errorbar(bin, array(reco) + array(background), xerr=0.5,fmt='o',ecolor='g',mfc='g',label='data+background')
 plt.errorbar(bin, pseudodata, xerr=0.5,fmt='o',ecolor='k',mfc='b',label='pseudodata')

 plt.show()
 plt.savefig('dY.eps')
 f.clf()
 plt.close()


def plotInputEnsembles(reco, background, pseudodata,folder ='stat'):
 f = plt.figure()
 bin=range(len(reco))
 plt.errorbar(bin, array(reco) + array(background), xerr=0.5,yerr=np.sqrt(array(reco) + array(background)),fmt='o',ecolor='g',mfc='g',label='data+background')
 plt.errorbar(bin, pseudodata, xerr=0.5,fmt='o',ecolor='k',mfc='b',label='pseudodata')
 outFolder = jsondir+'/'+folder+'/'

 outFileCentral = outFolder+'InputDistribution_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.eps'

 #plt.show()
 plt.savefig(outFileCentral)
 f.clf()
 plt.close()

def addUnc(backgrounddict,unc='fakes', fiducial = True):
   l = len(backgrounddict['signal']['el_idsf'])
   backgrounddict['signal'][unc]=[0.00]*l
   if fiducial:
       backgrounddict['background'][unc] = {'ttv':[0.]*l,'zjets':[0.]*l,'zjets_low':[0.]*l,'dibosons':[0.]*l,'single_top':[0.]*l,'fiducial':[0.]*l,'fakes':[1.67]*(l/3)+[0.49]*(l/3)+[0.77]*(l/3)}
   else:
       backgrounddict['background'][unc] = {'ttv':[0.]*l,'zjets':[0.]*l,'zjets_low':[0.]*l,'dibosons':[0.]*l,'single_top':[0.]*l,'fakes':[1.67]*(l/3)+[0.49]*(l/3)+[0.77]*(l/3)}

def dyInfoEnsembles(trace, truth,var, folder,firstEnsSyst,lastEnsSyst):

  binContents = []
  binErr      = []

  nbins = len(trace)

  outFolder = jsondir+'/'+folder+'/'

  outFileCentral = outFolder+'SystDistribution_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.json'
  outFileErr     = outFolder+'SystDistributionErr_'+signal+'_'+var+'_'+binName+'_'+regFunction+'_'+rpName+'_'+outFile+'.json'

  if not os.path.isdir(outFolder): os.system("mkdir -p %s"%outFolder)


  for i in xrange(0,nbins):
    meanBin = np.mean(trace[i])
    rmsBin  = np.std(trace[i])
    binContents.append(meanBin)
    binErr.append(rmsBin)



  if not firstEnsSyst:
    a = json.load(open(outFileCentral))
    a.append(binContents)
    b = json.load(open(outFileErr))
    b.append(binErr)

    with open(outFileCentral,'w') as outfile:
        json.dump(a,outfile)
    with open(outFileErr,'w') as outfile:
        json.dump(b,outfile)
  else:

    with open(outFileCentral,'w') as outfile:
        json.dump([binContents],outfile)
    with open(outFileErr,'w') as outfile:
        json.dump([binErr],outfile)


def cleanNuissance(dictionary,syst):

 newdict = {}
 newdict['signal'] = {}
 newdict['signal'][syst] = dictionary['signal'][syst]
 newdict['background'] = {}
 newdict['background'][syst] =  dictionary['background'][syst]




 return newdict




if __name__ == '__main__':

  debug = 10

  outDir  = Configuration.outputDir
  workDir = Configuration.workDir
  from binningOptimization import binnings_dict

  # -- Get settings from command line
  channel            = setChan(sys.argv)
  var                = setVar(sys.argv)
  signal             = setSignal2Unfold(sys.argv)
  binName            = setOptimalBinning(sys.argv,var)
  binning            = getOptimalBinning(sys.argv,var)

  btemp = binnings_dict(var)

  for bn,bc in btemp.iteritems():
     binName = bn
     binning = binning


  rpName,rpValue     = setRegPar(sys.argv) #'rpZero', 0.0
  regFunction        = 'Tikhonov'
  ensemble           = setEnsemble(sys.argv)
  syst               = setSyst(sys.argv)
  sel                = 'btag'

  useBKG             = setUseBackground(sys.argv) # default value it's TRUE
  isSignalModeling   = setSignalModeling(sys.argv) # default value it's False
  fiducial           = setFiducial(sys.argv)
  bootstrap,pos      = setBootstrapP(sys.argv)
  pdf,pdfType        = setPDF(sys.argv)
  pdfNum             = setPDFNum(sys.argv)

  extra = pdfType+'_'+str(pdfNum)
  signalor = signal

  if pdf: signal = signal+'_'+pdfType+'_'+str(pdfNum)

  if not fiducial:
       outDir  = Configuration.outputDirt
       outDirC = Configuration.outputDirt

  if bootstrap:

    if fiducial: outDir = Configuration.outputDirB
    else: outDir = Configuration.outputDirBt

  nomDirH = Configuration.outputDir if fiducial else Configuration.outputDirt

  if pdf: jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':pdfType.lower(),'channel':channel}
  else: jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'syst':syst.lower(),'channel':channel}


  jsondirNom  = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':var,'channel':channel}
  jsondirNom2 = nomDirH + '/%(var)s/nominal/%(channel)s/'%{'var':var,'channel':channel}
  jsondirB    = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos,'syst':syst.lower()}
  jsondirNO  = outDir+'/%(var)s/nominal/%(channel)s/'%{'var':var,'channel':channel}
  if syst.lower() == 'mcatnlo': sf =1.29791
  else: sf =1


  print "\nbinName, regPar, regFunction: ", binName, rpName, rpValue, regFunction
  if ensemble: print "\n => running ensembles\n"

  # Get input (signal+background) distributions
  data        = jsondir+'/'+var+'_'+signal+'_'+sel+'_'+binName+'.json'
  reco        = jsondir+'/'+var+'_reco_'+sel+'_'+binName+'.json'
  if bootstrap:
    data        = jsondirB+'/'+var+'_'+signal+'_'+sel+'_'+binName+'.json'
    #reco        = jsondirB+'/'+var+'_reco_'+sel+'_'+binName+'.json'


  #recodist    = json.load(open(reco))

  resmat = jsondirNom+'/'+var+'_resmat_'+sel+'_'+binName+'.json'

  #if 'pol' in var or 'corr' in var:
  #  resmat = jsondirNom+'/'+var+'_resmatobspos0_'+sel+'_'+binName+'.json'


  #resmat = jsondirNom+'/'+var+'_resmat'+signal+'_'+sel+'_'+binName+'.json'



  print "resmat: ",resmat
  print "data: ",data

  if ensemble: resmatErr = jsondirNom+'/'+var+'_resmatErr_'+sel+'_'+binName+'.json'

  if pdf:
    bckg    = jsondir+'/'+var+'_bckg_'+extra+'_'+sel+'_'+binName+'.json'
    bckgNom = jsondirNom+'/'+var+'_bckg_'+sel+'_'+binName+'.json'
  else:
    bckg    = jsondir+'/'+var+'_bckg_'+sel+'_'+binName+'.json'
    bckgNom = jsondirNom+'/'+var+'_bckg_'+sel+'_'+binName+'.json'

  if ensemble: bckgErr   = jsondir+'/'+var+'_bckgErr_'+sel+'_'+binName+'.json'

  objsyst = jsondirNom2+'/SystDict_'+var+'_btag_ALL.json'
  #print objsyst
  #-- Get outFile name in case of ensemble
  if ensemble: outFile = sys.argv[3] # Warning ! Hardcoded !


  pyfbu = fbu.PyFBU()


  truthf = jsondirNom+'/'+var+'_partonic_btag_'+binName+'.json'
  truth  = json.load(open(truthf)) # Get truth dist


  if 'reco' in signal or 'data' in signal: truthforcalib = json.load(open(jsondir+'/'+var+'_partonic_btag_'+binName+'.json'))
  else: truthforcalib = json.load(open(jsondir+'/'+var+'_partonic'+signal+'_btag_'+binName+'.json'))
  if bootstrap:
     truthforcalib = json.load(open(jsondirB+'/'+var+'_partonic'+signal+'_btag_'+binName+'.json'))

  recoforcalib = json.load(open(jsondir+'/'+var+'_'+signal+'_btag_'+binName+'.json'))

  if bootstrap:
     recoforcalib = json.load(open(jsondirB+'/'+var+'_'+signal+'_btag_'+binName+'.json'))
  # -- get reference curvature
  refcurv = getCurvature(truth)

  #print "Reference curvature",refcurv

   # -- Set limit of the hyperbox (sampling area)
  pyfbu.lower = [t/10 for t in truth]
  pyfbu.upper = [t*2 for t in truth]

  # -- Load input distributions
  pyfbu.data           = array(json.load(open(data)))*sf # Get data
  #print data
  pyfbu.response       = json.load(open(resmat)) # Get resp matrix
  #print resmat

  #backgrounddict = json.load(open(bckg),object_hook=_decode_dict)
  #bckgNom        = json.load(open(bckgNom),object_hook=_decode_dict)

  #if fiducial:
  #  pyfbu.background     = {'zjets':bckgNom['zjets'],'zjets_low':bckgNom['zjets_low'],'dibosons':bckgNom['dibosons'],'single_top':bckgNom['single_top'],'fiducial':bckgNom['fiducial'],'fakes':bckgNom['fakes'],'ttv':bckgNom['ttv']}
  #  pseudodata = array(pyfbu.data)+array(bckgNom['dibosons'])+array(bckgNom['zjets'])+array(bckgNom['zjets_low'])+array(bckgNom['single_top'])+array(bckgNom['fakes'])+array(bckgNom['fiducial'])+array(bckgNom['ttv'])
  #else:
  #  pyfbu.background     = {'zjets':bckgNom['zjets'],'zjets_low':bckgNom['zjets_low'],'dibosons':bckgNom['dibosons'],'single_top':bckgNom['single_top'],'fakes':bckgNom['fakes'],'ttv':bckgNom['ttv']}
  #  pseudodata           = array(pyfbu.data)+array(backgrounddict['dibosons'])+array(backgrounddict['zjets'])+array(backgrounddict['zjets_low'])+array(backgrounddict['single_top'])+array(backgrounddict['fakes'])+array(backgrounddict['ttv']) # How to add JSON files

  #pseudodata = array(pyfbu.data)
  #pyfbu.data = pseudodata

  #if fiducial:
  #   fiducial_fraction =  array(bckgNom['fiducial'])/(array(recodist)+array(bckgNom['fiducial']))
  #   pyfbu.background['fiducial'] = (array(pyfbu.data)-(array(bckgNom['dibosons'])+array(bckgNom['zjets'])+array(bckgNom['zjets_low'])+array(bckgNom['ttv'])+array(bckgNom['single_top'])+array(bckgNom['fakes'])))*fiducial_fraction

  # -- Set background to nominal
  #if fiducial: pyfbu.backgroundsyst = {'zjets':0.06,'zjets_low':0.3,'single_top':0.07,'dibosons':0.05,'fiducial':0.,'fakes':0.,'ttv':0.02}
  #else: pyfbu.backgroundsyst = {'zjets':.06,'zjets_low':0.3,'single_top':0.07,'dibosons':0.05,'fakes':0.0,'ttv':0.02}
  #if fiducial: pyfbu.backgroundsyst = {'zjets':0.0,'zjets_low':0.0,'single_top':0.0,'dibosons':0.0,'fiducial':0.,'fakes':0.,'ttv':0.}
  #else: pyfbu.backgroundsyst = {'zjets':.0,'zjets_low':0.0,'single_top':0.0,'dibosons':0.0,'fakes':0.,'ttv':0.}
  #pyfbu.backgroundsyst = {'zjets':0.0,'zjets_low':0.0,'single_top':0.0,'dibosons':0.0,'fakes':0.}
  #print json.load(open(objsyst),object_hook=_decode_dict)

  #nuisance_dict = json.load(open(objsyst),object_hook=_decode_dict)
  #print objsyst
  #nuisance_dict = cleanNuissance(nuisance_dict,'JesEffectiveStat1'.lower())
  #addUnc(nuisance_dict,'fakesNorm',fiducial)

  #plotInput(recodist,array(backgrounddict['dibosons'])+array(backgrounddict['zjets'])+array(backgrounddict['zjets_low'])+array(backgrounddict['single_top'])+array(backgrounddict['fakes'])+array(backgrounddict['ttv']), pyfbu.data)
  #pyfbu.objsyst = nuisance_dict


  pyfbu.name = jsondirNom
  # -- Set monitoring
  pyfbu.monitoring = False
  # -- Set N_mcmc
  pyfbu.nMCMC = 200000
  pyfbu.nBurn= 20000

  # -- regularization
  if rpName != "noReg":
    if DEBUG: print "setting up the regularization"
    from fbu import Regularization
    if   regFunction == 'Tikhonov':
      pyfbu.regularization = Regularization('Tikhonov',parameters=[{'refcurv':refcurv,'alpha':rpValue}])

    elif regFunction == 'Entropy':
      pyfbu.regularization = Regularization('Entropy' ,parameters=[{'alpha':rpValue}])
    else:
      print "unknown regularization function: ", regFunction

  if ensemble:
    ninit = int(sys.argv[1])
    nfin  = int(sys.argv[2])+1
  else:
    ninit = 0
    nfin  = 1


  nominaldata = pyfbu.data


  for i_seed in xrange(ninit,nfin):

    if ensemble:
      from numpy import random
      random.seed(i_seed)

      pyfbu.data = random.poisson(nominaldata)


    #   init_resmat    = json.load(open(resmat))
    #   init_resmatErr = json.load(open(resmatErr))
    #   for i in xrange(0,len(pyfbu.response)):
    #     for j in xrange(0,len(pyfbu.response)):
    #       if ttbarAsym:
    #         pyfbu.response[i][j] = random.normal(init_resmat[i][j],init_resmatErr[i][j])
    #       else:
    #         if i == j: pyfbu.response[i][j] = random.normal(init_resmat[i][j],init_resmatErr[i][j])





    # -- Run the unfolding
    pyfbu.run()

    trace = pyfbu.trace

    ntrace= pyfbu.nuisancestrace

    #if not pdf:

    #   correlation = {'data':[],'name':[]}

    #   for key, value in ntrace.iteritems():
    #     correlation['data'].append(value.tolist())
    #     correlation['name'].append(key)

    #   with open(jsondirNO+'/'+var+'_'+signal+'_correlation.json','w') as outfilet:
    #     json.dump(correlation,outfilet)

    #   marg = {}
    #   for key, value in ntrace.iteritems():
    #      marg[key] = [np.mean(value),np.std(value)]
    #   with open(jsondirNO+'/'+var+'_'+signal+'_marginalization.json','w') as outfilet:
    #     json.dump(marg,outfilet)



    truthObs = computeObs(var,truth,binning)
    truthObsCalib = computeObs(var,truthforcalib,binning)
    unfObs = [computeObs(var,bins,binning) for bins in zip(*trace)]

    # from binningOptimization import binnings_dict
    # bins = binnings_dict(var)[binName]
    # binX    = []
    # binXErr = []
    # binWidth = []
    # for i in xrange(len(bins)-1):
    #   binX.append( (bins[i+1] + bins[i])/2. )
    #   binXErr.append( (bins[i+1] - bins[i])/2. )
    #   binWidth.append(abs((bins[i+1] - bins[i])))
    #
    # binContents = []
    # binErr      = []
    # nbins = len(trace)
    # for i in xrange(0,nbins):
    #   meanBin = np.mean(trace[i])
    #   rmsBin  = np.std(trace[i])
    #   #if 'pol' in var:  meanBin =meanBin / binWidth[i]
    #
    #   binContents.append(meanBin)
    #   binErr.append(rmsBin)
    #
    # unfObs =   computeObs(var,binContents,binning)

    if bootstrap:
        jsondir    = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos,'syst':syst.lower()}


    if ensemble:
      firstEnsSyst  = False
      lastEnsSyst   = False
      if i_seed==ninit: firstEnsSyst  = True
      if i_seed==nfin:  lastEnsSyst   = True
      write2FileForEnsemblesSyst(unfObs,outFile,binName,rpName,regFunction,signal,firstEnsSyst,lastEnsSyst,'stat')
      dyInfoEnsembles(trace,truthObs, var,'stat',firstEnsSyst,lastEnsSyst)
      plot_distribution_ensembles(trace, truth, var,'stat')

      #plotInputEnsembles(recodist,[] pyfbu.data,'stat')


      print "truth: ",truthObsCalib if 'obs' in signal else truthObs
      print "unfolded: ", np.mean(unfObs),'+-',np.std(unfObs)
    else:

      #if 'obs' in signal or 'protos' in signal:
      #   write2FileForCalib(truthObsCalib,unfObs,signal,binName,rpName,regFunction,bootstrap)
         #if bootstrap:write2FileForCalib(truthObsCalib,unfObs,signal,binName,rpName,regFunction, bootstrap)

      traceFile = jsondir+'/trace_'+binName+'_'+regFunction+"_"+rpName+"_"+var
      import numpy
      numpy.save(traceFile, trace)

      plotUnfoldedEvents(trace)


      writeUnfoldInfo(unfObs,syst,binName,rpName)
      #printAndPlotUnfoldInfo(unfObs,truthObs)
      #if 'obs' in signal or 'protos' in signal:
      #    plot_distribution(trace, truthforcalib, var)
      #else:
      #    plot_distribution(trace, truth, var)

      print "truth: ",truthObsCalib if 'obs' in signal else truthObs
      print "unfolded: ", np.mean(unfObs),'+-',np.std(unfObs)
      print truthforcalib
    #   if 'obs' in signal:
    #      write2FileForCalib(truthACCalib,unfAC,signal,binName,rpName,regFunction,binsX)
    #      write2FileForCalibReco(truthACCalib,recocalib,signal,binName,rpName,regFunction,binsX)
    #      write2FileForCalibNevent(trace,truthforcalib,signal,binName,rpName,regFunction,binsX)

      #if regFunction == 'Tikhonov': plotCurvature(trace,refcurv) #fix for 2D???
