from Config import Configuration
import utils
import json
import numpy as np
import sys


obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{k}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{k}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{n}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{n}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{k}cos\theta_{-}^{k}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{n}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{n}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{n}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{n}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{n}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{n}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{r}$"

def plotHistogram(values,title):

    import matplotlib.pyplot as plt
    f = plt.figure(figsize=(14, 6))
    ax = plt.subplot(111)
    plt.rc('text', usetex=True)
    first = True
    n, bins, patches = ax.hist(values)

    plt.xlabel(title)
    plt.ylabel("Events")
    plt.title(title+' distribution')


    filename = 'systematics/plots/%(var)s_%(modelling)s.eps' %{'var':var,'modelling':title.replace(' ','_').replace('/','_').lower()}
    plt.savefig(filename)
    plt.close()


def createTable(results,filename,var):

  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{l|c|c|}
\hline
Source of systematic uncertainty & Fiducial Region & Full Phase Space \\\\ \hline \hline
"""
  footer = """
\hline
\end{tabular}
\caption{Modelling systematic uncertanty contributions of the different sources for %(var)s. }
\end{table}
\end{center}
""" % {'var':obs_label[var]}
  table=''

  mc_full         = results['full']['mc']
  shower_full     = results['full']['shower']
  isrfsr_full     = results['full']['isrfsr']
  color_full      = results['full']['color']
  underlying_full = results['full']['underlying']
  pdf_full        = results['full']['pdf']
  #fbu_full        = results['full']['fbu'] 
  #stat_full       = results['full']['stat']



  mc_fiducial         = results['fiducial']['mc']
  shower_fiducial     = results['fiducial']['shower']
  isrfsr_fiducial     = results['fiducial']['isrfsr']
  color_fiducial      = results['fiducial']['color']
  underlying_fiducial = results['fiducial']['underlying']
  pdf_fiducial        = results['fiducial']['pdf']
  #fbu_fiducial        = results['fiducial']['fbu']
  #stat_fiducial       = results['fiducial']['stat']


  #FOR TOTAL
  t_mc         = max(abs(np.mean(mc_full)),abs(np.std(mc_full)))
  t_shower     = max(abs(np.mean(shower_full)),abs(np.std(shower_full)))
  t_isrfsr     = max(abs(np.mean(isrfsr_full)),abs(np.std(isrfsr_full)))
  t_color      = max(abs(np.mean(color_full)),abs(np.std(color_full)))
  t_underlying = max(abs(np.mean(underlying_full)),abs(np.std(underlying_full)))

  t_mc_fiducial         = max(abs(np.mean(mc_fiducial)),abs(np.std(mc_fiducial)))
  t_shower_fiducial     = max(abs(np.mean(shower_fiducial)),abs(np.std(shower_fiducial)))
  t_isrfsr_fiducial     = max(abs(np.mean(isrfsr_fiducial)),abs(np.std(isrfsr_fiducial)))
  t_color_fiducial      = max(abs(np.mean(color_fiducial)),abs(np.std(color_fiducial)))
  t_underlying_fiducial = max(abs(np.mean(underlying_fiducial)),abs(np.std(underlying_fiducial)))



  total_full = np.sqrt(t_mc*t_mc + t_isrfsr*t_isrfsr + t_shower*t_shower + t_color*t_color+t_underlying*t_underlying+pdf_full*pdf_full)
  total_fiducial = np.sqrt(t_mc_fiducial*t_mc_fiducial + t_isrfsr_fiducial*t_isrfsr_fiducial + t_shower_fiducial*t_shower_fiducial + t_color_fiducial*t_color_fiducial+t_underlying_fiducial*t_underlying_fiducial+pdf_fiducial*pdf_fiducial)


  

  table = table + 'MC generator   & $%.5f \pm %.5f$ & $%.5f \pm %.5f$  \\\\ \hline \n' % (abs(np.mean(mc_fiducial)),abs(np.std(mc_fiducial)),abs(np.mean(mc_full)),abs(np.std(mc_full)))
  table = table + 'Parton Shower/Hadronization   & $%.5f \pm %.5f$  & $%.5f \pm %.5f$ \\\\ \hline \n' % (abs(np.mean(shower_fiducial)),abs(np.std(shower_fiducial)),abs(np.mean(shower_full)),abs(np.std(shower_full)))
  table = table + 'ISR/FSR & $%.5f \pm %.5f$ & $%.5f \pm %.5f$  \\\\ \hline \n' % (abs(np.mean(isrfsr_fiducial)),abs(np.std(isrfsr_fiducial)),abs(np.mean(isrfsr_full)),abs(np.std(isrfsr_full)))
  table = table + 'Color Reconnection & $%.5f \pm %.5f$ & $%.5f \pm %.5f$  \\\\ \hline \n' % (abs(np.mean(color_fiducial)),abs(np.std(color_fiducial)),abs(np.mean(color_full)),abs(np.std(color_full)))
  table = table + 'Underlying Event & $%.5f \pm %.5f$ & $%.5f \pm %.5f$  \\\\ \hline \n' % (abs(np.mean(underlying_fiducial)),abs(np.std(underlying_fiducial)),abs(np.mean(underlying_full)),abs(np.std(underlying_full)))
  table = table + 'PDF & $ \pm %.5f$ & $ \pm %.5f$  \\\\ \hline \hline \n' % (abs(pdf_fiducial),abs(pdf_full))

  #table = table + 'Non-Closure FBU & $ \pm %.5f$ & $ \pm %.5f$  \\\\ \hline \hline \n' % (abs(fbu_fiducial),abs(fbu_full))

  table = table + 'Total & $\pm %.5f$ & $\pm %.5f$  \\\\ \hline \n' % (total_fiducial,total_full)


  plotHistogram(mc_full,'MC Generator Full')
  plotHistogram(shower_full,'Parton Shower Full')
  plotHistogram(isrfsr_full,'ISR/FSR Full')
  plotHistogram(color_full,'Color Reconnection Full')
  plotHistogram(underlying_full,'Underlying Event Full')

  plotHistogram(mc_fiducial,'MC Generator Fiducial')
  plotHistogram(shower_fiducial,'Parton Shower Fiducial')
  plotHistogram(isrfsr_fiducial,'ISR/FSR Fiducial')
  plotHistogram(color_fiducial,'Color Reconnection Fiducial')
  plotHistogram(underlying_fiducial,'Underlying Event Fiducial')

  f1 = open(filename,'w')
  f1.write(header+table+footer)
  f1.close()

  return total_fiducial,total_full





if __name__ == '__main__':


    outDir      = Configuration.outputDirBt
    outDirNom   = Configuration.outputDirt
    outDirFiducial      = Configuration.outputDirB
    outDirFiducialNom   = Configuration.outputDir


    regions      = ['full','fiducial']

    variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                   'helcorr',\
                   'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                   'transheldiff']

    #variables = ['rpol_plus']#sys.argv[1].split()



    fiducial    = utils.setFiducial(sys.argv)


    if fiducial:
        outDir = Configuration.outputDirB
        outDirNom   = Configuration.outputDir

    systs =['nominal','perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia']


    results = {}

    replicas = 100

    for var in variables:

        import binningOptimization
        binnings = binningOptimization.binnings_dict(var)
        binName  = binnings.keys()[0]

        if 'corr' in var or 'diff' in var or 'sum' in var:
           factor = -9
        else:
           factor = 1

        for region in regions:

            if region == 'full':
                outDir      = Configuration.outputDirBt
                outDirNom   = Configuration.outputDirt

            else:

                outDir      = Configuration.outputDirB
                outDirNom   = Configuration.outputDir



            unfolded_data = json.load(open(outDirNom+'/%(var)s/nominal/ALL/optimization/Unfolded_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))


            results[region] = {
                    'mc' : [],
                    'isrfsr': [],
                    'shower': [],
                    'underlying':[],
                    'color':[],

            }



            offsets = []

            try:

               results[region]['pdf'] = json.load(open(outDirNom+'/%(var)s/nominal/ALL/PDFError_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))*factor
            except:
               print "error:",var,region 
               results[region]['pdf'] = 0

            #try:

            #   print outDirNom+'/%(var)s/nominal/ALL/optimization/CalibrationResult_%(var)s_%(binName)s_Tikhonov_rpZero.json' % {'var':var,'binName':binName}

            #   fbuFile = json.load(open(outDirNom+'/%(var)s/nominal/ALL/optimization/CalibrationResult_%(var)s_%(binName)s_Tikhonov_rpZero.json' % {'var':var,'binName':binName}))
            #   print fbuFile

            #   results[region]['fbu'] = (fbuFile[0][0]*unfolded_data[0] +  fbuFile[1][0])*factor
            #   results[region]['stat'] =  fbuFile[1][1]*factor

            #except:

            #   print "error fbu:",var,region
            #   results[region]['fbu'] = 0
            #   results[region]['stat'] =  0;
 

            print "---------------%s----------------" %(var)
            for i in xrange(replicas):

                try:
                   nominalCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'nominal','pos':i,'binName':binName}))
                   mcatnloCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'mcatnlo','pos':i,'binName':binName}))
                   herwigCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'herwig','pos':i,'binName':binName}))
                   perugiaCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'perugia','pos':i,'binName':binName}))
                   fastpythiaCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'fastpythia','pos':i,'binName':binName}))
                   perugialocrCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'perugialocr','pos':i,'binName':binName}))
                   perugiampihiCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'perugiampihi','pos':i,'binName':binName}))
                   radhiCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'radhi','pos':i,'binName':binName}))
                   radloCalib = json.load(open(outDir+'/%(var)s/%(syst)s/ALL/optimization/boostrap/%(pos)d/Calibration_%(var)s_%(binName)s_Tikhonov_rpZero.json'%{'var':var,'syst':'radlo','pos':i,'binName':binName}))
                   


                   if np.isnan(mcatnloCalib[0]): continue
                   if np.isnan(herwigCalib[0]): continue
                   if np.isnan(fastpythiaCalib[0]): continue
                   if np.isnan(perugiaCalib[0]): continue
                   if np.isnan(perugialocrCalib[0]): continue 
                   if np.isnan(perugiampihiCalib[0]): continue
                   if np.isnan(radloCalib[0]): continue  
                   if np.isnan(radhiCalib[0]): continue 


                   if abs(mcatnloCalib[0]) > 1.25: continue
                   if abs(herwigCalib[0] ) > 1.25: continue
                   if abs(fastpythiaCalib[0]) > 1.25: continue
                   if abs(perugiaCalib[0] )   >1.25: continue

                   if abs(perugialocrCalib[0] )    >1.25: continue
                   if abs(perugiampihiCalib[0] )   >1.25: continue

                   if abs(radhiCalib[0]   ) >1.25: continue
                   if abs(radloCalib[0]   ) >1.25: continue


                   #We throw away the reeeeally bad results -> problems in the fitting
                   if abs(mcatnloCalib[1]) > 0.310: continue
                   if abs(herwigCalib[1] ) > 0.310: continue
                   if abs(fastpythiaCalib[1]) > 0.310: continue
                   if abs(perugiaCalib[1])    >0.310: continue


                   if abs(perugialocrCalib[1])    >0.310: continue
                   if abs(perugiampihiCalib[1])    >0.310: continue
                   if abs(radloCalib[1])    >0.310: continue
                   if abs(radhiCalib[1])    >0.310: continue

                   #if abs((radhiCalib[0]*unfolded_data[0] + radhiCalib[1] )/2. - (radloCalib[0]*unfolded_data[0] + radloCalib[1])/2.) > 50:     
                   print radhiCalib[0],unfolded_data[0],radhiCalib[1],radloCalib[0],unfolded_data[0],radloCalib[1]  
                   results[region]['mc'].append(         ((mcatnloCalib[0]*unfolded_data[0] + mcatnloCalib[1] ) - (herwigCalib[0]*unfolded_data[0] + herwigCalib[1])  )*factor)
                   results[region]['isrfsr'].append(     ((radhiCalib[0]*unfolded_data[0] + radhiCalib[1] )/2. - (radloCalib[0]*unfolded_data[0] + radloCalib[1])/2.  )*factor)
                   results[region]['shower'].append(     ((fastpythiaCalib[0]*unfolded_data[0] + fastpythiaCalib[1] ) - (herwigCalib[0]*unfolded_data[0] + herwigCalib[1]) )*factor)
                   results[region]['underlying'].append( ((perugiampihiCalib[0]*unfolded_data[0] + perugiampihiCalib[1] ) - (perugiaCalib[0]*unfolded_data[0] + perugiaCalib[1]) )*factor)
                   results[region]['color'].append(      ((perugialocrCalib[0]*unfolded_data[0] + perugialocrCalib[1] ) - (perugiaCalib[0]*unfolded_data[0] + perugiaCalib[1]) )*factor)
                except:

                    print "Replica Missing:",i
                    continue

            #np. 

        total = createTable(results,'systematics/tables/%(var)s_modeling.tex' %{'var':var},var)

        outDir              = Configuration.outputDirBt
        outDirNom           = Configuration.outputDirt
        outDirFiducial      = Configuration.outputDirB
        outDirFiducialNom   = Configuration.outputDir

        totalFile         = outDirNom+'/%(var)s/nominal/ALL/modeling.json' %{'var':var}
        totalFileFiducial = outDirFiducialNom+'/%(var)s/nominal/ALL/modeling.json' %{'var':var}

        print totalFileFiducial
        print totalFile

        with open(totalFile,'w') as f:
            json.dump(total[1],f)

        with open(totalFileFiducial,'w') as f:
            json.dump(total[0],f)
