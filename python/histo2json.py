#!/bin/env python

import json
import time
import os
import sys
import itertools
import gc
from array import array
from ROOT import *
import ROOT as r # variable r is 'ROOT'

import utils
import Rebin
from utils import writefile
from Config import Configuration, getSampleLists, getNtupleDir, getProtosSamples2Json, getLinearSamples2Json, getFitProtosSamples2Json, getObsSamples2Json
r.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

Debug        = 10
mybins       = None
mybins_truth = None
bound        = None
bound_truth  = None
Ndiff        = None
dorebin      = False

def histo2list(histo):
    nbinsx = histo.GetNbinsX()
    nbinsy = histo.GetNbinsY()
    binlist = [histo.GetBinContent(x,y) for y in xrange(1,nbinsy+1) for x in xrange(1,nbinsx+1)]
    return binlist

def histoErr2list(histo):
    nbinsx = histo.GetNbinsX()
    nbinsy = histo.GetNbinsY()
    binlist = [histo.GetBinError(x,y) for y in xrange(1,nbinsy+1) for x in xrange(1,nbinsx+1)]
    return binlist

def getBckgBootstrap(hfile,i,binning):
    bckgdict = {}
    bckgdictErr = {}
    for sample in samples_BCKG:
        samp = sample.lower()
        hnominal = TH2DBootstrap()

        hfile.GetObject(samp, hnominal)

        histo = TH2D(hnominal.GetReplica(i))
        Rebin.myrebinX(histo, binning)
        bins = histo2list(histo)
        binsErr = histoErr2list(histo)
        bckgdict[samp] = bins
        bckgdictErr[samp] = binsErr
        histo.Reset()
        del hnominal
        del histo
    return bckgdict, bckgdictErr


def getBckg(hfile,binning):
    bckgdict = {}
    bckgdictErr = {}
    for sample in samples_BCKG:
        samp    = sample.lower()
        histo   = hfile.Get(samp)
        histo   = Rebin.rebin(histo, binning)
        bins    = histo2list(histo)
        binsErr = histoErr2list(histo)
        bckgdict[samp] = bins
        bckgdictErr[samp] = binsErr
    return bckgdict, bckgdictErr


def getBckgPDF(hfile,pdfType,i,binning):
    bckgdict = {}
    bckgdictErr = {}
    for sample in samples_BCKG+['bckg']:
        samp = sample.lower()
        histo = hfile.Get(samp+'_'+pdfType+'_'+str(i))
        if dorebin: histo = Rebin.rebin(histo, mybins)
        bins = histo2list(histo)
        binsErr = histoErr2list(histo)
        bckgdict[samp] = bins
        bckgdictErr[samp] = binsErr
    return bckgdict, bckgdictErr

def getData(hfile,dataname,binning):
    histo = hfile.Get(dataname)
    histo = Rebin.rebin(histo, binning)
    bins = histo2list(histo)
    binsErr = histoErr2list(histo)
    return bins, binsErr


def getDataBootstrap(hnominal,i,binning):
    #hnominal = TH2DBootstrap()
    #hnominal = hfile.Get(dataname)
    #print hnominal.GetReplica(i)
    #hfile.GetObject(dataname, hnominal)
    histo = hnominal.GetReplica(i)
    histo = Rebin.rebin(histo, binning)
    bins = histo2list(histo)
    binsErr = histoErr2list(histo)
    histo.Reset()

    histo.IsA().Destructor( histo )
    return bins, binsErr




def getDataPDF(hfile,dataname,binning):
    histo = hfile.Get(dataname)
    histo = Rebin.rebin(histo, binning)
    bins = histo2list(histo)
    binsErr = histoErr2list(histo)
    return bins, binsErr


def getEff(resmat,htruth,y):
    #get coordinates in 2D truth hist
    #note: resp matrix is 2D: (nXtruth*nYtruth) x (nXtruth*nYtruth) --> y = (yy-1)*Nbins_dy + xx
    nxx = htruth.GetNbinsX()
    xx = y % nxx
    if xx == 0: xx = nxx
    yy = int((y-1)/nxx) + 1
    return resmat.Integral(0,-1,y,y) / htruth.GetBinContent(xx,yy)
    #used for 1D:
    #return resmat.Integral(0,-1,y,y)/(htruth.GetBinContent(xx))

def getMigration(hfile,binning, name='resmat'):
    resmat = hfile.Get(name).Clone()
    resmat = Rebin.rebin2D(resmat, binning)

    htruth = hfile.Get('partonic')
    htruth = Rebin.rebin(htruth, binning)

    if Debug > 2:
        print 'htruth: '
        htruth.Print()

    nbinsx = resmat.GetNbinsX() # reconstructed distribution
    nbinsy = resmat.GetNbinsY() # truth distribution
    assert(nbinsx==nbinsy),'getMigration::error not matching dimensions'

    migration    = []
    migrationErr = []
    acceff       = []
    for y in xrange(1,nbinsy+1):
        truthbin = []
        truthbinErr = []
        f = r.TFile("a.root","recreate")
        resmat.Write()
        f.Close()
        norm = resmat.Integral(0,-1,y,y) # integral of the rows (along de X axis)
        #print  norm
        for x in xrange(1,nbinsx+1):
            value  = resmat.GetBinContent(x,y)/norm
            effbin = getEff(resmat,htruth,y)
            value  = value*effbin
            truthbin.append(value)
            from math import sqrt
            def resMatError(resmat,htruth, x,y):
              A  = resmat.GetBinContent(x,y)
              dA = resmat.GetBinError(x,y)
              nxx = htruth.GetNbinsX()
              xx = y % nxx
              if xx == 0: xx = nxx
              yy = int((y-1)/nxx) + 1
              B  = htruth.GetBinContent(xx,yy)
              dB = htruth.GetBinError(xx,yy)
              return sqrt( (dA*dA)/(B*B) + (A/(B*B))*(A/(B*B))*dB*dB )
            valueErr = resMatError(resmat,htruth, x,y)
            truthbinErr.append(valueErr)
        migration.append(truthbin)
        migrationErr.append(truthbinErr)
        acceff.append(effbin)
    return migration, acceff, migrationErr

def getResmat(hfile,binning,name = 'resmat'):
    resmat = hfile.Get(name).Clone()
    resmat = Rebin.rebin2D(resmat, binning)

    nbinsx = resmat.GetNbinsX() # reconstructed distribution
    nbinsy = resmat.GetNbinsY() # truth distribution
    assert(nbinsx==nbinsy),'getMigration::error not matching dimensions'
    migration    = []


    for y in xrange(1,nbinsy+1):
        truthbin = []
        truthbinErr = []
        for x in xrange(1,nbinsx+1):
            value  = resmat.GetBinContent(x,y)
            truthbin.append(value)
        migration.append(truthbin)


    return migration

def writejsons(var='map_dy',binName='200Bin', binning=[], sel='btag' ): # main function !!


    if pdf: jsondir = outDir+'/%(var)s/%(systname)s/%(channel)s/'%{'var':var,'systname':pdfType.lower(),'channel':channel}
    else: jsondir = outDir+"/%(var)s/%(systname)s/%(channel)s/"%{'var':var,'systname':syst.lower(),'channel':channel}


    if Debug > 0: print 'jsondir: ',jsondir

    filename = jsondir+'%(var)s_%(sel)s_%(binName)s.root'%{'var':'map_incl', 'sel':sel,'binName':binName}

    if optimization: filename = jsondir+'%(var)s_%(sel)s_%(binName)s.root'%{'var':'map_incl', 'sel':sel,'binName':'200Bin'}

    hfile = r.TFile(filename)

    if optimization:
        jsondir = jsondir + 'optimization/'
        if not os.path.isdir(jsondir): os.system("mkdir -p %s"%jsondir)


    if pdf: datanames = ['reco','partonic','bckg']
    else: datanames = ['data','reco','partonic']
    if reweight:
      if 'linear'    in rewType: datanames = getLinearSamples2Json()
      if 'protos'    in rewType: datanames = getProtosSamples2Json()
      if 'fitprotos' in rewType: datanames = getFitProtosSamples2Json()
      if 'obs'       in rewType: datanames = ['obspos0'] #getObsSamples2Json(var)

    if Debug > 1:  print 'histfile, binName: ',hfile, binName


    if pdf:
        pdfdict   = {"CT10nlo":53, "MSTW2008nlo68cl":41,"NNPDF23_nlo_as_0119":101}
        for dataname in datanames:
            print 'dataname: ',dataname
            for i in xrange(pdfdict[pdfType]):
              pdfname = dataname+'_'+pdfType+'_'+str(i)
              print  jsondir+var+'_'+pdfname+'_'+''.join(sels)+'_'+binName
              data, dataErr = getData(hfile,pdfname,binning)
              writefile(jsondir+var+'_'+pdfname+'_'+''.join(sels)+'_'+binName,data)
              writefile(jsondir+var+'_'+pdfname+'Err_'+''.join(sels)+'_'+binName,dataErr)

        for i in xrange(pdfdict[pdfType]):
          bckgs, bckgsErr = getBckgPDF(hfile,pdfType,i,binning)
          #print jsondir+var+'_bckg_'+pdfType+'_'+str(i)+'_'+''.join(sels)+'_'+binName,bckgs
          writefile(jsondir+var+'_bckg_'+pdfType+'_'+str(i)+'_'+''.join(sels)+'_'+binName,bckgs)
          writefile(jsondir+var+'_bckgErr_'+pdfType+'_'+str(i)+'_'+''.join(sels)+'_'+binName,bckgsErr)

    else:
         # get data, signal, partonic, etc. as defined above
         if bootstrap:
           for dataname in datanames:
              #print dataname,binning
              hnominal = TH2DBootstrap()
              hnominal = hfile.Get(dataname)
              for i in xrange(2000):

                 data, dataErr = getDataBootstrap(hnominal,i,binning)
                 directory =  jsondir+'bootstrap/%d/'%(i)
                 if not os.path.exists(directory):
                    os.makedirs(directory)
                 writefile(directory+var+'_'+dataname+'_'+''.join(sels)+'_'+binName,data)
                 writefile(directory+var+'_'+dataname+'Err_'+''.join(sels)+'_'+binName,dataErr)
              hnominal.IsA().Destructor( hnominal )

        #    datanames = ['partonic']
        #    for dataname in datanames:
        #       print dataname
        #       hnominal = TH2DBootstrap()
        #       hnominal = hfile.Get(dataname)
        #       for i in xrange(2000):
        #          data, dataErr = getDataBootstrap(hnominal,i,binning)
        #          directory =  jsondir+'bootstrap/%d/'%(i)
        #          writefile(directory+var+'_'+dataname+'_'+''.join(sels)+'_'+binName,data)
        #          writefile(directory+var+'_'+dataname+'Err_'+''.join(sels)+'_'+binName,dataErr)
        #       hnominal.IsA().Destructor( hnominal )
         else:

          for dataname in datanames:
            if Debug > 2: print 'dataname: ',dataname
            data, dataErr = getData(hfile,dataname,binning)
            writefile(jsondir+var+'_'+dataname+'_'+''.join(sels)+'_'+binName,data)
            writefile(jsondir+var+'_'+dataname+'Err_'+''.join(sels)+'_'+binName,dataErr)
            if "partonic" in dataname: continue
            resmatname = 'resmat'
            if 'obs': resmatname = 'resmat'+dataname
            print resmatname 
            resmat,acceff,resmatErr = getMigration(hfile,binning,resmatname)
            mig = getResmat(hfile,binning,resmatname)
            writefile(jsondir+var+'_resmat'+dataname+'_btag'+'_'+binName,resmat) # write the list of response matrix
            writefile(jsondir+var+'_resmatErr'+dataname+'_btag'+'_'+binName,resmatErr) # write the list of response matrix
            writefile(jsondir+var+'_acceff'+dataname+'_btag'+'_'+binName,acceff) # write the list of acceptance efficiency
            writefile(jsondir+var+'_mig'+dataname+'_btag'+'_'+binName,mig)

            print jsondir+var+'_resmat'+dataname+''.join(sels)+'_'+binName



          # get all individual backgrounds and total background
          bckgs, bckgsErr = getBckg(hfile,binning)
          writefile(jsondir+var+'_bckg_'+'btag'+'_'+binName,bckgs)
          writefile(jsondir+var+'_bckgErr_'+'btag'+'_'+binName,bckgsErr)



          # get response matrix (migration + sel. eff.)

          resmat,acceff,resmatErr = getMigration(hfile,binning,'resmat')
          mig = getResmat(hfile,binning,'resmat')
          writefile(jsondir+var+'_resmat_'+'btag'+'_'+binName,resmat) # write the list of response matrix
          writefile(jsondir+var+'_resmatErr_'+'btag'+'_'+binName,resmatErr) # write the list of response matrix
          writefile(jsondir+var+'_acceff_'+'btag'+'_'+binName,acceff) # write the list of acceptance efficiency
          writefile(jsondir+var+'_mig_'+'btag'+'_'+binName,mig)

    hfile.Close()
#-----------------
# -- Main code --
#-----------------
if __name__ == '__main__':

    samples,samples_MC,samples_BCKG,samples_data,truthttbar,ttbar = getSampleLists()

    outDir = Configuration.outputDir

    channel             = utils.setChan(sys.argv)
    variable            = utils.setVar(sys.argv)
    reweight, rewType   = utils.setRew(sys.argv)
    syst                = utils.setSyst(sys.argv)
    sels                = 'btag'
    fiducial            = utils.setFiducial(sys.argv)
    bootstrap           = utils.setBootstrap(sys.argv)
    pdf,pdfType         = utils.setPDF(sys.argv)
    optimization        = utils.setBinningOptimization(sys.argv)
    #r.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

    if not fiducial:
       outDir = Configuration.outputDirt

    if bootstrap:


       if fiducial: outDir = Configuration.outputDirB
       else: outDir = Configuration.outputDirBt

    if optimization:

        import binningOptimization

        binnings = binningOptimization.binnings_dict(variable)

        for binName,bins in binnings.iteritems():

            writejsons(variable, binName, bins,'btag')

    else:

        writejsons(variable)
