#!/bin/env python

import os, sys, math, json
import utils
from Functions import runChannel
#from Functions import getNormVar,getNormQCDVar
import Config
import ROOT as r
from multiprocessing import Process,Queue
from time import sleep
from ROOT import *
gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
gROOT.ProcessLine("load_packages()")

Debug = 10


def producehistos(systname,samples,binning,var,lumiweight,rewasymm='', fiducial = False, bootstrap = False, pdf = False, pdfType = "CT10nlo"):

    histodict = dict(zip(mapnames+resmatnames,
                         [dict() for h in mapnames+resmatnames]))



    # set channel list (only use if you want to combine the channels)
    channels = [channel] if 'ALL' not in channel else Config.Configuration.possibleChannels
    if 'ALL' in channels: channels.remove('ALL')

    histos_queue = Queue()

    processes = []

    if Debug > 3: print "Processing Queues"
    for ch in channels:
        syst = systname
        processes.append(Process(target=runChannel,args=(var,ch,
                                                         selection,
                                                         syst,
                                                         samples,
                                                         histos_queue,
                                                         binning,
                                                         lumiweight,
                                                         rewasymm,fiducial,bootstrap,pdf,pdfType)))


    if Debug > 3: print "Starting processes"
    for process in processes:
        process.start()
        print(process, process.is_alive())
    if Debug > 3: print "End starting processes"
    histos_list = []
    # Append the histogramms for each processed channel
    while len(histos_list) < len(channels):
        histos_list.append(histos_queue.get())
        sleep(1)
    # Join the process threaded above
    if Debug > 3: print "Join processes"
    for process in processes:
        process.join()

    #here, we finished parallel processing and have the histos ready
    #if Debug > 3: print "histos_list: ", histos_list

    #get all hists (maps_dy, 2D,...,resp. matrix) for all samples
    for sample in samples:

        alsoresmat = ( ('ttbar' in sample)           or
                       ('obs'  in rewasymm)
                       #('nominal' in systname)
                       )
	#alsoresmat = False #temporary RL

        for ch,histos in zip(channels, histos_list):

            histo = histos[sample]


            hnames = mapnames
            if alsoresmat: hnames = mapnames + resmatnames

            #if Debug > 5: print "hnames: ", hnames

            for hname in hnames:
                hh = getattr(histo,hname)
                if channels.index(ch)==0:
                  histodict[hname][sample] = hh
                else:
                  histodict[hname][sample].Add(hh)

    #print "histodict: ", histodict['map_dy']['ttbar']

    return histodict


def saveHistos(histos,map_name='map_incl',map_resmat=None,var='helpol',systname='nominal',binning='200Bin', bootstrap = False,pdf = False, pdfType = "CT10nlo"):

    samples,samples_MC,samples_BCKG,samples_data,truthttbar,ttbar = Config.getSampleLists(systname)

    histoDir = outDir+"/%(var)s/%(systname)s/%(channel)s/"%{'var':var,'systname':systname.lower(),'channel':channel}
    if pdf:histoDir= outDir+"/%(var)s/%(systname)s/%(channel)s/"%{'var':var,'systname':pdfType.lower(),'channel':channel}
    if not os.path.isdir(histoDir): os.system("mkdir -p %s"%histoDir)

    #output file, separately for different unfolding(1D,2D vs. X) selection and binning
    thefile = r.TFile('%s/%s_%s_%s.root'%(histoDir,map_name,selection,binning),'recreate')

    #write individual samples to one file
    hbckg  = None
    hdata  = None
    htruth = None

    ispecial = ['partonicTtbar','mcatnlo','perugiampihi','perugialocr','topmass_167.5','topmass_170','topmass_175','topmass_177.5','topmass2_167.5','topmass2_170','topmass2_175','topmass2_177.5','radhi','radlo','herwig','fastpythia','powhegpythia','perugia','alpgen']

    if pdf:
       hreco = histos[ ttbar ]
       for i in xrange(len(hreco)):
         hreco[i].Write("reco_"+pdfType+'_'+str(i))

       for n in ispecial:
         if n in samples:
           htruth = histos[n]
           for i in xrange(len(htruth)):
             htruth[i].Write("partonic_"+pdfType+'_'+str(i))


       for sample in samples_MC:
           histo = histos[sample]
           for i in xrange(len(histo)):
             if ttbar not in sample:
               if hbckg is None:
                   hbckg = histo[i].Clone(var)
               else:
                   hbckg.Add(histo[i])
             if hbckg:
               hbckg.Write('bckg_'+pdfType+'_'+str(i))

       #individual backgrounds
       for sample in samples_BCKG:
           hbckg = histos[sample]
           for i in xrange(len(hbckg)):

             hbckg[i].Write('%s_%s_%d'%(sample.lower(),pdfType,i))

    else:


       hdata = histos[ samples_data[0] ]
       if hdata:
           hdata.Write('data')

       #SIGNAL
       hreco = histos[ ttbar ]
       hreco.Write('reco')

       #TOTAL BCKG
       #make histogram for sum of all backgrounds
       for sample in samples_MC:
           histo = histos[sample]
           if ttbar not in sample:
               if hbckg is None:
                   hbckg = histo.Clone(var)
               else:
                   if not bootstrap: hbckg.Add(histo)
       if hbckg:
           hbckg.Write('bckg')

       #individual backgrounds
       for sample in samples_BCKG:
           hbckg = histos[sample]
           hbckg.Write('%s'%sample.lower())

       #TRUTH SIGNAL
       #if 'nominal' in systname:
       for n in ispecial:
         if n in samples:
           print n
           htruth = histos[n]
           if htruth:
               htruth.Write('partonic')

       #RESPONSE MATRIX
       if map_resmat:
           map_resmat.Write('resmat')

    thefile.Close()


def combinehisto(histos,resmat,map_name,var,binning,asymmrew,systname='nominal',useBKG = True, bootstrap = False, pdf = False, pdfType = "CT10nlo"):


    histoDir = outDir+"/%(var)s/%(systname)s/%(channel)s/"%{'var':var,'systname':systname.lower(),'channel':channel}
    if pdf:histoDir= outDir+"/%(var)s/%(systname)s/%(channel)s/"%{'var':var,'systname':pdfType.lower(),'channel':channel}
    if not os.path.isdir(histoDir): os.system("mkdir -p %s"%histoDir)

    samples,samples_MC,samples_BCKG,samples_data,truthttbar,ttbar = Config.getSampleLists(systname)
    samples2rew = [ttbar] + truthttbar + [samples_BCKG[5]]

    #update histogram file
    thefile = r.TFile('%s/%s_%s_%s.root'%(histoDir,map_name,selection,binning),'update')
    print '%s/%s_%s_%s.root'%(histoDir,map_name,selection,binning)
    ispecial = ['partonicTtbar','mcatnlo','perugiampihi','perugialocr','topmass_167.5','topmass_170','topmass_175','topmass_177.5','topmass2_167.5','topmass2_170','topmass2_175','topmass2_177.5','radhi','radlo','herwig','fastpythia','powhegpythia','perugia','alpgen']
    #adding histogram for sum of reweighted signal + default' bckg
    if pdf:
       hreco = histos[ ttbar ]
       for i in xrange(len(hreco)):
         print hreco[i],asymmrew+'_'+pdfType+'_'+str(i),hreco[i].GetEntries()
         hreco[i].Write(asymmrew+'_'+pdfType+'_'+str(i))

       for n in ispecial:
         if n in samples2rew:
           htruth = histos[n]
           for i in xrange(len(htruth)):
             #print htruth[i],"partonic"+asymmrew+'_'+pdfType+'_'+str(i)
             htruth[i].Write("partonic"+asymmrew+'_'+pdfType+'_'+str(i))

    else:
       if 'ttbar' in samples2rew:
         hbckg = thefile.Get('bckg')
         hrewdata = histos[ ttbar ]
         if useBKG:
           if not bootstrap: hrewdata.Add(hbckg)
         hrewdata.Write(asymmrew)

       if 'fiducial' in samples2rew:
          hrewdata = histos[ samples_BCKG[5] ]
          hrewdata.Write(asymmrew+"fiducial")
          hrewtruth = histos["fiducialp"]
          hrewtruth.Write('partonic'+asymmrew+'fiducial')

       #adding reweighted truth
       for n in ispecial:
        if n in samples2rew:
         hrewtruth = histos[n]
         hrewtruth.Write('partonic'+asymmrew)


       if resmat:
            resmat.Write('resmat'+asymmrew)

    thefile.Close()


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

    r.gROOT.SetBatch(1) # Avoid histograms

    mapnames,resmatnames = Config.getNames()
    outDir = Config.Configuration.outputDir

    channel             = utils.setChan(sys.argv)
    selection           = 'btag'
    variable            = utils.setVar(sys.argv)
    reweight, rewType   = utils.setRew(sys.argv)
    systematics         = utils.setSystematics(sys.argv)
    lumiW               = utils.setLumiWeight(sys.argv)
    useBKG              = utils.setUseBackground(sys.argv) # default value it's TRUE
    fiducial            = utils.setFiducial(sys.argv)
    bootstrap           = utils.setBootstrap(sys.argv)
    rewSt               = utils.setRewSignal(sys.argv,variable)
    pdf,pdfType         = utils.setPDF(sys.argv)
    print reweight, rewType, systematics
    if bootstrap:

       if fiducial: outDir = Config.Configuration.outputDirB
       else: outDir = Config.Configuration.outputDirBt

    else:
       if fiducial: outDir = Config.Configuration.outputDir
       else: outDir = Config.Configuration.outputDirt

    print "Output directory: ",outDir,pdf,pdfType



    from bindict import binnings_dict
    binnings_dict = binnings_dict(variable)

    if reweight:
        syst = systematics
        rewhistos = {}
        samples,samples_MC,samples_BCKG,samples_data,truthttbar,ttbar = Config.getSampleLists(syst)
        samples2rew = [ttbar] + truthttbar+['fiducialp']

        rewSamples = rewSt
        if 'protos'    in rewType:
           if rewSt == False: rewSamples = Config.getProtosSamples()
        if 'fitprotos' in rewType:
           if rewSt == False: rewSamples = Config.getFitProtosSamples()
        if 'linear'    in rewType:
           if rewSt == False: rewSamples = Config.getLinearSamples()
        if 'obs'       in rewType:
            if rewSt == False: rewSamples = Config.getObsSamples(variable)


        for asymm in rewSamples:
            for binning in binnings_dict:
                if Debug > 1: print 'binning:',binning,binnings_dict[binning]
                rewhistodict = producehistos(syst,samples2rew,binnings_dict[binning],variable,lumiW, asymm, fiducial,bootstrap,pdf,pdfType)

                print "HOTOOTOTOTOT", rewhistodict, mapnames
                for mapname in mapnames:

                    combinehisto(rewhistodict[mapname],rewhistodict['resmat_incl']['ttbar'],mapname,variable,binning,asymm,syst,useBKG,bootstrap,pdf,pdfType)

    else:
        syst = systematics
        if Debug > 0: print 'syst:', syst
        for binning in binnings_dict: # Loop over all the binnings
            if Debug > 1: print 'binning:', binning, binnings_dict[binning]
            # Get the different samples from Config
            samples,samples_MC,samples_BCKG,samples_data,truthttbar,ttbar = Config.getSampleLists(syst)
            histodict = producehistos(syst,samples,binnings_dict[binning],variable,lumiW,'',fiducial,bootstrap,pdf,pdfType)
            for mapname,resmatname in zip(mapnames,resmatnames):
                resmat = histodict[resmatname][ttbar] if ttbar in histodict[resmatname] else None
                saveHistos(histodict[mapname],mapname, resmat ,variable, syst, binning,bootstrap, pdf,pdfType)
