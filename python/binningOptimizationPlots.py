

def binnings_dict(var="helpol"):

    binnings_helpol_dict = {
        #'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.],
        '4Bin2':[-1.,-0.4,0.,0.4,1.], ##
        '4Bin3':[-1.,-0.6,0.,0.6,1.], 
        '4Bin4':[-1.,-0.4,0.,0.5,1.],##
        '6Bin1':[-1.,-0.6,-0.25,0.,0.25,0.6,1.],
        '6Bin2':[-1.,-0.6,-0.3,0.,0.3,0.6,1.],
        '6Bin3':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        }

    binnings_transpol_dict = {
        #'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.], ##
        '4Bin2':[-1.,-0.4,0.,0.4,1.], ##
        #'4Bin3':[-1.,-0.3,0.,0.3,1.],
        #'4Bin4':[-1.,-0.6,0.,0.6,1.],
        }

#    binnings_beampol_dict = {
#        '4BinSym':[-1.,-0.5,0.,0.5,1.]
#        }


    binnings_rpol_dict = {
        #'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.],#
        '4Bin2':[-1.,-0.4,0.,0.4,1.],#
        #'4Bin3':[-1.,-0.3,0.,0.3,1.],
        #'4Bin4':[-1.,-0.6,0.,0.6,1.],
        #'6Bin1':[-1.,-0.6,-0.25,0.,0.25,0.6,1.],
        #'6Bin2':[-1.,-0.55,-0.25,0.,0.25,0.55,1.],
        #'6Bin3':[-1.,-0.6,-0.3,0.,0.3,0.6,1.],
        #'6Bin4':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        }

    binnings_helcorr_dict = {
        #'4Bin1':[-1.,-0.5,0.,0.5,1.],
        #'6Bin1':[-1.,-0.5,-0.2,0.,0.2,0.5,1.], #CMS
        '6Bin2':[-1.,-0.6,-0.25,0.,0.25,0.6,1.], #
        #'6Bin3':[-1.,-0.6,-0.3,0.,0.3,0.6,1.],
        #'6Bin4':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        #'8Bin1':[-1.,-0.55,-0.325,-0.15,0.,0.15,0.325,0.55,1.],
        #'8Bin2':[-1.,-0.6,-0.375,-0.175,0.,0.175,0.375,0.6,1.],
        #'8Bin3':[-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.],
        '8Bin4':[-1.,-0.75,-0.5,-0.2,0.,0.2,0.5,0.75,1.], #
        #'10Bin':[-1.,-0.675,-0.45,-0.275,-0.125,0.,0.125,0.275,0.425,0.65,1.],
        }

    binnings_transcorr_dict = {
        #'4Bin1':[-1.,-0.5,0.,0.5,1.],
        '6Bin1':[-1.,-0.5,-0.2,0.,0.2,0.5,1.], #CMS
        '6Bin2':[-1.,-0.6,-0.25,0.,0.25,0.6,1.],#
        '6Bin3':[-1.,-0.6,-0.3,0.,0.3,0.6,1.],
        '6Bin4':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        '8Bin1':[-1.,-0.55,-0.325,-0.15,0.,0.15,0.325,0.55,1.],
        '8Bin2':[-1.,-0.6,-0.375,-0.175,0.,0.175,0.375,0.6,1.],
        '8Bin3':[-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.],
        '8Bin4':[-1.,-0.75,-0.5,-0.2,0.,0.2,0.5,0.75,1.], #
        '10Bin':[-1.,-0.675,-0.45,-0.275,-0.125,0.,0.125,0.275,0.425,0.65,1.],
        }

    binnings_rcorr_dict = {
        #'2Bin':[-1.,0.,1.],
        #'4Bin1':[-1.,-0.5,0.,0.5,1.], #
        #'6Bin1':[-1.,-0.5,-0.2,0.,0.2,0.5,1.], #CMS
        '6Bin2':[-1.,-0.66,-0.33,0.,0.33,0.66,1.], #
        #'8Bin1':[-1.,-0.55,-0.325,-0.15,0.,0.15,0.325,0.55,1.],
        #'8Bin2':[-1.,-0.6,-0.375,-0.175,0.,0.175,0.375,0.6,1.],
        #'8Bin3':[-1.,-0.7,-0.45,-0.2,0.,0.2,0.45,0.7,1.],
        '8Bin4':[-1.,-0.75,-0.5,-0.2,0.,0.2,0.5,0.75,1.], #
        #'10Bin':[-1.,-0.675,-0.45,-0.275,-0.125,0.,0.125,0.275,0.425,0.65,1.],
        }

    binnings_transhel_dict = {
        #'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.], ##
        #'4Bin4':[-1.,-0.6,0.,0.6,1.],
        #'4Bin2':[-1.,-0.4,0.,0.4,1.],
        #'4Bin3':[-1.,-0.35,0.,0.35,1.],
        '6Bin1':[-1.,-0.6,-0.3,0.,0.3,0.6,1.], ###
        #'6Bin2':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        #'6Bin3':[-1.,-0.55,-0.25,0.,0.25,0.55,1.],
        #'6Bin4':[-1.,-0.5,-0.2,0.,0.2,0.5,1.],
        #'8Bin':[-1.,-0.7,-0.45,-0.2,0.,0.25,0.45,0.7,1.],
        }

    binnings_transr_dict = {
            #:'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.], ###
        #'4Bin4':[-1.,-0.6,0.,0.6,1.],
        #'4Bin2':[-1.,-0.4,0.,0.4,1.],
        #'4Bin3':[-1.,-0.35,0.,0.35,1.],
        '6Bin1':[-1.,-0.6,-0.3,0.,0.3,0.6,1.], ###
        #'6Bin2':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        #'6Bin3':[-1.,-0.55,-0.25,0.,0.25,0.55,1.],
        #'6Bin4':[-1.,-0.5,-0.2,0.,0.2,0.5,1.],
        #'8Bin':[-1.,-0.7,-0.45,-0.2,0.,0.25,0.45,0.7,1.],
        }

    binnings_rhel_dict = {
        #'2Bin':[-1.,0.,1.],
        '4Bin1':[-1.,-0.5,0.,0.5,1.], ##
        #'4Bin4':[-1.,-0.6,0.,0.6,1.],
        #'4Bin2':[-1.,-0.4,0.,0.4,1.],
        #'4Bin3':[-1.,-0.35,0.,0.35,1.],
        '6Bin1':[-1.,-0.6,-0.3,0.,0.3,0.6,1.], ##
        #'6Bin2':[-1.,-0.66,-0.33,0.,0.33,0.66,1.],
        #'6Bin3':[-1.,-0.55,-0.25,0.,0.25,0.55,1.],
        #'6Bin4':[-1.,-0.5,-0.2,0.,0.2,0.5,1.],
        #'8Bin':[-1.,-0.7,-0.45,-0.2,0.,0.25,0.45,0.7,1.],
        }


    if "helpol" in var: return binnings_helpol_dict
    elif "transpol" in var :         return binnings_transpol_dict
    elif "beampol" in var :         return binnings_beampol_dict
    elif "rpol" in var :         return binnings_rpol_dict
    elif var=="helcorr": return binnings_helcorr_dict
    elif var=="transcorr": return binnings_transcorr_dict
    elif var=="rcorr": return binnings_rcorr_dict
    elif 'transhel' in var: return binnings_transhel_dict
    elif 'transr' in var: return binnings_transr_dict
    elif 'rhel' in var: return binnings_rhel_dict
    else :
        print "no binning found"
        return {}

#no ll included anymore
