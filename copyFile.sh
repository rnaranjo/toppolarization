#!/bin/bash

variables="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  transheldiff"
syst="nominal mcatnlo perugia perugialocr perugiampihi radlo radhi fastpythia herwig"

#syst="pol_up pol_down corr_up corr_down"

for s in $syst;do

for var in $variables; do
    cp /nfs/dust/atlas/user/naranjo/TopPolarization/results_particle/${var}/${s}/ALL/optimization/${var}*.json  /nfs/dust/atlas/user/naranjo/TopPolarization/results_particle_bootstrap/${var}/${s}/ALL/optimization/
done

done
