#include <iostream>
#include <array>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TLorentzVector.h"

using namespace std;

int debug = 0;

int addVariables(TString partonFilename,TString particleFilename, TString outFilename="ttbar.root", TString systname="gen")
{


    TFile  partonFile(partonFilename.Data(),"READ");
    TFile  particleFile(particleFilename.Data(),"READ");
    TFile  outFile(outFilename.Data(), "RECREATE");


    TTree* particleTree    = (TTree*) particleFile.Get("particles");
    
    particleTree->BuildIndex("runNumber","eventNumber");

    int par_lep_n,par_lep_charge[2],par_lep_type[2],inFiducial,OR;
    float true_rew_lep_pt[2], true_rew_lep_eta[2], true_rew_lep_phi[2], true_rew_lep_m[2];
    float par_lep_pt[2],par_lep_eta[2],par_lep_phi[2],par_lep_E[2],par_lep_m[2];   
    float par_top_pt,par_top_eta,par_top_phi,par_top_m,par_top_E;
    float par_tbar_pt,par_tbar_eta,par_tbar_phi,par_tbar_m,par_tbar_E;
    float lumiweight;

    
    inFiducial  = 0;
    OR          = 0;

    particleTree->SetBranchAddress("par_lep_n"      ,&par_lep_n      );
    particleTree->SetBranchAddress("par_lep_pt"     ,par_lep_pt     );
    particleTree->SetBranchAddress("par_lep_eta"    ,par_lep_eta    );
    particleTree->SetBranchAddress("par_lep_phi"    ,par_lep_phi    );
    particleTree->SetBranchAddress("par_lep_E"      ,par_lep_E      ); 
    particleTree->SetBranchAddress("par_lep_charge" ,par_lep_charge );
    particleTree->SetBranchAddress("par_lep_type"   ,par_lep_type   );
    particleTree->SetBranchAddress("inFiducial"     ,&inFiducial     );
    particleTree->SetBranchAddress("OR"             ,&OR             );
    particleTree->SetBranchAddress("par_top_pt"     ,&par_top_pt     );
    particleTree->SetBranchAddress("par_top_eta"    ,&par_top_eta    );
    particleTree->SetBranchAddress("par_top_phi"    ,&par_top_phi    );
    particleTree->SetBranchAddress("par_top_m"      ,&par_top_m      );
    particleTree->SetBranchAddress("par_top_E"      ,&par_top_E      );
    particleTree->SetBranchAddress("par_tbar_pt"    ,&par_tbar_pt    );
    particleTree->SetBranchAddress("par_tbar_eta"   ,&par_tbar_eta   );
    particleTree->SetBranchAddress("par_tbar_phi"   ,&par_tbar_phi   );
    particleTree->SetBranchAddress("par_tbar_m"     ,&par_tbar_m     );
    particleTree->SetBranchAddress("par_tbar_E"     ,&par_tbar_E     );

    
        
    TString inTreename  = systname.Data();


    TTree* partonTree = (TTree*) partonFile.Get(inTreename.Data());
    TTree *outTree  = (TTree*) partonTree->CloneTree(0);

    outTree->SetName("particles");
    outTree->SetDirectory(&outFile);
    outTree->SetMaxTreeSize((Long64_t)100000000000LL); //100GB limit

    if ( partonTree == 0 ) return 1;

    int      runNumber, eventNumber;
    partonTree->SetBranchAddress("runNumber"    ,&runNumber);
    partonTree->SetBranchAddress("eventNumber"  ,&eventNumber);
    outTree->Branch(  "eventWeightLumi"       ,  &lumiweight         ,    "eventWeightLumi/F"        );

    outTree->Branch(  "par_lep_n"       ,  &par_lep_n         ,    "par_lep_n/I"        );
    outTree->Branch(  "par_lep_pt"      ,  par_lep_pt        ,    "par_lep_pt[par_lep_n]/F" );
    outTree->Branch(  "par_lep_eta"     ,  par_lep_eta       ,    "par_lep_eta[par_lep_n]/F"      );
    outTree->Branch(  "par_lep_phi"     ,  par_lep_phi       ,    "par_lep_phi[par_lep_n]/F"      );
    outTree->Branch(  "par_lep_E"       ,  par_lep_E         ,    "par_lep_E[par_lep_n]/F"     );   
    outTree->Branch(  "par_lep_m"       ,  par_lep_m         ,    "par_lep_m[par_lep_n]/F"     );
    outTree->Branch(  "par_lep_charge"  ,  par_lep_charge    ,    "par_lep_charge[par_lep_n]/I");
    outTree->Branch(  "par_lep_type"    ,  par_lep_type      ,    "par_lep_type[par_lep_n]/I"  );
    outTree->Branch(  "inFiducial"      ,  &inFiducial        ,    "inFiducial/I"    );
    outTree->Branch(  "OR"              ,  &OR                ,    "OR/I"            );
    outTree->Branch(  "par_top_pt"      ,  &par_top_pt        ,    "par_top_pt/F"    );
    outTree->Branch(  "par_top_eta"     ,  &par_top_eta       ,    "par_top_eta/F"   );
    outTree->Branch(  "par_top_phi"     ,  &par_top_phi       ,    "par_top_phi/F"   );
    outTree->Branch(  "par_top_m"       ,  &par_top_m         ,    "par_top_m/F"     );
    outTree->Branch(  "par_top_E"       ,  &par_top_E         ,    "par_top_E/F"     );
    outTree->Branch(  "par_tbar_pt"     ,  &par_tbar_pt       ,    "par_tbar_pt/F"   );
    outTree->Branch(  "par_tbar_eta"    ,  &par_tbar_eta      ,    "par_tbar_eta/F"  );
    outTree->Branch(  "par_tbar_phi"    ,  &par_tbar_phi      ,    "par_tbar_phi/F"  );
    outTree->Branch(  "par_tbar_m"      ,  &par_tbar_m        ,    "par_tbar_m/F"    );
    outTree->Branch(  "par_tbar_E"      ,  &par_tbar_E        ,    "par_tbar_E/F"    );


    //----------------------------------------------------
    // Loop over the mini tree
    //----------------------------------------------------
    int const nEntries = (int) partonTree->GetEntries();
    

    for (int iev=0; iev < nEntries; ++iev) {

        if ( iev % 10000 == 0) {
            cout << "Processing event: " << iev << " / " << nEntries << endl;
        }

        partonTree->GetEntry(iev);

        lumiweight = 0.0557454;

        if (particleTree->GetEntryWithIndex(runNumber, eventNumber) <= 0){
            inFiducial = 0;
        }else{

            TLorentzVector *lepton1 = new TLorentzVector();
            TLorentzVector *lepton2 = new TLorentzVector();
            lepton1->SetPtEtaPhiE(par_lep_pt[0],par_lep_eta[0],par_lep_phi[0],par_lep_E[0]);
            lepton2->SetPtEtaPhiE(par_lep_pt[1],par_lep_eta[1],par_lep_phi[1],par_lep_E[1]);

            par_lep_m[0] = lepton1->M();
            par_lep_m[1] = lepton2->M();
            

        }

        outTree->Fill();


    } //loop over the events
    outTree->Write();
    outFile.Close();
    particleFile.Close();
    partonFile.Close();
    return 0;
}


int main(int argc, char* argv[])
{

    TString inFileName   = argv[1];

    TString weightFileName   = argv[2];

    TString outFileName   = argv[3];

    TString treename   = argv[4];



    addVariables(inFileName, weightFileName, outFileName,treename);


    return 0;
}
