#include <iostream>
#include <array>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TLorentzVector.h"

using namespace std;

int debug = 0;

int addVariables(TString originalFilename,TString extraFilename, TString outFilename="ttbar.root", TString systname="mini")
{


    TFile  originalFile(originalFilename.Data(),"READ");
    TFile  extraFile(extraFilename.Data(),"READ");
    TFile  outFile(outFilename.Data(), "RECREATE");


    TTree* extraTree    = (TTree*) extraFile.Get("gen");
    extraTree->BuildIndex("runNumber","eventNumber");

    int true_lep_n;
    float true_rew_lep_pt[2], true_rew_lep_eta[2], true_rew_lep_phi[2], true_rew_lep_m[2];
    extraTree->BuildIndex("runNumber","eventNumber");
    extraTree->SetBranchAddress("true_rew_lep_pt"    ,true_rew_lep_pt);
    extraTree->SetBranchAddress("true_rew_lep_eta"    ,true_rew_lep_eta);
    extraTree->SetBranchAddress("true_rew_lep_phi"    ,true_rew_lep_phi);
    extraTree->SetBranchAddress("true_rew_lep_m"    ,true_rew_lep_m);



        TString inTreename  = systname.Data();
        TTree* originalTree = (TTree*) originalFile.Get(inTreename.Data());
        TTree *outTree  = (TTree*) originalTree->CloneTree(0);

        outTree->SetDirectory(&outFile);
        outTree->SetMaxTreeSize((Long64_t)100000000000LL); //100GB limit

        if ( originalTree == 0 ) return 1;

        int      runNumber, eventNumber;
        originalTree->SetBranchAddress("runNumber"    ,&runNumber);
        originalTree->SetBranchAddress("eventNumber"  ,&eventNumber);

        outTree->Branch("true_rew_lep_pt"    ,true_rew_lep_pt       , "true_rew_lep_pt[true_lep_n]/F");
        outTree->Branch("true_rew_lep_eta"   ,true_rew_lep_eta      , "true_rew_lep_eta[true_lep_n]/F");
        outTree->Branch("true_rew_lep_phi"   ,true_rew_lep_phi      , "true_rew_lep_phi[true_lep_n]/F");
        outTree->Branch("true_rew_lep_m"     ,true_rew_lep_m        , "true_rew_lep_m[true_lep_n]/F");


        //----------------------------------------------------
        // Loop over the mini tree
        //----------------------------------------------------
        int const nEntries = (int) originalTree->GetEntries();
        

        for (int iev=0; iev < nEntries; ++iev) {

            if ( iev % 10000 == 0) {
                cout << "Processing event: " << iev << " / " << nEntries << endl;
            }

            originalTree->GetEntry(iev);

            if (extraTree->GetEntryWithIndex(runNumber, eventNumber) <= 0) continue;

            outTree->Fill();


        } //loop over the events
        outTree->Write();
    outFile.Close();
    originalFile.Close();
    extraFile.Close();
    return 0;
}


int main(int argc, char* argv[])
{

    TString inFileName   = argv[1];

    TString weightFileName   = argv[2];

    TString outFileName   = argv[3];

    TString treename   = argv[4];



    addVariables(inFileName, weightFileName, outFileName,treename);


    return 0;
}
