ROOTLIBS  = `root-config --libs` -lMinuit
ROOTFLAGS = `root-config --cflags`
 
CFLAGS = -I. -I/opt/local/include 
RCXX = $(CFLAGS) $(ROOTFLAGS) $(ROOTLIBS)

LFLAGS = -L/opt/local/lib -lgsl -lcblas


DBG = -g -O2
CC = g++ ${DBG} $(RCXX)
MINCC = g++ ${DBG} $(CFLAGS) $(ROOTFLAGS) 

SRC = src
INCLUDE = include


all: $(OBJ) addLeptonVariables mergeTruth

addLeptonVariables: utils/addLeptonVariables.cxx
	$(CC) -o bin/addLeptonVariables utils/addLeptonVariables.cxx

mergeTruth: utils/mergeTruth.cxx
	$(CC) -o bin/mergeTruth utils/mergeTruth.cxx
clean:
	\rm -f bin/*
