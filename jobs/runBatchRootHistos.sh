#!/bin/bash

PCfarm=DESY #other options: Prague

if [ $# -ge 1 ]; then  PCfarm=$1;  fi

if [ ! -d $AcDilFBU_DIR/batch ]; then mkdir -p $AcDilFBU_DIR/batch; fi
cd $AcDilFBU_DIR/batch/

python $AcDilFBU_DIR/jobs/runBatchRootHistos.py $PCfarm
