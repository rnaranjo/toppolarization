#!/bin/bash


     channels="ALL"
       signal="reco"
       regpar="rpZero"
       PCfarm="Desy" #other options: Prague
         syst="nominal"
          #helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
          var="transrdiff"
     fiducial="full"




cd $AcDilFBU_DIR/batch

for ch in $channels; do
  for sig in $signal; do
     for sys in $syst; do
         for v in $var; do
          for f in $fiducial;do
             cmd="python $AcDilFBU_DIR/jobs/runBatchEnsembles.py $PCfarm $ch $v $sig $sys $fiducial"
             echo "running: $cmd"
             $cmd
          done
        done
    done
  done
done
