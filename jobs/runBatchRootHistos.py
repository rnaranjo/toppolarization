#!/bin/env python

import sys
import json
def writeScript(PCfarm,baseDir,channel,syst,var,tag,applyRew,rewType,rewSignal,fiducial,bootstrap,pdf):
    print 'Submitting: analyzer.py', channel, syst, var, tag,  applyRew, rewType, rewSignal,pdf,fiducial
    template_begin_lxplus="""#!/bin/sh

"""

    template_begin_Prague="""#!/bin/sh

# -- queue name
#PBS -q gridatlas

# -- job name
#PBS -N FBUmakeROOThisto

# -- stdout and err stream
##PBS -o job.out
##PBS -e job.err
# -- out and err to the same file
#PBS -j oe

##PBS -m aeb
#PBS -m e

# -- e-mail
#PBS -M lysak@fzu.cz

# -- walltime limit
##PBS -l walltime=168:00:00

# -- memory size
##PBS -l vmem=2gb

# -- non-restartable job
#PBS -r n

# -- go to directory where you submitted the job from, otherwise using $HOME directory
cd $PBS_O_WORKDIR

"""

    template_main = """

date
echo \"Running on the host: `/bin/hostname`\"
echo $SHELL

echo \"Current directory: `pwd`\"
workDir=`pwd`

echo \"Setting up the environment\"
cd  %(baseDir)s
source scripts/setup.sh

cd $workDir

cmd="%(baseDir)s/scripts/makeRootHistos.sh %(var)s %(channel)s %(syst)s %(applyRew)s %(rewType)s %(rewSignal)s %(fiducial)s %(bootstrap)s %(pdf)s"
echo "running: $cmd"
#$cmd



date
echo \"Done.\"

""" % {'baseDir': baseDir,
       'channel': channel,
       'syst'   : syst,
       'var'    : var,
       'tag'    : tag,
       'applyRew' : applyRew,
       'rewType'  : rewType,
       'rewSignal': rewSignal,
       'fiducial' : fiducial,
       'bootstrap': bootstrap,
       'pdf': pdf
       }

    template_begin = template_begin_lxplus #default: lxplus
    if PCfarm == "Prague":  template_begin = template_begin_Prague
    template = template_begin + template_main

    #create submission script
    if 'true' in applyRew :
        scriptFileName = baseDir+'/batch/run_analyzer_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s.sh'%(channel,syst,var,tag,applyRew,fiducial,bootstrap,rewType,rewSignal,pdf)
        name  = baseDir+'/batch/run_analyzer_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(channel,syst,var,tag,applyRew,fiducial,bootstrap,rewType,rewSignal,pdf)
    else :
        scriptFileName = baseDir+'/batch/run_analyzer_%s_%s_%s_%s_%s_%s_%s.sh'%(channel,syst,var,tag,fiducial,bootstrap,pdf)
        name = baseDir+'/batch/run_analyzer_%s_%s_%s_%s_%s_%s_%s'%(channel,syst,var,tag,fiducial,bootstrap,pdf)
    scriptFile = open(scriptFileName,'w')
    scriptFile.write(template)
    scriptFile.close()

    #submit job
    import commands
    commands.getoutput("chmod +x %s" % scriptFileName)
    submCmd="bsub -q 8nh %s" % scriptFileName  #default: lxplus

    if PCfarm == "DESY":
       submCmd="""qsub -b y -N %s -o %s.out -e %s.err -M roger.naranjo@desy.de -m bea -l  hostname="!(bird657.desy.de|bird638.desy.de|bird646.desy.de|bird643.desy.de|bird649.desy.de|bird644.desy.de|bird643.desy.de|bird638.desy.de|bird654.desy.de|bird647.desy.de|bird649.desy.de|bird654.desy.de|bird657.desy.de|bird643.desy.de|bird650.desy.de|bird648.desy.de|bird638.desy.de|bird639.desy.de|bird614.desy.de|bird617.desy.de|bird613.desy.de|bird618.desy.de|bird610.desy.de|bird616.desy.de|bird037.desy.de|bird036.desy.de|bird651.desy.de|bird652.desy.de|bird653.desy.de|bird640.desy.de|bird637.desy.de|bird642.desy.de|bird656.desy.de|bird655.desy.de)" -l site=hh -l h_cpu=24:00:00 -l h_rt=24:00:00 -l h_vmem=6000M -l s_vmem=2900M -l distro=sld6 %s""" % (syst+'CalibAnalyzer',name,name,scriptFileName)

    if PCfarm == "Prague": submCmd="qsub %s" % scriptFileName
    rc,out = commands.getstatusoutput(submCmd)
    print "job submitted, rc= ",rc
    print "submission output: ",out


if __name__ == "__main__":

    import Config
    baseDir = Config.Configuration.workDir

    PCfarm = sys.argv[1]

    channels = ['EMMU','MUMU','EE']
    tags = ['btag']

    #channels = ['EE']

    variables  = [

            'helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
            'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
            'transheldiff'

    ]

    #variables =  "helpol_plus".split()
    rew         = True
    applyRew = "false"
    rewTypes  = ["linear"]
    rewSignals = ["none"]
    if rew:
        applyRew = "true"
        rewTypes  = ["obs"]
        #rewSignals = Config.getObsSamples(var)



    #systs =['nominal','mcatnlo','herwig','perugiampihi','perugialocr',\
            #'topmass_167.5','topmass_170','topmass_175','topmass_177.5',\
    #        'radhi','radlo',\
    #        'fastpythia','perugia']
#    systs = [
#            
# 'nominal',
# 'musc_down',
# 'musc_up',
# 'EtaIntercalibrationModel_down',
# 'jer', 'jeff', 'ees_up', 'ees_down', 'eer_up', 'eer_down', 'el_trigSF_up', 'el_trigSF_down',\
#'el_recSF_up', 'el_recSF_down', 'el_idSF_up', 'el_idSF_down', 'musc_up', 'musc_down',\
#'mums_res', 'muid_res', 'mu_trigSF_up', 'mu_trigSF_down', 'mu_recSF_up', 'mu_recSF_down',\
#'mu_idSF_up', 'mu_idSF_down', 'jvf_up', 'jvf_down', 'res_soft_up', 'res_soft_down',\
#'sc_soft_up', 'sc_soft_down', 'JesEffectiveStat1_up', 'JesEffectiveStat2_up',\
#'JesEffectiveStat3_up', 'JesEffectiveStat4_up', 'JesEffectiveModel1_up',\
#'JesEffectiveModel2_up', 'JesEffectiveModel3_up', 'JesEffectiveModel4_up', \
#'JesEffectiveDet1_up', 'JesEffectiveDet2_up', 'JesEffectiveDet3_up', 'JesEffectiveMix1_up', \
#'JesEffectiveMix2_up', 'JesEffectiveMix3_up', 'JesEffectiveMix4_up', \
#'EtaIntercalibrationModel_up', 'EtaIntercalibrationTotalStat_up', 'SinglePart_up',\
#'Pileup_OffsetMu_up', 'Pileup_OffsetNPV_up', 'Pileup_Pt_up', 'Pileup_Rho_up',
#'flavor_comp_up', 'flavor_response_up', 'BJesUnc_up', 'PunchThrough_up', 'JesEffectiveStat1_down', \
#'JesEffectiveStat2_down', 'JesEffectiveStat3_down', 'JesEffectiveStat4_down', 'JesEffectiveModel1_down', \
#'JesEffectiveModel2_down', 'JesEffectiveModel3_down', 'JesEffectiveModel4_down', 'JesEffectiveDet1_down',\
#'JesEffectiveDet2_down', 'JesEffectiveDet3_down', 'JesEffectiveMix1_down', 'JesEffectiveMix2_down',\
#'JesEffectiveMix3_down', 'JesEffectiveMix4_down', 'EtaIntercalibrationModel_down', 'EtaIntercalibrationTotalStat_down', 'SinglePart_down', 'Pileup_OffsetMu_down', 'Pileup_OffsetNPV_down', 'Pileup_Pt_down'
#
#            
#            ]
    #systs = "eer_down JesEffectiveStat1_up JesEffectiveStat3_up JesEffectiveMix4_up Pileup_Rho_up BJesUnc_up JesEffectiveStat2_down JesEffectiveStat4_down SinglePart_down flavor_response_down BJesUnc_down btag2_up ctautag1_down ctautag2_down".split()
    #systs = ['nominal','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia','powhegpythia','perugia'] #['mcatnlo','perugiampiHI','perugialoCR','mt1675','mt1700','mt1775'] #['nominal']
    systs = ['nominal']
    #systs = ['topmass2_167.5','topmass2_170','topmass2_175','topmass2_177.5']
    #systs = ['pol_up','pol_down','corr_up','corr_down'] #['nominal']#['musc_up','musc_down','mu_trigSF_up','mu_trigSF_down']

    pdf   = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]

    fiducial = ['fiducial','full']
    pdf      = [ '' ]
    bootstrap= [ '' ]

    
    #for f in fiducial:
    #     if f == 'fiducial': repeat = json.load(open('/nfs/dust/atlas/user/naranjo/TopPolarization/jobs/missingfiducial.json'))
    #     else: repeat = json.load(open('/nfs/dust/atlas/user/naranjo/TopPolarization/jobs/missing.json'))

    #     
    #    

    #     [writeScript(PCfarm,baseDir,ch,sys,var,tag,applyRew,rewType,rewSignal,f,b,p) if rewSignal != None else 0
    #            for var in repeat
    #            for sys in repeat[var]
    #            for ch in repeat[var][sys]
    #            for rewSignal in repeat[var][sys][ch]
    #            for tag in tags
    #            for rewType in rewTypes
    #            for b in bootstrap
    #            for p in pdf]    


    #number of jobs: 3 channels * 2 asymmetries * XX systematics
    # each job takes: ~ 3 (reweightings) * 7 (samples) = ~20-30 minutes
    [writeScript(PCfarm,baseDir,ch,sys,var,tag,applyRew,rewType,rewSignal,f,b,p)
       for ch in channels
       for sys in systs
       for var in variables
       for tag in tags
       for rewType in rewTypes
       for rewSignal in ['obspos0']#Config.getObsSamples(var)
       for f in fiducial
       for b in bootstrap
       for p in pdf]
