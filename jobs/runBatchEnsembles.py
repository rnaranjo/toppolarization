#!/bin/env python

def writeScript(PCfarm,baseDir,channel,syst,var,signal,n_init,n_fin,binName,regparName,f):
    #print 'Submitting: runfbu.py', channel, syst, var, tag, whichAsym, n_init, n_fin, binName, regparName, signal
    template_begin_lxplus="""#!/bin/sh

"""

    template_begin_Prague="""#!/bin/sh

# -- queue name
#PBS -q gridatlas

# -- job name
#PBS -N FBUrunEnsembles

# -- stdout and err stream
##PBS -o job.out
##PBS -e job.err
# -- out and err to the same file
#PBS -j oe

##PBS -m aeb
#PBS -m e

# -- e-mail
#PBS -M lysak@fzu.cz

# -- walltime limit
##PBS -l walltime=168:00:00

# -- memory size
##PBS -l vmem=2gb

# -- non-restartable job
#PBS -r n
"""

    template_main = """

date
echo \"Running on the host: `/bin/hostname`\"


cd %(baseDir)s


echo \"Current directory: `pwd`\"
workDir=`pwd`
source scripts/setup.sh


echo "copying python fbu code"

date
cmd="python python/fbuOptimization.py %(n_init)d %(n_fin)d ensembles_%(n_init)d_%(n_fin)d %(channel)s ensemble %(syst)s %(var)s %(signal)s  %(binName)s %(regparName)s %(fiducial)s"
echo "running $cmd"
$cmd
date


echo \"Done.\"

""" % {'baseDir': baseDir,
       'channel': channel,
       'syst'   : syst,
       'var'    : var,
       'n_init' : n_init,
       'n_fin'  : n_fin,
       'binName'  : binName,
       'regparName': regparName,
       'signal' : signal,
       'fiducial':f
       }

    template_begin = template_begin_lxplus #default: lxplus
    if PCfarm == "Prague":  template_begin = template_begin_Prague
    template = template_begin + template_main

    #create submission script
    scriptFileName = baseDir+'/batch/run_%s_%s_%s_%s_%s_%s_%d_%d_%s.sh'%(channel,syst,var,signal,binName,regparName,n_init,n_fin,f)
    scriptFile = open(scriptFileName,'w')
    scriptFile.write(template)
    scriptFile.close()

    #submit job
    import commands
    commands.getoutput("chmod +x %s" % scriptFileName)
    #submCmd="bsub -q 1nd %s" % scriptFileName  #default: lxplus
    name = "%s_%s_%s_%s_%s_%d_%d_%s" %(channel,syst,var,binName,regparName,n_init,n_fin,f)
    nameDir = baseDir+'/batch/'+name
    submCmd='qsub -b y -N %s -o %s.out -e %s.err -M roger.naranjo@desy.de -m bea -l hostname="!(bird614.desy.de|bird617.desy.de|bird613.desy.de|bird618.desy.de|bird610.desy.de|bird616.desy.de|bird037.desy.de|bird036.desy.de)"  -l site=hh -l h_cpu=12:00:00 -l h_rt=12:00:00 -l h_vmem=3500M -l distro=sld6 %s' % (name,nameDir,nameDir,scriptFileName)
    rc,out = commands.getstatusoutput(submCmd)
    print "job submitted, rc= ",rc
    print "submission output: ",out



if __name__ == "__main__":

    import os
    import Config
    import sys
    import utils

    PCfarm = sys.argv[1]

    baseDir = Config.Configuration.workDir

    channel            = utils.setChan(sys.argv)
    var                = utils.setVar(sys.argv)
    signal             = utils.setSignal2Unfold(sys.argv)
    binName            = utils.setOptimalBinning(sys.argv,var)


    rpName,rpValue     = 'rpZero', 0.0
    regFunction        = 'Tikhonov'
    syst               = utils.setSyst(sys.argv)

    f = 'full'


    nens = 2000
    nEvtPerJob = 20

    print '-- Run settings for ensembles -- \n channel\t',channel,'\n var\t\t',var,'\n signal\t\t',signal,'\n binName\t',binName,'\n rpName\t\t',rpName,'\n nEns\t\t',nens

    import binningOptimization

    binnings = binningOptimization.binnings_dict(var)

    for x in xrange(1, nens/nEvtPerJob+1):
        n_init = int(1+(x-1)*nEvtPerJob)
        n_fin  = int(x*nEvtPerJob)
        print 'Ensembles from',n_init,'to',n_fin
        [writeScript(PCfarm,baseDir,channel,syst,var,signal,n_init,n_fin,binName,rpName,f) for binName in binnings]
