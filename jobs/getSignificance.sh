#!/bin/bash -l

WORKDIR=/nfs/dust/atlas/user/naranjo/TopPolarization

cd ${WORKDIR}
source ${WORKDIR}/scripts/setup.sh
fiducial=$2
echo "Fiducial Case"
python ${WORKDIR}/python/getSignificance.py $fiducial  $1


ini () {
  eval "`/usr/share/NAF_profiles/ini.pl -b $*`"
}
