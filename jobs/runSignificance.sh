#!/bin/bash
OUTDIR=/nfs/dust/atlas/user/naranjo/TopPolarization/batch

region="full fiducial"
list="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  transheldiff"
          #
for n in $list
do
for r in $region
do
name="$n$r"
qsub -b y -N "PseudoTOP$n $r" \
     -o ${OUTDIR}/Significance$name.out \
     -e ${OUTDIR}/Significance$name.err \
     -l site=hh \
     -l h_cpu=48:00:00 \
     -l h_rt=48:00:00 \
     -l h_vmem=5000M \
     -l s_vmem=4900M \
     -l distro=sld6 \
     -M roger.naranjo@desy.de -m bea -l hostname="!(bird614.desy.de|bird617.desy.de|bird613.desy.de|bird618.desy.de|bird610.desy.de|bird616.desy.de|bird037.desy.de|bird036.desy.de)"\
     /nfs/dust/atlas/user/naranjo/TopPolarization/jobs/getSignificance.sh $n $r
done
done
