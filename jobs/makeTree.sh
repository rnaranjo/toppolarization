#!/bin/bash

WORKDIR=/nfs/dust/atlas/user/naranjo/TopPolarization
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
#sleep 3
localSetupROOT



inFilename=$1
parFilename=$2
outfile=$3
syst=$4


#first, select events after full dilepton selection
echo "selecting events passing full dilepton selection..."
$WORKDIR/bin/addLeptonVariables $inFilename $parFilename $outfile $syst
