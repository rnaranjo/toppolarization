#!/bin/env python
import sys
import Config
def writeScript(baseDir,variable,channel,syst,binName,signal,pdf,pdfNum,fiducial,rpName,bootstrap,replica):
    print 'Submitting: runfbu.py', baseDir,variable,channel,syst,binName,signal,pdf,pdfNum,fiducial,rpName,bootstrap,replica
    template_begin_lxplus="""#!/bin/sh

"""

    template_begin_Prague="""#!/bin/sh

# -- queue name
#PBS -q gridatlas

# -- job name
#PBS -N FBUrun

# -- stdout and err stream
##PBS -o job.out
##PBS -e job.err
# -- out and err to the same file
#PBS -j oe

##PBS -m aeb
#PBS -m e

# -- e-mail
#PBS -M lysak@fzu.cz

# -- walltime limit
##PBS -l walltime=168:00:00

# -- memory size
##PBS -l vmem=2gb

# -- non-restartable job
#PBS -r n
"""
    if pdf != '':
       template_main = """

date
echo \"Running on the host: `/bin/hostname`\"
echo \"Current directory: `pwd`\"
workDir=`pwd`

echo \"Setting up the environment\"
cd  %(baseDir)s
source scripts/setup.sh

cd $workDir

cmd="%(baseDir)s/scripts/runFbuPDF.sh %(variable)s %(binName)s %(channel)s %(syst)s %(pdf)s %(pdfNum)d %(fiducial)s %(rpName)s"
echo "running $cmd"
$cmd

echo \"Done.\"

""" % {'baseDir': baseDir,
       'channel': channel,
       'syst': syst,
       'variable': variable,
       'pdf': pdf,
       'pdfNum':pdfNum,
       'fiducial':fiducial,
       'binName':binName,
       'rpName':rpName
       }

    else:

       template_main = """

date
echo \"Running on the host: `/bin/hostname`\"
echo \"Current directory: `pwd`\"
workDir=`pwd`

echo \"Setting up the environment\"
cd  %(baseDir)s
source scripts/setup.sh

cd $workDir

cmd="%(baseDir)s/scripts/runFbuFinal.sh %(binName)s %(channel)s %(syst)s %(fiducial)s %(variable)s %(signal)s  %(rpName)s %(bootstrap)s %(replica)d"
echo "running $cmd"
$cmd

echo \"Done.\"

""" % {'baseDir': baseDir,
       'channel': channel,
       'syst': syst,
       'variable': variable,
       'pdf': pdf,
       'pdfNum':pdfNum,
       'fiducial':fiducial,
       'binName':binName,
       'signal':signal,
       'rpName':rpName,
       'bootstrap':bootstrap,
       'replica':replica}


    template_begin = template_begin_lxplus #default: lxplus
    if PCfarm == "Prague":  template_begin = template_begin_Prague
    template = template_begin + template_main

    scriptFileName = baseDir+'/batch/run_fbu_%s_%s_%s_%s_%d_%s_%s_%s_%s_%s_%d.sh'%(binName,channel,syst,pdf,pdfNum,fiducial,variable,signal,rpName,bootstrap,replica)
    name = baseDir+'/batch/run_fbu_%s_%s_%s_%s_%d_%s_%s_%s_%s_%s_%d'%(binName,channel,syst,pdf,pdfNum,fiducial,variable,signal,rpName,bootstrap,replica)


    scriptFile = open(scriptFileName,'w')
    scriptFile.write(template)
    scriptFile.close()



    #submit job
    import commands
    commands.getoutput("chmod +x %s" % scriptFileName)
    submCmd="bsub -q 8nh %s" % scriptFileName  #default: lxplus

    if PCfarm == "DESY":
       submCmd="""qsub -b y -N %s -o %s.out -e %s.err -M roger.naranjo@desy.de -m bea -l hostname="!(bird656.desy.de|bird655.desy.de)" -l site=hh -l h_cpu=06:00:00 -l h_rt=06:00:00 -l h_vmem=8000M -l distro=sld6 %s""" % ('UnfoldingData',name,name,scriptFileName)

    if PCfarm == "Prague": submCmd="qsub %s" % scriptFileName
    rc,out = commands.getstatusoutput(submCmd)
    print "job submitted, rc= ",rc
    print "submission output: ",out



if __name__=="__main__":
    import os,sys

    PCfarm = 'DESY'

    from Config import Configuration
    import utils

    baseDir    = Configuration.workDir

    channel            = utils.setChan(sys.argv)
    var                = utils.setVar(sys.argv)
    signal             = utils.setSignal2Unfold(sys.argv)

    rpName,rpValue     = utils.setRegPar(sys.argv)
    regFunction        = 'Tikhonov'
    syst               = utils.setSyst(sys.argv)

    Calibration = False

    region = ['full','fiducial']
    bootstrap = ''



    pdfdict = {"CT10nlo":53, "MSTW2008nlo68cl":41,"NNPDF23_nlo_as_0119":101}
    pdf     = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]
    pdf     = [""]


    import binningOptimization

    binnings = binningOptimization.binnings_dict(var)


    [writeScript(baseDir,var,channel,syst,binName,signal,p,n,f,rpName,bootstrap,replica)
        for f in region
        for replica in xrange(1)
        for signal in ['data']#Config.getObsSamples(var)
        for binName in binnings #['2Bin']
        for p in pdf
        for n in [0]] #xrange(pdfdict[p])]
