#!/bin/bash
OUTDIR=/nfs/dust/atlas/user/naranjo/TopPolarization/batch

list="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  transheldiff"

syst="nominal mcatnlo herwig perugia radhi radlo perugiampihi perugialocr"

syst="fastpythia"

region="full fiducial"

for s in $syst
do          #
for n in $list
do
for r in $region
do
name="$n$r$s"
qsub -b y -N "BJSON$n$s$r" \
     -o ${OUTDIR}/BJSON$name.out \
     -e ${OUTDIR}/BJSON$name.err \
     -l site=hh \
     -l h_cpu=1:00:00 \
     -l h_rt=1:00:00 \
     -l h_vmem=4000M \
     -l s_vmem=3900M \
     -l distro=sld6 \
     -M roger.naranjo@desy.de -m bea -l hostname="!(bird036.desy.de)"\
     /nfs/dust/atlas/user/naranjo/TopPolarization/scripts/makeJson.sh $n $s $r
done
done
done
