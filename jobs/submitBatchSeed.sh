#!/bin/bash

## Vars
here=/nfs/dust/atlas/user/naranjo/TopPolarization
variables="transpol_plus" #helpol_plus helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus helcorr transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transheldiff"

for var in $variables
do
    for i in {10000..200000..500}
    do
        qsub -b y -N "seed_${i}_${var}" -o ${here}/batch/see_${i}_${var}.out -e ${here}/batch/seed_${i}_${var}.err -M roger.naranjo@desy.de -m bea -l distro=sld6 -l site=hh -l h_rt=12:00:00 -l h_vmem=2500M -l hostname="!(bird614.desy.de|bird617.desy.de|bird613.desy.de|bird618.desy.de|bird610.desy.de|bird616.desy.de)" -l h_vmem=1900M -l h_cpu=12:00:00 $here/tests/runSeed.sh $i $var 
    done

done    
    
