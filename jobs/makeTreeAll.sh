#!/bin/bash

HERE=/nfs/dust/atlas/user/naranjo/TopPolarization/jobs
OUTDIR=/nfs/dust/atlas/user/naranjo/TopPolarization/ntuples

syst="mini jer jeff ees_up ees_down eer_up eer_down el_trigSF_up el_trigSF_down el_recSF_up el_recSF_down el_idSF_up el_idSF_down musc_up musc_down mums_res muid_res mu_trigSF_up mu_trigSF_down mu_recSF_up mu_recSF_down mu_idSF_up mu_idSF_down jvf_up jvf_down res_soft_up res_soft_down sc_soft_up sc_soft_down JesEffectiveStat1_up JesEffectiveStat2_up JesEffectiveStat3_up JesEffectiveStat4_up JesEffectiveModel1_up JesEffectiveModel2_up JesEffectiveModel3_up JesEffectiveModel4_up JesEffectiveDet1_up JesEffectiveDet2_up JesEffectiveDet3_up JesEffectiveMix1_up JesEffectiveMix2_up JesEffectiveMix3_up JesEffectiveMix4_up EtaIntercalibrationModel_up EtaIntercalibrationTotalStat_up SinglePart_up Pileup_OffsetMu_up Pileup_OffsetNPV_up Pileup_Pt_up Pileup_Rho_up flavor_comp_up flavor_response_up BJesUnc_up PunchThrough_up JesEffectiveStat1_down JesEffectiveStat2_down JesEffectiveStat3_down JesEffectiveStat4_down JesEffectiveModel1_down JesEffectiveModel2_down JesEffectiveModel3_down JesEffectiveModel4_down JesEffectiveDet1_down JesEffectiveDet2_down JesEffectiveDet3_down JesEffectiveMix1_down JesEffectiveMix2_down JesEffectiveMix3_down JesEffectiveMix4_down EtaIntercalibrationModel_down EtaIntercalibrationTotalStat_down SinglePart_down Pileup_OffsetMu_down Pileup_OffsetNPV_down Pileup_Pt_down Pileup_Rho_down flavor_comp_down flavor_response_down BJesUnc_down PunchThrough_down mistag_up mistag_down btag_up btag0_up btag1_up btag2_up btag3_up btag4_up btag5_up btag6_up btag7_up btag8_up btag_down btag0_down btag1_down btag2_down btag3_down btag4_down btag5_down btag6_down btag7_down btag8_down ctautag_up ctautag0_up ctautag1_up ctautag2_up ctautag3_up ctautag4_up ctautag5_up ctautag6_up ctautag_down ctautag0_down ctautag1_down ctautag2_down ctautag3_down ctautag4_down ctautag5_down ctautag6_down"

syst="mini"
inFile=/nfs/dust/atlas/user/naranjo/Fbu/ntuples/ttbar.root
parFile=/nfs/dust/atlas/user/raschaef/Unfolding_input/partonicTtbar.root

for s in $syst ; do
echo "procesing file: $s"
filename="ttbar_${s}.root"
    qsub -b y -N IN${s} \
         -l site=hh \
         -l h_cpu=24:00:00 \
         -l h_rt=24:00:00 \
         -l h_vmem=3G \
         -l distro=sld6 \
         -o ${OUTDIR}/log/_${s}.out \
         -e ${OUTDIR}/log/_${s}.err \
$HERE/makeTree.sh $inFile $parFile  $OUTDIR/$filename $s

done 
