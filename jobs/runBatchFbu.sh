#!/bin/bash


     channels="ALL"
       signal="data"
       regpar="rpZero"
       PCfarm="Desy" #other options: Prague
         syst="nominal"
         #syst="topmass2_167.5 topmass2_170 topmass2_175 topmass2_177.5"
          #helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
          var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  transheldiff"
          var="helpol_plus"
     fiducial="full"
     regPars="rpZero"



cd $AcDilFBU_DIR/batch


for ch in $channels; do
  for sig in $signal; do
     for sys in $syst; do
         for reg in $regPars;do
         for v in $var; do
          for f in $fiducial;do

             cmd="python $AcDilFBU_DIR/jobs/runBatchFbu.py $PCfarm $reg $ch $v $sig $sys $fiducial"
             echo "running: $cmd"
             $cmd
          done
        done
    done
    done
  done
done
