import sys
import json
import array

import ROOT as r
import utils
from Config import Configuration
from ROOT import *

import numpy as np


obs_label = {}

obs_label["helpol_plus"]=r"cos\theta_{+}^{k}"
obs_label["helpol_minus"]=r"cos\theta_{-}^{k}"
obs_label["transpol_plus"]=r"cos\theta_{+}^{n}"
obs_label["transpol_minus"]=r"cos\theta_{-}^{n}"
obs_label["rpol_plus"]=r"cos\theta_{+}^{r}"
obs_label["rpol_minus"]=r"cos\theta_{-}^{r}"
obs_label["helcorr"]=r"cos\theta_{+}^{k}cos\theta_{-}^{k}"
obs_label["transcorr"]=r"cos\theta_{+}^{n}cos\theta_{-}^{n}"
obs_label["rcorr"]=r"cos\theta_{+}^{r}cos\theta_{-}^{r}"
obs_label["transhelsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transheldiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transrsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["transrdiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["rhelsum"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{r}"
obs_label["rheldiff"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{r}"



def getModelingUncertainty(var,fiducial):

    
    from binningOptimization import binnings_dict

    # -- Get settings from command line

    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc

    nbins = len(binning) - 1
    bin_arr = array.array('d',binning)

    h_tmp = TH1F("tmp",var,nbins,bin_arr)

    #initialize the array

    estimationDict = {}

    systList = []
    
    systDict = {}

    systematics = ['nominal','perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia']

    for syst in systematics:

       temptList = []
       #if syst.lower() == 'mcatnlo': sf =1.29791
       sf =1


       for pos in xrange(100):

          jsondir    = outDirB+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos,'syst':syst.lower()}  

          jsonDirN   = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'channel':channel,'syst':syst.lower()}
     
          truth = json.load(open(jsondir+'/'+var+'_partonicobspos0_btag_'+binName+'.json'))

          sumt = np.sum(truth)
          
          fname = jsondir+'/UnfoldedDistribution_obspos0_'+var+'_'+syst+'_'+binName+'_rpZero.json'

          try:

              distribution = json.load(open(fname))
              if 'pol' in var:
                for i in xrange(len(distribution[0])):
                  bin_width = h_tmp.GetXaxis().GetBinWidth(i+1)
                  distribution[0][i] = bin_width*distribution[0][i]

              sumu = np.sum(distribution[0])
              
              temptList.append((np.array(distribution[0])*(sumt/sumu)-np.array(truth)*sf)/np.array(truth)*sf)

          except:

              print "error in" , fname   
 

       #[[1, 2], [3, 4], [5, 6]]
       totalList = []
       #print "Binning:", binning
       #print temptList[0]
       for i in xrange(len(binning)-1):
           totalList.append([temptList[j][i] for j in xrange(len(temptList))])


        

        

       systDict[syst] = np.array([np.mean(a) for a in totalList ])
    #print systDict
    estimationDict['mc'] = np.fabs(systDict['mcatnlo']-systDict['herwig'])
    estimationDict['shower'] = np.fabs(systDict['fastpythia']-systDict['herwig']) 
    estimationDict['isrfsr'] = (np.fabs(systDict['radhi']-systDict['radlo']))/2
    estimationDict['color'] = np.fabs(systDict['perugia']-systDict['perugialocr'])
    estimationDict['underlying'] = np.fabs(systDict['perugia']-systDict['perugiampihi'])

    total = np.sqrt(estimationDict['mc']*estimationDict['mc'] + estimationDict['shower']*estimationDict['shower'] + estimationDict['isrfsr']*estimationDict['isrfsr'] + estimationDict['color']*estimationDict['color'] + estimationDict['underlying']*estimationDict['underlying'])     


    return total
   
       
def makeTable(central, uncert, modelling, total, variables,name):


  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{l|cccc|cccc}
\multicolumn{1}{c}{}&  \multicolumn{4}{c}{\\rule{0pt}{3ex} Fiducial Region} & \multicolumn{4}{c}{\\rule{0pt}{3ex}  Full Phase Space} \\\\ [2ex] \hline

Bin & Central  & Stat+Detect & Modelling & Total & Central  & Stat+Detect & Modelling & Total  \\\\ \hline 
"""
  footer = """
\hline
\end{tabular}
\caption{Unfolded values for different observables. Statistical plus detector systematic uncertainties, modelling uncertainties, and the total uncertainties are shown.}
\end{table}
\end{center}
  """


  from binningOptimization import binnings_dict


  table = ''


  for var in variables:

     btemp = binnings_dict(var)

     for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc




     binLabel = []

     for i in xrange(len(binning)-1):

       binLabel.append("[ %0.1f, %0.1f ]"%(binning[i],binning[i+1]))

     if 'sum' in var or 'diff' in var or 'corr' in var:
         nameVarFiducial = r'-9$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}
     else:
         nameVarFiducial = r'3$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}

     nameVarFull = '$ %(var)s $' %{'var':'\\'+var.replace('_','')}

     table = table + "\n \n \multicolumn{1}{c}{}  &  \multicolumn{8}{c}{ \\rule{0pt}{3ex} %s}  \\\\ [2ex]  \hline \n \n " % (obs_label[var])

     for i,boundary in enumerate(binLabel):
        table = table + "%(boundary)s & %(central_fiducial)0.3f & $ \pm %(marginalization_fiducial)0.3f $ & $ \pm %(modeling_fiducial)0.3f $ & $ \pm %(total_fiducial)0.3f $ & %(central_full)0.3f & $ \pm %(marginalization_full)0.3f$ & $ \pm %(modeling_full)0.3f $ & $ \pm %(total_full)0.3f $ \\\\ \hline \n" % {

    'boundary':boundary,
    'central_fiducial'        :central[var]   ['fiducial'][i],
    'marginalization_fiducial':uncert[var]    ['fiducial'][i],
    'modeling_fiducial'       :modelling[var] ['fiducial'][i],
    'total_fiducial'          :total[var]     ['fiducial'][i], 
    'central_full'        :central[var]   ['full'][i],
    'marginalization_full':uncert[var]    ['full'][i],
    'modeling_full'       :modelling[var] ['full'][i],
    'total_full'          :total[var]     ['full'][i],

    }

  latex = header + table+ footer


  f1 = open(name,'w')
  f1.write(latex)
  f1.close()

  
        
          

     
 


  

gROOT.SetBatch(True)


if __name__ == '__main__':

    gStyle.SetOptStat(0)

    outDir   = Configuration.outputDirt
    outDirB  = Configuration.outputDirBt

    variables  = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                 'helcorr',\
                 'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 'transheldiff']
    regions    = ['full','fiducial']
    channel    = 'ALL'


    central  = {}
    total    = {}
    modeling = {}
    uncert   = {}


    for var in variables:
        central[var]  = {}
        total[var]    = {}
        modeling[var] = {}
        uncert[var]   = {}        

        for region in regions:
            central[var][region]  = []
            total[var][region]    = []
            modeling[var][region] = []
            uncert[var][region]   = []

 
            if region == 'fiducial':
               outDir = Configuration.outputDir
               outDirB  = Configuration.outputDirB
            else:
               outDir  = Configuration.outputDirt
               outDirB  = Configuration.outputDirBt               

            signal = 'data'
            rpName = 'rpZero'

            from binningOptimization import binnings_dict

            btemp = binnings_dict(var)

            for bn,bc in btemp.iteritems():
               binName = bn
               binning = bc 


            jsondir  = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':var,'channel':'ALL'}
            truth    = jsondir  +'/'+var+'_partonic_btag_'+binName+'.json' #read in the truth distribution
            unf      = jsondir + 'UnfoldedDistribution_'+signal+'_'+var+'_nominal_'+binName+'_'+rpName+'.json'

            truth_val = json.load(open(truth))
            unf_val   = json.load(open(unf))

            fmodeling = getModelingUncertainty(var,region)

            nbins = len(binning) - 1
            bin_arr = array.array('d',binning)

            h_truth = TH1F("truth",var,nbins,bin_arr)
            h_unf   = TH1F("unf",var,nbins,bin_arr)

            h_unf  .Sumw2()
            h_truth.Sumw2()

          
            for bin,val in enumerate(truth_val):
                h_truth.SetBinContent(bin+1,val)

            for bin,val in enumerate(unf_val[0]):
                bin_width = h_truth.GetXaxis().GetBinWidth(bin+1)
                print var,bin_width 
                h_unf.SetBinContent(bin+1,val*bin_width if 'pol' in var else val)
                h_unf.SetBinError(bin+1,unf_val[1][bin])

            int_unf = h_unf.Integral()

            for bin in range(1,nbins+1):
               bin_width = h_truth.GetXaxis().GetBinWidth(bin)

               central[var][region].append(h_unf.GetBinContent(bin)/(int_unf*bin_width))
               total[var][region].append(np.sqrt(pow((fmodeling[bin-1])*h_unf.GetBinContent(bin),2)+pow(h_unf.GetBinError(bin),2))/(int_unf*bin_width))
               modeling[var][region].append(np.sqrt(pow((fmodeling[bin-1])*h_unf.GetBinContent(bin),2))/(int_unf*bin_width))
               uncert[var][region].append(np.sqrt(pow(h_unf.GetBinError(bin),2))/(int_unf*bin_width)) 




    polarizations = ['helpol_plus','helpol_minus','rpol_plus','rpol_minus','transpol_plus','transpol_minus']
    correlations  = ['helcorr','rcorr','transcorr']
    crosssum      = ['rhelsum','transhelsum','transrsum']
    crossdiff      = ['rheldiff','transheldiff','transrdiff']
    makeTable(central, uncert, modeling, total,polarizations,'distributions_polarizations.tex')
    makeTable(central, uncert, modeling, total,correlations,'distributions_correlations.tex')
    makeTable(central, uncert, modeling, total,crosssum,'distributions_crosssum.tex')
    makeTable(central, uncert, modeling, total,crossdiff,'distributions_crossdiff.tex')
 

