##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt


gROOT.SetBatch(True)


def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated value")
  calib_graph.GetYaxis().SetTitle("Unfolded value")
  #calib_graph.GetYaxis().SetTitle("Reconstructed asymmetry")

  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  #DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()

  return [par1,par0]




def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate')
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean


def fitCalibration(truth, unf, dtruth, dunf):


    f = plt.figure()



    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)

    plt.errorbar(x, y, xerr=0.2, yerr=0.4)

    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')

    plt.show();

def plotSlopes(slopes):

    f = plt.figure()
    rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']
    x = range(len(rpNames))
    for binName in slopes:
        y = []
        for rp in rpNames:
            y.append(slopes[binName][rp])
        print x,y
        plt.plot(x, y, 'o',label=binName)
    plt.xticks(x, rpNames)
    plt.ylabel('Slope')
    plt.title(variable)
    plt.axis([-1, len(y)+1,0.9*min(y),max(y)*1.1])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/slopesReg_'+variable+'.png')
    plt.close()

def plotOffsets(offsets):

    import matplotlib.pyplot as plt
    f = plt.figure()
    rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']
    x = range(len(rpNames))
    for binName in offsets:
        y = []
        for rp in rpNames:
            y.append(offsets[binName][rp])
        print x,y
        plt.plot(x, y, 'o',label=binName)
    plt.xticks(x, rpNames)
    plt.ylabel('Offset')
    plt.title(variable)
    plt.axis([-1, len(y)+1,0.9*min(y),max(y)*1.1])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/OffsetsReg_'+variable+'.png')
    plt.close()

def plotUnc(expunc):

    import matplotlib.pyplot as plt
    f = plt.figure()
    rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']
    x = range(len(rpNames))
    for binName in expunc:
        y = []
        for rp in rpNames:
            y.append(expunc[binName][rp])
        print x,y
        plt.plot(x, y, 'o',label=binName)
    plt.xticks(x, rpNames)
    plt.ylabel('Expected Statistical Uncertainty')
    plt.title(variable)
    plt.axis([-1, len(y)+1,0.9*min(y),max(y)*1.1])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/ExpStatUncReg_'+variable+'.png')
    plt.close()







if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  signal          = setSignal2Unfold(sys.argv)
  systematics     = ['nominal']
  rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']#setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'



  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  offsets = {}
  slopes  = {}
  expunc  = {}


  for syst in systematics:


      print "---------------- " + syst + " -------------"

      jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      jsondirOptimization = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      outFolder           = jsondirOptimization

      results = [[9,9],[9,9]]
      newresults = [[9,9],[9,9]]
      for binName in binnings:
          print "binName", binName
          offsets[binName] = {}
          slopes[binName] = {}
          expunc[binName] = {}

          for rpName in rpNames:

             truthf = jsondirOptimization+'/'+variable+'_partonic_btag_'+binName+'.json'
             truth  = json.load(open(truthf)) # Get truth dist
             truthObs = computeObs(variable,truth,binnings[binName])



             filenameResults = jsondirOptimization+'CalibrationResult_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'
             calibration = json.load(open(jsondirOptimization+'Calibration_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'))
             unfolded    = json.load(open(jsondirOptimization+'Unfolded_reco_'+syst+'_'+variable+'_'+binName+'_'+rpName+'.json'))  

             offsets[binName][rpName] =  calibration[1]
             slopes[binName][rpName] = calibration[0]
             expunc[binName][rpName] = unfolded[1]
            
      plotSlopes(slopes)
      plotOffsets(offsets)
      plotUnc(expunc)
