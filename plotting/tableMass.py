from Config import Configuration
import utils
import json
import sys

obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{hel}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{hel}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"



def createLatex(massSyst,name):

  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{|lcc|}
\hline
Variable & Fiducial  & Full \\\\ \hline \hline
"""
  footer = """
\hline
\end{tabular}
\end{table}
\end{center}
  """

  table = ''

  for var in variables:
    region = massSyst[var]
    
    table = table + '%s    & $ %.4f \pm %.4f $ & $ %.4f \pm %.4f $ \\\\ \hline  \n ' % ('\\'+var.replace('_',''), round(region['fiducial'][0],4),round(region['fiducial'][1],4), round(region['full'][0],4),round(region['full'][1],4))



  
  latex = header + table + footer

  f1 = open(name,'w')
  print name
  f1.write(latex)
  f1.close()


if __name__ == '__main__':


  outDir          = Configuration.outputDir

  variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                 'helcorr',\
                 'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 'transheldiff']


  rpName          = 'rpZero' #setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'

  results = {}


  unfolded  = {}
  expunc   = {}
  for var in variables:
    results[var] = {}

    for region in ['full','fiducial']:

       if region == 'full':
         outDir          = Configuration.outputDirt
       else:
         outDir          = Configuration.outputDir


       value = json.load( open(outDir+'/%(var)s/nominal/ALL/massSlope_%(var)s.json'%{'var':var}))

       results[var][region] = value


  createLatex(results,'mass.tex')


