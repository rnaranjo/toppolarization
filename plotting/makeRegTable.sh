#!/bin/bash


     channels="ALL"
       signal="obs"
       PCfarm="Desy" #other options: Prague
         syst="nominal"
          #helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
          var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"
          #var="transheldiff transrsum"
     fiducial="full"




cd $AcDilFBU_DIR/batch

for ch in $channels; do
  for sig in $signal; do
     for sys in $syst; do
         for v in $var; do
          for f in $fiducial;do
             cmd="python $AcDilFBU_DIR/plotting/tableReg.py $v $ch $sys $sig $fiducial"

             echo "running: $cmd"
             $cmd
          done
      done
    done
  done
done
