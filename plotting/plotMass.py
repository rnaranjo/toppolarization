##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
from ROOT import *

gROOT.SetBatch(True)

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
TOPUNC=0.8
obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{hel}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{hel}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"



def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated value")
  calib_graph.GetYaxis().SetTitle("Unfolded value")
  #calib_graph.GetYaxis().SetTitle("Reconstructed asymmetry")

  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  #DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()

  return [par1,par0]




def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate')
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean


def fitCalibration(truth, unf, dtruth, dunf):


    f = plt.figure()



    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)

    plt.errorbar(x, y, xerr=0.2, yerr=0.4)

    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')

    plt.show();

def plotSlopes(slopes):

    f = plt.figure()
    rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']
    x = range(len(rpNames))
    for binName in slopes:
        y = []
        for rp in rpNames:
            y.append(slopes[binName][rp])
        print x,y
        plt.plot(x, y, 'o',label=binName)
    plt.xticks(x, rpNames)
    plt.ylabel('Slope')
    plt.title(variable)
    plt.axis([-1, len(y)+1,0.9*min(y),max(y)*1.1])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/slopesReg_'+variable+'.png')
    plt.close()

def plotOffsets(offsets):

    import matplotlib.pyplot as plt
    f = plt.figure()
    rpNames  = ['rpZero','rp12','rp11','rp9','rp8','rp7','rp6']
    x = range(len(rpNames))
    for binName in offsets:
        y = []
        for rp in rpNames:
            y.append(offsets[binName][rp])
        print x,y
        plt.plot(x, y, 'o',label=binName)
    plt.xticks(x, rpNames)
    plt.ylabel('Offset')
    plt.title(variable)
    plt.axis([-1, len(y)+1,0.9*min(y),max(y)*1.1])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/OffsetsReg_'+variable+'.png')
    plt.close()

def plotMass(unfolded, expunc):

    import matplotlib.pyplot as plt
    f = plt.figure()
    x = range(len(unfolded)-1)

    ax = f.add_subplot(111)
    
    systs = ['topmass_167.5','topmass_170','topmass_175','topmass_177.5']
    systName = ['167.5','170.0','175.0','177.5']


    ymax = 0.10

    for binName in unfolded['nominal']:

      y    = []
      yerr = []

      for syst in systs:

            y.append(unfolded[syst][binName])
            yerr.append(expunc[syst][binName])

      if 'corr' in variable or 'diff' in variable or 'sum' in variable: 
        y = np.array(y)*-9
        yerr = np.array(yerr)*-9 
      
      z = np.polyfit(x, y, 1)

      print z

      graph =  TGraphErrors(len(y),array('f',x),array('f',y),array('f',[0]*len(y)),array('f',yerr))
      par = graph.Fit("pol1")
      fitfn1 = graph.GetFunction("pol1")
      par0 = fitfn1.GetParameter(0)
      err0 = fitfn1.GetParError(0)
      par1 = fitfn1.GetParameter(1)
      err1 = fitfn1.GetParError(1)

      print par1,err1


      xfn = np.linspace(-10,15,100) # 100 linearly spaced numbers
      yfn = z[0]*xfn + z[1] # computing the values of sin(x)/x

      plt.plot(xfn,yfn,label="Fit") 

      plt.errorbar(x, y, fmt='o',yerr=yerr,label="Unfolded")
    
    plt.xticks(x, systName)
    plt.ylabel('Unfolded Value')
    plt.xlabel('Top Mass [GeV]')
    plt.title(obs_label[variable])

    maxy = max(np.fabs(np.array(y)))*5
    plt.axis([-1, len(x)+1,-maxy,maxy])

    if 'corr' in variable or 'diff' in variable or 'sum' in variable: unc = abs(TOPUNC*z[0])
    else: unc = abs(TOPUNC*z[0])

    plt.text( 0.05,0.92, 'ATLAS',fontsize=16, fontweight='bold',style='italic',transform=ax.transAxes)
    plt.text( 0.2,0.92, 'Internal',fontsize=16,style='italic',transform=ax.transAxes,)
    plt.text( 0.05,0.86, "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=16,transform=ax.transAxes,)
    plt.text( 0.05,0.80, r'Slope: $%.2f$ - Uncertainty: $\pm %0.3f$'%(z[0],unc),fontsize=14,style='italic',transform=ax.transAxes,) 

    plt.legend(numpoints=1)
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/Mass_'+variable+'_full.png')
    plt.close()

    return [par1,err1]







if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  signal          = setSignal2Unfold(sys.argv)
  systematics     = ['nominal','topmass_167.5','topmass_170','topmass_175','topmass_177.5']
  rpName          = 'rpZero' #setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'



  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  unfolded  = {}
  expunc   = {}


  for syst in systematics:

      unfolded[syst] = {}
      expunc[syst] = {}



      print "---------------- " + syst + " -------------"

      jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      jsondirOptimization = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      outFolder           = jsondirOptimization

      results = [[9,9],[9,9]]
      newresults = [[9,9],[9,9]]
      for binName in binnings:
          #print "binName", binName



          unfoldedFile    = json.load(open(jsondirOptimization+'Unfolded_reco_'+syst+'_'+variable+'_'+binName+'_'+rpName+'.json'))  

          expunc[syst][binName]    = unfoldedFile[1]
          unfolded[syst][binName]  = unfoldedFile[0]


  #print unfolded          
  unc = plotMass(unfolded,expunc)
  print outDir+'/%(var)s/nominal/ALL/massSlope_%(var)s.json'%{'var':variable}
  with open(outDir+'/%(var)s/nominal/ALL/massSlope_%(var)s.json'%{'var':variable},'w') as f:
    json.dump(unc,f)
