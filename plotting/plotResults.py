from ROOT import *
import utils
import sys
from Config import Configuration
from plot_helper import *
import json
from array import array
import numpy as np
from math import sqrt
VARS = "helpol_plus helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus \
        helcorr transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  \
        transheldiff".split()

HUMAN = {


        'helpol_plus'   : 'Helicity Polarization +',
        'helpol_minus'  : 'Helicity Polarization -',
        'transpol_plus' : 'Transverse Polarization +',
        'transpol_minus': 'Transverse Polarization -',
        'rpol_plus'     : 'R Polarization +',
        'rpol_minus'    : 'R Polarization -',
        'helcorr'       : 'Helicity Correlation',
        'transcorr'     : 'Transverse Correlation',
        'rcorr'         : 'R Correlation',
        'rhelsum'       : 'R Helicity Sum',
        'rheldiff'      : 'R Helicity Diff',
        'transrsum'     : 'Transverse R Sum',
        'transrdiff'    : 'Transverse R Diff',
        'transhelsum'   : 'Transverse Helicity Sum',
        'transheldiff'  : 'Transverse Helicity Diff',
        
        
}        


def plotResult(results):



    x = results[0]
    xerr = results[1]
    totalerr = results[2]
    title = []
    text  = []


    for i,var in enumerate(VARS):
        title.append(HUMAN[var])
        text.append('%0.4f #pm %0.4f' % (results[0][i],results[2][i])) 


    y  = reversed(range(1,len(x)+1))


    yerr= [0.]*len(x)


    c = TCanvas()


    gr = TGraphErrors(len(x),array('d',x),array('d',y),array('d',xerr),array('d',yerr))
    gr.SetTitle("")

    gr.SetMarkerColor(4);
    gr.SetMarkerStyle(20);
    #gr.Draw("AP");
    gr.GetXaxis().SetLimits(-15.,15.)
    gr.GetHistogram().SetMaximum(20.)
    gr.GetHistogram().SetMinimum(0.)
    gr.SetLineColor(4)
    gr.GetYaxis().SetNdivisions(0);
    gr.GetXaxis().SetLabelSize(0.03);
    gr.GetXaxis().SetTitle("Top Spin Observables") ;
    gr.GetXaxis().SetTitleColor(1) ;
    gr.GetXaxis().SetTitleSize(0.05) ;
    gr.GetXaxis().CenterTitle(kTRUE) 

    y  = range(len(x),-1,-1)

    print len(x),array('d',x),array('d',y),array('d',totalerr),array('d',yerr)

    totalgr = TGraphErrors(len(x),array('d',x),array('d',y),array('d',totalerr),array('d',yerr))
    totalgr.SetTitle("")

    totalgr.SetMarkerColor(2);
    totalgr.SetMarkerStyle(20);
    totalgr.SetLineColor(2)
    totalgr.GetXaxis().SetLimits(-15.,15.)
    totalgr.GetHistogram().SetMaximum(20.)
    totalgr.GetHistogram().SetMinimum(0.)

    totalgr.GetYaxis().SetNdivisions(0);
    totalgr.GetXaxis().SetLabelSize(0.03);
    totalgr.GetXaxis().SetTitle("Top Spin Observables") ;
    totalgr.GetXaxis().SetTitleColor(1) ;
    totalgr.GetXaxis().SetTitleSize(0.05) ;
    totalgr.GetXaxis().CenterTitle(kTRUE) 





    mg =  TMultiGraph();
    mg.Add(totalgr,"AP")
    mg.Add(gr,"AP SAME")
    mg.Draw("AP")

    mg.GetXaxis().SetLimits(-0.50,0.50)
    mg.GetHistogram().SetMaximum(20.)
    mg.GetHistogram().SetMinimum(0.)

    mg.GetYaxis().SetNdivisions(0);
    mg.GetXaxis().SetLabelSize(0.03);
    mg.GetXaxis().SetTitle("Top Spin Observables") ;

    mg.GetXaxis().SetTitleColor(1) ;
    mg.GetXaxis().SetTitleSize(0.04) ;
    mg.GetXaxis().CenterTitle(kTRUE) 


    leg = TLegend(0.47, 0.75, 0.88, 0.88);


    leg.SetFillColor(0);
    leg.SetBorderSize(0)
    leg.AddEntry(gr, "ATLAS data", "lp");
    leg.SetTextSize(0.030)
    leg.Draw()

    l = TLatex()
    l.SetTextSize(0.035) 
    l.SetTextColor(1) 
    l.SetTextAlign(12)
    l.SetTextFont(40)
    r = TLatex()
    r.SetTextSize(0.035) ;
    r.SetTextColor(1) ;
    r.SetTextAlign(12);
    #r.SetTextFont(40)

    for i in xrange(len(VARS)):

       yPosition = len(VARS) - i


       l.DrawLatex(-0.45,yPosition,title[i]) 
       r.DrawLatex(0.2,yPosition,text[i])



    make_ATLAS_label( 0.15, 0.83, 1, 0.05, "Internal" )
    make_ATLAS_lumi(       0.32, 0.76, 20.3,    0.05)
    make_ATLAS_energy(     0.15, 0.76, 8,     0.05)

    c.Update()


     #tr.Draw("AP SAME");




    raw_input('PRESS ENTER TO EXIT')


if __name__ == "__main__":


    outDir = Configuration.outputDirt



    fiducial            = utils.setFiducial(sys.argv)

    if fiducial: outDir = Configuration.outputDir



    results = [[],[],[]]

    for var in VARS:

        import binningOptimization
        binnings = binningOptimization.binnings_dict(var)
        binName  = binnings.keys()[0]


        data = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/Unfolded_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))
        
        reco = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/Unfolded_reco_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))

        modeling = json.load(open(outDir+'/%(var)s/nominal/ALL/modeling.json' %{'var':var}))


        results[0].append(data[0])

        results[1].append(reco[1])

        print var,data[0],data[1],modeling,np.sqrt(pow(data[1],2)+pow(modeling,2))

        results[2].append(sqrt(pow(data[1],2)+pow(modeling,2)))




    print results[2]

    plotResult(results)    




