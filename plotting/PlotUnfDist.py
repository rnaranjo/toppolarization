import sys
import json
import array

import ROOT as r
import utils
from Config import Configuration
from ROOT import *

import numpy as np


comparison = {
'helpol_plus' : {

    'plus': {
       'legend':'B_{+}^{k}=0.1','file':'/nfs/dust/atlas/user/raschaef/Unfolding/Partonic/results/helpol_plus/nominal/ALL/4Bin4_3by1/map_incl_btag_4Bin4_histos_reweight_afterNWbugfix.root','histo':'partonicobspos10'
     },

    'minus': {
       'legend':'B_{+}^{k}=-0.1','file':'/nfs/dust/atlas/user/raschaef/Unfolding/Partonic/results/helpol_plus/nominal/ALL/4Bin4_3by1/map_incl_btag_4Bin4_histos_reweight_afterNWbugfix.root','histo':'partonicobsneg10'
     },

},
'helcorr':  {
    'plus': {
       'legend':'C(k,k)=0.39','file':'/nfs/dust/atlas/user/raschaef/Unfolding/Partonic/results/helcorr/nominal/ALL/6Bin2_3by1/map_incl_btag_6Bin2_histos_reweight_afterNWbugfix.root','histo':'partonicobspos39'
     },

    'minus': {
       'legend':'C(k,k)=0.25','file':'/nfs/dust/atlas/user/raschaef/Unfolding/Partonic/results/helcorr/nominal/ALL/6Bin2_3by1/map_incl_btag_6Bin2_histos_reweight_afterNWbugfix.root','histo':'partonicobspos25'
     },



}
}


ADD_MODELS = False

def getModelingUncertainty(var,fiducial):

    
    from binningOptimization import binnings_dict

    # -- Get settings from command line

    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc


    nbins = len(binning) - 1
    bin_arr = array.array('d',binning)

    h_tmp = TH1F("tmp",var,nbins,bin_arr)
    #initialize the array


    estimationDict = {}

    systList = []
    
    systDict = {}

    systematics = ['nominal','perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia']

    for syst in systematics:

       temptList = []
       #if syst.lower() == 'mcatnlo': sf =1.29791
       sf =1


       for pos in xrange(100):

          jsondir    = outDirB+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos,'syst':syst.lower()}  

          jsonDirN   = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'channel':channel,'syst':syst.lower()}
     
          truth = json.load(open(jsondir+'/'+var+'_partonicobspos0_btag_'+binName+'.json'))

          sumt = np.sum(truth)
          
          fname = jsondir+'/UnfoldedDistribution_obspos0_'+var+'_'+syst+'_'+binName+'_rpZero.json'

          try:

              distribution = json.load(open(fname))
              if 'pol' in var:
                for i in xrange(len(distribution[0])):
                  bin_width = h_tmp.GetXaxis().GetBinWidth(i+1)
                  distribution[0][i] = bin_width*distribution[0][i]
                  

              

              sumu = np.sum(distribution[0])
              
              temptList.append((np.array(distribution[0])*(sumt/sumu)-np.array(truth)*sf)/np.array(truth)*sf)

          except:

              print "error in" , fname   
 

       #[[1, 2], [3, 4], [5, 6]]
       totalList = []
       #print "Binning:", binning
       #print temptList[0]
       for i in xrange(len(binning)-1):
           totalList.append([temptList[j][i] for j in xrange(len(temptList))])


        

        

       systDict[syst] = np.array([np.mean(a) for a in totalList ])
    #print systDict
    estimationDict['mc'] = np.fabs(systDict['mcatnlo']-systDict['herwig'])
    estimationDict['shower'] = np.fabs(systDict['fastpythia']-systDict['herwig']) 
    estimationDict['isrfsr'] = (np.fabs(systDict['radhi']-systDict['radlo']))/2
    estimationDict['color'] = np.fabs(systDict['perugia']-systDict['perugialocr'])
    estimationDict['underlying'] = np.fabs(systDict['perugia']-systDict['perugiampihi'])

    total = np.sqrt(estimationDict['mc']*estimationDict['mc'] + estimationDict['shower']*estimationDict['shower'] + estimationDict['isrfsr']*estimationDict['isrfsr'] + estimationDict['color']*estimationDict['color'] + estimationDict['underlying']*estimationDict['underlying'])     


    print estimationDict
    return total
   
       


gROOT.SetBatch(True)


if __name__ == '__main__':

    gStyle.SetOptStat(0)

    #NOTE: no modelling uncertainties added yet

    norm_unit = False #normalize to unity
    norm_dsigma = True #normalize to the differential cross section

    outDir  = Configuration.outputDirt
    outDirB  = Configuration.outputDirBt

#    channel = utils.setChan(sys.argv)
    channel = 'ALL'
    var = utils.setVar(sys.argv)
    region = 'full'

    fiducial  = utils.setFiducial(sys.argv)
    label = 'Parton level'
    if fiducial:
       label = 'Particle level'
       region = 'fiducial' 
       outDir = Configuration.outputDir
       outDirB  = Configuration.outputDirB
    print "-----------Region: ", region

#    syst               = setSyst(sys.argv)
    signal = 'data'
#    rpName,rpValue     = utils.setRegPar(sys.argv, 'ttbar')
#    rpName = 'rp95'
    rpName = 'rpZero'

    from binningOptimization import binnings_dict

    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
       binName = bn
       binning = bc 



    obs_label = {}

    obs_label["helpol_plus"]="cos#theta_{#plus}^{k}"
    obs_label["helpol_minus"]="cos#theta_{#minus}^{k}"
    obs_label["transpol_plus"]="cos#theta_{#plus}^{n}"
    obs_label["transpol_minus"]="cos#theta_{#minus}^{n}"
    obs_label["rpol_plus"]="cos#theta_{#plus}^{r}"
    obs_label["rpol_minus"]="cos#theta_{#minus}^{r}"
    obs_label["helcorr"]="cos#theta_{#plus}^{k} cos#theta_{#minus}^{k}"
    obs_label["transcorr"]="cos#theta_{#plus}^{n} cos#theta_{#minus}^{n}"
    obs_label["rcorr"]="cos#theta_{#plus}^{r} cos#theta_{#minus}^{r}"
    obs_label["transhelsum"]="cos#theta_{#plus}^{n} cos#theta_{#minus}^{k} + cos#theta_{#plus}^{k} cos#theta_{#minus}^{n}"
    obs_label["transheldiff"]="cos#theta_{#plus}^{n} cos#theta_{#minus}^{k} - cos#theta_{#plus}^{k} cos#theta_{#minus}^{n}"
    obs_label["transrsum"]="cos#theta_{#plus}^{n} cos#theta_{#minus}^{r} + cos#theta_{#plus}^{r} cos#theta_{#minus}^{n}"
    obs_label["transrdiff"]="cos#theta_{#plus}^{n} cos#theta_{#minus}^{r} - cos#theta_{#plus}^{r} cos#theta_{#minus}^{n}"
    obs_label["rhelsum"]="cos#theta_{#plus}^{r} cos#theta_{#minus}^{k} + cos#theta_{#plus}^{k} cos#theta_{#minus}^{r}"
    obs_label["rheldiff"]="cos#theta_{#plus}^{r} cos#theta_{#minus}^{k} - cos#theta_{#plus}^{k} cos#theta_{#minus}^{r}"

    jsondir  = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':var,'channel':channel}
    print 'jsondir:',jsondir

    truth = jsondir  +'/'+var+'_partonic_btag_'+binName+'.json' #read in the truth distribution
    
    unf = jsondir + 'UnfoldedDistribution_'+signal+'_'+var+'_nominal_'+binName+'_'+rpName+'.json'
    print 'unf:',unf

    truth_val = json.load(open(truth))
    unf_val = json.load(open(unf))


    modeling = getModelingUncertainty(var,region)

    canv = TCanvas('canvas',var,700,800)
    canv.Divide(1,2,0,0)
    canv.cd(1)
    gPad.SetPad(0.,.3,0.95,0.95)
    gPad.SetRightMargin(0.02)
    gPad.SetTopMargin(0.01)
    gPad.SetLeftMargin(0.20)
    gPad.SetBottomMargin(0.02)

    nbins = len(binning) - 1
    bin_arr = array.array('d',binning)

    h_truth = TH1F("truth",var,nbins,bin_arr)
    h_unf = TH1F("unf",var,nbins,bin_arr)

    h_truth.SetMarkerStyle(8)
    h_unf.SetMarkerStyle(24)
    h_unf.SetLineColor(1)
    h_unf.SetLineWidth(2)
    h_unf.SetMarkerStyle(20)
    h_unf.SetMarkerSize(1.3)
    h_truth.SetLineWidth(2)
    h_unf.SetLineWidth(2)


    if ADD_MODELS:

      f_mod_plus = TFile(comparison[var]['plus']['file'])
      h_mod_plus = f_mod_plus.Get(comparison[var]['plus']['histo']) 
      h_mod_plus.SetLineWidth(2) 
      #h_mod_plus.SetMarkerStyle(8)
      h_mod_plus.SetLineColor(8)


      f_mod_minus = TFile(comparison[var]['plus']['file'])
      h_mod_minus = f_mod_minus.Get(comparison[var]['minus']['histo']) 
      h_mod_minus.SetLineWidth(2) 
      #h_mod_minus.SetMarkerStyle(8)
      h_mod_minus.SetLineColor(2) 

      








    h_unf.Sumw2()
    h_truth.Sumw2() #dont have uncertainties on this one
    h_unf.SetTitle('')

    if norm_unit:
        h_unf.GetYaxis().SetTitle('Unit-Normalized')
    elif norm_dsigma:
        h_unf.GetYaxis().SetTitle('#frac{1}{#sigma} #frac{d#sigma}{d('+obs_label[var]+')}')
    else:
        h_unf.GetYaxis().SetTitle('#Events')
 #   h_unf.GetYaxis().SetLabelSize(0.07)
    h_unf.GetYaxis().SetTitleSize(0.06)
    h_unf.GetYaxis().SetTitleOffset(1.2)
    h_unf.GetYaxis().SetLabelSize(0.055)
    #h_unf.GetXaxis().SetLabelOffset(0.14)
    h_unf.GetXaxis().SetLabelSize(0)

    for bin,val in enumerate(truth_val):
        h_truth.SetBinContent(bin+1,val)

    for bin,val in enumerate(unf_val[0]):
        bin_width = h_truth.GetXaxis().GetBinWidth(bin+1)
        h_unf.SetBinContent(bin+1, val*bin_width if 'pol' in var else val)
        h_unf.SetBinError(bin+1,unf_val[1][bin])


    if norm_dsigma:

        int_truth = h_truth.Integral()
        int_unf = h_unf.Integral()

        for bin in range(1,nbins+1):

            bin_width = h_truth.GetXaxis().GetBinWidth(bin)


            #print "Before:", h_unf.GetBinContent(bin), h_unf.GetBinError(bin)
 
            h_truth.SetBinContent(bin,h_truth.GetBinContent(bin)/(int_truth*bin_width))

            if ADD_MODELS:
              h_mod_plus.SetBinContent(bin,h_mod_plus.GetBinContent(bin)/(int_truth*bin_width))
              h_mod_minus.SetBinContent(bin,h_mod_minus.GetBinContent(bin)/(int_truth*bin_width)) 
              

            h_unf.SetBinContent(bin,h_unf.GetBinContent(bin)/(int_unf*bin_width))

            #print "After:",bin, bin_width, h_unf.GetBinContent(bin),h_unf.GetBinError(bin)
            #h_unf.SetBinError(bin,h_unf.GetBinError(bin)/(int_unf*bin_width))
            #print "After:",bin, bin_width, h_unf.GetBinContent(bin),h_unf.GetBinError(bin)
            h_unf.SetBinError(bin,np.sqrt(pow((modeling[bin-1])*h_unf.GetBinContent(bin)*int_unf*bin_width,2)+pow(h_unf.GetBinError(bin),2))/(int_unf*bin_width))

    #print "Fit Unfolded Polarization:",np.polyfit(range(1,nbins+1),unf_val[0],1)
    #print "Fit Error Polarization:",np.polyfit(range(1,nbins+1),np.array(unf_val[0])*np.array(modeling)+np.array(unf_val[0]),1)

#    h_unf.SetMaximum(1.2*h_unf.GetMaximum())



    h_truth.SetLineColor(kBlue-3)
    h_truth.SetLineColorAlpha(kBlue, 0.75)

    h_truth.SetMarkerColor(kBlue-3)
    h_unf.SetLineColor(1)
    h_unf.SetMarkerColor(1)

    hmodelling = h_unf.Clone()

    hmodelling.SetName("Modeling uncertainty")
    hmodelling.SetFillStyle(3354)
    hmodelling.SetFillColor(kBlack)
    hmodelling.SetLineColor(0)
    hmodelling.SetLineWidth(0)
    hmodelling.SetMarkerSize(0)
 

    for i in xrange(hmodelling.GetNbinsX()):
        hmodelling.SetBinError(i+1,np.sqrt(pow((modeling[i])*h_unf.GetBinContent(i+1),2)+pow(h_unf.GetBinError(i+1),2)))

    if ADD_MODELS:leg = TLegend(0.58,0.72,0.96,0.95)
    else: leg = TLegend(0.55,0.78,0.96,0.95)
#    leg = TLegend(0.75,0.75,0.95,0.90)
    leg.SetTextFont(62)

    leg.SetHeader(label)
    leg.AddEntry(h_unf,'data','lp')
    leg.AddEntry(h_truth,'POWHEG-hvq+PYTHIA6','l')

    if ADD_MODELS:
      leg.AddEntry(h_mod_plus,comparison[var]['plus']['legend'],'l')
      leg.AddEntry(h_mod_minus,comparison[var]['minus']['legend'],'l')
    #leg.SetFillColor(0)
    #leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(62)

    if norm_unit:
        h_unf.Scale(1./h_unf.Integral())
        h_truth.Scale(1./h_truth.Integral())

#    h_unf.SetTitle("")
#    h_unf.GetYaxis().SetTitle("# events")
    if ADD_MODELS:h_unf.SetMaximum(1.7*h_unf.GetMaximum())
    else:h_unf.SetMaximum(1.6*h_unf.GetMaximum())
    #if 'sum' in var or 'diff' in var:
    #    h_unf.SetMaximum(1.5*h_truth.GetMaximum())
    h_unf.SetMinimum(0.00)

    h_unf.Draw("E X0")
    h_truth.Draw('same')
    
    if ADD_MODELS:
     h_mod_plus.Draw("HIST same")
     h_mod_minus.Draw("HIST same")
     h_unf.Draw("E X0 same")
    #hmodelling.Draw("E2 SAME")
    leg.Draw('same')

    g = TLatex()
    g.SetNDC()
    g.SetTextSize(0.045)#55
    g.SetTextColor(1)
#    g.DrawLatex(0.15,0.84,"#font[72]{ATLAS}")
#    g.DrawLatex(0.24,0.84, "Internal")
#    g.DrawLatex(0.15,0.79, "Work in Progress");                          
    #if 'pol' in var:
    #    g.DrawLatex(0.15,0.34,"#font[72]{ATLAS}")
    #    g.DrawLatex(0.26,0.34, "Internal")
    #    g.SetTextSize(0.05)
    #    tmpplumi = "#sqrt{s} = 8 TeV, 20.3 fb^{-1}"
    #    g.DrawLatex(0.15,0.25,tmpplumi)
    #    #g.DrawLatex(0.15,0.175, "#sqrt{s} = 8 TeV")
    #    #g.DrawLatex(0.30,0.175, label)
    #else:
    g.DrawLatex(0.25,0.90,"#font[72]{ATLAS}")
    #g.DrawLatex(0.37,0.90, "Internal") #40
    g.SetTextSize(0.045)
    tmpplumi = "#sqrt{s} = 8 TeV, 20.2 fb^{-1}"
    g.DrawLatex(0.25,0.80,tmpplumi)

        #g.DrawLatex(0.30,0.775, label)                                                                                            

    #g.SetTextSize(0.04)
    #tmpplumi = "dilepton - #int L dt = 20.3 fb^{-1}"
    #    g.DrawLatex(0.75,0.55,"incl. channel");                                                                                                                                               

    #g.DrawLatex(0.15,0.75,tmpplumi)
    #g.DrawLatex(0.15,0.70, "#sqrt{s} = 8 TeV")


    canv.cd(2)
    gPad.SetPad(0.0,0.0,0.95,0.3)
    gPad.SetRightMargin(0.02)
    gPad.SetLeftMargin(0.2)
    gPad.SetBottomMargin(0.40)    
    gPad.SetTopMargin(0.06)
    h_ratio = h_unf.Clone('ratio')
    h_ratio.SetTitle('')
    h_ratio.SetLineColor(1)
    h_ratio.SetMarkerColor(1)

    h_ratio.Divide(h_truth)
    h_ratio.Print()
    if ADD_MODELS:
       h_ratio_mod_plus = h_mod_plus.Clone("ratio_mod_plus")
       h_ratio_mod_plus.Divide(h_truth)
       h_ratio_mod_minus = h_mod_minus.Clone("ratio_mod_minus")
       h_ratio_mod_minus.Divide(h_truth) 
#    print h_ratio.GetBinContent(1),h_ratio.GetBinContent(2),h_ratio.GetBinContent(3),h_ratio.GetBinContent(4)

#    h_ratio.GetXaxis().SetTitle(h_unf.GetXaxis().GetTitle())
#    h_ratio.GetYaxis().SetTitle('unf./truth')
#    h_ratio.GetYaxis().SetRangeUser(0.5,1.5)
    h_ratio.GetXaxis().SetTitle(obs_label[var])
    h_ratio.GetXaxis().SetLabelSize(0.12)
    h_ratio.GetXaxis().SetTitleSize(0.14)
    h_ratio.GetXaxis().SetTitleOffset(1.10)
    h_ratio.GetYaxis().SetTitle("#frac{Data}{Pred.}")
#    h_ratio.GetYaxis().SetRangeUser(0.65,1.35)
    if signal == 'reco': h_ratio.GetYaxis().SetRangeUser(0.9,1.1)
    else: h_ratio.GetYaxis().SetRangeUser(0.5,1.5)
    h_ratio.GetYaxis().SetTitleSize(0.12)
    h_ratio.GetYaxis().SetTitleOffset(0.57)
    h_ratio.GetYaxis().SetLabelSize(0.12)
    h_ratio.GetYaxis().SetNdivisions(105)
    h_ratio.GetYaxis().CenterTitle(True)
    func = TF1( "func", "1", h_ratio.GetXaxis().GetXmin(), h_ratio.GetXaxis().GetXmax() );
    func.SetLineWidth(1);
    func.SetLineColor(kBlue -3);

    func.SetLineWidth(2);
    func.SetLineStyle(2);

    h_ratio.Draw("E X0")
    func.Draw("same")
    if ADD_MODELS:
       h_ratio_mod_plus.Draw("HIST same")
       h_ratio_mod_minus.Draw("HIST same")
       h_ratio.Draw("E X0 same")
#    func.Draw()a

    canv.Draw()

    #raw_input('WAIT')

#    canv.SaveAs(var+'/unf/'+var+'_'+signal+'_'+binName+'_fixedseed.pdf')
#    canv.SaveAs(var+'/unf/'+var+'_'+signal+'_'+binName+'_nuisancefit_fixedseed.pdf')

    if norm_unit:
        canv.SaveAs('unf/'+var+'_'+signal+'_'+binName+'_'+rpName+'_'+region+'_norm_unit.pdf')
    elif norm_dsigma:
        canv.SaveAs('unf/'+var+'_'+signal+'_'+binName+'_'+rpName+'_'+region+'_norm_dsigma.pdf')
    else:
        canv.SaveAs('unf/'+var+'_'+signal+'_'+binName+'_'+rpName+'_'+region+'_notnorm.pdf')
