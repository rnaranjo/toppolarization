##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt


gROOT.SetBatch(True)


def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated value")
  calib_graph.GetYaxis().SetTitle("Unfolded value")
  #calib_graph.GetYaxis().SetTitle("Reconstructed asymmetry")

  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  #DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()

  return [par1,par0]




def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate')
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean


def fitCalibration(truth, unf, dtruth, dunf):


    f = plt.figure()



    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)

    plt.errorbar(x, y, xerr=0.2, yerr=0.4)

    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')

    plt.show();

def plotSlopes(slopes,errors):



    f = plt.figure()


    labels = []
    x     = range(len(slopes))
    value = []


    for binName,slope in slopes.iteritems():
        labels.append(binName)
        value.append(slope)


    plt.plot(x, value, 'ro')
    if errors[0][0] != 9: plt.axhspan(errors[0][0]-errors[0][1], errors[0][0]+errors[0][1], facecolor='0.5', alpha=0.2)
    plt.xticks(x, labels)
    plt.ylabel('Slopes')
    plt.title('Slopes of calibration curves '  + variable)

    plt.axis([-1, len(value)+1,0.9*min(value),max(value)*1.1])



    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/slopes_'+variable+'_'+rpName+'.png')
    plt.close()

def plotOffsets(offsets,errors):

    import matplotlib.pyplot as plt


    labels = []
    x     = xrange(len(offsets))
    value = []

    for binName,slope in offsets.iteritems():
        labels.append(binName)
        value.append(slope)



    plt.plot(x, value, 'ro')
    if errors[0][0] != 9: plt.axhspan(errors[1][0]-errors[1][1],errors[1][0]+errors[1][1], facecolor='0.5', alpha=0.2)

    plt.xticks(x, labels)
    plt.ylabel('Offset')
    plt.title('Offsets of calibration curves ' + variable)
    plt.axis([-1, len(value),-0.01,0.01])

    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/offsets_'+variable+'_'+rpName+'.png')




    #plt.show()
    plt.close()

if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  signal          = setSignal2Unfold(sys.argv)
  systematics     = ['nominal','herwig','mcatnlo','perugia','perugialocr','perugiampihi','radhi','radlo']
  rpNames         = ['rpZero']#setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'



  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  offsets = {}
  slopes  = {}
  center  = {}
  stat    = {}

  for syst in systematics:

      offsets[syst] = {}
      slopes[syst] = {}
      center[syst] = {}
      stat[syst] = {}


      jsondirNominal      = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      jsondirOptimization = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      outFolder           = jsondirOptimization

      results = [[9,9],[9,9]]
      newresults = [[9,9],[9,9]]
      for binName in binnings:
          offsets[syst][binName] = {}
          slopes [syst][binName] = {}
          center[syst][binName]  = {}
          stat[syst][binName]    = {}

          for rpName in rpNames:
              

             truthf = jsondirOptimization+'/'+variable+'_partonic_btag_'+binName+'.json'
             truth  = json.load(open(truthf)) # Get truth dist
             truthObs = computeObs(variable,truth,binnings[binName])



             filenameResults = jsondirOptimization+'CalibrationResult_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'
             calibration = json.load(open(jsondirOptimization+'Calibration_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'))
             unfolded    = json.load(open(jsondirNominal+'Unfolded_reco_nominal_'+variable+'_'+binName+'_'+rpName+'.json'))  

             offsets[syst][binName][rpName] = calibration[1]
             slopes[syst][binName][rpName] = calibration[0]*unfolded[0]
             center[syst][binName][rpName] = unfolded[0]
             stat[syst][binName][rpName] = unfolded[1]

            

   

  for key in offsets['nominal']:

      print "--------",key,"----------"


      for rp in ['rpZero']:

         mcGenerator = sqrt(pow(abs(slopes['herwig'][key][rp]-slopes['mcatnlo'][key][rp]),2)+pow(abs(offsets['herwig'][key][rp]-offsets['mcatnlo'][key][rp]),2)) 
         isrFsr      = sqrt(pow(abs(slopes['radhi'][key][rp]-slopes['radlo'][key][rp])/2,2)+pow(abs(offsets['radhi'][key][rp]-offsets['radlo'][key][rp])/2,2))
         colorRe     = sqrt(pow(abs(slopes['perugia'][key][rp]-slopes['perugialocr'][key][rp]),2)+pow(abs(offsets['perugia'][key][rp]-offsets['perugialocr'][key][rp]),2))
         urEvent     = sqrt(pow(abs(slopes['perugia'][key][rp]-slopes['perugiampihi'][key][rp]),2)+pow(abs(offsets['perugia'][key][rp]-offsets['perugiampihi'][key][rp]),2))


         totalModelling = sqrt(pow(mcGenerator,2)+pow(isrFsr,2)+pow(colorRe,2)+pow(urEvent,2))
         total          = sqrt(pow(stat['nominal'][key][rp],2)+pow(totalModelling,2))



         print "MCGenerator:",  abs(slopes['herwig'][key][rp]-slopes['mcatnlo'][key][rp]),abs(offsets['herwig'][key][rp]-offsets['mcatnlo'][key][rp])
         print "ISR/FSR:"     ,  abs(slopes['radhi'][key][rp]-slopes['radlo'][key][rp])/2,abs(offsets['radhi'][key][rp]-offsets['radlo'][key][rp])/2
         print "ColorReconnection:", abs(slopes['perugia'][key][rp]-slopes['perugialocr'][key][rp]),abs(offsets['perugia'][key][rp]-offsets['perugialocr'][key][rp])
         print "UnderlyingEvent:"  , abs(slopes['perugia'][key][rp]-slopes['perugiampihi'][key][rp]),abs(offsets['perugia'][key][rp]-offsets['perugiampihi'][key][rp])

         print "StatisticalUnc:",stat['nominal'][key][rp]
         print "TotalModeling:",totalModelling 
         print "Total:",total










                  

      
    


