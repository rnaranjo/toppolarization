import sys
import json
import os
import numpy as np
from array import array

from Config import Configuration
from utils import *
import ROOT


def getPulls(bins = False, true = [], unfolded = [], error = [], plot = False, folder='', name = '' ):

    if bins:
        mean        = []
        rms         = []
        numberBins  = len(true)


        for i in xrange(numberBins):
            tmp = [(unfolded[i][j]-true[i])/error[i][j] for j in xrange(len(unfolded[i]))]
            mean.append(np.mean(tmp))
            rms.append(np.std(tmp))
        return mean, rms



    pulls = [(unfolded[j] - true)/error[j] for j in xrange(len(unfolded))]
    expectedUnc = np.mean([error[j] for j in xrange(len(unfolded))])
    if plot:

        import matplotlib.pyplot as plt
        mu, sigma =  np.mean(pulls ),np.std (pulls )
        count, bins, ignored = plt.hist(pulls, 20, normed=True)
        plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
                   np.exp( - (bins - mu)**2 / (2 * sigma**2) ),
               linewidth=2, color='r')
        plt.savefig(folder+name+'.eps')
        plt.close()

    mean = np.mean(pulls )
    rms  = np.std (pulls )

    return mean,rms,expectedUnc

def getRelativeDifference(bins = False, true = [], unfolded = [], error = []):

    if bins:
        mean        = []
        rms         = []
        numberBins  = len(true)


        for i in xrange(numberBins):
            tmp = [(unfolded[i][j]-true[i])/true[i] for j in xrange(len(unfolded[i]))]
            mean.append(np.mean(tmp))
            rms.append(np.std(tmp))
        return mean, rms



    pulls = [(unfolded[j] - true)/true for j in xrange(len(unfolded))]

    expectedUnc = np.mean([error[j] for j in xrange(len(unfolded))])


    mean = np.mean(pulls )
    rms  = np.std (pulls )

    return mean,rms,expectedUnc

def getChi2(true = [], unfolded = [], errors = [], distribution = False):

    ordered = []
    orderedError = []



    for i in xrange(len(unfolded[0])):
        ordered.append([])
        orderedError.append([])
        for j in xrange(len(unfolded)):
          ordered[i].append(unfolded[j][i])
        for j in xrange(len(errors)):
          orderedError[i].append(errors[j][i])

    from scipy.stats import chisquare
    histoUnfolded =  ROOT.TH1F('unfolded','',len(unfolded[0]),0,len(unfolded[0]))
    histoTruth    =  ROOT.TH1F('truth','',len(unfolded[0]),0,len(unfolded[0]))

    for j,content in enumerate(true):
        histoTruth.SetBinContent(j+1,content)

    chi2 = []

    #print true,ordered

    for i in xrange(len(ordered)):
        print ordered[i]
        for j,content in enumerate(ordered[i]):
            histoUnfolded.SetBinContent(j+1,content)
            histoUnfolded.SetBinError(j+1,orderedError[i][j])
        chi2.append(histoTruth.Chi2Test(histoUnfolded,'WW CHI2'))
    print chi2
    if distribution: return chi2
    return np.mean(chi2),np.std(chi2)


def plotSummaryPulls(observables,distributions):

    import matplotlib.pyplot as plt

    f = plt.figure(figsize=(14, 6))
    ax = plt.subplot(111)


    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    first = True
    order = [ '4Bin1', '4Bin2','4Bin4','6Bin1','6Bin2']
    for binName in order:
        pulls = distributions[binName] 
        if first or len(x) != len(pulls[0]) :
            x       = np.array(xrange(1,len(pulls[0])+1))
            first   =   False

        ax.errorbar(x, pulls[0], yerr=pulls[1], fmt="s", label="Bin: %s \nPull | $\mu= %0.2f$, $\sigma=%0.2f$ | Expected Uncertainty %0.4f " % (binName,observables[binName][0],observables[binName][1],observables[binName][2]))
        x = x + 0.08

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.5, box.height])

# Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel("Bin Number")
    plt.ylabel("Pulls")
    plt.title('Pulls of individual bins ' + variable.replace('_',' '))
    plt.axis([0, (len(pulls[0])+3), -max(np.array(pulls[1]) + np.array(pulls[0]))*1.5, max(np.array(pulls[1]) + np.array(pulls[0]))*1.5])


    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/pull_'+variable+'.eps')
    plt.close()



def plotSummaryRelative(observables,distributions,chi2):

    import matplotlib.pyplot as plt

    f = plt.figure(figsize=(14, 6))
    ax = plt.subplot(111)


    plt.rc('text', usetex=True)
    #plt.rc('font', family='sans')

    first = True

    order = [ '4Bin1', '4Bin2','4Bin4','6Bin1','6Bin2']
    for binName in order:
        pulls = distributions[binName]
    
#    for binName,pulls in distributions.iteritems():

        if first or len(x) != len(pulls[0]):
            x       = np.array(xrange(1,len(pulls[0])+1))
            first   =   False

        ax.errorbar(x, pulls[0], yerr=pulls[1], fmt="s", label="Bin: %s \nRelative Difference |  Average ${\chi}^{2}$ $%0.1f$" % (binName,chi2[binName][0]))
        x = x + 0.05

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.5, box.height])

# Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    plt.xlabel("Bin Number")
    plt.ylabel("Relative Difference")
    plt.title('Relative Difference '+ variable.replace('_',' '))
    plt.axis([0, (len(pulls[0])+3), -max(np.array(pulls[1]) + np.array(pulls[0]))*1.5, max(np.array(pulls[1]) + np.array(pulls[0]))*1.5])


    #plt.legend()

    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/relative_'+variable+'.eps')
    plt.close()

def plotChi2(chi2):

    import matplotlib.pyplot as plt

    f = plt.figure(figsize=(14, 6))
    ax = plt.subplot(111)



    plt.rc('text', usetex=True)

    first = True

    bins = xrange(0,20,1)
    for binName,values in chi2.iteritems():

        if np.mean(values) > 20: continue

        n, bins, patches = ax.hist(values, bins=bins,  histtype='step',label="Bin: %s \n $\mu = %0.2f$, $\sigma=%0.2f$"%(binName,np.mean(values),np.std(values)))




    plt.xlabel("Chi2")
    plt.ylabel("Events")

    plt.title('Chi2 distribution '+ variable.replace('_',' '))



    plt.legend()

    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/histogram_'+variable+'.eps')
    plt.close()



if __name__ == '__main__':

    outDir          = Configuration.outputDirt
    channel         = setChan(sys.argv)
    variable        = setVar(sys.argv)
    signal          = setSignal2Unfold(sys.argv)
    syst            = 'nominal'
    rpName,rpValue  = 'rpZero', 0.0
    regFunction     = 'Tikhonov'
    folder          = 'stat'

    nens        = 2000
    nEvtPerJob  = 20

    jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
    jsondirOptimization = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
    outFolder           = jsondirOptimization+'/'+folder+'/'



    import binningOptimizationPlots
    binnings = binningOptimizationPlots.binnings_dict(variable)

    observable      = {}
    observableErr   = {}
    distribution    = {}
    distributionErr = {}
    observablePulls      = {}
    distributionPulls    = {}
    observableRelativeDifference    = {}
    distributionRelativeDifference  = {}
    chi2Value  = {}
    distributionChi2  = {}

    order = [ '4Bin1', '4Bin2','4Bin4','6Bin1','6Bin2']

    for binName in order: # binnings.iteritems():
        print binName
        bins = binnings[binName]

        observable[binName]      = []
        observableErr[binName]   = []
        distribution[binName]    = [[] for i in xrange(len(bins)-1)]
        distributionErr[binName] = [[] for i in xrange(len(bins)-1)]


        partonicDistribution     = json.load(open(jsondirOptimization+variable+'_partonic_btag_'+binName+'.json'))
        partonicObservable       = computeObs(variable,partonicDistribution,bins)


        for x in xrange(1, nens/nEvtPerJob+1):

           n_init = int(1+(x-1)*nEvtPerJob)
           n_fin  = int(x*nEvtPerJob)

           observableFile       = outFolder+'Syst_'+signal+'_'+variable+'_'+binName+'_'+regFunction+'_'+rpName+'_ensembles_'+str(n_init)+'_'+str(n_fin)+'.json'
           observableErrFile    = outFolder+'SystErr_'+signal+'_'+variable+'_'+binName+'_'+regFunction+'_'+rpName+'_ensembles_'+str(n_init)+'_'+str(n_fin)+'.json'
           distributionFile     = outFolder+'SystDistribution_'+signal+'_'+variable+'_'+binName+'_'+regFunction+'_'+rpName+'_ensembles_'+str(n_init)+'_'+str(n_fin)+'.json'
           distributionErrFile  = outFolder+'SystDistributionErr_'+signal+'_'+variable+'_'+binName+'_'+regFunction+'_'+rpName+'_ensembles_'+str(n_init)+'_'+str(n_fin)+'.json'

           try: observable[binName]     = observable[binName] + json.load(open(observableFile))
           except: pass

           try: observableErr[binName]  = observableErr[binName] + json.load(open(observableErrFile))
           except: pass

           try:
               for i,content in enumerate(zip(*json.load(open(distributionFile)))):
                   distribution[binName][i]     = distribution[binName][i] + list(content)
               for i,content in enumerate(zip(*json.load(open(distributionErrFile)))):
                   distributionErr[binName][i]  = distributionErr[binName][i] + list(content)
           except: pass

        observablePulls[binName]   = getPulls(False, partonicObservable, observable[binName], observableErr[binName], True, outFolder,'PullObservable'+binName)
        distributionPulls[binName] = getPulls(True, partonicDistribution, distribution[binName], distributionErr[binName])
        observableRelativeDifference[binName] = getRelativeDifference(False, partonicObservable, observable[binName], observableErr[binName])
        distributionRelativeDifference[binName] = getRelativeDifference(True, partonicDistribution, distribution[binName], distributionErr[binName])
        chi2Value[binName] = getChi2(partonicDistribution,distribution[binName],distributionErr[binName])
        distributionChi2[binName] = getChi2(partonicDistribution,distribution[binName],distributionErr[binName],True)


    plotChi2(distributionChi2)
    plotSummaryPulls(observablePulls,distributionPulls)

    plotSummaryRelative(observableRelativeDifference,distributionRelativeDifference,chi2Value)
