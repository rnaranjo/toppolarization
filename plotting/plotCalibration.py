##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt


gROOT.SetBatch(True)

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
TOPUNC=0.8
obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{k}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{k}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"



def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated value")
  calib_graph.GetYaxis().SetTitle("Unfolded value")
  #calib_graph.GetYaxis().SetTitle("Reconstructed asymmetry")

  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  #DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()

  return [par1,par0]




def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate')
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean


def fitCalibration(truth, unf, dtruth, dunf):


    f = plt.figure()



    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)

    plt.errorbar(x, y, xerr=0.2, yerr=0.4)

    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')

    plt.show();

def plotSlopes(slopes,errors):



    f = plt.figure()


    labels = []
    x     = range(len(slopes))
    value = []


    for binName,slope in slopes.iteritems():
        labels.append(binName)
        value.append(slope)
    f = plt.figure()
    ax = f.add_subplot(111)


    plt.plot(x, value, 'ro')
    if errors[0][0] != 9: plt.axhspan(errors[0][0]-errors[0][1], errors[0][0]+errors[0][1], facecolor='0.5', alpha=0.2)
    plt.xticks(x, labels)
    plt.ylabel('Slopes')
    plt.title('Slopes of calibration curves '  + obs_label[variable])

    plt.axis([-1, len(value)+1,0.9*min(value),max(value)*1.1])


    #plt.text( 0.05,0.92, 'ATLAS',fontsize=16, fontweight='bold',style='italic',transform=ax.transAxes)
    #plt.text( 0.2,0.92, 'Internal',fontsize=16,style='italic',transform=ax.transAxes,)
    plt.text( 0.05,0.86, "$\sqrt{s}$ = $8$ TeV, $20.2$ ${fb}^{-1}$",fontsize=16,transform=ax.transAxes,)
    if errors[0][0] != 9: plt.text( 0.05,0.80, r'Slope uncertainty: $\pm %.4f$ '%(errors[1][1]),fontsize=14,style='italic',transform=ax.transAxes)

    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/slopesWithError_'+variable+'_'+rpName+'.eps')
    plt.close()

def plotOffsets(offsets,errors):

    import matplotlib.pyplot as plt


    labels = []
    x     = xrange(len(offsets))
    value = []

    for binName,slope in offsets.iteritems():
        labels.append(binName)
        value.append(slope)

    f = plt.figure()
    ax = f.add_subplot(111)

    plt.plot(x, value, 'ro')
    if errors[0][0] != 9: plt.axhspan(errors[1][0]-errors[1][1],errors[1][0]+errors[1][1], facecolor='0.5', alpha=0.2)
    


    plt.xticks(x, labels)
    plt.ylabel('Offset')
    plt.title('Offsets of calibration curves ' + obs_label[variable])
    plt.axis([-1, len(value),-0.03,0.03])

    #plt.text( 0.05,0.92, 'ATLAS',fontsize=16, fontweight='bold',style='italic',transform=ax.transAxes)
    #plt.text( 0.2,0.92, 'Internal',fontsize=16,style='italic',transform=ax.transAxes,)
    plt.text( 0.05,0.86, "$\sqrt{s}$ = $8$ TeV, $20.2$ ${fb}^{-1}$",fontsize=16,transform=ax.transAxes,)
    if errors[0][0] != 9: plt.text( 0.05,0.80, r'Offset Uncertainty: $\pm %.4f$ '%(errors[1][1]),fontsize=14,style='italic',transform=ax.transAxes)


    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/offsetsWithError_'+variable+'_'+rpName+'.eps')




    #plt.show()
    plt.close()

if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  signal          = setSignal2Unfold(sys.argv)
  systematics     = ['nominal'] #['pol_up','pol_down','corr_up','corr_down'] #['nominal']#"mcatnlo herwig nominal perugia perugialocr perugiampihi radhi radlo fastpythia".split()#['nominal']
  rpName,rpValue  = setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'



  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  offsets = {}
  slopes  = {}


  for syst in systematics:

      jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      jsondirOptimization = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}
      outFolder           = jsondirOptimization

      results = [[9,9],[9,9]]
      newresults = [[9,9],[9,9]]
      for binName in binnings:

         truthf = jsondirOptimization+'/'+variable+'_partonic_btag_'+binName+'.json'
         truth  = json.load(open(truthf)) # Get truth dist
         truthObs = computeObs(variable,truth,binnings[binName])



         truth = []
         truth_e = []
         unfolded = []
         unfolded_e = []

         filenameResults = jsondirOptimization+'CalibrationResult_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'

         try:
             f = open(filenameResults)
             newresults = json.load(f)
             print filenameResults
             print newresults
         except:
             print "error:",filenameResults

             pass

         if newresults[1][1] < results[1][1]:
            results = newresults



         filename     = jsondirOptimization+'Calibration_obs_'+variable+'_'+binName+'_Tikhonov_'+rpName
         print filename
         try:

           f = open(filename)
           lines = f.readlines()

           for l in lines:
             v = l.split()
             if np.isnan(float(v[0])): continue
             if np.isnan(float(v[1])): continue
             if np.isnan(float(v[2])): continue
             if np.isnan(float(v[3])): continue

             truth.append(float(v[0]))
             truth_e.append(0)
             unfolded.append(float(v[2]))
             unfolded_e.append(float(v[3]))

         except:
             print 'error'
           #print truth, truth_e, asym, asym_e
         nAsym    = len(unfolded)
         truthAC  = array('d', truth   )
         dtruthAC = array('d', truth_e )
         unfAC    = array('d', unfolded    )
         dunfAC   = array('d', unfolded_e  )
         #print nAsym, truth, unfAC, dtruthAC, dunfAC,

         points = makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, jsondirOptimization+'Calibration_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.eps')
         print jsondirOptimization+'Calibration_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.eps'
         offsets[binName] = points[1]
         slopes[binName]  = points[0]

  print offsets,results
  plotSlopes(slopes,results)
  plotOffsets(offsets,results)
