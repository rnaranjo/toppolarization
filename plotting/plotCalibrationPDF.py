#system modules
import sys
import json
import gc
import os
from math import pow,sqrt,fabs
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
from   numpy import mean,std


def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated asymmetry")
  calib_graph.GetYaxis().SetTitle("Unfolded asymmetry")
  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()
  return par1,par0 



def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate') 
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean

def setLabels(ax0,xmin,ymax,syst,error):
  ax0.text( 0.1,0.95, 'ATLAS Internal',fontsize=14, fontweight='bold',style='italic',transform=ax0.transAxes)

  #if (syst == 'CT10nlo'):  ax0.text( xmin+10,0.90*ymax, 'Internal',fontsize=13,style='italic')
  #if (syst == 'MSTW2008nlo68cl'):    ax0.text( xmin+20,0.90*ymax, 'Internal',fontsize=13,style='italic')
  #if (syst == 'NNPDF23_nlo_as_0119'): ax0.text( xmin+10,0.90*ymax, 'Internal',fontsize=13,style='italic')

  ax0.text( 0.1,0.90, "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=13,transform=ax0.transAxes)
  ax0.text( 0.1,0.85, "Enveloped uncertainty: %0.4f"%error,fontsize=13,transform=ax0.transAxes)


def makePlot(slopes):

 import numpy as np
 import matplotlib.pyplot as plt
 ax0 = plt.subplot()
 syst = 'CT10nlo'
 ymax = slopes[syst][0]
 xmin = slopes[syst][0]
 # example data
 for syst, value in slopes.iteritems():
    xticks = [syst+'_%d'%i for i in xrange(1,len(slopes)+1)]
    y = [slopes[syst][0]-slopes[syst][s] for s in xrange(len(slopes[syst]))]
    y = slopes[syst]
    ymax = max(slopes[syst]+[ymax])*1.05 if ymax > 0 else max(slopes[syst]+[ymax])*0.90
    xmin = min(slopes[syst]+[xmin])*0.95 if xmin > 0 else min(slopes[syst]+[xmin])*1.05
    x = list(xrange(1,len(y)+1))
    print len(y),len(x)
    if syst == 'CT10nlo':
       errct=(0.5*sqrt(np.sum([pow(y[i] - y[i+1],2) for i in xrange(1,53,2)])))/1.645
       ct10 = ax0.errorbar(x[0::2], y[0::2], fmt='go')
       ax0.errorbar(x[2::2], y[1::2], fmt='go')
       print "CT10nlo Error:",errct
       ax0.axhspan(y[0]-errct,y[0]+errct, facecolor='g', alpha=0.3)
    if syst == 'MSTW2008nlo68cl':
       errorup  =sqrt(sum( [ pow(max([0,y[2*i]-y[0],y[2*i-1]-y[0]]),2) for i in xrange(1,21) ] ))
       errordown=sqrt(sum( [ pow(max([0,y[0]-y[2*i],y[0]-y[2*i-1]]),2) for i in xrange(1,21) ] ))
       mstw = ax0.errorbar(x[::2], y[::2], fmt='ro')
       ax0.errorbar(x[2::2], y[1::2], fmt='ro') 
       ax0.axhspan(y[0]-errordown, y[0]+errorup, facecolor='r', alpha=0.3)
       print "MSTW2008nlo68cl Error:",errorup, errordown

    if syst =='NNPDF23_nlo_as_0119':
       err = std(y)
       print 'NNPDF23_nlo_as_0119 Error:',err
       print y[0]-err,y[0]+err
       nnpdf = ax0.errorbar(x[::2], y[::2], fmt='bo')
       ax0.errorbar(x[2::2], y[1::2], fmt='bo')

       ax0.axhspan(y[0]-err,y[0]+err, facecolor='b', alpha=0.3)
 
 errorfinal= fabs(max(y[0]-err,y[0]+err,y[0]-errct,y[0]+errct,y[0]-errordown, y[0]+errorup) - min(y[0]-err,y[0]+err,y[0]-errct,y[0]+errct,y[0]-errordown, y[0]+errorup))/2.

 lg = ax0.legend( (ct10,mstw,nnpdf), ('CT10nlo','MSTW2008nlo68cl', 'NNPDF23_nlo_as_0119'), numpoints=1,ncol =1, fontsize=12 )
 lg.draw_frame(False)

# if fiducial:ymax = asymmetry+asymmetry*0.5
# else: ymax = asymmetry+asymmetry*0.5
 #if var == 'map_dypttt': ymax = asymmetry*0.5
 plt.ylim(xmin, ymax)


 with open(outDir+'/'+var+'/nominal/'+channel+'/PDFError_data_nominal_'+var+'_'+binName+'_rpZero.json',"w") as f:
  json.dump(errorfinal,f)
 print outDir+'/'+var+'/nominal/'+channel+'/PDFError_data_nominal_'+var+'_'+binName+'_rpZero.json' 
 
 plt.ylabel('Unfolded Value') 
 plt.xlabel('Reweighted Samples')

 plt.tight_layout()
 xmin = 10
 setLabels(ax0,xmin,ymax,syst,errorfinal)
 #plt.show()
 print outDir+'/'+var+'/nominal/'+channel+'/PDFError_data_nominal_'+var+'_'+binName+'_rpZero.png' 
 plt.savefig(outDir+'/'+var+'/nominal/'+channel+'/PDFError_data_nominal_'+var+'_'+binName+'_rpZero.png')
 plt.close() 




if __name__ == '__main__':

  gROOT.SetBatch(True)

  outDir  = Configuration.outputDirt
  workDir = Configuration.workDir

  # -- Get settings from command line
  var                = setVar(sys.argv)
  channel            = 'ALL'
  rpName             = 'rpZero' 
  systs              = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]
  pdfdict            = {"CT10nlo":53, "MSTW2008nlo68cl":41,"NNPDF23_nlo_as_0119":101}
  fiducial           = setFiducial(sys.argv)
  slopes,offsets,results     = {},{},{}
  signal = 'obs'

  from binningOptimization import binnings_dict


  btemp = binnings_dict(var)

  for bn,bc in btemp.iteritems():
     binName = bn


  if fiducial: outDir  = Configuration.outputDir

  results[var],slopes[var],offsets[var] = {},{},{}
  for syst in systs:
    results[var][syst],slopes[var][syst],offsets[var][syst] = [],[],[]
    for i in xrange(pdfdict[syst]):
      unfolded = json.load(open(outDir+'/'+var+'/nominal/'+channel+'/optimization/Unfolded_data_nominal_'+var+'_'+binName+'_rpZero.json'))
      unfolded = unfolded[0]
      asym, asym_e,truth,truth_e = [],[],[],[] 
      file     = outDir+'/'+var+'/'+syst.lower()+'/'+channel+'/optimization/Calibration_'+signal+'_'+var+'_'+binName+'_Tikhonov_'+rpName+'_'+str(i)
      print file
      try:

        f = open(file)
        lines = f.readlines()
        if len(lines) > 8 : lines = lines[7:] 
        for l in lines:
          v = l.split()
          truth.append(float(v[0]))
          truth_e.append(float(v[1]))
          asym.append(float(v[2]))
          asym_e.append(float(v[3]))

      except:
          print 'error'
        #print truth, truth_e, asym, asym_e  
      nAsym    = len(asym)
      truthAC  = array('f', truth   )
      dtruthAC = array('f', truth_e )
      unfAC    = array('f', asym    )
      dunfAC   = array('f', asym_e  )
      
       
      tmp_s,tmp_o = makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, outDir+'/'+var+'/'+syst.lower()+'/'+channel+'/optimization/CalibrationTruth_'+var+'_'+syst+'.eps')
     
      results[var][syst].append(unfolded*tmp_s+tmp_o)
      slopes[var][syst].append(tmp_s)
      offsets[var][syst].append(tmp_o)
      
  #makePlotShifted(results)      
  print results[var] 
  makePlot(results[var]) 
  #makePlotO(offsets,syst)          





