from ROOT import *
from array import array
from tqdm import tqdm
import numpy as np
import json
import matplotlib.pyplot as plt
import utils
import sys
from Config import Configuration
from utils import *


def plotMass(values):

    import matplotlib.pyplot as plt
    f = plt.figure()
    x = range(len(values))

    systName = ['167.5','170.0','172.5','175.0','177.5']


    plt.errorbar(x, values, fmt='o',label=binName)
    
    plt.xticks(x, systName)
    plt.ylabel('Unfolded Value')
    plt.title(var)
    plt.axis([-1, len(x)+1,min(values)*0.9 if min(values)>0 else min(values)*1.1,max(values)*1.1 if max(values) > 0 else max(values)*0.9])
    plt.legend()
    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/TrueMass_'+var+'.png')
    plt.close()


   


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':


    outDir    = Configuration.outputDirt

    var       = sys.argv[1]
    binName   = setOptimalBinning(sys.argv,var)
    binning   = getOptimalBinning(sys.argv,var)

    channel   = 'ALL'

    systs  = ['topmass_167.5','topmass_170','nominal','topmass_175','topmass_177.5']
    values = []


    for syst in systs:

        jsondirNom  = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'channel':channel,'syst':syst}
        truth =  json.load(open(jsondirNom+'/'+var+'_partonic_btag_'+binName+'.json'))


        values.append(computeObs(var,truth,binning))


    print values    
    plotMass(values)        





