#!/bin/bash


var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"

fiducial="full fiducial"




for v in $var; do
  for f in $fiducial;do
      cmd="python $AcDilFBU_DIR/plotting/plotCalibrationPDF.py $v $f"

       echo "running: $cmd"
       $cmd
  done
done
