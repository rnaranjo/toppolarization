from ROOT import *
import numpy as np
import json
import  utils
from Config import *
import sys



def json2resmat(name,bincontent):

    x = len(bincontent[0])
    y = len(bincontent)

    h = TH2F(name,name,x,0,x,y,0,y)

    for i in xrange(x):
        for j in xrange(y):
            h.SetBinContent(i+1,j+1,bincontent[j][i])


    return h
def NormalizeByColumn(histo):
  nbinx=histo.GetNbinsX()
  nbiny=histo.GetNbinsY()
  for x in xrange(1,nbinx+1):
    norm = histo.Integral(x,x,0,-1) # integral of the column (along de Y axis)
    for y in xrange(1,nbiny+1):
            value  = histo.GetBinContent(x,y)/norm
            histo.SetBinContent(x,y,value)

if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = utils.setChan(sys.argv)
  variable        = utils.setVar(sys.argv)
  systematics     = 'nominal'
  regFunction     = 'Tikhonov'
  folder          = 'stat'


  reweightedPoints = getObsSamples(variable)
  jsondirNom = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':systematics.lower(),'channel':channel}
  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  resmats = []

  print binnings, reweightedPoints

  for binName in binnings:

     resmat = json.load(open(jsondirNom+'/'+variable+'_resmat_btag_'+binName+'.json'))

     resmats.append(json2resmat(variable+'_'+binName,resmat))


     for rewName in reweightedPoints:

        rew = json.load(open(jsondirNom+'/'+variable+'_resmat'+rewName+'_btag_'+binName+'.json'))

        for i in xrange(len(rew)):

            for j in xrange(len(rew[i])):

                rew[i][j] = (rew[i][j] - resmat[i][j])/ resmat[i][j]



        resmats.append(json2resmat(variable+'_'+rewName+'_'+binName,rew))

  f = TFile('testmig.root','recreate')
  for i in resmats:
      #NormalizeByColumn(i)
      i.Write()
