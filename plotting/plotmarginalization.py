import matplotlib.pyplot as plt
import json
import sys
import utils

from Config import Configuration


a = ['btag6','dibosons','jeseffectivestat2','jeseffectivestat1','etaintercalibrationtotalstat','jeseffectivestat4','ctautag4','ctautag5','single_top','ctautag0','ctautag1','ctautag2','ctautag3','mu_recsf','flavor_response','pileup_pt','jeseffectivedet3','jeseffectivedet2','jeseffectivedet1','flavor_comp','mu_idsf','res_soft','jeseffectivestat3','musc','pileup_offsetnpv','el_idsf','pileup_rho','eer','punchthrough','pileup_offsetmu','etaintercalibrationmodel','bjesunc','jer','jeseffectivemodel1','jeseffectivemodel3','jeseffectivemodel2','btag8','jeseffectivemodel4','ees','btag4','btag5','mistag','btag7','btag0','sc_soft','btag2','btag3','ctautag6','jeseffectivemix1','jeseffectivemix2','jeseffectivemix3','jeseffectivemix4','el_trigsf','mu_trigsf','jeff','zjets','zjets_low']
a = ['musc','btag6','dibosons','jeseffectivestat2','jeseffectivestat1','etaintercalibrationtotalstat','jeseffectivestat4','ctautag4','ctautag5','single_top','ctautag0','ctautag1','ctautag2','ctautag3','mu_recsf','flavor_response','pileup_pt','jeseffectivedet3','jeseffectivedet2','jeseffectivedet1','flavor_comp','mu_idsf','res_soft','jeseffectivestat3','pileup_offsetnpv','el_idsf','pileup_rho','eer','punchthrough','pileup_offsetmu','etaintercalibrationmodel','bjesunc','jer','jeseffectivemodel1','jeseffectivemodel3','jeseffectivemodel2','btag8','jeseffectivemodel4','ees','btag4','btag5','mistag','btag7','btag0','sc_soft','btag2','btag3','ctautag6','jeseffectivemix1','jeseffectivemix2','jeseffectivemix3','jeseffectivemix4','el_trigsf','jeff','zjets']


a = a[::-1]


if __name__ == '__main__':

  debug = 10

  outDir  = Configuration.outputDirt
  from binningOptimization import binnings_dict
  fiducial           = utils.setFiducial(sys.argv)

  region = 'fiducial' if fiducial else 'full'
  region_h = 'Fiducial Region' if fiducial else 'Full Phase Space'
   
  if fiducial:
     outDir  = Configuration.outputDir
     workDir = Configuration.workDir



  variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                   'helcorr',\
                   'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                   'transheldiff']

  for var in variables:

     print "Processing:",var
  
     l = []

     jsondir = outDir+'/%(var)s/nominal/ALL/'%{'var':var}  

     data  = json.load(open(jsondir+'/'+var+'_data_marginalization.json'))
     pseudo  = json.load(open(jsondir+'/'+var+'_pseudo_marginalization.json'))
     
     x =[]
     
     xerr = []
     
     xPseudo    = []
     xerrPseudo = []
     
     for key in a:
     
        l.append(key)
        x.append(data[key][0])
        xerr.append(data[key][1])
     
        xPseudo.append(pseudo[key][0])
        xerrPseudo.append(pseudo[key][1])
     
     
        y = range(0,len(l))
      
     
     plt.figure(figsize=(8, 12))
     point = plt.errorbar(x, y, xerr=xerr, fmt='o',color='r')
     point2 = plt.errorbar(xPseudo, y, xerr=xerrPseudo, fmt='o',color='k')
     
     
     plt.axis([-5, 5, -1, len(y)])
     plt.yticks(y,l)
     p1 = plt.axvspan(-2., 2., facecolor='#F7FE2E', alpha=1)
     p2 = plt.axvspan(-1., 1.0, facecolor='#00FF00', alpha=1)
     plt.legend((point,point2,p2,p1),(r'Data',r'Pseudodata','$\pm1\sigma$ ','$\pm2\sigma$'),numpoints = 1)
     plt.xlabel(r'Nuisance parameter ($\theta$)')
     plt.tight_layout()
        # Tweak spacing to prevent clipping of tick-labels
     plt.savefig('systematics/plots/nuisance_%s_%s.eps' % (var,region), format='eps')
     plt.clf()
  
        
  
  
