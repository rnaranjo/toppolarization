#!/bin/bash


var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"


for v in $var; do
    cmd="python $AcDilFBU_DIR/plotting/plotMass.py ALL $v reco"
    echo "running: $cmd"
    $cmd
done
