from ROOT import *
from utils import *
import Rebin
import sys
import array
gStyle.SetOptStat(0)
gROOT.SetBatch(True)

obs_label = {}

obs_label["helpol_plus"]="cos#theta_{#plus}^{k}"
obs_label["helpol_minus"]="cos#theta_{#minus}^{k}"
obs_label["transpol_plus"]="cos#theta_{#plus}^{n}"
obs_label["transpol_minus"]="cos#theta_{#minus}^{n}"
obs_label["rpol_plus"]="cos#theta_{#plus}^{r}"
obs_label["rpol_minus"]="cos#theta_{#minus}^{r}"
obs_label["helcorr"]="cos#theta_{#plus}^{k}cos#theta_{#minus}^{k}"
obs_label["transcorr"]="cos#theta_{#plus}^{n}cos#theta_{#minus}^{n}"
obs_label["rcorr"]="cos#theta_{#plus}^{r}cos#theta_{#minus}^{r}"
obs_label["transhelsum"]="cos#theta_{#plus}^{n}cos#theta_{#minus}^{k} + cos#theta_{#plus}^{k}cos#theta_{#minus}^{n}"
obs_label["transheldiff"]="cos#theta_{#plus}^{n}cos#theta_{#minus}^{k} - cos#theta_{#plus}^{k}cos#theta_{#minus}^{n}"
obs_label["transrsum"]="cos#theta_{#plus}^{n}cos#theta_{#minus}^{r} + cos#theta_{#plus}^{r}cos#theta_{#minus}^{n}"
obs_label["transrdiff"]="cos#theta_{#plus}^{n}cos#theta_{#minus}^{r} - cos#theta_{#plus}^{r}cos#theta_{#minus}^{n}"
obs_label["rhelsum"]="cos#theta_{#plus}^{r}cos#theta_{#minus}^{k} + cos#theta_{#plus}^{k}cos#theta_{#minus}^{r}"
obs_label["rheldiff"]="cos#theta_{#plus}^{r}cos#theta_{#minus}^{k} - cos#theta_{#plus}^{k}cos#theta_{#minus}^{r}"




def getAcceptance(resmat,truth):
    nbinsx = resmat.GetNbinsX() # reconstructed distribution
    nbinsy = resmat.GetNbinsY() # truth distribution
    assert(nbinsx==nbinsy),'getMigration::error not matching dimensions'

    migration    = []
    migrationErr = []
    acceff       = []
    for y in xrange(1,nbinsy+1):
       acceff.append(getEff(resmat,truth,y))

    return acceff


def getEff(resmat,htruth,y):
    #get coordinates in 2D truth hist
    #note: resp matrix is 2D: (nXtruth*nYtruth) x (nXtruth*nYtruth) --> y = (yy-1)*Nbins_dy + xx
    nxx = htruth.GetNbinsX()
    xx = y % nxx
    if xx == 0: xx = nxx
    yy = int((y-1)/nxx) + 1

    return resmat.Integral(0,-1,y,y) / htruth.GetBinContent(xx,yy)

def getNonFiducialFraction(fiducial,reco):

   total =  fiducial.Clone()
   total.Add(reco)
   print total
   nonFiducial = fiducial.Clone()
   nonFiducial.Divide(total)

   return nonFiducial

if __name__ == '__main__':


  outDir          = Configuration.outputDir
  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  syst            = 'nominal'
  rpName          = 'rpZero' #setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  fiducial        = setFiducial(sys.argv)

  if fiducial: 
    outDir          = Configuration.outputDir
    region          = 'fiducial'
  else:
    outDir          = Configuration.outputDirt
    region          = 'full'

  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)
  print binnings

  for bn,bc in binnings.iteritems():
     binName = bn
     binning = bc




  jsondir             = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':variable,'syst':syst.lower(),'channel':channel}
  rootFile            = TFile(jsondir + 'map_incl_btag_200Bin.root')

  resmat              = Rebin.rebin2D(rootFile.Get("resmat")  , binning ) 
  fiducial            = Rebin.rebin( rootFile.Get("fiducial") , binning )
  reco                = Rebin.rebin( rootFile.Get("reco")     , binning )
  truth               = Rebin.rebin( rootFile.Get("partonic") , binning )


  

  acceptanceEff = getAcceptance(resmat,truth) #List

  acceptanceHist = TH1F("accept","",len(acceptanceEff), array.array('f',binning))

  for i,content in enumerate(acceptanceEff):
    acceptanceHist.SetBinContent(i+1,content)

  if fiducial: acceptanceHist.SetMaximum(0.40)
  else: acceptanceHist.SetMaximum(0.25)
  acceptanceHist.SetMinimum(-0.01)
  acceptanceHist.SetTitle("acceptance efficiency for "+obs_label[variable])
  acceptanceHist.GetXaxis().SetTitle(obs_label[variable])
  acceptanceHist.GetYaxis().SetTitle("Efficiency")
  acceptanceHist.GetYaxis().SetTitleOffset(1.20)
  acceptanceHist.SetMarkerStyle(22)
  acceptanceHist.SetLineColorAlpha(kBlue, 0.35)


  c2 = TCanvas()

  acceptanceHist.Draw()
  g = TLatex()
  g.SetNDC()
  g.SetTextSize(0.055)
  g.SetTextColor(1)
  g.DrawLatex(0.15,0.80,"#font[72]{ATLAS}")
  g.DrawLatex(0.28,0.80, "Internal")
  g.SetTextSize(0.045)
  tmpplumi = "#sqrt{s} = 8 TeV, 20.3 fb^{-1}"
  g.DrawLatex(0.15,0.70,tmpplumi)

  c2.Update()

  c2.Print("plots/acceptance_efficiency_"+variable+'_'+region+'.pdf')
 

  nonFiducialFraction = getNonFiducialFraction(fiducial,reco)
  
  c1 = TCanvas()
 
  nonFiducialFraction.SetMaximum(0.15)
  nonFiducialFraction.SetMinimum(-0.01)
  nonFiducialFraction.SetTitle("non-Fiducial Fraction for "+obs_label[variable])
  nonFiducialFraction.GetXaxis().SetTitle(obs_label[variable])
  nonFiducialFraction.GetYaxis().SetTitle("Non Fiducial Fraction")
  nonFiducialFraction.GetYaxis().SetTitleOffset(1.20) 
  nonFiducialFraction.SetMarkerStyle(22)
  nonFiducialFraction.SetLineColorAlpha(kBlue, 0.35)


  print acceptanceEff
  nonFiducialFraction.Draw()
  g = TLatex()
  g.SetNDC()
  g.SetTextSize(0.055)
  g.SetTextColor(1)
  g.DrawLatex(0.15,0.80,"#font[72]{ATLAS}")
  g.DrawLatex(0.28,0.80, "Internal")
  g.SetTextSize(0.045)
  tmpplumi = "#sqrt{s} = 8 TeV, 20.3 fb^{-1}"
  g.DrawLatex(0.15,0.70,tmpplumi)

  c1.Update()
  c1.Print("plots/non_fiducial_"+variable+'_'+region+'.pdf')

  #raw_input("wait") 


  
 
