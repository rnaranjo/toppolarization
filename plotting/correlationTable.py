from Config import Configuration
import utils
import json
import sys
from utils import *
import numpy as np
obs_label = {}

obs_label["helpol_plus"]=r"$cos\theta_{+}^{hel}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{hel}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"



def createLatex(massSyst,name):

  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{|lcc|}
\hline
Variable & Fiducial  & Full \\\\ \hline \hline
"""
  footer = """
\hline
\end{tabular}
\end{table}
\end{center}
  """

  table = ''
  for var in variables:
    region = massSyst[var]
    
    table = table + '\%s    & $ \pm %.4f $ & $ \pm %.4f $ \\\\ \hline  \n ' % (var.replace("_",""), region['fiducial'], region['full'])



  
  latex = header + table + footer

  f1 = open(name,'w')
  print name
  f1.write(latex)
  f1.close()


if __name__ == '__main__':


  outDir          = Configuration.outputDir

  variables   = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                 'helcorr',\
                 'transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 'transheldiff']


  rpName          = 'rpZero' #setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'

  results = {}
  mcstat  = {}

  unfolded  = {}
  expunc    = {}


  for var in variables:
    results[var] = {}
    import binningOptimization
 
    btemp = binningOptimization.binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc


    #if 'corr' in var or 'diff' in var or 'sum' in var:
    #  factor = -9
    #else:
    factor = 1



    for region in ['full','fiducial']:

       if region == 'full':
         outDir          = Configuration.outputDirt
       else:
         outDir          = Configuration.outputDir


       unfolded_data = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/Unfolded_data_nominal_%(var)s_%(binName)s_rpZero.json' %{'var':var,'binName':binName}))
 
       truth = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/%(var)s_partonic_btag_%(binName)s.json' %{'var':var,'binName':binName}))

       fbuFile = json.load(open(outDir+'/%(var)s/nominal/ALL/optimization/CalibrationResult_%(var)s_%(binName)s_Tikhonov_rpZero.json' % {'var':var,'binName':binName}))
 
       truthObs = computeObs(var,truth,binning)


       #print outDir+'/%(var)s/nominal/ALL/optimization/CalibrationResult_%(var)s_%(binName)s_Tikhonov_rpZero.json' % {'var':var,'binName':binName}
    
       #print truthObs , unfolded_data[0], fbuFile[1][0]

       fbuOutFile   = outDir+'/%(var)s/nominal/ALL/fbu.json' %{'var':var}
       mcOutFile    = outDir+'/%(var)s/nominal/ALL/mcStat.json' %{'var':var}
       totalFile    = outDir+'/%(var)s/nominal/ALL/others.json' %{'var':var}


       results[var][region] =  abs((abs(1-fbuFile[0][0])*0 +  fbuFile[1][0]-truthObs)*factor)

       fbuTotal = results[var][region]
       mcStat   = fbuFile[1][1]


       print var,region,'- Fbu:',fbuTotal,'- Stat:',mcStat 

       with open(fbuOutFile,'w') as f:
         json.dump(fbuTotal,f) 

       with open(mcOutFile,'w') as f:
         json.dump(mcStat,f)


       with open(totalFile,'w') as f:
         json.dump(np.sqrt(pow(mcStat,2)+pow(fbuTotal,2)),f)

      









  createLatex(results,'fbu.tex')


