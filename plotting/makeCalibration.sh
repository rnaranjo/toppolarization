#!/bin/bash


     channels="ALL"
       signal="obs"
       regpar="rpZero"
       PCfarm="Desy" #other options: Prague
         syst="nominal"
          #helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
          var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"
          var="helcorr"
     fiducial="full"




cd $AcDilFBU_DIR/batch

for ch in $channels; do
  for sig in $signal; do
     for sys in $syst; do
         for v in $var; do
         for reg in $regpar; do
          for f in $fiducial;do
             cmd="python $AcDilFBU_DIR/plotting/plotCalibrationBootstrap.py $v $ch $sys $sig $fiducial $reg"

             echo "running: $cmd"
             $cmd
          done
      done
        done
    done
  done
done
