##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
import numpy as np
from ROOT import TH1F, TH2F,TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gStyle
from collections import OrderedDict
gStyle.SetPaintTextFormat(".2g")
gStyle.SetOptStat(0)
#from math import round

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)


obs_label = {}
obs_label["helpol_plus"]=r"$cos\theta_{+}^{hel}$"
obs_label["helpol_minus"]=r"$cos\theta_{-}^{hel}$"
obs_label["transpol_plus"]=r"$cos\theta_{+}^{trans}$"
obs_label["transpol_minus"]=r"$cos\theta_{-}^{trans}$"
obs_label["rpol_plus"]=r"$cos\theta_{+}^{r}$"
obs_label["rpol_minus"]=r"$cos\theta_{-}^{r}$"
obs_label["helcorr"]=r"$cos\theta_{+}^{hel}cos\theta_{-}^{hel}$"
obs_label["transcorr"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{trans}$"
obs_label["rcorr"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{r}$"
obs_label["transhelsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transheldiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{trans}$"
obs_label["transrsum"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["transrdiff"]=r"$cos\theta_{+}^{trans}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{trans}$"
obs_label["rhelsum"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} + cos\theta_{+}^{hel}cos\theta_{-}^{r}$"
obs_label["rheldiff"]=r"$cos\theta_{+}^{r}cos\theta_{-}^{hel} - cos\theta_{+}^{hel}cos\theta_{-}^{r}$"

def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);




def json2Histo(bincontent,bincontentErr,binning,name):
   h = TH1F(name,name,len(binning)-1,array('d',binning))
   print bincontent
   for i in xrange(1,len(bincontent)+1):

     h.SetBinContent(i,bincontent[i-1])
     h.SetBinError(i,bincontentErr[i-1])
   return h

def jsontoHisto(bincontent,bincontentErr,binning,name):
   h = TH1F(name,name,len(binning)-3,array('d',xrange(len(binning)-2)))
   print bincontent
   for i in xrange(1,len(bincontent)+1):
     
     h.SetBinContent(i,bincontent[i-1])
     h.SetBinError(i,bincontentErr[i-1])
   return h


def jsontoHisto2D(bincontent,bincontentErr,binningX,binningY,name):
   gStyle.SetPaintTextFormat("4.2g")
   h = TH2F(name,name,len(binningX)-3,array('d',xrange(len(binningX)-2)),len(binningY)-1,array('d',binningY))
   for i in xrange(1,len(bincontent)+1):
     print i
     for j in xrange(1,len(bincontent[i-1])+1):
       print bincontent[i-1][j-1]
       h.SetBinContent(i,j,round(bincontent[i-1][j-1],2))
       h.SetMarkerSize(1.4)
       
       h.SetTitle('Rectangular Response Matrix')
       #h.SetBinError(j,i,bincontentErr[i-1][j-1])
       #h.SetBinError(j,i,0.)
       
   return h


def NormalizeByColumn(histo):
  nbinx=histo.GetNbinsX()
  nbiny=histo.GetNbinsY()
  for x in xrange(1,nbinx+1):
    norm = histo.Integral(x,x,0,-1) # integral of the column (along de Y axis)
    for y in xrange(1,nbiny+1):
            value  = histo.GetBinContent(x,y)/norm
            histo.SetBinContent(x,y,value)


def plotResmat(resmatObj,var):
   import matplotlib.pyplot as plt
   fig = plt.figure(figsize=(15,8))
   #fig.set_figheight(20)
   #fig.set_figwidth(30)
   ax = fig.add_subplot(111)
   ax.set_aspect(1)
   
   #normalize ee

   resmatObj = np.array(resmatObj)
   for i in xrange(len(resmatObj)):
     print i
     norm_factor = sum(resmatObj[i][0:len(resmatObj[i])/3])  
     for j in xrange(0,len(resmatObj[i][0:len(resmatObj[i])/3])):
        resmatObj[i][j] = resmatObj[i][j] / norm_factor

     norm_factor = sum(resmatObj[i][len(resmatObj[i])/3:2*len(resmatObj[i])/3])
     for j in xrange(len(resmatObj[i])/3, 2*len(resmatObj[i])/3):
        resmatObj[i][j] = resmatObj[i][j] / norm_factor

     norm_factor = sum(resmatObj[i][2*len(resmatObj[i])/3:3*len(resmatObj[i])/3])
     for j in xrange(2*len(resmatObj[i])/3, len(resmatObj[i])):
        resmatObj[i][j] = resmatObj[i][j] / norm_factor




   im = ax.matshow(resmatObj, interpolation='nearest',cmap='Purples',vmin=0, vmax=1)
   #im.clim(0,1)
   ax.xaxis.tick_bottom()

   for (i, j), z in np.ndenumerate(resmatObj):
    ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center',fontsize=20)

   plt.text( 0.01,1.03, 'ATLAS Internal',fontsize=20, fontweight='bold',style='italic',transform=ax.transAxes)
   plt.text( 0.15,1.03, 'Simulation',fontsize=20, style='italic',transform=ax.transAxes)
   plt.text( 0.25,1.03, "$\sqrt{s}$ = $8$ TeV",fontsize=20,transform=ax.transAxes)
   plt.text( 0.37,1.03, obs_label[var],fontsize=20,transform=ax.transAxes)
   plt.vlines(len(resmatObj)-0.5,-0.5,len(resmatObj)-0.5)
   plt.vlines(2*len(resmatObj)-0.5,-0.5,len(resmatObj)-0.5)
   plt.gca().invert_yaxis()

   width = len(resmatObj[0])
   height = len(resmatObj)
     
   t  = range(width) 
   ty = range(height)

   plt.xticks(range(width),t,fontsize=15,rotation='horizontal' )
   plt.yticks(range(height), ty,fontsize=15)

   from mpl_toolkits.axes_grid1 import make_axes_locatable
   divider = make_axes_locatable(plt.gca())
   #cax = divider.append_axes("right", "5%", pad="3%")
   #fig.colorbar(im, cax=cax)
   plt.text( 0.155,-0.1, '$ee$',fontsize=20, transform=ax.transAxes)
   plt.text( 0.49,-0.1, '$\mu\mu$',fontsize=20, transform=ax.transAxes)
   plt.text( 0.82,-0.1, '$e\mu$',fontsize=20, transform=ax.transAxes)
   ax.set_xlabel('Reconstructed Bins',fontsize=20,y=1)
   #ax.get_xaxis().get_offset_text().set_y(2)

   ax.xaxis.set_label_coords(0.5, -.15)


   ax.set_ylabel('True Bins',fontsize=20)
   plt.tight_layout()
   #plt.show()
   plt.savefig('plots/resmat_'+var+'_'+region+'.png', format='png')



if __name__ == '__main__':


  outDir  = Configuration.outputDirt
  workDir = Configuration.workDir

  #################################  
  var               = setVar(sys.argv)
  fiducial = setFiducial(sys.argv)
  from binningOptimization import binnings_dict
  btemp = binnings_dict(var)
  for bn,bc in btemp.iteritems():
     binName = bn
     binning = bc 

  #################################

  # Get settings from command line
  syst              = 'nominal'
  region            = 'full'

  if fiducial: 
      outDir  = Configuration.outputDir
      region  = 'fiducial'

  resmatJson = outDir+'/'+var+'/'+syst+'/ALL/optimization/'+var+'_mig_btag_'+binName+'.json'
  resmatObj  = json.load(open(resmatJson))
  bin        = binning + binning + binning

       
  plotResmat(resmatObj,var)

