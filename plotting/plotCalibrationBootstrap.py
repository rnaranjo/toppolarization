##system modules
import sys
import json
import gc
import os
from   numpy import mean,std
from array import array
#our modules
from Config import Configuration
from utils import *
from ROOT import TH1F, TFile,TF1, TGraph, TGraphErrors, TLatex, TCanvas, TLegend, gROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt

gROOT.SetBatch(True)

def DrawATLASLabel(_c, _g, _xleft, _ytop, _desc):

  _level="Internal Simulation";
  _ecm_desc="#sqrt{s} = 8 TeV";
  _ATLAS_desc="#font[72]{ATLAS}";

  _c.cd();
  _sh="";
  _sh+=_ATLAS_desc+" "+_level;
  _g.DrawLatex(_xleft,_ytop, _sh);
  _sh = " " + _ecm_desc;
  if (_desc!=""): _sh += " " + _desc;
  _g.DrawLatex(_xleft,_ytop-_g.GetTextSize()*1.15, _sh);

def plotHisto(values,title,filename):

    import matplotlib.pyplot as plt

    f = plt.figure(figsize=(14, 6))
    ax = plt.subplot(111)

    plt.rc('text', usetex=True)

    bins = xrange(0,20,1)
    for binName,v in values.iteritems():

        n, bins, patches = ax.hist(v,  bins=20,label="Bin: %s \n $\mu = %0.4f$, $\sigma=%0.4f$"%(binName,np.mean(v),np.std(v)))




    plt.xlabel(title)
    plt.ylabel("Events")

    plt.legend()

    #plt.show()
    plt.savefig(filename)
    plt.close()
def makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, name):
  tf1 = TF1("f1", "x", -1., 1.)
  tf1.SetLineStyle(2)
  calib_graph = TGraph(nAsym, truthAC, unfAC)
  calib_graph_errors = TGraphErrors(nAsym, truthAC, unfAC, dtruthAC, dunfAC)
  calib_graph.Draw("ap")

  calib_graph.SetLineWidth(2)
  calib_graph.SetMarkerStyle(21)
  calib_graph.SetMarkerColor(62)
  calib_graph.SetLineColor(62)
  calib_graph.Fit("pol1")
  fitfn1 = calib_graph.GetFunction("pol1")
  fitfn1.SetLineColor(62)
  par0 = fitfn1.GetParameter(0)
  err0 = fitfn1.GetParError(0)
  par1 = fitfn1.GetParameter(1)
  err1 = fitfn1.GetParError(1)

  _xl=.12
  _yt=.82
  _xr=.55
  _yb=.60

  legstr1= "y = %0.4fx+%0.4f" % (par1,par0)
  leg1 = TLegend(_xl, _yb, _xr, _yt)
  leg1.SetBorderSize(0)
  leg1.SetFillStyle(0)
  leg1.AddEntry(calib_graph,legstr1, "l")

  c = TCanvas("calib", "calib", 50, 50, 1000, 700)
  calib_graph.SetTitle(" ")
  #calib_graph.GetYaxis().SetRangeUser(-0.08, 0.08)
  #calib_graph.GetXaxis().SetLimits(-0.08, 0.08);
  #calib_graph.GetYaxis().SetLimits(-0.08, 0.08);
  calib_graph.GetXaxis().SetTitle("Generated value")
  calib_graph.GetYaxis().SetTitle("Unfolded value")
  #calib_graph.GetYaxis().SetTitle("Reconstructed asymmetry")

  calib_graph.Draw("ap")
  calib_graph_errors.SetFillColor(62)
  calib_graph_errors.SetMarkerColor(62)
  calib_graph_errors.SetLineColor(62)
  calib_graph_errors.Draw("samep")
  tf1.Draw("same")
  leg1.Draw("same")
  c.Update();

  g= TLatex()
  g.SetNDC()
  g.SetTextSize(0.05)
  g.SetTextColor(1)

  #DrawATLASLabel(c, g, _xl, _yt+0.02, 'e#mu')
  jsonname = name.split('.')
  jsonname =  name[:-4]+'.json'
  with open(jsonname,'w') as f:
    json.dump([par1,par0],f)

  print name+' have been created'
  c.Update()
  c.SaveAs(name)
  c.Close()

  return [par1,par0]




def writeValues2Histogram(outdir,name,values):
  from ROOT import TFile, TH1F, TH1
  f = TFile(outdir+name+'.root','recreate')
  h = TH1F(name,name,100,-0.1, 0.1)
  map(h.Fill,values)
  mean= h.GetMean()
  h.Write()
  f.Close()
  return mean


def fitCalibration(truth, unf, dtruth, dunf):


    f = plt.figure()



    fit = np.polyfit(x,y,1)
    fit_fn = np.poly1d(fit)

    plt.errorbar(x, y, xerr=0.2, yerr=0.4)

    plt.plot(x,y, 'yo', x, fit_fn(x), '--k')

    plt.show();

def plotSlopes(slopes):



    f = plt.figure()


    labels = []
    x     = range(len(slopes))
    value = []


    for binName,slope in slopes.iteritems():
        labels.append(binName)
        value.append(slope)


    plt.plot(x, value, 'ro')

    plt.xticks(x, labels)
    plt.ylabel('Slopes')
    plt.title('Slopes of calibration curves '  + variable)

    plt.axis([-1, len(value)+1,0.9*min(value),max(value)*1.1])



    #plt.show()
    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/slopes_'+variable+'_'+rpName+'.eps')
    plt.close()

def plotOffsets(offsets):

    import matplotlib.pyplot as plt


    labels = []
    x     = xrange(len(offsets))
    value = []

    for binName,slope in offsets.iteritems():
        labels.append(binName)
        value.append(slope)



    plt.plot(x, value, 'ro')
    plt.xticks(x, labels)
    plt.ylabel('Offset')
    plt.title('Offsets of calibration curves ' + variable)
    plt.axis([-1, len(value),-0.01,0.01])

    plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/plots/offsets_'+variable+'_'+rpName+'.eps')




    #plt.show()
    plt.close()

if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  outDirB          = Configuration.outputDirBt

  channel         = setChan(sys.argv)
  variable        = setVar(sys.argv)
  signal          = setSignal2Unfold(sys.argv)
  systematics     = ['nominal']#'mcatnlo','herwig','fastpythia','radhi','radlo','perugia','perugialocr','perugiampihi']
  rpName,rpValue  = setRegPar(sys.argv) #'rpZero', 0.0
  regFunction     = 'Tikhonov'
  folder          = 'stat'

  fiducial        = setFiducial(sys.argv) 

  if fiducial: 
    outDir          = Configuration.outputDir
    outDirB          = Configuration.outputDirB

  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  offsets = {}
  slopes  = {}


  for syst in systematics:
    for binName in binnings:
            offsets[binName]  = []
            slopes[binName]   = []

            jsondir = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':syst.lower(),'channel':channel}

            for i in xrange(100):

               jsondirOptimization = outDirB+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)d/'%{'var':variable,'syst':syst.lower(),'channel':channel,'pos':i}
               outFolder           = jsondirOptimization

               truthf = jsondir+'/'+variable+'_partonic_btag_'+binName+'.json'

               truth  = json.load(open(truthf)) # Get truth dist

               truthObs = computeObs(variable,truth,binnings[binName])

               #print truthObs

               #raw_input("WAIT")

               truth_t = []
               truth_e = []
               unfolded_t = []
               unfolded_e = []

               filename     = jsondirOptimization+'Calibration_obs_'+variable+'_'+binName+'_Tikhonov_'+rpName
               print filename
               try:

                 f = open(filename)
                 lines = f.readlines()

                 

                 for l in lines:

                   v = l.split()
                  
                   if np.isnan(float(v[0])) or np.isnan(float(v[1])) or np.isnan(float(v[2])) or np.isnan(float(v[3])): continue

                   truth_t.append(float(v[0])-truthObs)
                   truth_e.append(0)
                   unfolded_t.append(float(v[2]))
                   unfolded_e.append(float(v[3]))

               except Exception, err:
                   print 'error',err
                   #print truth, truth_e, unfolded_t, unfolded_e
               nAsym    = len(unfolded_t)

               if nAsym == 0 or nAsym == 1 or nAsym == 2: continue

               truthAC  = array('d', truth_t   )
               dtruthAC = array('d', truth_e )
               unfAC    = array('d', unfolded_t    )
               dunfAC   = array('d', unfolded_e  )
               print nAsym, truthAC, unfAC, dtruthAC, dunfAC
                
               points = makeCalibration(nAsym, truthAC, unfAC, dtruthAC, dunfAC, jsondirOptimization+'Calibration_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.eps')
               if np.isnan(points[1]) or np.isnan(points[0]): continue
               offsets[binName].append(points[1])
               slopes[binName].append(points[0])

            print offsets[binName]
            tmp = [[np.mean(slopes[binName]),np.std(slopes[binName])],[np.mean(offsets[binName]),np.std(offsets[binName])]]

            print tmp

            with open(jsondir+'CalibrationResult_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json','w') as f:
              json.dump(tmp,f)
            print  jsondir+'CalibrationResult_'+variable+'_'+binName+'_Tikhonov_'+rpName+'.json'


    #plotHisto(offsets,'Offsets', jsondir+'CalibrationOffsets_'+variable+'_'+'Tikhonov_'+rpName+'.eps')
    #plotHisto(slopes,'Slopes',jsondir+'CalibrationSlopes_'+variable+'_'+'Tikhonov_'+rpName+'.eps')
    #print jsondir+'CalibrationOffsets_'+variable+'_'+'Tikhonov_'+rpName+'.eps'
