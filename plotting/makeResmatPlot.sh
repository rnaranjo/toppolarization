#!/bin/bash


var="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"
region="full fiducial"
for r in $region; do
for v in $var; do
    cmd="python $AcDilFBU_DIR/plotting/plotResmat.py $v $r"
    echo "running: $cmd"
    $cmd
done
done
