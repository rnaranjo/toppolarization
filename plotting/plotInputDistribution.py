from ROOT import *
import numpy as np
import json
import  utils
from Config import *
import sys
from   matplotlib import pyplot as plt



if __name__ == '__main__':


  outDir          = Configuration.outputDirt
  channel         = utils.setChan(sys.argv)
  variable        = utils.setVar(sys.argv)
  systematics     = 'nominal'
  regFunction     = 'Tikhonov'
  folder          = 'stat'


  reweightedPoints = ['obspos0','obspos10','obsneg10','obspos4','obsneg4']#getObsSamples(variable)
  jsondirNom = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':variable,'syst':systematics.lower(),'channel':channel}
  import binningOptimization
  binnings = binningOptimization.binnings_dict(variable)

  resmats = []

  print binnings, reweightedPoints

  for binName in ['4Bin1']:

     bins = binnings[binName]
     binX    = []
     binXErr = []
     binWidth = []

     for i in xrange(len(bins)-1):
        binX.append( (bins[i+1] + bins[i])/2. )
        binXErr.append( (bins[i+1] - bins[i])/2. )
        binWidth.append(abs((bins[i+1] - bins[i])))

     partonic = json.load(open(jsondirNom+'/'+variable+'_partonic_btag_'+binName+'.json'))
     binErr   = np.sqrt(np.array(partonic))





     for i in xrange(len(partonic)):
        partonic[i] = partonic[i]/binWidth[i]

     plt.errorbar(binX, partonic, yerr=binErr, xerr=binXErr,fmt='o',label='Nominal')



     for rewName in reweightedPoints:

        partonicReweighted = json.load(open(jsondirNom+'/'+variable+'_partonic'+rewName+'_btag_'+binName+'.json'))
        for i in xrange(len(partonicReweighted)):
            partonicReweighted[i] = partonicReweighted[i]/binWidth[i]
        plt.errorbar(binX, partonicReweighted, xerr=binXErr,fmt='o',label=rewName)



     plt.ylabel(r'Events')
     plt.xlabel('Bin')

     plt.legend()
     plt.show()
     #plt.savefig(jsondir+channel+"_"+signal+"_"+binName+"_"+rpName+'_distribution.eps')
     plt.close()
