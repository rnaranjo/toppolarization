# TOP GROUP STANDARD COLOUR SCHEME
DYCOLOUR      = 95
ZECOLOUR      = 95
ZMUCOLOUR     = 95
ZTAUCOLOUR    = 97
DIBOSONCOLOUR = 5
STWTCOLOUR    = 62
STSCHANCOLOUR = 61
STTCHANCOLOUR = 60
TTBARCOLOUR   = 0
TTHCOLOUR     = 2

LINEWIDTH     = 2
LINESTYLE     = 1

# TCANVAS SIZES
CANVASWIDTH                = 840
CANVASHEIGHT_NORATIO       = 660
CANVASHEIGHT_WITHRATIO     = 825
MAINPADMARGINLEFT          = 0.16
MAINPADMARGINRIGHT         = 0.07
MAINPADMARGINTOP           = 0.05
MAINPADMARGINBOTOMNORATIO  = 0.16
MAINPADMARGINBOTOMRATIO    = 0.0
RATIOPADMARGINLEFT         = 0.16
RATIOPADMARGINRIGHT        = 0.05
RATIOPADMARGINTOP          = 0.0
RATIOPADMARGINBOTTOM       = 0.34

MAINPADXLOW                = 0.0 
MAINPADXHIGH               = 1.0
MAINPADYLOW                = 0.26
MAINPADYHIGH               = 1.0 
RATIOPADXLOW               = 0.0 
RATIOPADXHIGH              = 1.0
RATIOPADYLOW               = 0.0
RATIOPADYHIGH              = 0.24 

# LABELS
ATLASLABEL_TOPLEFT_X       = 0
ATLASLABEL_TOPLEFT_Y       = 0
ATLASLABEL_TOPLEFT_WIDTH   = 0
ATLASLABEL_TOPLEFT_HEGITH  = 0

ATLASLABEL_TOPRIGHT_X      = 0
ATLASLABEL_TOPRIGHT_Y      = 0
ATLASLABEL_TOPRIGHT_WIDTH  = 0
ATLASLABEL_TOPRIGHT_HEGITH = 0

# LEGENDS
LEGEND_TOPLEFT_X           = 0.2
LEGEND_TOPLEFT_Y           = 0.92
LEGEND_TOPRIGHT_X          = 0.7
LEGEND_TOPRIGHT_Y          = 0.92
LEGEND_WIDTH               = 0.25
LEGEND_HEIGHT              = 0.35

# TEXT SIZES
TEXTSIZE                   = 0.010 #36
XLABELSIZE                 = 0.030
YLABELSIZE                 = 0.030
ZLABELSIZE                 = 0.030

YTITLESIZE                 = 0.05
YTITLEOFFSET               = 1.50
XTITLESIZE                 = 0.15
XTITLEOFFSET               = 0.90

BLANK = 0
FONT = 42

def atlasStyle():

    """ Function to set ATLAS style for ROOT plots """

    from ROOT import TStyle

    atlasStyle = TStyle("ATLAS","Atlas style")

    atlasStyle.SetFrameBorderMode(  BLANK )
    atlasStyle.SetFrameFillColor(   BLANK )
    atlasStyle.SetCanvasBorderMode( BLANK )
    atlasStyle.SetCanvasColor(      BLANK )

    atlasStyle.SetCanvasDefW( CANVASWIDTH )
    atlasStyle.SetCanvasDefH( CANVASHEIGHT_WITHRATIO )
    atlasStyle.SetPadBorderMode( BLANK )
    atlasStyle.SetPadColor(      BLANK )
    atlasStyle.SetStatColor(     BLANK )
    atlasStyle.SetPaperSize( 20, 26 )

    #FIT IN Z AXIS LABEL
    atlasStyle.SetPadTopMargin(    MAINPADMARGINTOP     )
    atlasStyle.SetPadRightMargin(  MAINPADMARGINRIGHT   )
    atlasStyle.SetPadBottomMargin( RATIOPADMARGINBOTTOM )
    atlasStyle.SetPadLeftMargin(   MAINPADMARGINLEFT    )
    atlasStyle.SetTitleXOffset(    YTITLEOFFSET )
    atlasStyle.SetTitleYOffset(    YTITLEOFFSET )
    atlasStyle.SetTextFont( FONT )
    atlasStyle.SetTextSize( TEXTSIZE )
    atlasStyle.SetLabelFont( FONT,"x" )
    atlasStyle.SetTitleFont( FONT,"x" )
    atlasStyle.SetLabelFont( FONT,"y" )
    atlasStyle.SetTitleFont( FONT,"y" )
    atlasStyle.SetLabelFont( FONT,"z" )
    atlasStyle.SetTitleFont( FONT,"z" )
    
    atlasStyle.SetLabelSize( XLABELSIZE,"x" )
    atlasStyle.SetTitleSize( YTITLESIZE,"x" )

    atlasStyle.SetLabelSize( YLABELSIZE,"y" )
    atlasStyle.SetTitleSize( YTITLESIZE,"y" )

    atlasStyle.SetLabelSize( ZLABELSIZE,"z" )
    atlasStyle.SetTitleSize( YTITLESIZE,"z" )

    atlasStyle.SetMarkerStyle(     20 )
    atlasStyle.SetMarkerSize(      1.2 )
    atlasStyle.SetHistLineWidth(   LINEWIDTH )
    atlasStyle.SetLineStyleString( 2,"[12 12]" )
    atlasStyle.SetEndErrorSize(    0.)
    atlasStyle.SetOptTitle(        0 )
    atlasStyle.SetOptStat(         0 )
    atlasStyle.SetOptFit(          0 )
    atlasStyle.SetPadTickX(        1 )
    atlasStyle.SetPadTickY(        1 )
    atlasStyle.SetErrorX(          0.5 )
    atlasStyle.SetEndErrorSize( 4 )
    
    return atlasStyle



def make_ATLAS_lumi(x, y, lumi, size):

    """ Function to make an ATLAS luminosity TLatex Box """

    from ROOT import TLatex

    luminosity = str(lumi) + " fb^{-1}"

    lumitext = TLatex()
    lumitext.SetNDC()
    lumitext.SetTextColor(1)
    lumitext.SetTextFont(42)
    lumitext.SetTextSize(size) #0.03
    lumitext.DrawLatex(x, y, luminosity)

def make_ATLAS_energy(x, y, energy, size):

    """ Function to make an ATLAS sqrt(s) TLatex Box """

    from ROOT import TLatex

    col_energy = "#bf{#sqrt{s} = " + str(energy) + " TeV},"

    energytext = TLatex()
    energytext.SetNDC()

    energytext.SetTextColor(1)
    energytext.SetTextSize(size)
    energytext.DrawLatex(x, y, col_energy)

def make_ATLAS_text(x, y, text, size):

    """ Function to make an ATLAS sqrt(s) TLatex Box """

    from ROOT import TLatex

    col_energy = text

    energytext = TLatex()
    energytext.SetNDC()

    energytext.SetTextColor(1)
    energytext.SetTextSize(size)
    energytext.DrawLatex(x, y, col_energy)

def make_ATLAS_label(x, y, color, size, word):

    """ Function to return an ATLAS style TLatex Object """

    from ROOT import TLatex, TPad

    l = TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.SetTextSize(size)

    l.DrawLatex(x,y,"ATLAS");

    p = TLatex()
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextSize(size)
    diff = size*2.4
    p.DrawLatex(x+diff,y,word)
   
