#!/bin/bash                                                                                                                                                                                                  
declare -A BIN_ARRAY

vars=(helpol_plus helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus helcorr transcorr rcorr transhelsum transheldiff transrsum transrdiff rhelsum rheldiff) #

helpol_bins='4Bin4'
transpol_bins='4Bin1' #'3Bin4' #5BinSym'                                                                                                                                                                  
rpol_bins='4Bin1'
helcorr_bins='6Bin2'
transcorr_bins='6Bin2'
rcorr_bins='6Bin2'
transhel_bins='6Bin1'
transrsum_bins='6Bin1'
transrdiff_bins='6Bin1'
rhelsum_bins='6Bin1'
rheldiff_bins='6Bin1'


BIN_ARRAY[helpol_plus]=${helpol_bins}
BIN_ARRAY[helpol_minus]=${helpol_bins}
BIN_ARRAY[transpol_plus]=${transpol_bins}
BIN_ARRAY[transpol_minus]=${transpol_bins}
BIN_ARRAY[rpol_plus]=${rpol_bins}
BIN_ARRAY[rpol_minus]=${rpol_bins}
BIN_ARRAY[helcorr]=${helcorr_bins}
BIN_ARRAY[transcorr]=${transcorr_bins}
BIN_ARRAY[rcorr]=${rcorr_bins}
BIN_ARRAY[transhelsum]=${transhel_bins}
BIN_ARRAY[transheldiff]=${transhel_bins}
BIN_ARRAY[transrsum]=${transrsum_bins}
BIN_ARRAY[transrdiff]=${transrdiff_bins}
BIN_ARRAY[rhelsum]=${rhelsum_bins}
BIN_ARRAY[rheldiff]=${rheldiff_bins}

for var in ${vars[@]}
do
    for bin in ${BIN_ARRAY[${var}]}
    do
#  python PlotBinCorrRoger.py /nfs/dust/atlas/user/raschaef/Unfolding/Partonic/RogerCorrFiles/trace_${bin}_Tikhonov_rpZero_${var}_data.npy ${var}
python PlotBinCorrRoger.py /nfs/dust/atlas/user/raschaef/Unfolding/Partonic/RogerCorrFiles/fiducial/trace_${bin}_Tikhonov_rpZero_${var}_data.npy ${var}
    done
done
