import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import json
import sys
from   ROOT import *
from numpy import mean,sqrt


#posterior = np.load((sys.argv[1]))
data      = np.load((sys.argv[1]))

#could convert the observable name from the argv 1... but... I won't 
var = str(sys.argv[2])

print data
distributions =  zip(*data)
#Let's normalize
#binsX     = [0,1]
#posterior = np.array( [computeAC_2D(bins, binsX)[0] for bins in zip(*data)] ) 
#p         = mean(posterior)
#posterior = np.array( [i / p for i in posterior] )

print len(data)

nbins = len(data)

#print posterior
print len(distributions),len(data[0])
newdata = []
for i in xrange(len(data)):
  newdata.append([])
  for j in xrange(len(data[i])):
    
    newdata[i].append(data[i][j]*1./sum(distributions[j]))
 

var_dict = {"helpol_plus": "$\cos \\theta_{+}^{k}$",
            "helpol_minus": "$\cos \\theta_{-}^{k}$",
            "transpol_plus": "$\cos \\theta_{+}^{n}$",
            "transpol_minus": "$\cos \\theta_{-}^{n}$",
            "rpol_plus": "$\cos \\theta_{+}^{r}$",
            "rpol_minus": "$\cos \\theta_{-}^{r}$",
            "helcorr": "$\cos \\theta_{+}^{k} \cos \\theta_{-}^{k}$",
            "transcorr": "$\cos \\theta_{+}^{n} \cos \\theta_{-}^{n}$",
            "rcorr": "$\cos \\theta_{+}^{r} \cos \\theta_{-}^{r}$",
            "transhelsum": "$\cos \\theta_{+}^{n} \cos \\theta_{-}^{k} + \cos \\theta_{+}^{k} \cos \\theta_{-}^{n}$",
            "transheldiff": "$\cos \\theta_{+}^{n} \cos \\theta_{-}^{k} - \cos \\theta_{+}^{k} \cos \\theta_{-}^{n}$",
            "transrsum": "$\cos \\theta_{+}^{n} \cos \\theta_{-}^{r} + \cos \\theta_{+}^{r} \cos \\theta_{-}^{n}$",
            "transrdiff": "$\cos \\theta_{+}^{n} \cos \\theta_{-}^{r} - \cos \\theta_{+}^{r} \cos \\theta_{-}^{n}$",
            "rhelsum": "$\cos \\theta_{+}^{r} \cos \\theta_{-}^{k} + \cos \\theta_{+}^{k} \cos \\theta_{-}^{r}$",
            "rheldiff": "$\cos \\theta_{+}^{r} \cos \\theta_{-}^{k} - \cos \\theta_{+}^{k} \cos \\theta_{-}^{r}$",
}


#print newdata            
#f,ax      = plt.subplots(figsize=(10, 10))
f,ax      = plt.subplots(figsize=(8, 8))
#f = plt.figure()
a=np.corrcoef(newdata)
with open('/nfs/dust/atlas/user/naranjo/TopPolarization/binbybinCorr/plots/correlation_'+var+'_particle.json',"w") as f:
  json.dump(a.tolist(),f)

#img = ax.matshow(a, cmap='Purples',interpolation="none",vmax=1.3, vmin=-1.3)
#img = ax.matshow(a, cmap='RdBu',interpolation="none",vmax=1.3, vmin=-1.3)
#img = ax.matshow(a, cmap='coolwarm',interpolation="none",vmax=1.3, vmin=-1.3)
img = ax.matshow(a, cmap='coolwarm',vmax=1.3, vmin=-1.3)
#f.colorbar(img)  # draw colorbara

ax.xaxis.tick_bottom()

for (i, j), z in np.ndenumerate(a):
  ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center',fontsize=20)

plt.text( 0.,1.085, 'ATLAS',fontsize=20, fontweight='bold',style='italic',transform=ax.transAxes)
#plt.text( 0.225,1.085, 'Preliminary',fontsize=20, fontweight='bold',transform=ax.transAxes)
#plt.text( 0.225,1.085, 'Internal',fontsize=20, fontweight='bold',transform=ax.transAxes)
#plt.text( 0.12,1.03, '',fontsize=20, style='italic',transform=ax.transAxes)
plt.text( 0.,1.025,  r"$\sqrt{s}$ = $8$ TeV, $20.2$ $\rm{{fb}}^{-1}$",fontsize=20,transform=ax.transAxes)
plt.text( 0.52,1.025,  r"%s"%(var_dict[var]),fontsize=20,transform=ax.transAxes)

#if 'corr' in var:
  #plt.text( 0.52,1.025,  r"%s"%(var_dict[var]),fontsize=20,transform=ax.transAxes)
#elif 'sum' in var or 'diff' in var:
  #plt.text( 0.52,1.025,  r"%s"%(var_dict[var]),fontsize=20,transform=ax.transAxes)
#else:
  #plt.text( 0.52,1.025,  r"%s"%(var_dict[var]),fontsize=20,transform=ax.transAxes)

#plt.grid(False)


#plt.xticks(range(0,16), range(1,17),fontsize=20)
#plt.yticks(range(0,16), range(1,17),fontsize=20)
plt.xticks(range(0,nbins), range(1,nbins+1),fontsize=20) #should make that automatic, but don't care at the moment
plt.yticks(range(0,nbins), range(1,nbins+1),fontsize=20)
#plt.yticks(range(len(data['name'])), data['name']) # the plot is sooo crazy

ax.set_xlabel('Unfolded Bin',fontsize=20)


#ax.xaxis.set_label_coords(0.5, -.15)


ax.set_ylabel('Unfolded Bin',fontsize=20)

#plt.tight_layout()
#plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/binbybinCorr/plots/correlation_'+var+'_parton.eps', format='eps')
plt.savefig('/nfs/dust/atlas/user/naranjo/TopPolarization/binbybinCorr/plots/correlation_'+var+'_particle.eps', format='eps')
#plt.clf()

#plt.show()

#fi2 = plt.figure()
#plt.scatter(data[1],data[0],color='blue',s=5,edgecolor='none')
#plt.show()
