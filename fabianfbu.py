
##system modules
import sys
import json
import gc
import os

from   numpy import mean,std,array, linspace,dot
import numpy as np
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

from   matplotlib import pyplot as plt

#fbu
import fbu
LUMISF = 0.998145
#our modules
from Config import Configuration
from utils import *

DEBUG=False
PSEUDO=False

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')

        rv[key] = value
    return rv



def plotUnfoldedEvents(trace):
  plt.show()
  binContents = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    plt.hist(trace[i],bins=20,alpha=0.85,normed=True)
    plt.ylabel('Unfoled event probability')
    plt.savefig(jsondir+'UnfEvtBin'+str(i)+'_posterior.eps')
    plt.close()
    meanBin = mean(trace[i])
    binContents.append(meanBin)
    print 'UnfoldEventnBin',i,meanBin
  writefile(jsondir+'UnfoldedStat_bin',binContents)

def writeUnfoldInfo(unfAC,syst,binName,rpName):



  ACmean = np.mean(unfAC)
  ACrms  = np.std (unfAC)
  datatowrite = [ACmean,ACrms]

  fname = jsondir+'/UnfoldedStat_'+signal+'_'+syst+'_'+var+'_'+binName+'_'+rpName
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)



def printTruthInfo(truth, truthAC, syst, binName, binsX):
  print ' '
  print '  ** Truth informations **'
  print 'Truth bin contents:',truth
  writefile(jsondir+'Truth_binContent',truth)
  print 'truth AC',truthAC
  print ' '
  fname = jsondir+'/Truth_'+var+'_'+syst+'_'+binName
  datatowrite = truth
  with open(fname+'.json','w') as outfile:
     json.dump(datatowrite,outfile)




def plot_distribution(trace, truth, reco):
  bins = [0.0, 0.5, 1.0]
  binX    = []
  binXErr = []
  binWidth = []
  for i in xrange(len(bins)-1):
    binX.append( (bins[i+1] + bins[i])/2. )
    binXErr.append( (bins[i+1] - bins[i])/2. )
    binWidth.append(abs((bins[i+1] - bins[i])))

  binContents = []
  binErr      = []
  nbins = len(trace)
  for i in xrange(0,nbins):
    meanBin = np.mean(trace[i])
    rmsBin  = np.std(trace[i])

    binContents.append(meanBin)
    binErr.append(rmsBin)

  reco = np.array(reco)
  reco = reco/sum(reco)
  truth = np.array(truth)/sum(truth)
  binContents = np.array(binContents)/sum(binContents)
  plt.errorbar(binX, binContents,  xerr=binXErr,fmt='o',ecolor='g',mfc='g',label='unfolded')
  plt.errorbar(binX, truth, xerr=binXErr,fmt='o',ecolor='b',mfc='b',label='truth')
  plt.errorbar(binX, reco, fmt='o',ecolor='r',mfc='r',label='reco')


  plt.ylabel(r'$Nevents$')
  plt.legend()
  plt.savefig('test_distribution.eps')
  plt.close()

  #datatowrite = [binContents, binErr]
  #with open('fabian.json','w') as outfile:
  #   json.dump(datatowrite,outfile)
  print "Bin COntent", binContents, "Bin Error", binErr
  print "Truth Bins" , truth
  print "Reco Bins", reco



def getEff(resmat,truth):
    #get coordinates in 2D truth hist
    #note: resp matrix is 2D: (nXtruth*nYtruth) x (nXtruth*nYtruth) --> y = (yy-1)*Nbins_dy + xx

    eff = [] 
    for t in range(len(resmat)):
      eff.append(sum(resmat[t])/truth[t])

    return eff



def resmatEstimator(resmat,eff):

    tmp = resmat

    for y in xrange(2):
        norm = sum(resmat[y])
        for x in xrange(2):
            value  = resmat[y][x]/norm
            tmp[y][x] = value*eff[y]

    return tmp 


if __name__ == '__main__':

  debug = 10


  pyfbu = fbu.PyFBU()

 

  truth        = [228688926.14237487, 191805662.36835793]

  data         =  [210036046.35324416, 183195053.2131004]
  
  resmat       = [[167705958.70950705, 48990866.699837916], [47056595.856497124, 129552838.76210847]]



  #print "Reference curvature",refcurv

   # -- Set limit of the hyperbox (sampling area)
  pyfbu.lower = [t/10 for t in truth]
  pyfbu.upper = [t*5 for t in truth]

  # -- Load input distributions
  pyfbu.data           = data
  
  resmat = resmat
  
  response = resmatEstimator(resmat,getEff(resmat,truth))


  pyfbu.response       = response


  # -- Set N_mcmc
  pyfbu.nMCMC = 100000
  pyfbu.nBurn= 10000

  pyfbu.run()

  trace = pyfbu.trace


  plot_distribution(trace, truth, data)

