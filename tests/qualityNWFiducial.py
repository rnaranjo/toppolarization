from ROOT import *
from array import array
from tqdm import tqdm

import numpy as np
import json
import matplotlib.pyplot as plt
import utils
import sys
import os


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

    neutrinoFile    = TFile(sys.argv[1])
    neutrinoTree    = neutrinoFile.Get('mini')

    partonicFile    = TFile(sys.argv[2])
    partonicTree = partonicFile.Get('particles')
    partonicTree.BuildIndex("runNumber","eventNumber")

    entries = neutrinoTree.GetEntries()
    counterFiducial = 0
    counterNonFiducial = 0
    for ev in tqdm(xrange(entries)):

        neutrinoTree.GetEntry(ev)

        if not neutrinoTree.isReconstructed: continue

        if partonicTree.GetEntryWithIndex(neutrinoTree.runNumber,neutrinoTree.eventNumber) <= 0:continue

        if partonicTree.inFiducial : 
            counterFiducial = counterFiducial + 1

        if not partonicTree.inFiducial :
            counterNonFiducial = counterNonFiducial + 1 


    print "counterFiducial:",counterFiducial
    print "counterNonFiducial:",counterNonFiducial
