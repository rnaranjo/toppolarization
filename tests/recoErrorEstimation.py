from ROOT import *
from array import array
from Rebin import binning_dict
from tqdm import tqdm

import numpy as np
import json
import matplotlib.pyplot as plt
import utils
import sys
import os

def plotCorrelation(a,name = 'correlation.eps'):
  f,ax      = plt.subplots(figsize=(10, 10))
  img = ax.matshow(a, cmap='jet',interpolation="none",vmax=1.3, vmin=-1.30)
  ax.xaxis.tick_bottom()
  for (i, j), z in np.ndenumerate(a):
    ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center',fontsize=15)
  plt.text( 0.01,1.03, 'ATLAS',fontsize=20, fontweight='bold',style='italic',transform=ax.transAxes)
  plt.text( 0.14,1.03, 'Internal',fontsize=20, style='italic',transform=ax.transAxes)
  plt.text( 0.3,1.03,  "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=20,transform=ax.transAxes)
  plt.xticks(range(0,12), range(1,13),fontsize=20)
  plt.yticks(range(0,12), range(1,13),fontsize=20)
  ax.set_xlabel('Reco Bin',fontsize=20)
  ax.set_ylabel('Reco Bin',fontsize=20)
  #plt.show()
  plt.savefig(name, format='eps')

##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

  polarizationBinning = binning_dict
  seed                = sys.argv[1]
  var                 = utils.setVar(sys.argv)

  outputFolder = '/nfs/dust/atlas/user/naranjo/TopPolarization/tests/results/seeds/%s/%s/' % (var,seed)
  if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)

  fileName    = "/nfs/dust/atlas/user/naranjo/KIN_OUTPUT/mctestseed/merged/reco_%s_110404_mini.root" % (seed)
  rootFile    = TFile(fileName)
  tree        = rootFile.Get("mini")
  entries     = tree.GetEntries()

  variable = var.split('_')[0]
  binning =  polarizationBinning[variable]
  histogram = TH1F(var,var,len(binning)-1,array('d',binning))

  binContent   = []

  for i in xrange(histogram.GetNbinsX()):
    binContent.append([])

  top   = TLorentzVector()
  tbar  = TLorentzVector()
  lep_p = TLorentzVector()
  lep_n = TLorentzVector()

  entries = tree.GetEntries()

  for ev in tqdm(xrange(entries)):

    tree.GetEntry(ev)

    if not tree.isReconstructed: continue

    top.SetPtEtaPhiM(tree.top_pt, tree.top_eta, tree.top_phi, tree.top_m)
    tbar.SetPtEtaPhiM(tree.tbar_pt, tree.tbar_eta, tree.tbar_phi, tree.tbar_m)

    if tree.lep_charge[0] == 1:

      lep_p.SetPtEtaPhiE( tree.lep_pt[0], tree.lep_eta[0], tree.lep_phi[0], tree.lep_E[0])
      lep_n.SetPtEtaPhiE( tree.lep_pt[1], tree.lep_eta[1], tree.lep_phi[1], tree.lep_E[1])

    elif tree.lep_charge[1] == 1:

      lep_p.SetPtEtaPhiE(tree.lep_pt[1], tree.lep_eta[1], tree.lep_phi[1], tree.lep_E[1])
      lep_n.SetPtEtaPhiE(tree.lep_pt[0], tree.lep_eta[0], tree.lep_phi[0], tree.lep_E[0])

    reco_m, reco_p  = utils.getCosThetas(var, top, tbar, lep_p, lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
    reco            = reco_m

    if 'corr' in var:
      reco = reco_m*reco_p # this can be changed later again depending on how I want to reweight spin correlation...  (just by the correlation or removing the polarization as well)
    elif 'sum' in var:
      reco = reco_m+reco_p
    elif 'diff' in var:
      reco = reco_m-reco_p

    histogram.Fill(reco,tree.eventWeight_BTAG)

  tree.IsA().Destructor( tree )
  histogram.Scale(1./histogram.Integral())  

  for i in xrange(histogram.GetNbinsX()):
    binContent[i] = histogram.GetBinContent(i+1)   

  utils.writefile(outputFolder+'binContent', binContent) 

  print "Success, file saved at: " + outputFolder+'binContent.json'


      






