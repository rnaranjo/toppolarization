from ROOT import *
from array import array
from tqdm import tqdm

import numpy as np
import json
import matplotlib.pyplot as plt
import utils
import sys
import os


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':


    variables = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
    'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
    'transheldiff']

    humanVariables = ['Helicity Polarization +','Helicity Polarization -','Transverse Polarization +','Transverse Polarization -','R Polarization +','R Polarization -',\
    'Helicity Correlation','Transverse Correlation','R Correlation','R Helicity Sum', 'R Helicity Diff','Transverse R Sum','Transverse R Diff','Transverse Helicity Sum',\
    'Transverse Helicity Diff']

    results = TFile('comparison.root')

    print "Observable\tNW Resolution\tKIN Resolution\tRelative Difference (%)"

    for i,var in enumerate(variables):

        histogram_nw  = results.Get(var+'_nw')
        histogram_kin = results.Get(var+'_kin')

        print "%s\t%0.2f\t%0.2f\t%0.1f" % (humanVariables[i],histogram_nw.GetRMS(),histogram_kin.GetRMS(),(histogram_nw.GetRMS()-histogram_kin.GetRMS())/histogram_nw.GetRMS()*100)


    print "------------------------------------------------------------------"
    print "Observable\tNW Bias\tKIN Bias\tRelative Difference (%)"

    for i,var in enumerate(variables):

        histogram_nw  = results.Get(var+'_nw')
        histogram_kin = results.Get(var+'_kin')

        print "%s\t%0.2f+-%0.2f\t%0.2f+-%0.2f\t%0.1f" % (humanVariables[i],histogram_nw.GetMean(),histogram_nw.GetMeanError(),histogram_kin.GetMean(),histogram_kin.GetMeanError(),(histogram_nw.GetMean()-histogram_kin.GetMean())/histogram_nw.GetMean()*100)

    results.Close()
