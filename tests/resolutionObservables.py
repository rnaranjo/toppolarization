from ROOT import *
from array import array
from tqdm import tqdm

import numpy as np
import json
import utils
import sys
import os


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

    neutrinoFile    = TFile(sys.argv[1])
    neutrinoTree    = neutrinoFile.Get('mini')

    partonicFile    = TFile(sys.argv[2])
    partonicTree = partonicFile.Get('particles')
    partonicTree.BuildIndex("runNumber","eventNumber")

    variables = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                'transheldiff']

    histogram_nw  = {}

    for var in variables:

        histogram_nw[var]  = TH2F(var+'_nw' ,var,10,-1,1,30,-2,2,)

    results = TFile(sys.argv[3],'recreate')


    top_nw   = TLorentzVector()
    tbar_nw  = TLorentzVector()
    lep_p_nw = TLorentzVector()
    lep_n_nw = TLorentzVector()
    true_top   = TLorentzVector()
    true_tbar  = TLorentzVector()
    true_lep_p = TLorentzVector()
    true_lep_n = TLorentzVector()

    top_kin   = TLorentzVector()
    tbar_kin  = TLorentzVector()
    lep_p_kin = TLorentzVector()
    lep_n_kin = TLorentzVector()


    entries = neutrinoTree.GetEntries()

    for ev in tqdm(xrange(entries)):

        neutrinoTree.GetEntry(ev)

        if not neutrinoTree.isReconstructed: continue


        if partonicTree.GetEntryWithIndex(neutrinoTree.runNumber,neutrinoTree.eventNumber) <= 0:continue

        top_nw.SetPtEtaPhiM(neutrinoTree.top_pt, neutrinoTree.top_eta, neutrinoTree.top_phi, neutrinoTree.top_m)
        tbar_nw.SetPtEtaPhiM(neutrinoTree.tbar_pt, neutrinoTree.tbar_eta, neutrinoTree.tbar_phi, neutrinoTree.tbar_m)
        true_top.SetPtEtaPhiM(partonicTree.true_top_pt, partonicTree.true_top_eta, partonicTree.true_top_phi, partonicTree.true_top_m)
        true_tbar.SetPtEtaPhiM(partonicTree.true_tbar_pt, partonicTree.true_tbar_eta, partonicTree.true_tbar_phi, partonicTree.true_tbar_m)

        if neutrinoTree.lep_charge[0] == 1:
            lep_p_nw.SetPtEtaPhiE( neutrinoTree.lep_pt[0], neutrinoTree.lep_eta[0], neutrinoTree.lep_phi[0], neutrinoTree.lep_E[0])
            lep_n_nw.SetPtEtaPhiE( neutrinoTree.lep_pt[1], neutrinoTree.lep_eta[1], neutrinoTree.lep_phi[1], neutrinoTree.lep_E[1])
        elif neutrinoTree.lep_charge[1] == 1:
            lep_p_nw.SetPtEtaPhiE(neutrinoTree.lep_pt[1], neutrinoTree.lep_eta[1], neutrinoTree.lep_phi[1], neutrinoTree.lep_E[1])
            lep_n_nw.SetPtEtaPhiE(neutrinoTree.lep_pt[0], neutrinoTree.lep_eta[0], neutrinoTree.lep_phi[0], neutrinoTree.lep_E[0])
        if partonicTree.true_lep_charge[0] == 1:
            true_lep_p.SetPtEtaPhiM( partonicTree.true_rew_lep_pt[0], partonicTree.true_rew_lep_eta[0], partonicTree.true_rew_lep_phi[0], partonicTree.true_rew_lep_m[0])
            true_lep_n.SetPtEtaPhiM( partonicTree.true_rew_lep_pt[1], partonicTree.true_rew_lep_eta[1], partonicTree.true_rew_lep_phi[1], partonicTree.true_rew_lep_m[1])
        elif partonicTree.true_lep_charge[1] == 1:
            true_lep_p.SetPtEtaPhiM(partonicTree.true_rew_lep_pt[1], partonicTree.true_rew_lep_eta[1], partonicTree.true_rew_lep_phi[1], partonicTree.true_rew_lep_m[1])
            true_lep_n.SetPtEtaPhiM(partonicTree.true_rew_lep_pt[0], partonicTree.true_rew_lep_eta[0], partonicTree.true_rew_lep_phi[0], partonicTree.true_rew_lep_m[0])

        for var in variables:

            variable = var.split('_')[0]

            reco_p_nw, reco_m_nw  = utils.getCosThetas(var, top_nw, tbar_nw, lep_p_nw, lep_n_nw) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
            reco_nw            = reco_p_nw
            true_p, true_m  = utils.getCosThetas(var, true_top, true_tbar, true_lep_p, true_lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
            true            = true_m

            if 'corr' in var:
                reco_nw     = reco_m_nw*reco_p_nw
                true        = true_m*true_p
            elif 'sum' in var:
                reco_nw     = reco_m_nw+reco_p_nw
                true        = true_m+true_p
            elif 'diff' in var:
                reco_nw     = reco_m_nw+reco_p_nw
                true        = true_m+true_p



            if 'plus' in var:

                histogram_nw[var].Fill(reco_p_nw,true_p - reco_p_nw)
            elif 'minus' in var:
                histogram_nw[var].Fill(reco_m_nw,true_m - reco_m_nw)
            else:
                histogram_nw[var].Fill(reco_nw,true-reco_nw)






    for var in variables:
        histogram_nw[var].Write()

    results.Close()
