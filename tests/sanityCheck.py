import Rebin
import ROOT
import sys



f = ROOT.TFile(sys.argv[1])
resmat = f.Get("resmat")
reco   = f.Get("reco")
truth  = f.Get("partonic")


binning = [-1.,-0.5,0.,0.5,1.]


n_resmat = Rebin.rebin2D(resmat,binning)
n_reco   = Rebin.rebin(reco,binning)
n_reco.SetName("reco")
n_truth  = Rebin.rebin(truth,binning)
n_truth.SetName("truth")
n_truth_proyection = n_resmat.ProjectionY("_truth")
n_reco_proyection  = n_resmat.ProjectionX("_reco")


#n_resmat = resmat
#n_reco   = reco
#n_reco.SetName("reco")
#n_truth  = truth
#n_truth.SetName("truth")
#n_truth_proyection = n_resmat.ProjectionY("_truth")
#n_reco_proyection  = n_resmat.ProjectionX("_reco")



acceptance = n_reco.Clone()
acceptance.Divide(n_truth)
acceptance.SetName("acceptance")


f2 = ROOT.TFile(sys.argv[2],"recreate")

n_resmat.Write()
n_reco.Write()
n_truth.Write()
n_truth_proyection.Write()
n_reco_proyection.Write()
acceptance.Write()


f2.Close()






