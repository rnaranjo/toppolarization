import ROOT
import sys

treelist = ['mini',
'jer', 'jeff', 'ees_up', 'ees_down', 'eer_up', 'eer_down', 'el_trigSF_up', 'el_trigSF_down',\
'el_recSF_up', 'el_recSF_down', 'el_idSF_up', 'el_idSF_down', 'musc_up', 'musc_down',\
'mums_res', 'muid_res', 'mu_trigSF_up', 'mu_trigSF_down', 'mu_recSF_up', 'mu_recSF_down',\
'mu_idSF_up', 'mu_idSF_down', 'jvf_up', 'jvf_down', 'res_soft_up', 'res_soft_down',\
'sc_soft_up', 'sc_soft_down', 'JesEffectiveStat1_up', 'JesEffectiveStat2_up',\
'JesEffectiveStat3_up', 'JesEffectiveStat4_up', 'JesEffectiveModel1_up',\
'JesEffectiveModel2_up', 'JesEffectiveModel3_up', 'JesEffectiveModel4_up', \
'JesEffectiveDet1_up', 'JesEffectiveDet2_up', 'JesEffectiveDet3_up', 'JesEffectiveMix1_up', \
'JesEffectiveMix2_up', 'JesEffectiveMix3_up', 'JesEffectiveMix4_up', \
'EtaIntercalibrationModel_up', 'EtaIntercalibrationTotalStat_up', 'SinglePart_up',\
'Pileup_OffsetMu_up', 'Pileup_OffsetNPV_up', 'Pileup_Pt_up', 'Pileup_Rho_up',
'flavor_comp_up', 'flavor_response_up', 'BJesUnc_up', 'PunchThrough_up', 'JesEffectiveStat1_down', \
'JesEffectiveStat2_down', 'JesEffectiveStat3_down', 'JesEffectiveStat4_down', 'JesEffectiveModel1_down', \
'JesEffectiveModel2_down', 'JesEffectiveModel3_down', 'JesEffectiveModel4_down', 'JesEffectiveDet1_down',\
'JesEffectiveDet2_down', 'JesEffectiveDet3_down', 'JesEffectiveMix1_down', 'JesEffectiveMix2_down',\
'JesEffectiveMix3_down', 'JesEffectiveMix4_down', 'EtaIntercalibrationModel_down', 'EtaIntercalibrationTotalStat_down', 'SinglePart_down', 'Pileup_OffsetMu_down', 'Pileup_OffsetNPV_down', 'Pileup_Pt_down'


]

rootFile = ROOT.TFile(sys.argv[1])

for tree in treelist:

    treeObj = rootFile.Get(tree)
    print tree, treeObj.GetEntries()




