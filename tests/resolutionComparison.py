from ROOT import *
from array import array
from tqdm import tqdm

import numpy as np
import json
import matplotlib.pyplot as plt
import utils
import sys
import os


##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

    neutrinoFile    = TFile(sys.argv[1])
    neutrinoTree    = neutrinoFile.Get('mini')

    kinFile         = TFile(sys.argv[2])
    kinTree         = kinFile.Get('mini')
    kinTree.BuildIndex("runNumber","eventNumber")

    partonicFile    = TFile(sys.argv[3])
    partonicTree = partonicFile.Get('particles')
    partonicTree.BuildIndex("runNumber","eventNumber")

    variables = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
    'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
    'transheldiff']
    histogram_nw  = {}
    histogram_kin = {}
    for var in variables:

        histogram_nw[var]  = TH1F(var+'_nw' ,var,50,-5,5)
        histogram_kin[var] = TH1F(var+'_kin',var,50,-5,5)

    results = TFile(sys.argv[4],'recreate')

    histogram_nw_pt     = TH1F('nw_pt' ,'',50,-50,50)
    histogram_kin_pt    = TH1F('kin_pt','',50,-50,50)
    histogram_nw_eta    = TH1F('nw_eta' ,'',50,-5,5)
    histogram_kin_eta   = TH1F('kin_eta','',50,-5,5)
    histogram_nw_phi    = TH1F('nw_phi' ,'',50,-5,5)
    histogram_kin_phi   = TH1F('kin_phi','',50,-5,5)
    histogram_nw_y      = TH1F('nw_rapidity' ,'',50,-5,5)
    histogram_kin_y     = TH1F('kin_rapidity','',50,-5,5)



    top_nw   = TLorentzVector()
    tbar_nw  = TLorentzVector()
    lep_p_nw = TLorentzVector()
    lep_n_nw = TLorentzVector()
    true_top   = TLorentzVector()
    true_tbar  = TLorentzVector()
    true_lep_p = TLorentzVector()
    true_lep_n = TLorentzVector()

    top_kin   = TLorentzVector()
    tbar_kin  = TLorentzVector()
    lep_p_kin = TLorentzVector()
    lep_n_kin = TLorentzVector()


    entries = neutrinoTree.GetEntries()

    for ev in tqdm(xrange(entries)):

        neutrinoTree.GetEntry(ev)

        if not neutrinoTree.isReconstructed: continue

        if kinTree.GetEntryWithIndex(neutrinoTree.runNumber,neutrinoTree.eventNumber) <= 0:continue

        if not kinTree.isReconstructed: continue

        if partonicTree.GetEntryWithIndex(neutrinoTree.runNumber,neutrinoTree.eventNumber) <= 0:continue

        top_nw.SetPtEtaPhiM(neutrinoTree.top_pt, neutrinoTree.top_eta, neutrinoTree.top_phi, neutrinoTree.top_m)
        tbar_nw.SetPtEtaPhiM(neutrinoTree.tbar_pt, neutrinoTree.tbar_eta, neutrinoTree.tbar_phi, neutrinoTree.tbar_m)
        top_kin.SetPtEtaPhiM(kinTree.top_pt, kinTree.top_eta, kinTree.top_phi, kinTree.top_m)
        tbar_kin.SetPtEtaPhiM(kinTree.tbar_pt, kinTree.tbar_eta, kinTree.tbar_phi, kinTree.tbar_m)
        true_top.SetPtEtaPhiM(partonicTree.true_top_pt, partonicTree.true_top_eta, partonicTree.true_top_phi, partonicTree.true_top_m)
        true_tbar.SetPtEtaPhiM(partonicTree.true_tbar_pt, partonicTree.true_tbar_eta, partonicTree.true_tbar_phi, partonicTree.true_tbar_m)

        if neutrinoTree.lep_charge[0] == 1:
            lep_p_nw.SetPtEtaPhiE( neutrinoTree.lep_pt[0], neutrinoTree.lep_eta[0], neutrinoTree.lep_phi[0], neutrinoTree.lep_E[0])
            lep_n_nw.SetPtEtaPhiE( neutrinoTree.lep_pt[1], neutrinoTree.lep_eta[1], neutrinoTree.lep_phi[1], neutrinoTree.lep_E[1])
        elif neutrinoTree.lep_charge[1] == 1:
            lep_p_nw.SetPtEtaPhiE(neutrinoTree.lep_pt[1], neutrinoTree.lep_eta[1], neutrinoTree.lep_phi[1], neutrinoTree.lep_E[1])
            lep_n_nw.SetPtEtaPhiE(neutrinoTree.lep_pt[0], neutrinoTree.lep_eta[0], neutrinoTree.lep_phi[0], neutrinoTree.lep_E[0])
        if kinTree.lep_charge[0] == 1:
            lep_p_kin.SetPtEtaPhiE( kinTree.lep_pt[0], kinTree.lep_eta[0], kinTree.lep_phi[0], kinTree.lep_E[0])
            lep_n_kin.SetPtEtaPhiE( kinTree.lep_pt[1], kinTree.lep_eta[1], kinTree.lep_phi[1], kinTree.lep_E[1])
        elif kinTree.lep_charge[1] == 1:
            lep_p_kin.SetPtEtaPhiE(kinTree.lep_pt[1], kinTree.lep_eta[1], kinTree.lep_phi[1], kinTree.lep_E[1])
            lep_n_kin.SetPtEtaPhiE(kinTree.lep_pt[0], kinTree.lep_eta[0], kinTree.lep_phi[0], kinTree.lep_E[0])
        if partonicTree.true_lep_charge[0] == 1:
            true_lep_p.SetPtEtaPhiM( partonicTree.true_rew_lep_pt[0], partonicTree.true_rew_lep_eta[0], partonicTree.true_rew_lep_phi[0], partonicTree.true_rew_lep_m[0])
            true_lep_n.SetPtEtaPhiM( partonicTree.true_rew_lep_pt[1], partonicTree.true_rew_lep_eta[1], partonicTree.true_rew_lep_phi[1], partonicTree.true_rew_lep_m[1])
        elif partonicTree.true_lep_charge[1] == 1:
            true_lep_p.SetPtEtaPhiM(partonicTree.true_rew_lep_pt[1], partonicTree.true_rew_lep_eta[1], partonicTree.true_rew_lep_phi[1], partonicTree.true_rew_lep_m[1])
            true_lep_n.SetPtEtaPhiM(partonicTree.true_rew_lep_pt[0], partonicTree.true_rew_lep_eta[0], partonicTree.true_rew_lep_phi[0], partonicTree.true_rew_lep_m[0])

        for var in variables:


            variable = var.split('_')[0]


            reco_m_nw, reco_p_nw  = utils.getCosThetas(var, top_nw, tbar_nw, lep_p_nw, lep_n_nw) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
            reco_nw            = reco_m_nw
            reco_m_kin, reco_p_kin  = utils.getCosThetas(var, top_kin, tbar_kin, lep_p_kin, lep_n_kin) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
            reco_kin            = reco_m_kin
            true_m, true_p  = utils.getCosThetas(var, true_top, true_tbar, true_lep_p, true_lep_n) #for minus polarizations, truth is the minus pol, truth 2 the plus pol
            true            = true_m

            if 'corr' in var:
                reco_nw     = reco_m_nw*reco_p_nw
                reco_kin    = reco_m_kin*reco_p_kin
                true        = true_m*true_p

            elif 'sum' in var:
                reco_nw     = reco_m_nw+reco_p_nw
                reco_kin    = reco_m_kin+reco_p_kin
                true        = true_m+true_p
            elif 'diff' in var:
                reco_nw     = reco_m_nw+reco_p_nw
                reco_kin    = reco_m_kin+reco_p_kin
                true        = true_m+true_p



            if 'plus' in var:

                histogram_nw[var].Fill(true_p-reco_p_nw)
                histogram_kin[var].Fill(true_p-reco_p_kin)
            elif 'minus' in var:
                histogram_nw[var].Fill(true_m-reco_m_nw)
                histogram_kin[var].Fill(true_m-reco_m_kin)
            else:
                histogram_nw[var].Fill(true-reco_nw)
                histogram_kin[var].Fill(true-reco_kin)




        histogram_nw_pt.Fill((true_top.Pt()-top_nw.Pt())/1000.)
        histogram_kin_pt.Fill((true_top.Pt()-top_kin.Pt())/1000.)
        histogram_nw_eta.Fill(true_top.Eta()-top_nw.Eta())
        histogram_kin_eta.Fill(true_top.Eta()-top_kin.Eta())
        if (abs(true_top.Phi()-top_nw.Phi()) > 3):
            print "-----------"
            print neutrinoTree.max_weight, true_top.Phi(),top_nw.Phi()
            print neutrinoTree.max_weight, true_tbar.Phi(),tbar_nw.Phi()

        histogram_nw_phi.Fill(true_top.Phi()-top_nw.Phi())
        histogram_kin_phi.Fill(true_top.Phi()-top_kin.Phi())
        histogram_nw_y.Fill(true_top.Rapidity()-top_nw.Rapidity())
        histogram_kin_y.Fill(true_top.Rapidity()-top_kin.Rapidity())

    histogram_nw_pt.Write()
    histogram_kin_pt.Write()
    histogram_nw_eta.Write()
    histogram_kin_eta.Write()
    histogram_nw_phi.Write()
    histogram_kin_phi.Write()
    histogram_nw_y.Write()
    histogram_kin_y.Write()

    for var in variables:
        histogram_nw[var].Write()
        histogram_kin[var].Write()

    results.Close()
