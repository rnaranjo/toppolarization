from ROOT import *

gStyle.SetOptStat(0)



f_nominal_rpol_plus = TFile("results_parton/rpol_plus/nominal/EMMU/map_incl_btag_200Bin.root")
h_nominal_rpol_plus = f_nominal_rpol_plus.Get("partonic")
h_nominal_rpol_plus.Rebin(50)
#h_nominal_rpol_plus.Scale(h_nominal_rpol_plus.GetEntries()/h_nominal_rpol_plus.Integral())

f_radhi_rpol_plus = TFile("results_parton/rpol_plus/radhi/EMMU/map_incl_btag_200Bin.root")
h_radhi_rpol_plus = f_radhi_rpol_plus.Get("partonic")
h_radhi_rpol_plus.Rebin(50)
#h_radhi_rpol_plus.Scale(h_radhi_rpol_plus.GetEntries()/h_radhi_rpol_plus.Integral())

f_radlo_rpol_plus = TFile("results_parton/rpol_plus/radlo/EMMU/map_incl_btag_200Bin.root")
h_radlo_rpol_plus = f_radlo_rpol_plus.Get("partonic")
h_radlo_rpol_plus.Rebin(50)
#h_radlo_rpol_plus.Scale(h_radlo_rpol_plus.GetEntries()/h_radlo_rpol_plus.Integral())

f_nominal_rpol_minus = TFile("results_parton/rpol_minus/nominal/EMMU/map_incl_btag_200Bin.root")
h_nominal_rpol_minus = f_nominal_rpol_minus.Get("partonic")
h_nominal_rpol_minus.Rebin(50)
#h_nominal_rpol_minus.Scale(h_nominal_rpol_minus.GetEntries()/h_nominal_rpol_minus.Integral())

f_radhi_rpol_minus  = TFile("results_parton/rpol_minus/radhi/EMMU/map_incl_btag_200Bin.root")
h_radhi_rpol_minus = f_radhi_rpol_minus.Get("partonic")
h_radhi_rpol_minus.Rebin(50)
#h_radhi_rpol_minus.Scale(h_radhi_rpol_minus.GetEntries()/h_radhi_rpol_minus.Integral())

f_radlo_rpol_minus  = TFile("results_parton/rpol_minus/radlo/EMMU/map_incl_btag_200Bin.root")
h_radlo_rpol_minus = f_radlo_rpol_minus.Get("partonic")
h_radlo_rpol_minus.Rebin(50)
#h_radlo_rpol_minus.Scale(h_radlo_rpol_minus.GetEntries()/h_radlo_rpol_minus.Integral())



#for i in xrange(4):


h_nominal_rpol_plus.SetLineColor(1)
h_radhi_rpol_plus.SetLineColor(4)
h_radlo_rpol_plus.SetLineColor(2)

h_nominal_rpol_minus.SetLineColor(1)
h_radhi_rpol_minus.SetLineColor(4)
h_radlo_rpol_minus.SetLineColor(2)


c_plus = TCanvas()

h_nominal_rpol_plus.Scale(1/h_nominal_rpol_plus.Integral())
h_nominal_rpol_minus.Scale(1/h_nominal_rpol_minus.Integral())

h_radhi_rpol_plus.Scale(1/h_radhi_rpol_plus.Integral())
h_radhi_rpol_minus.Scale(1/h_radhi_rpol_minus.Integral())

h_radlo_rpol_plus.Scale(1/h_radlo_rpol_plus.Integral())
h_radlo_rpol_minus.Scale(1/h_radlo_rpol_minus.Integral())


h_nominal_rpol_plus.SetMaximum(0.255)
h_nominal_rpol_plus.SetMinimum(0.245)
h_nominal_rpol_minus.SetMaximum(0.255)
h_nominal_rpol_minus.SetMinimum(0.245)

h_nominal_rpol_minus.SetTitle("")
h_nominal_rpol_plus.SetTitle("")

h_nominal_rpol_minus.GetXaxis().SetTitle("R Polarization -")
h_nominal_rpol_plus.GetXaxis().SetTitle("R Polarization +")
h_nominal_rpol_minus.GetYaxis().SetTitle("Normalized Events")
h_nominal_rpol_plus.GetYaxis().SetTitle ("Normalized Events")



h_nominal_rpol_plus.Draw()
h_radhi_rpol_plus.Draw("same")
h_radlo_rpol_plus.Draw("same")


leg = TLegend(0.60,0.70,0.88,0.85)
#leg.SetHeader("rpol_plus")
leg.SetTextFont(62)
leg.AddEntry(h_nominal_rpol_plus,'nominal','l')
leg.AddEntry(h_radhi_rpol_plus  ,'radhi','l')
leg.AddEntry(h_radlo_rpol_plus  ,'radlo','l')
leg.SetBorderSize(0)
leg.SetTextFont(62)

leg.Draw()
c_plus.Update()

c_plus.SaveAs("rpol_plus.eps")

raw_input("wait")


c_minus = TCanvas()

h_nominal_rpol_minus.Draw()
h_radhi_rpol_minus.Draw("same")
h_radlo_rpol_minus.Draw("same")

leg = TLegend(0.60,0.70,0.88,0.85)
#leg.SetHeader("rpol_plus")
leg.SetTextFont(62)
leg.AddEntry(h_nominal_rpol_plus,'nominal','l')
leg.AddEntry(h_radhi_rpol_plus  ,'radhi','l')
leg.AddEntry(h_radlo_rpol_plus  ,'radlo','l')
leg.SetBorderSize(0)
leg.SetTextFont(62)

leg.Draw()
c_minus.Update()


c_minus.SaveAs("rpol_minus.eps")
raw_input("wait")
