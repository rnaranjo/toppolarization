#!/bin/env python

import json
import time
import os
import sys
import itertools
import gc
from array import array
from ROOT import *
import ROOT as r # variable r is 'ROOT'
import os.path, time

import utils
import Rebin
from utils import writefile
from Config import Configuration, getSampleLists, getNtupleDir, getProtosSamples2Json, getLinearSamples2Json, getFitProtosSamples2Json, getObsSamples2Json, getObsSamples
r.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

#-----------------
# -- Main code --
#-----------------
if __name__ == '__main__':


    outDir = Configuration.outputDirBt

    channels = ['EE','MUMU','EMMU']
    
    variables = ['helpol_plus','helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                'transheldiff'] 

    systematics = ['nominal','mcatnlo','herwig','perugiampihi','perugialocr',\
                   #'topmass_167.5','topmass_170','topmass_175','topmass_177.5',\
                   'radhi','radlo',\
                   'fastpythia','perugia']

    #systematics = ["CT10nlo","MSTW2008nlo68cl","NNPDF23_nlo_as_0119"]



    #systematics = ['nominal',\
    #            'jer', 'jeff', 'ees_up', 'ees_down', 'eer_up', 'eer_down', 'el_trigSF_up', 'el_trigSF_down',\
    #            'el_recSF_up', 'el_recSF_down', 'el_idSF_up', 'el_idSF_down', 'musc_up', 'musc_down',\
    #            'mums_res', 'muid_res', 'mu_trigSF_up', 'mu_trigSF_down', 'mu_recSF_up', 'mu_recSF_down',\
    #            'mu_idSF_up', 'mu_idSF_down', 'jvf_up', 'jvf_down', 'res_soft_up', 'res_soft_down',\
    #            'sc_soft_up', 'sc_soft_down', 'JesEffectiveStat1_up', 'JesEffectiveStat2_up',\
    #            'JesEffectiveStat3_up', 'JesEffectiveStat4_up', 'JesEffectiveModel1_up',\
    #            'JesEffectiveModel2_up', 'JesEffectiveModel3_up', 'JesEffectiveModel4_up', \
    #            'JesEffectiveDet1_up', 'JesEffectiveDet2_up', 'JesEffectiveDet3_up', 'JesEffectiveMix1_up', \
    #            'JesEffectiveMix2_up', 'JesEffectiveMix3_up', 'JesEffectiveMix4_up', \
    #            'EtaIntercalibrationModel_up', 'EtaIntercalibrationTotalStat_up', 'SinglePart_up',\
    #            'Pileup_OffsetMu_up', 'Pileup_OffsetNPV_up', 'Pileup_Pt_up', 'Pileup_Rho_up',
    #            'flavor_comp_up', 'flavor_response_up', 'BJesUnc_up', 'PunchThrough_up', 'JesEffectiveStat1_down', \
    #            'JesEffectiveStat2_down', 'JesEffectiveStat3_down', 'JesEffectiveStat4_down', 'JesEffectiveModel1_down', \
    #            'JesEffectiveModel2_down', 'JesEffectiveModel3_down', 'JesEffectiveModel4_down', 'JesEffectiveDet1_down',\
    #            'JesEffectiveDet2_down', 'JesEffectiveDet3_down', 'JesEffectiveMix1_down', 'JesEffectiveMix2_down',\
    #            'JesEffectiveMix3_down', 'JesEffectiveMix4_down', 'EtaIntercalibrationModel_down', 'EtaIntercalibrationTotalStat_down', \
    #            'SinglePart_down', 'Pileup_OffsetMu_down', 'Pileup_OffsetNPV_down', 'Pileup_Pt_down', 'Pileup_Rho_down', 'flavor_comp_down',\
    #            'flavor_response_down', 'BJesUnc_down', 'PunchThrough_down', 'mistag_up', 'mistag_down', 'btag0_up', 'btag1_up', 'btag2_up',\
    #            'btag3_up', 'btag4_up', 'btag5_up', 'btag6_up', 'btag7_up', 'btag8_up', 'btag0_down', 'btag1_down', 'btag2_down',\
    #            'btag3_down', 'btag4_down', 'btag5_down', 'btag6_down', 'btag7_down', 'btag8_down', 'ctautag0_up', 'ctautag1_up', \
    #            'ctautag2_up', 'ctautag3_up', 'ctautag4_up', 'ctautag5_up', 'ctautag6_up', 'ctautag0_down', 'ctautag1_down', 'ctautag2_down',\
    #            'ctautag3_down', 'ctautag4_down', 'ctautag5_down', 'ctautag6_down'] 


    #systematics = ['pol_up','pol_down','corr_up','corr_down']
    repeat = {}

    for var in variables:

        repeat[var] = {}

        for syst in systematics:

            repeat[var][syst] = {}

            for channel in channels:

                 repeat[var][syst][channel] = []



                 jsondir  = outDir+'/%(var)s/%(syst)s/%(channel)s/'%{'var':var,'syst':syst.lower(),'channel':channel}

                 filename = jsondir+'%(var)s_%(sel)s_%(binName)s.root'%{'var':'map_incl', 'sel':'btag','binName':'200Bin'}

                 print "OPENING",filename

                 print "last modified: %s" % time.ctime(os.path.getmtime(filename))

                 rootFile = r.TFile(filename)

                 pointsToCheck = getObsSamples(var)

                 for point in pointsToCheck:

                     #if 'Jun' in time.ctime(os.path.getmtime(filename)):
                     #    print "missin: ",var,syst,channel,point
                     #    repeat[var][syst][channel].append(point)


                     try: 
                         point2 = point#+'_'+syst+'_0'
                         histo = rootFile.Get(point2)
                         histo.GetReplica(0)
                         #histo.GetEntries()
                     except: 
                         print "Error in: ",var,syst,channel,point
                         repeat[var][syst][channel].append(point)

                 rootFile.Close()    


   
    with open('jobs/missing.json','w') as f:

        json.dump(repeat,f)

    print repeat
  



