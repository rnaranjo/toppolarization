import sys
import json
import array

import ROOT as r
import utils
from Config import Configuration
from ROOT import *

import numpy as np


obs_label = {}

obs_label["helpol_plus"]=r"cos\theta_{+}^{k}"
obs_label["helpol_minus"]=r"cos\theta_{-}^{k}"
obs_label["transpol_plus"]=r"cos\theta_{+}^{n}"
obs_label["transpol_minus"]=r"cos\theta_{-}^{n}"
obs_label["rpol_plus"]=r"cos\theta_{+}^{r}"
obs_label["rpol_minus"]=r"cos\theta_{-}^{r}"
obs_label["helcorr"]=r"cos\theta_{+}^{k}cos\theta_{-}^{k}"
obs_label["transcorr"]=r"cos\theta_{+}^{n}cos\theta_{-}^{n}"
obs_label["rcorr"]=r"cos\theta_{+}^{r}cos\theta_{-}^{r}"
obs_label["transhelsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transheldiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{n}"
obs_label["transrsum"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} + cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["transrdiff"]=r"cos\theta_{+}^{n}cos\theta_{-}^{r} - cos \theta_{+}^{r}cos\theta_{-}^{n}"
obs_label["rhelsum"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} + cos\theta_{+}^{k}cos\theta_{-}^{r}"
obs_label["rheldiff"]=r"cos\theta_{+}^{r}cos\theta_{-}^{k} - cos\theta_{+}^{k}cos\theta_{-}^{r}"



def getModelingUncertainty(var,fiducial):

    
    from binningOptimization import binnings_dict

    # -- Get settings from command line

    btemp = binnings_dict(var)

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc

    #initialize the array

    estimationDict = {}

    systList = []
    
    systDict = {}
    total   = {}

    errorDict = {}

    systematics = ['nominal','perugia','mcatnlo','perugiampihi','perugialocr','radhi','radlo','herwig','fastpythia']

    for syst in systematics:

       temptList = []
       errorList = []
       #if syst.lower() == 'mcatnlo': sf =1.29791
       sf =1


       for pos in xrange(80):

          jsondir    = outDirB+'/%(var)s/%(syst)s/%(channel)s/optimization/boostrap/%(pos)s/'%{'var':var,'channel':channel,'pos':pos,'syst':syst.lower()}  

          jsonDirN   = outDir+'/%(var)s/%(syst)s/%(channel)s/optimization/'%{'var':var,'channel':channel,'syst':syst.lower()}
     
          truth = json.load(open(jsondir+'/'+var+'_partonicobspos0_btag_'+binName+'.json'))

          sumt = np.sum(truth)
          
          fname = jsondir+'/UnfoldedDistribution_obspos0_'+var+'_'+syst+'_'+binName+'_rpZero.json'

          try:

              distribution = json.load(open(fname))
              sumD = np.sum(distribution[0])
              
              
              temptList.append(np.array(distribution[0])/sumD)


          except:

              print "error in" , fname   
 

       #[[1, 2], [3, 4], [5, 6]]
       totalList = []
       #print "Binning:", binning
       #print temptList[0]
       for i in xrange(len(binning)-1):
           totalList.append([temptList[j][i] for j in xrange(len(temptList))])



       

        

        

       systDict[syst] = np.array([np.mean(a) for a in totalList ])
       errorDict[syst] = np.array([np.std(a) for a in totalList ])
       total[syst]  = totalList

    return systDict,errorDict,total
   
       
def makeTable(central, uncert, modelling, total, variables,name):


  header = """
\\begin{center}
\\begin{table}
\centering
\\begin{tabular}{l|cccc|cccc}
\multicolumn{1}{c}{}&  \multicolumn{4}{c}{\\rule{0pt}{3ex} Fiducial Region} & \multicolumn{4}{c}{\\rule{0pt}{3ex}  Full Phase Space} \\\\ [2ex] \hline

Bin & Central  & Stat+Detect & Modelling & Total & Central  & Stat+Detect & Modelling & Total  \\\\ \hline 
"""
  footer = """
\hline
\end{tabular}
\caption{Unfolded values for different observables. Statistical plus detector systematic uncertainties, modelling uncertainties, and the total uncertainties are shown.}
\end{table}
\end{center}
  """


  from binningOptimization import binnings_dict


  table = ''


  for var in variables:

     btemp = binnings_dict(var)

     for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc




     binLabel = []

     for i in xrange(len(binning)-1):

       binLabel.append("[ %0.1f, %0.1f ]"%(binning[i],binning[i+1]))

     if 'sum' in var or 'diff' in var or 'corr' in var:
         nameVarFiducial = r'-9$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}
     else:
         nameVarFiducial = r'3$\langle %(var)s                    \rangle$' %{'var':obs_label[var]}

     nameVarFull = '$ %(var)s $' %{'var':'\\'+var.replace('_','')}

     table = table + "\n \n \multicolumn{1}{c}{}  &  \multicolumn{4}{c}{ \\rule{0pt}{3ex} %s} & \multicolumn{4}{c}{\\rule{0pt}{3ex} %s} \\\\ [2ex]  \hline \n \n " % (nameVarFiducial,nameVarFull)

     for i,boundary in enumerate(binLabel):
        table = table + "%(boundary)s & %(central_fiducial)0.3f & $ \pm %(marginalization_fiducial)0.3f $ & $ \pm %(modeling_fiducial)0.3f $ & $ \pm %(total_fiducial)0.3f $ & %(central_full)0.3f & $ \pm %(marginalization_full)0.3f$ & $ \pm %(modeling_full)0.3f $ & $ \pm %(total_full)0.3f $ \\\\ \hline \n" % {

    'boundary':boundary,
    'central_fiducial'        :central[var]   ['fiducial'][i],
    'marginalization_fiducial':uncert[var]    ['fiducial'][i],
    'modeling_fiducial'       :modelling[var] ['fiducial'][i],
    'total_fiducial'          :total[var]     ['fiducial'][i], 
    'central_full'        :central[var]   ['full'][i],
    'marginalization_full':uncert[var]    ['full'][i],
    'modeling_full'       :modelling[var] ['full'][i],
    'total_full'          :total[var]     ['full'][i],

    }

  latex = header + table+ footer


  f1 = open(name,'w')
  f1.write(latex)
  f1.close()

  
        
          

     
 
  



if __name__ == '__main__':

    gStyle.SetOptStat(0)

    outDir   = Configuration.outputDirt
    outDirB  = Configuration.outputDirBt

    variables  = [
                 #'helpol_plus','helpol_minus','transpol_plus','transpol_minus',\
                 'rpol_plus','rpol_minus',\
                 #'helcorr','transcorr','rcorr',\
                 #'rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                 #'transheldiff'
                 ]
    regions    = ['full','fiducial']
    channel    = 'ALL'


    central  = {}
    error    = {}


    for var in variables:
        central[var] = {}
        error[var]   = {} 
        from binningOptimization import binnings_dict
        btemp = binnings_dict(var)

        for bn,bc in btemp.iteritems():
          binName = bn
          binning = bc


        for region in regions:

 
            if region == 'fiducial':
               outDir = Configuration.outputDir
               outDirB  = Configuration.outputDirB
            else:
               outDir  = Configuration.outputDirt
               outDirB  = Configuration.outputDirBt               

            signal = 'data'
            rpName = 'rpZero'


            jsondir  = outDir+'/%(var)s/nominal/%(channel)s/optimization/'%{'var':var,'channel':'ALL'}
            truth    = jsondir  +'/'+var+'_partonic_btag_'+binName+'.json' #read in the truth distribution
            unf      = jsondir + 'UnfoldedDistribution_'+signal+'_'+var+'_nominal_'+binName+'_'+rpName+'.json'

            truth_val = json.load(open(truth))
            unf_val   = json.load(open(unf))
      
            mod= getModelingUncertainty(var,region)

            central[var][region] = mod[0]

            temp = [] 
            for i in xrange(len(mod[2]['radhi'])):
                temp.append([])
                for j in xrange(80):      
                  temp[i].append((mod[2]['radhi'][i][j]-mod[2]['radlo'][i][j])/2)

            error[var][region]   = np.array([np.std(a) for a in temp ])
            print error[var][region]

 
    print central


    from binningOptimization import binnings_dict

    btemp = binnings_dict('rpol_plus')

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc



    h_rpol_plus_radhi = TH1F("rpol_plus_radhi","",len(binning)-1,array.array('f',binning))

    for i,binContent in enumerate(central['rpol_plus']['full']['radhi']): 
        h_rpol_plus_radhi.SetBinContent(i+1,binContent)
        #h_rpol_plus_radhi.SetBinError(i+1,error['rpol_plus']['full']['radhi'][i]) 

    h_rpol_plus_radlo = TH1F("rpol_plus_radlo","",len(binning)-1,array.array('f',binning))

    for i,binContent in enumerate(central['rpol_plus']['full']['radlo']): 
        h_rpol_plus_radlo.SetBinContent(i+1,binContent)
        #h_rpol_plus_radlo.SetBinError(i+1,error['rpol_plus']['full']['radlo'][i])
        

    h_rpol_plus_nominal = TH1F("rpol_plus_nominal","",len(binning)-1,array.array('f',binning))
 
    for i,binContent in enumerate(central['rpol_plus']['full']['nominal']): 
        h_rpol_plus_nominal.SetBinContent(i+1,binContent)
        h_rpol_plus_nominal.SetBinError(i+1,error['rpol_plus']['full'][i])



    btemp = binnings_dict('rpol_minus')

    for bn,bc in btemp.iteritems():
        binName = bn
        binning = bc


    h_rpol_minus_radhi = TH1F("rpol_minus_radhi","",len(binning)-1,array.array('f',binning))

    for i,binContent in enumerate(central['rpol_minus']['full']['radhi']): 
       h_rpol_minus_radhi.SetBinContent(i+1,binContent)
       #h_rpol_minus_radhi.SetBinError(i+1,error['rpol_minus']['full']['radhi'][i])

    

    h_rpol_minus_radlo = TH1F("rpol_minus_radlo","",len(binning)-1,array.array('f',binning))

    for i,binContent in enumerate(central['rpol_minus']['full']['radlo']): 
       h_rpol_minus_radlo.SetBinContent(i+1,binContent)
       #h_rpol_minus_radlo.SetBinError(i+1,error['rpol_minus']['full']['radlo'][i])


    h_rpol_minus_nominal = TH1F("rpol_minus_nominal","",len(binning)-1,array.array('f',binning))

    for i,binContent in enumerate(central['rpol_minus']['full']['nominal']): 
       h_rpol_minus_nominal.SetBinContent(i+1,binContent)
       h_rpol_minus_nominal.SetBinError(i+1,error['rpol_minus']['full'][i])


    c_plus = TCanvas()


    h_rpol_plus_nominal.SetMaximum(1.2*h_rpol_plus_nominal.GetMaximum())
    h_rpol_plus_nominal.SetMinimum(0.2)
    h_rpol_plus_nominal.SetLineColor(1)
    h_rpol_plus_radhi.SetLineColor(2)
    h_rpol_plus_radlo.SetLineColor(3)

    h_rpol_plus_nominal.Draw()
    h_rpol_plus_radhi.Draw("same")
    h_rpol_plus_radlo.Draw("same")


    
 
    leg = TLegend(0.60,0.80,0.98,0.95)
    leg.SetHeader("rpol_plus")
    leg.SetTextFont(62)
    leg.AddEntry(h_rpol_plus_nominal,'nominal','l')
    leg.AddEntry(h_rpol_plus_radhi  ,'radhi','l')
    leg.AddEntry(h_rpol_plus_radlo  ,'radlo','l')
    #leg.SetFillColor(0)
    #leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(62)

    leg.Draw()
    c_plus.Update()

    c_plus.Draw()

    c_plus.SaveAs("tests/rpol_plus_mod.eps")

    raw_input("WAIT")



    

    
 




    c_minus = TCanvas()


    h_rpol_minus_nominal.SetMaximum(1.2*h_rpol_minus_nominal.GetMaximum())
    h_rpol_minus_nominal.SetMinimum(0.2)
    h_rpol_minus_nominal.SetLineColor(1)
    h_rpol_minus_radhi.SetLineColor(2)
    h_rpol_minus_radlo.SetLineColor(3)

    h_rpol_minus_nominal.Draw()
    h_rpol_minus_radhi.Draw("same")
    h_rpol_minus_radlo.Draw("same")




    leg = TLegend(0.60,0.80,0.98,0.95)
    leg.SetHeader("rpol_minus")
    leg.SetTextFont(62)
    leg.AddEntry(h_rpol_minus_nominal,'nominal','l')
    leg.AddEntry(h_rpol_minus_radhi  ,'radhi','l')
    leg.AddEntry(h_rpol_minus_radlo  ,'radlo','l')
    #leg.SetFillColor(0)
    #leg.SetLineColor(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(62)

    leg.Draw()
    c_minus.Update()

    c_minus.Draw()


    c_minus.SaveAs("tests/rpol_minus_mod.eps")


    raw_input("wait2")











