from ROOT import *
from array import array
from Rebin import binning_dict
from tqdm import tqdm

import numpy as np
import json

import matplotlib.pyplot as plt
import utils
import sys
import os

def plotCorrelation(a,name = 'correlation.eps'):
  f,ax      = plt.subplots(figsize=(10, 10))
  img = ax.matshow(a, cmap='jet',interpolation="none",vmax=1.3, vmin=-1.30)
  ax.xaxis.tick_bottom()
  for (i, j), z in np.ndenumerate(a):
    ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center',fontsize=15)
  plt.text( 0.01,1.03, 'ATLAS',fontsize=20, fontweight='bold',style='italic',transform=ax.transAxes)
  plt.text( 0.14,1.03, 'Internal',fontsize=20, style='italic',transform=ax.transAxes)
  plt.text( 0.3,1.03,  "$\sqrt{s}$ = $8$ TeV, $20.3$ ${fb}^{-1}$",fontsize=20,transform=ax.transAxes)
  plt.xticks(range(0,12), range(1,13),fontsize=20)
  plt.yticks(range(0,12), range(1,13),fontsize=20)
  ax.set_xlabel('Reco Bin',fontsize=20)
  ax.set_ylabel('Reco Bin',fontsize=20)
  #plt.show()
  plt.savefig(name, format='eps')



##------------------------------------------------------------------------------
# == Main ==
##------------------------------------------------------------------------------
if __name__ == '__main__':

  polarizationBinning = binning_dict

  outputFolder = '/nfs/dust/atlas/user/naranjo/TopPolarization/tests/results/seeds/'

  colors =  ['black','silver','r','sienna','bisque','gold','olivedrab','chartreuse' ,\
            'darksage', 'green','c', 'darkblue','slateblue','blueviolet','magenta','royalblue' ]
  variables = ['helpol_plus', 'helpol_minus','transpol_plus','transpol_minus','rpol_plus','rpol_minus',\
                'helcorr','transcorr','rcorr','rhelsum','rheldiff','transrsum','transrdiff','transhelsum',\
                'transheldiff']
  humanVariables = ['Helicity Polarization +','Helicity Polarization -','Transverse Polarization +','Transverse Polarization -','R Polarization +','R Polarization -',\
  'Helicity Correlation','Transverse Correlation','R Correlation','R Helicity Sum', 'R Helicity Diff','Transverse R Sum','Transverse R Diff','Transverse Helicity Sum',\
  'Transverse Helicity Diff']
  plots = []
  fig = plt.figure()


  for i,var in enumerate(variables):
    data = np.array(json.load(open(outputFolder+var+"_ErrorData.json")))
    reco = np.array(json.load(open(outputFolder+var+"_ErrorReco.json")))


    plots.append(plt.errorbar(range(1,len(data)+1), reco/data,fmt='o',color=colors[i]))
  plt.ylabel(r'$\sigma_{\rm{reconstruction}}$ / $\sigma_{\rm{statistical}}$',fontsize=22)
  plt.xlabel('Bin Number',fontsize=18)
  plt.xlim([0,9])
  plt.legend(tuple(plots),tuple(humanVariables), ncol=3,numpoints=1,loc=1)
  plt.title('Reco and Stat Uncertainty Comparison',fontsize=20)
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')

  plt.show()



      






