#!/bin/bash

#source ~/ENVFBU/bin/activate

#defaults
bootstrap=false
 channels="ALL"
variables="transhelsum"
  signal="obspos0"
  binning="4Bin1"
  regpar="rpZero"
fiducial="full"
  syst="nominal"
replica=0
boot='bootstrap'
#parameters:
# 1st: run lepton/ttbar: 1 (or 'lepton') - run leptons dEta
#                        2 (or 'ttbar')  - run ttbar dY
#                        anything else   - run both (default)
# 2nd: ee/emu/mumu channel: "EE MUMU EMMU" - run all (default)
# %(whichAsym)s %(channel)s %(syst) %(rew)s  %(rewType)s %(binning)s %(regPar)s %(regFunc)s

if [ $# -ge 1 ]; then  binning=$1;  fi
if [ $# -ge 2 ]; then  channels=$2;  fi
if [ $# -ge 3 ]; then  syst=$3;      fi
if [ $# -ge 4 ]; then  fiducial=$4;     fi
if [ $# -ge 5 ]; then  variables=$5;     fi
if [ $# -ge 6 ]; then  signal=$6;     fi
if [ $# -ge 7 ]; then  regpar=$7;     fi
if [ $# -ge 8 ]; then  boot=$8;     fi
if [ $# -ge 9 ]; then  replica=$9;     fi


#if [[ $variables =~ .*pol.* ]]; then
#   signal="obspos0 obspos2 obspos4 obspos7 obspos10 obsneg2 obsneg4 obsneg7 obsneg10"
#elif [[ $variables =~ .*corr.* ]]; then
#    if [[ $variables =~ .*rcorr.* ]];then
#       signal="obspos0 obspos2 obspos4 obspos7 obspos10 obsneg2 obsneg4 obsneg7 obsneg10"
#   else
#       signal="obspos0 obspos20 obspos25 obspos28 obspos30 obspos32 obspos34 obspos36 obspos39 obspos44"
#   fi
#elif [[ $variables =~ .*sum.* ]]; then
#    signal="obspos0 obspos2 obspos7 obspos15 obsneg2 obsneg7 obsneg15"
#
#elif [[ $variables =~ .*diff.*  ]]; then
#    signal="obspos0 obspos2 obspos7 obspos15 obsneg2 obsneg7 obsneg15"
#
#fi



START=$replica
END=$(($replica+20))

#signal="reco"
for (( replica=$START; replica<=$END; replica++ )); do
for sys in $syst; do
    for ch in $channels; do
        for bin in $binning; do
	           for rp in $regpar; do
                   for sig in $signal; do
                       for var in $variables; do


            cmd="python $AcDilFBU_DIR/python/fbuOptimization.py $boot $replica $ch btag $var $sig $sys $bin $rp $fiducial"
            echo "running: $cmd"
            $cmd

                    done
                done
	       done
       done
    done
done

done
