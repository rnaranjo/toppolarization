#!/bin/bash

#source ~/ENVFBU/bin/activate

#defaults
bootstrap=false
 channels="ALL"
variables="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"
 signal="obspos0"
 binning="4Bin1"
  regpar="rpZero"
  fiducial="full"
  syst="nominal"
#parameters:
# 1st: run lepton/ttbar: 1 (or 'lepton') - run leptons dEta
#                        2 (or 'ttbar')  - run ttbar dY
#                        anything else   - run both (default)
# 2nd: ee/emu/mumu channel: "EE MUMU EMMU" - run all (default)
# %(baseDir)s/scripts/runFbuPDF.sh %(variable)s %(binName)s %(channel)s %(syst)s %(pdf)s %(pdfNum)d %(fiducial)s %(rpName)sa
#if [ $# -ge 1 ]; then  variables=$1;  fi

if [ $# -ge 2 ]; then  binning=$2;  fi
if [ $# -ge 3 ]; then  channels=$3;  fi

if [ $# -ge 4 ]; then  syst=$4;      fi
if [ $# -ge 5 ]; then  pdf=$5;      fi
if [ $# -ge 6 ]; then  pdfNum=$6;      fi
if [ $# -ge 7 ]; then  fiducial=$7;      fi


#if [[ $variables =~ .*pol.* ]]; then
#   signal="obspos0 obspos2 obspos4 obspos7 obspos10 obsneg2 obsneg4 obsneg7 obsneg10"
#elif [[ $variables =~ .*corr.* ]]; then
#    if [[ $variables =~ .*rcorr.* ]];then
#       signal="obspos0 obspos2 obspos4 obspos7 obspos10 obsneg2 obsneg4 obsneg7 obsneg10"
#   else
#       signal="obspos0 obspos20 obspos25 obspos28 obspos30 obspos32 obspos34 obspos36 obspos39 obspos44"
#   fi
#elif [[ $variables =~ .*sum.* ]]; then
#    signal="obspos0 obspos2 obspos7 obspos15 obsneg2 obsneg7 obsneg15"
#
#elif [[ $variables =~ .*diff.*  ]]; then
#    signal="obspos0 obspos2 obspos7 obspos15 obsneg2 obsneg7 obsneg15"
#
#fi










for sys in $syst; do
    for ch in $channels; do
        for bin in $binning; do
	           for rp in $regpar; do
                   for sig in $signal; do
                       for var in $variables; do


            cmd="python $AcDilFBU_DIR/python/runfbu.py $ch btag $var $sig $sys $bin $rp $fiducial $pdf $pdfNum"
            echo "running: $cmd"
            $cmd

                    done
                done
	       done
       done
    done
done

