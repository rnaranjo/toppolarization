#!/bin/bash

WORKDIR=/nfs/dust/atlas/user/naranjo/TopPolarization

cd ${WORKDIR}
source ${WORKDIR}/scripts/setup.sh

channels="EE MUMU EMMU"
rew=true
rewType="obs"

vars="helpol_plus helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
     helcorr transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum
     transheldiff"

#vars="rhelsum transrdiff"
syst="nominal mcatnlo herwig perugia radhi radlo perugiampihi perugialocr fastpythia"
#syst="pol_up pol_down corr_up corr_down"
syst="nominal"
#pdf="CT10nlo MSTW2008nlo68cl NNPDF23_nlo_as_0119"
pdf="none"
region="full fiducial"
b=""
optimization='optimization'

if [ $# -ge 1 ]; then  vars=$1; fi
if [ $# -ge 3 ]; then  region=$3; fi
if [ $# -ge 2 ]; then      syst=$2; fi
if [ $# -ge 4 ]; then       rew=$4; fi
if [ $# -ge 5 ]; then   rewType=$5; fi
if [ $# -ge 6 ]; then      maps=$6; fi

for var in $vars; do
    for sys in $syst; do
        for ch in $channels; do
            if $rew; then
                for rewT in $rewType; do
                    for p in $pdf;do
                        for f in $region;do
                            cmd="python $AcDilFBU_DIR/python/histo2json.py $ch $sys  $var  rewObs $rewT $p $f $b $optimization"
                            echo "running: $cmd"
                            $cmd
                        done
                    done
                done
            else
                for p in $pdf;do
                    for f in $region;do
                        cmd="python $AcDilFBU_DIR/python/histo2json.py $ch $sys $var $map  $p $f $b $optimization"
                        echo "running: $cmd"
                        $cmd
                    done
                done
            fi
        done
    done
done
