#!/bin/bash


#setup AcDilFBU
echo "setting up AcDilFBU..."
export AcDilFBU_DIR=$PWD
export AcDilFBU_NTPDIR=/nfs/dust/atlas/user/naranjo/TopPolarization/ntuples/
export PYTHONPATH=$AcDilFBU_DIR/python:$PYTHONPATH

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh


#setup FBU
echo "setting up FBU..."

source /nfs/dust/atlas/user/naranjo/TopPolarization/env/bin/activate     ;
localSetupROOT --skipConfirm 5.34.22-x86_64-slc6-gcc48-opt

echo "setting RootCore for bootstrapping"
source /nfs/dust/atlas/user/naranjo/Fbu/framework/RootCore/scripts/setup.sh


echo "setup finished."

