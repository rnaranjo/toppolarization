#!/bin/bash


vars="helpol_plus helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus
     helcorr transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum
     transheldiff"

region="fiducial full"

for var in $vars; do
    for r in $region; do


    cmd="python $AcDilFBU_DIR/python/createVariations.py $var $r"

    echo "running: $cmd"

    $cmd


    done
done
