#!/bin/bash

#source ~/ENVFBU/bin/activate

#defaults
bootstrap=false
 channels="ALL"
variables="transhelsum"
  signal="data"
  binning="4Bin1"
  regpar="rpZero"
fiducial="fiducial"
  syst="nominal"
replica=0
boot=''
#syst="topmass_167.5 topmass_170 topmass_175 topmass_177.5"
#variables="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum  transheldiff"

#parameters:
# 1st: run lepton/ttbar: 1 (or 'lepton') - run leptons dEta
#                        2 (or 'ttbar')  - run ttbar dY
#                        anything else   - run both (default)
# 2nd: ee/emu/mumu channel: "EE MUMU EMMU" - run all (default)
# %(whichAsym)s %(channel)s %(syst) %(rew)s  %(rewType)s %(binning)s %(regPar)s %(regFunc)s

if [ $# -ge 1 ]; then  binning=$1;  fi
if [ $# -ge 2 ]; then  channels=$2;  fi
if [ $# -ge 3 ]; then  syst=$3;      fi
if [ $# -ge 4 ]; then  fiducial=$4;     fi
if [ $# -ge 5 ]; then  variables=$5;     fi
if [ $# -ge 6 ]; then  signal=$6;     fi
if [ $# -ge 7 ]; then  regpar=$7;     fi
if [ $# -ge 8 ]; then  boot=$8;     fi
if [ $# -ge 9 ]; then  replica=$9;     fi


for sys in $syst; do
    for ch in $channels; do
        for bin in $binning; do
	           for rp in $regpar; do
                   for sig in $signal; do
                       for var in $variables; do


            cmd="python $AcDilFBU_DIR/python/runfbu.py $boot $replica $ch btag $var $sig $sys $bin $rp $fiducial"
            echo "running: $cmd"
            $cmd

                    done
                done
	       done
       done
    done
done

