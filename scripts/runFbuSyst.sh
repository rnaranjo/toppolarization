#!/bin/bash

#source ~/ENVFBU/bin/activate

#defaults
bootstrap=false
 channels="ALL"
variables="transhelsum"
  signal="psigma msigma"
  binning="4Bin1"
  regpar="rpZero"
fiducial="full"
  syst="btag8"
replica=0
boot=''

#variables="helpol_plus helcorr helpol_minus transpol_plus transpol_minus rpol_plus rpol_minus transcorr rcorr rhelsum rheldiff transrsum transrdiff transhelsum transpol_plus transpol_minus rpol_plus rpol_minus  transheldiff"
#parameters:
# 1st: run lepton/ttbar: 1 (or 'lepton') - run leptons dEta
#                        2 (or 'ttbar')  - run ttbar dY
#                        anything else   - run both (default)
# 2nd: ee/emu/mumu channel: "EE MUMU EMMU" - run all (default)
# %(whichAsym)s %(channel)s %(syst) %(rew)s  %(rewType)s %(binning)s %(regPar)s %(regFunc)s

if [ $# -ge 1 ]; then  binning=$1;  fi
if [ $# -ge 2 ]; then  channels=$2;  fi
if [ $# -ge 3 ]; then  syst=$3;      fi
if [ $# -ge 4 ]; then  fiducial=$4;     fi
if [ $# -ge 5 ]; then  variables=$5;     fi
if [ $# -ge 7 ]; then  regpar=$6;     fi


for sys in $syst; do
    for ch in $channels; do
        for bin in $binning; do
	           for rp in $regpar; do
                   for sig in $signal; do
                       for var in $variables; do


            cmd="python $AcDilFBU_DIR/python/systfbu.py  $ch btag $var $sig $sys $bin $rp $fiducial"
            echo "running: $cmd"
            $cmd

                    done
                done
	       done
       done
    done
done

