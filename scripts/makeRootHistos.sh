#!/bin/bash

#defaults
var="helpol_plus"
 channels="EMMU EE MUMU"
      rew=false
  rewType="linear protos fitprotos"
     syst="nominal"
 fiducial=""
bootstrap=""
optimization=true


if [ $# -ge 1 ]; then  var=$1;   fi
if [ $# -ge 2 ]; then  channels=$2;   fi
if [ $# -ge 3 ]; then      syst=$3;   fi
if [ $# -ge 4 ]; then       rew=$4;   fi
if [ $# -ge 5 ]; then   rewType=$5;   fi
if [ $# -ge 6 ]; then   testt=$6;   fi
if [ $# -ge 7 ]; then   fiducial=$7;   fi
if [ $# -ge 8 ]; then   bootstrap=$8;   fi
if [ $# -ge 9 ]; then   pdf=$9;   fi

for sys in $syst; do
  for ch in $channels; do
    if $rew; then
        cmd="python $AcDilFBU_DIR/python/analyzer.py $ch $sys $var rewObs $rewType noBKG $fiducial $testt $pdf $bootstrap"
        echo "running: $cmd"
        $cmd
    else
      cmd="python $AcDilFBU_DIR/python/analyzer.py $ch $sys $var $fiducial $bootstrap $pdf "
      echo "running: $cmd"
      $cmd
    fi
  done
done;
